﻿/*
 * measure_physics.h
 *
 *  Created on: 4 dic 2020
 *      Author: agrippino
 */

#ifndef COMPONENTS_ULTRAFLOW_MIDDLEWARE_MEASURE_LIB_MEASURE_PHYSICS_H_
#define COMPONENTS_ULTRAFLOW_MIDDLEWARE_MEASURE_LIB_MEASURE_PHYSICS_H_

#include "../wika_data_types/wika_data_types.h"



/***************************************************************************************
 * 	\author		Agrippino
 *
 *	\brief		Get the corrected sound speed
 *
 *	\param[in]	soundSpeed			-	the raw sound speed
 *	\param[in]	workingTemp			-	the temperature at which the system is working.
 *	\param[in]	referenceTemp		-	temperature at which the correction factor was given
 *	\param[in]	correctionFactor	-	sound speed correction factor
 *
 *	\return		soundSpeed	-	the float corrected sound speed.
 ***************************************************************************************/
float fMeasPhysix_getCorrectedSoundSpeed(const float soundSpeed,
										 const float workingTemp,
										 const float referenceTemp,
										 const float correctionFactor);

/***************************************************************************************
 * 	\author		Agrippino
 *
 *	\brief		Function to evaluate the refraction angle of the sound wave.
 *				This function implements the Snell-law.
 *
 *	\param[in]	tetaMaterial_0			-	incidence angle of the soundwave at the interface
 *	\param[in]	speedSoundMaterial_0	-	soundspeed in the first material
 *	\param[in]	speedSoundMaterial_1	-	soundspeed in the second material
 *
 *	\return		angle	-	the refraction angle in radiants
 ***************************************************************************************/
float fMeasPhysix_getAngleOfRefraction(const float tetaMaterial_0,
									   const float speedSoundMaterial_0,
									   const float speedSoundMaterial_1);

/***************************************************************************************
 * 	\author		Agrippino
 *
 *	\brief		Function to evaluate the refraction angle of the sound wave.
 *				This function implements the Snell-law.
 *
 *	\param[in]	angle		-	angle of the sound wave
 *	\param[in]	thickness	-	thickness of the medium
 *
 *	\return		length	-	the length of the sonic path.
 ***************************************************************************************/
float fMeasPhysix_getLength(const float angle, const float thickness);

/***************************************************************************************
 * 	\author		Agrippino
 *
 *	\brief		Function to retrieve the internal diameter of a pipe, given the external
 *				and its thickness. Tha results does not depends from the measurement unit.
 *
 *	\param[in]	pipeExternalDiameter	-	pipe external diameter
 *	\param[in]	pipeThickness			-	pipe thickness
 *	\param[in]	liningThickness			-	lining thickness
 *
 *	\return		diameter	-	the internal diameter
 ***************************************************************************************/
float fMeasPhysix_getPipeInnerDiameter(const float pipeExternalDiameter,
									const float pipeThickness,
									const float liningThickness);

/***************************************************************************************
 * 	\author		Agrippino
 *
 *	\brief		Function to retrieve the mounting distance of the two sensors, given the
 *				pipe dimensions and number of wave repetitions.
 *
 *	\param[in]	pipeLength		-	horizontal pipe distance run by the sonic wave
 *	\param[in]	liningLength	-	horizontal lining distance run by the sonic wave
 *	\param[in]	fluidLength		-	horizontal fluid distance run by the sonic wave
 *	\param[in]	waveRepetitions	-	number of sonic wave repetitions.
 *
 *	\return		distance		-	the mounting distance between the USS sensors.
 ***************************************************************************************/
float fMeasPhysix_getMountingDistance(const float pipeLength,
									  const float liningLength,
									  const float fluidLength,
									  const uint8_t waveRepetitions);

/***************************************************************************************
 * 	\author		Agrippino
 *
 *	\brief		Function to retrieve the propagation time of the sound wave
 *
 *	\param[in]	soundSpeed	-	sound speed
 *	\param[in]	thickness	-	medium thickness
 *	\param[in]	length		-	horizontal length
 *
 *	\return		time		-	propagation time of the wave into the medium
 ***************************************************************************************/
float fMeasPhysix_getPropagationTime(const float soundSpeed,
		                             const float thickness,
									 const float length);


/***************************************************************************************
 * 	\author		Agrippino
 *
 *	\brief		Function to retrieve the horizontal fluid section length
 *
 *	\param[in]	thickness	-	medium thickness
 *	\param[in]	length		-	horizontal length
 *
 *	\return		distance	-	horizontal fluid distance
 ***************************************************************************************/
float fMeasPhysix_getFluidDistance(const float thickness, const float length);

/***************************************************************************************
 * 	\author		Agrippino
 *
 *	\brief		Function to retrieve the pipe internal area
 *
 *	\param[in]	distance	-	the pipe diameter
 *
 *	\return		area		-	pipe internal area
 ***************************************************************************************/
float fMeasPhysix_getFluidArea(const float distance);


// post measurement

float fMeasPhysix_getTUpsTDns(const float tTotUp,
		                 const float tSens,
						 const float tPipe,
						 const float tLining,
						 const float tCircuit);

float fMeasPhysix_getDeltaT(const float tUp, const float tDown, const uint8_t waveRep);

float fMeasPhysix_getMeasuredFluidSpeed(const float deltaT,
		                                const float tUp,
										const float tDown,
										const uint8_t waveRep,
										const float diameter,
										const float angle);

float fMeasPhysix_getTheoricFluidSpeed(const float deltaT,
		                               const float distance,
									   const float angle,
									   const float soundSpeed);

float fMeasPhysix_getCorrectedFluidSpeed(const float speedFluid,
		                                 const float fluidThickness,
										 const float kinematicViscosity);

float fMeasPhysix_getFlow(const float corrSSFluid, const float fluidArea);

/***************************************************************************************
 * 	\author		BodiniA
 *
 *	\brief		Evaluate the fluid volume
 *
 *	\param[in]	fluidArea	-	pipe inner area expressed in SQUARE METERS	[m2]
 *	\param[in]	fluidSpeed	-	fluid speed expressed in METERS/SECOND [m/s]
 *	\param[in]	deltaT		-	time expressed in SECONDS [s]
 *
 *	\return		fluid volume	-	expressed in M^3 [m^3]
 ***************************************************************************************/
float fMeasPhysix_getVolume(const float fluidArea,
							const float fluidSpeed,
							const float deltaT);

/***************************************************************************************
 * 	\author		BodiniA
 *
 *	\brief		Evaluate the mass flow of the given fluid
 *
 *	\param[in]	volume	-	expressed in M^3 [m^3]
 *	\param[in]	density	-	fluid density expressed in KG/M^3 [kg/m^3]
 *
 *	\return		the mass flow expressed in KG/S	[kg/s]
 ***************************************************************************************/
float fMeasPhysix_getMassFlow(const float vol, const float density);

float fMeasPhysix_getMass(const float massFlow);

float fMeasPhysix_getEnergy(const float massFlow, const float dEnthalpy);

float fMeasPhysix_getKinematicViscosity(const float absViscosity, const float fluidDensity);

float fMeasPhysix_getMeasureRate(const float measuredSpeed, const float theoricSpeed);

#endif /* COMPONENTS_ULTRAFLOW_MIDDLEWARE_MEASURE_LIB_MEASURE_PHYSICS_H_ */
