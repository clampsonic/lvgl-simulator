﻿/**************************************************************************************
 *  \file       measure_utils.c
 *
 *  \brief      Measurement Utils
 *
 *  \details    Methods linked to the measurement process.
 *
 *  \author     Agrippino Luca
 *  \author     Bodini Andrea
 *
 *  \version    1.0
 *
 *  \date		30 nov 2021
 *
 *  \copyright  (C) Copyright 2021, Euromisure Sas di WIKA Italia Srl
***************************************************************************************/
//--------------------------------------------------------------------------------------
#include <math.h>
#include <string.h>
//--------------------------------------------------------------------------------------
#include "measure_utils.h"
#include "measure_physics.h"
//#include "wikaDsp.h"
//--------------------------------------------------------------------------------------

/********************************** LOCAL DEFINES *************************************/
#define NORMAL_MEASUREMENT   	0
#define ACCURATE_MEASUREMENT 	1
#define DEFAULT_CHANNEL      	0
#define MAX_USER_MEASURE     	5

#define FLUIDSPEED_GRANULARITY	5.0

// 4-20mA values
#define X1 4
#define X2 20

typedef struct {
    uint16_t error_id;
    uint8_t error_description[85];
} ussError_t;

static ussError_t uss_errors[] = {
    // NONE
    {.error_id = 0,    .error_description = "No Error"},
    // SAPH
    {.error_id = 1,    .error_description = "SAPH low Phase Period is invalid"},
    {.error_id = 2,    .error_description = "SAPH high phase period is invalid"},
    {.error_id = 3,    .error_description = "SAPH number of excitation pulses is invalid"},
    {.error_id = 4,    .error_description = "SAPH number of stop pulses is invalid"},
    {.error_id = 5,    .error_description = "Unable to update SAPH during an active conversion"},
    {.error_id = 6,    .error_description = "Invalid start PPG count value"},
    {.error_id = 7,    .error_description = "Turn on ADC Count value is invalid or less than 40 usec"},
    {.error_id = 8,    .error_description = "Start ADC Sampling Count is invalid"},
    {.error_id = 9,    .error_description = "Restart capture count is invalid"},
    {.error_id = 10,   .error_description = "Capture time out count is invalid"},
    {.error_id = 11,   .error_description = "Start PGA and INBias Count is invalid"},
    {.error_id = 12,   .error_description = "Bias impedance value is invalid"},
    {.error_id = 13,   .error_description = "Rx Charge Pump value is invalid"},
    {.error_id = 14,   .error_description = "Pulse Generation parameters are invalid"},
    // HSPLL
    {.error_id = 21,   .error_description = "Input frequency for HSPLL module is invalid"},
    {.error_id = 22,   .error_description = "The output frequency of the HSPLL is invalid"},
    {.error_id = 23,   .error_description = "An error occurred while locking the PLL"},
    {.error_id = 24,   .error_description = "HSPLL detected that the PLL has unlocked"},
    {.error_id = 26,   .error_description = "Attempted to update HSPLL during an active conversion"},
    {.error_id = 27,   .error_description = "Actual HSPLL frequency exceeded tolerance from expected frequency"},
    {.error_id = 28,   .error_description = "Invalid USS crystal settling time"},
    // SDHS
    {.error_id = 41,   .error_description = "SDHS High or low SDHS threshold values were exceeded"},
    {.error_id = 43,   .error_description = "SDHS SDHS conversion overflow error"},
    {.error_id = 44,   .error_description = "SDHS SDHS sample size is invalid"},
    {.error_id = 45,   .error_description = "SDHS Attempted to update SDHS during an active conversion"},
    {.error_id = 46,   .error_description = "SDHS Conversion reached low threshold during runtime"},
    {.error_id = 47,   .error_description = "SDHS Conversion reached high threshold during runtime"},
    {.error_id = 48,   .error_description = "SDHS Max sample size returned"},
    // UUPS
    {.error_id = 61,   .error_description = "UUPS Attempted to update UUPS during an active conversion"},
    {.error_id = 62,   .error_description = "UUPS UUPS power up time out detected"},
    {.error_id = 63,   .error_description = "UUPS UUPS power up error detected"},
    {.error_id = 64,   .error_description = "UUPS UUPS power down error detected"},
    {.error_id = 81,   .error_description = "UUPS Data error triggered during sequence"},
    {.error_id = 82,   .error_description = "UUPS ASQ time out occurred"},
    // STOP
    {.error_id = 101,   .error_description = "Error USS ongoing measurement was stopped"},
    {.error_id = 102,   .error_description = "Error USS sequence was stopped by debugger"},
    {.error_id = 103,   .error_description = "Time between measurements is greater than UPS0 to UPS1 Gap"},
    // ALGORITHM
    {.error_id = 121,   .error_description = "USS parameter check failed"},
    {.error_id = 122,   .error_description = "No problems were encountered while running the algorithms"},
    {.error_id = 123,   .error_description = "An error was encountered while running the algorithms"},
    {.error_id = 124,   .error_description = "Invalid Absolute TOF Interval selected"},
    {.error_id = 125,   .error_description = "No signal detected in UPS channel"},
    {.error_id = 126,   .error_description = "No signal detected in up and downstream channel"},
    {.error_id = 127,   .error_description = "No signal detected in DNS channel"},
    {.error_id = 128,   .error_description = "Upstream/downstream captures accumulated"},
    {.error_id = 129,   .error_description = "Clock Relative Error is outside the valid range"},
    {.error_id = 130,   .error_description = "Filter length is greater than 20"},
    {.error_id = 131,   .error_description = "Binary pattern does not fit in memory reserved by user"},
    {.error_id = 132,   .error_description = "Invalid absTof compution option selected"},
    {.error_id = 133,   .error_description = "Invalid USS_Meter_Configuration configuration"},
    {.error_id = 134,   .error_description = "The minimum volume flow rate falls outside the lowest alpha min calibration point"},
    {.error_id = 135,   .error_description = "DToF - Shift value was greater than maxSampleShift"},
    {.error_id = 136,   .error_description = "DToF - Correlation overrun error"},
    {.error_id = 137,   .error_description = "DToF - Correlation interpolation error"},
    {.error_id = 138,   .error_description = "DToF - Correlation threshold error"},
    {.error_id = 142,   .error_description = "User attempted to update configuration during an active conversion"},
    // ISTOP
    {.error_id = 161,   .error_description = "Invalid conversion data - conversion was interrupted"},
    // CALIBRATION
    {.error_id = 171,   .error_description = "Successfully calibrated DAC"},
    {.error_id = 172,   .error_description = "Error calibrating DAC"},
    {.error_id = 173,   .error_description = "Error calibrating gain"},
    {.error_id = 174,   .error_description = "Error during Abs ToF and DToF offset calculation"},
    {.error_id = 175,   .error_description = "Signal gain calibration timed out"},
    {.error_id = 176,   .error_description = "Signal gain calibration failed to settle"},
    {.error_id = 177,   .error_description = "Successfully calibrated signal gain"},
    {.error_id = 178,   .error_description = "Timeout occurred during DAC calibration"},
    {.error_id = 179,   .error_description = "Error during Abs ToF and DToF calculation"},
    {.error_id = 180,   .error_description = "Error during Abs ToF and DToF offset calculation"},
    // GENERIC
    {.error_id = 240,   .error_description = "ACLK failed to settle or clear oscillation flag in expected timeout period"},
    {.error_id = 241,   .error_description = "Functionality from Silicon rev B or later is attempted in a Rev A silicon"},
    {.error_id = 253,   .error_description = "USSXT oscillator not stable after start-up time"},
    {.error_id = 254,   .error_description = "USS module attempted to update while busy flag was set"},
    {.error_id = 255,   .error_description = "Unknown error occurred"},

};

/********************************** FUNCTION PROTOTYPES *******************************/
// helper function to solve the straight line equation
static float solveStraightLineEquation(float EndPoint4, float EndPoint20, float value);

static float fMeasUtils_addGranularityToMeasure(const float currentValue);

/**************************************************************************************/

/***************************************************************************************
 * 	\author		BodiniA
 *
 *	\brief		Function used to make all the pre-measurement calculations.
 *
 *	\param[in]	installation - current installation structure pointer
 *
 *	\return		function return
 ***************************************************************************************/
systemErrors_t tMeasUtils_preMeasurementCalculation(Stream_t* installation)
{
	systemErrors_t err = WIKA_NO_ERROR;

	return err;
}

/***************************************************************************************
 * 	\author		BodiniA
 *
 *	\brief		Brief Description
 *
 *	\param[in]	installation	- current installation structure pointer
 *	\param[in]	deltaT			-
 *	\param[in]	tUp				-
 *	\param[in]	tDown			-
 *	\param[out]	measData		-
 *
 *	\return		function return
 ***************************************************************************************/
systemErrors_t tMeasUtils_postMeasurementCalculation(Stream_t* installation,
		                                             const float deltaT,
		                                             const float tUp,
													 const float tDown,
													 MeasurementData_t* measData)
{
	systemErrors_t err = WIKA_NO_ERROR;

	return err;
}

uint8_t *u8WikaHal_DAQ_decode_error_value(uint16_t error_code)
{
    uint16_t counter = 0;
    size_t err_count = sizeof(uss_errors) / sizeof(ussError_t);

    uint8_t* p_error_desc_str = NULL;

    for (counter = 0; counter < err_count; counter++) {
        if (uss_errors[counter].error_id == error_code) {
            p_error_desc_str = &uss_errors[counter].error_description[0];
        }
    }

    return p_error_desc_str;
}


















