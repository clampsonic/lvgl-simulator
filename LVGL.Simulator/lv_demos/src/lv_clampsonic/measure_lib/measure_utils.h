﻿/**************************************************************************************
 *  \file       measure_utils.h
 *
 *  \brief      This is an helper module for calculation
 *
 *  \details    This module contains functions and structures needed
 *  			for pre/post processing the measurement data.
 *
 *  \author     Luca Agrippino
 *
 *  \version    0.0
 *
 *  \date		24 ago 2021
 *
 *  \copyright  (C) Copyright 2021, Euromisure Sas di WIKA Italia Srl
***************************************************************************************/
#ifndef COMPONENTS_ULTRAFLOW_MIDDLEWARE_MEASURE_LIB_MEASURE_UTILS_H_
#define COMPONENTS_ULTRAFLOW_MIDDLEWARE_MEASURE_LIB_MEASURE_UTILS_H_

#include <stdint.h>
#include <stddef.h>
#include "../wika_data_types/wika_data_types.h"
#include "../error/errorManager.h"

/***************************************************************************************
 * 	\author		agrippino
 *
 *	\brief		This function make the necessary pre-measurement calculation
 *
 *	\param[in]	installation: contain the user info for make the measure
 *
 *	\return		The error code \see systemErrors_t
 ***************************************************************************************/
systemErrors_t tMeasUtils_preMeasurementCalculation(Stream_t* installation);

/***************************************************************************************
 * 	\author		agrippino
 *
 *	\brief		This function make all the necessary post measurement calculation
 *
 *	\param[in]	installation: contain the user info's for make the measure
 *	\param[in]	deltaT: it is the deltaT that came from DAQ
 *	\param[in]	tUp:    it is the tUp that came from DAQ
 *	\param[in]	tDown:	it is the tDown that came from DAQ
 *	\param[out]	gData: contain the result of measurement
 *
 *	\return		The error code \see systemErrors_t
 ***************************************************************************************/
systemErrors_t tMeasUtils_postMeasurementCalculation(Stream_t* installation,
		                                             const float deltaT,
		                                             const float tUp,
													 const float tDown,
													 MeasurementData_t* gData);

/***************************************************************************************
 * 	\author		agrippino
 *
 *	\brief		This function use the process sensor value that came form the DAQ
 *				and return converted value according with the user choise.
 *
 *	\param[in]	installation: contain the process sensor user setting
 *	\param[in]	measure: contain the process sensor acquired value
 *	\param[out]	installation: contain the converted value of process sensor
 *
 *	\return		The error code \see systemErrors_t
 ***************************************************************************************/
systemErrors_t  tMeasUtils_processSensorCalculation(Stream_t* installation, UssDataPacket_t* measure);

uint8_t *u8WikaHal_DAQ_decode_error_value(uint16_t error_code);

#endif /* COMPONENTS_ULTRAFLOW_MIDDLEWARE_MEASURE_LIB_MEASURE_UTILS_H_ */
