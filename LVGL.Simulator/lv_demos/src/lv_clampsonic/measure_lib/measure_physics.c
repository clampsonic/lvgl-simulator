﻿/*
 * measure_physics.c
 *
 *  Created on: 4 dic 2020
 *      Author: agrippino
 */

#include "math.h"
#include "measure_physics.h"
#include "measure_conversion.h"
#include "stdlib.h"

float fMeasPhysix_getCorrectedSoundSpeed(const float soundSpeed,
										 const float workingTemp,
										 const float referenceTemp,
										 const float correctionFactor)
{
	// css = ss_0 +(coeff *(T_0 - T_w))
	return (soundSpeed + (correctionFactor * (referenceTemp - workingTemp)));
}

float fMeasPhysix_getAngleOfRefraction(const float tetaMaterial_0,
									   const float soundSpeedMaterial_0,
									   const float soundSpeedMaterial_1)
{
	//    material 0
	//   ------------
	//    material 1

	// teta_1 = arcsin( (ss_1/ss_0) * sin(teta_0) )

	return (asin( (soundSpeedMaterial_1/soundSpeedMaterial_0) * sin(tetaMaterial_0)));
}

float fMeasPhysix_getLength(const float angle, const float thickness)
{
	// l = s * tan(teta)
	return (thickness * tan(angle));
}

float fMeasPhysix_getPipeInnerDiameter(const float pipeExternalDiameter,
									const float pipeThickness,
									const float liningThickness)
{
	// ft = Dext - 2*(pt+pl)
	return (pipeExternalDiameter - (2.0 * (pipeThickness + liningThickness) ) );
}

float fMeasPhysix_getMountingDistance(const float pipeLength,
									  const float liningLength,
									  const float fluidLength,
									  const uint8_t waveRepetitions)
{
	// md = 2 * (Lp+Ll)+ n*Lf
	return ( (2.0 * (pipeLength + liningLength) ) + (waveRepetitions * fluidLength) );
}

float fMeasPhysix_getPropagationTime(const float soundSpeed,
		                             const float thickness,
									 const float length)
{
	// dp = sqrt(thickness^2+length^2)
	// pt = dp/ss

	float d = sqrt(pow(thickness,2) + pow(length,2) );
	return (d/soundSpeed);
}

float fMeasPhysix_getFluidDistance(const float thickness, const float length)
{
	// d = sqrt(thickness^2+length^2)
	return sqrt((pow(thickness,2)+pow(length,2)));
}

float fMeasPhysix_getFluidArea(const float distance)
{
	// A = (d/2)^2 * pi
	float tmp = (distance/2.0);
	return ( powf(tmp,2) * 3.14);
}

float fMeasPhysix_getTUpsTDns(const float tTotUp,
		                 const float tSens,
						 const float tPipe,
						 const float tLining,
						 const float tCircuit)
{
	// tUp = tTotUp - 2*tS - 2*tP - 2*tL - tC

	return (tTotUp - 2.0*tSens - 2.0*tPipe - 2.0*tLining - tCircuit );
}

float fMeasPhysix_getDeltaT(const float tUp, const float tDown, const uint8_t waveRep)
{
	// deltaT = tUp - tDown

	return ( (tUp - tDown) );
}

float fMeasPhysix_getMeasuredFluidSpeed(const float deltaT,
		                                const float tUp,
										const float tDown,
										const uint8_t waveRep,
										const float diameter,
										const float angle)
{
	// see here about this formula: http://www.ultrasonicscn.com/FAQ/Working-principle/44.html

	// Vf = (n*D)/(sin(2*teta))(deltaT/(tUp*tDown) )
	return ( ((waveRep*diameter)/(sin(2*angle))) * (deltaT/(tUp*tDown)) );
}

float fMeasPhysix_getTheoricFluidSpeed(const float deltaT,
		                               const float distance,
									   const float angle,
									   const float soundSpeed)
{
	// tSS = ( ( (cf^2*deltaT)/(2*Df) ) * sin(tetaf) )
	return ( ( (pow(soundSpeed,2) * deltaT ) / (2.0*distance) ) * sin(angle) );
}

/*************************************************************************************
 * \brief Reynolds correction algorithm approximation
 *************************************************************************************/
const float a0 = 76.17;		//#  (34.45, 117.9)
const float a1 = -18.53;	//#  (-48.22, 11.15)
const float b1 = 20.74;   	//#  (-53.22, 94.71)
const float a2 = 1.924;		//#  (-37.88, 41.72)
const float b2 = 14.78;		//#  (-34.74, 64.3)
const float a3 = 4.016;		//#  (-23.28, 31.31)
const float b3 = 2.664;		//#  (-18.16, 23.49)
const float a4 = 0.6944;	//#  (-7.483, 8.872)
const float b4 = -0.1907;	//#  (-3.426, 3.044)
const float w  = 0.0001567;	//#  (-0.0002182, 0.0005315)

float fMeasPhysix_getCorrectedFluidSpeed(const float fuildSpeed,
		                                 const float pipeInternalDiameter,
										 const float kinematicViscosity)
{
	// Re = (Vf*Sf)/kv;
	float Re = (float) ((fuildSpeed*pipeInternalDiameter)/kinematicViscosity);
	float kRe = 0.0;

	float ReLowLimit = 1300.0;
	float ReHighLimit = 20.0e3;

	float limit1 = 0.0;
	float limit2 = 0.0;

	limit1 = Re - ReLowLimit;

	if(limit1 < 0)
	{
		kRe = (float) 0.75;
	}
	else
	{
		limit2 = ReHighLimit - Re;

		if( ( limit1 >= 0 ) && ( limit2 >= 0 ))
		{
			kRe = (float) (a0 + a1*cos(Re*w) + b1*sin(Re*w) + a2*cos(2*Re*w) + b2*sin(2*Re*w) + a3*cos(3*Re*w) + b3*sin(3*Re*w) + a4*cos(4*Re*w) + b4*sin(4*Re*w));
		}
		else
		{
			kRe = (float) (1/(1.119-0.011*log10(Re)));
		}
	}

	// Vfc = Vf * kRe
	return ( fuildSpeed*kRe );
}

float fMeasPhysix_getFlow(const float corrSSFluid, const float fluidArea)
{
	return (corrSSFluid*fluidArea);
}

float fMeasPhysix_getVolume(const float fluidArea, const float fluidSpeed, const float deltaT)
{
	return (fluidSpeed * fluidArea * deltaT);
}

float fMeasPhysix_getMassFlow(const float vol, const float density)
{
	return (vol * density);
}

float fMeasPhysix_getMass(const float massFlow)
{
	return 0.0;
}

float fMeasPhysix_getEnergy(const float massFlow, const float dEnthalpy)
{
	return 0.0;
}


float fMeasPhysix_getKinematicViscosity(const float absViscosity, const float fluidDensity)
{
	// kV = va/rhof
	return (absViscosity/fluidDensity);

}

float fMeasPhysix_getMeasureRate(const float measuredSpeed, const float theoricSpeed)
{
	// r = (Vfm/Vft);
	return (measuredSpeed/theoricSpeed);
}
