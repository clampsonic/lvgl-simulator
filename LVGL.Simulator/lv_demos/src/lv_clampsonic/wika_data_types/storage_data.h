/*
 * storage_data.h
 *
 *  Created on: 17 feb 2021
 *      Author: agrippino
 */

#ifndef COMPONENTS_ULTRAFLOW_MIDDLEWARE_UTILS_WIKA_DATA_TYPES_STORAGE_DATA_H_
#define COMPONENTS_ULTRAFLOW_MIDDLEWARE_UTILS_WIKA_DATA_TYPES_STORAGE_DATA_H_

#include <stdint.h>
#include <stddef.h>

#define TABLE_HEADER_VALUE_LENGTH     	 36
#define TABLE_HEADER_ROW_LENGTH        	  5
#define TABLE_DATA_ROW_LENGTH         	 16
#define TABLE_DATA_COLUMNS             	  5
#define TABLE_DATA_HEADER_FIELDS       	  2
#define MAX_FLUID_NAME_LENGTH         	 32


typedef struct
{
	char table_info         [TABLE_HEADER_ROW_LENGTH][TABLE_HEADER_VALUE_LENGTH];
	char table_data_header  [TABLE_DATA_HEADER_FIELDS][TABLE_DATA_COLUMNS][TABLE_HEADER_VALUE_LENGTH];
}tableHeader;

typedef struct
{
	char table_data_row     [TABLE_DATA_COLUMNS][TABLE_DATA_ROW_LENGTH];
}waterTable;

typedef struct
{
	char table_data_row     [TABLE_DATA_COLUMNS][TABLE_DATA_ROW_LENGTH];
}otherFluidsTable;


#endif /* COMPONENTS_ULTRAFLOW_MIDDLEWARE_UTILS_WIKA_DATA_TYPES_STORAGE_DATA_H_ */
