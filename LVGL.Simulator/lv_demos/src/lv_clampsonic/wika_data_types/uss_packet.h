/*
 * uss_packet.h
 *
 *  Created on: 9 dic 2020
 *      Author: agrippino
 */

#ifndef COMPONENTS_ULTRAFLOW_MIDDLEWARE_UTILS_WIKA_DATA_TYPES_USS_PACKET_H_
#define COMPONENTS_ULTRAFLOW_MIDDLEWARE_UTILS_WIKA_DATA_TYPES_USS_PACKET_H_

#include <stdint.h>


#define CONFIGURATION_PACKET_PAYLOAD    4
#define PLOT_PACKET_PAYLOAD             12	//wika protocol
#define ALL_PLOT_PACKET_PAYLOAD        	60
#define CALIBRATION_PACKET_PAYLOAD     	20
#define UPS_DNS_PLOT_WAVEFORM_LEN		54


/*********************************************************************
 **************** DATA PACKET ****************************************
 *********************************************************************/
// Data from the DAQ arrives in floating-point precision
typedef struct
{
	float	fdtof_in_s;
	float	ftime_ups_in_s;
	float	ftime_dns_in_s;
	float	volume;
	float	soundSpeed;
	float	amplitude;
	float	temperature;
	float	gain;
}UssDataPayload_t;

typedef struct
{
	uint8_t packet_size;
	uint8_t command_id;
	uint8_t error_code;
	uint8_t channel;
	UssDataPayload_t payload;
}UssDataPacket_t;

typedef struct
{
	UssDataPacket_t UssData;
	uint8_t 		streamNumber;
}UssStreamDataPacket_t;

/*********************************************************************
 **************** STATUS PACKET **************************************
 *********************************************************************/
typedef struct
{
	float temperature;
	float signalLevel;
	float gain;
}UssStatPayload_t;

typedef struct
{
	uint8_t packet_size;
	uint8_t command_id;
	uint8_t error_code;
	uint8_t channel;
	UssStatPayload_t payload;
}UssStatPacket_t;

typedef struct
{
	UssStatPacket_t ussStat;
	uint8_t 		streamNumber;
}UssStreamStatPacket_t;

/*********************************************************************
 **************** CONFIG UPDATE PACKET *******************************
 *********************************************************************/
typedef struct
{
	uint8_t packet_size;
	uint8_t command_id;
	uint8_t error_code;
	uint8_t regToUpdate;
}UssCfgPacket_t;

/*********************************************************************
 **************** PLOT WAVEFORMS PACKET ******************************
 *********************************************************************/
typedef struct _UPS_DNS_CMD_HEADER_
{
	uint8_t packet_size;
	uint8_t cmd_handler_gui_id;
	uint8_t command_id;
	uint8_t cmd_handler_write_cmd;
	uint8_t cmd_handler_plot_adc_stat;
	uint8_t numOfPacks;
} UssUpsDnsHeadPacket_t;

typedef struct _UPS_DNS_PLOT_WAVEFORMS_
{
	uint8_t 	packet_size;
	uint8_t 	cmd_handler_gui_id;
	uint8_t 	command_id;
	uint8_t 	cmd_handler_write_cmd;
	uint8_t		plot_adc_stat;
	uint8_t		numOfPacks;
	int16_t 	waveFormPlotData[UPS_DNS_PLOT_WAVEFORM_LEN/2];
} UssUpsDnsPlotPacket_t;

typedef struct _GEN_PLOT_STRUCT_ {
	uint16_t		position;
	size_t			plotSize;
	int16_t 		plotWaveform[UPS_DNS_PLOT_WAVEFORM_LEN*10];
} UssPlot_t;

typedef struct _STREAM_PLOT_STRUCT_ {
	UssPlot_t	upsPlot;
	UssPlot_t	dnsPlot;
	uint8_t		streamNumber;
} UssStreamPlot_t;

/*********************************************************************/
/*** NOT USED ********************************************************/
/*********************************************************************/
typedef struct
{
	uint8_t packet_size;
	uint8_t msp_design_center_id;
	uint8_t command_id;
	uint8_t read_write_command;
	uint8_t payload[ALL_PLOT_PACKET_PAYLOAD];
	uint8_t checksum_lsb;
	uint8_t checksum_msb;
}UssVariableLenPlottingPacket_t;

typedef struct
{
	uint8_t packet_size;
	uint8_t msp_design_center_id;
	uint8_t command_id;
	uint8_t read_write_command;
	uint8_t payload[CALIBRATION_PACKET_PAYLOAD];
	uint8_t checksum_lsb;
	uint8_t checksum_msb;
}UssCalibrationPacket_t;

typedef struct
{
	uint8_t packet_size;
	uint8_t msp_design_center_id;
	uint8_t command_id;
	uint8_t read_write_command;
	uint8_t payload[CONFIGURATION_PACKET_PAYLOAD];
	uint8_t checksum_lsb;
	uint8_t checksum_msb;
}UssConfigPacket_t;

#endif /* COMPONENTS_ULTRAFLOW_MIDDLEWARE_UTILS_WIKA_DATA_TYPES_USS_PACKET_H_ */
