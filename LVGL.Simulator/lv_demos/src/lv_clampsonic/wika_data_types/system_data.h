﻿/*
 * system_data.h
 *
 *  Created on: 23 feb 2021
 *      Author: agrippino
 */

#ifndef COMPONENTS_ULTRAFLOW_MIDDLEWARE_UTILS_WIKA_DATA_TYPES_SYSTEM_DATA_H_
#define COMPONENTS_ULTRAFLOW_MIDDLEWARE_UTILS_WIKA_DATA_TYPES_SYSTEM_DATA_H_
 /*************************************************************
  ********* STREAM NUMBER DEFINITION **************************
  *************************************************************/
typedef enum _DAQ_STREAM_NUMBER_ {
    DAQ_STREAM_1 = 0,
    DAQ_STREAM_2 = 1,
} streamNumber_t;

/***************************************************************************************
 * 	\author		BodiniA
 *
 * 	\enum		systemState_t
 *
 *	\brief		These are the states of the taskSystem FSM
 ***************************************************************************************/
typedef enum _SYSTEM_FSM_STATES_
{
    undefined,
    initHal,
    initFileSystem,
    configDevice,
    running,
}systemState_t;

/*************************************************
 * \struct 	machine_state_t
 * \details	FSM Prototype
 * 			Structure containing a FSM std variables
 *************************************************/
typedef struct _FSM_PROTO_ {
    uint8_t 	currentState;
    uint8_t 	nextState;
    uint8_t		returnState;
    uint8_t		firstTransition;
} machine_state_t;

/*************************************************
 * \enum 	fwUpdatePhase_t
 * 			Variable describing the Firmware
 * 			upgrade process.
 *************************************************/
typedef enum _FW_UPDATE_PHASE_ {
    FW_UPDT_PHASE_ERROR = -2,
    FW_UPDT_PHASE_UNKNOWN = -1,
    FW_UPDT_PHASE_DOWNLOAD_FIRMWARE = 0,
    FW_UPDT_PHASE_COPY_DATA = 1,
    FW_UPDT_PHASE_CHECK_DATA = 2,
    FW_UPDT_PHASE_COMPLETED = 3,
} fwUpdatePhase_t;

/*************************************************
 * \struct 	fwUpdateDataStats_t
 * 			Structure containing variables to
 * 			feed the firmware update process linked
 * 			functions.
 *************************************************/
typedef struct _FW_UPDATE_DATA_STATS_ {
    int32_t i32curr_len;
    int32_t i32total_len;
    uint8_t u8percentage;
    fwUpdatePhase_t tcurrent_phase;
} fwUpdateDataStats_t;


//-----------------------------------------
//-- WEBSOCKET REQUEST DATA STRUCTURE -----
//-----------------------------------------
typedef struct _WEBSOCKET_ASYNC_RESP_ARG_ {
    //httpd_handle_t hd;
    int fd;
} wsAsyncRespArg_t;

typedef struct _ARRAY_AND_SIZE_ {
    void* arrayPointer;
    size_t arraySize;
} arrayAndSize_t;


#endif /* COMPONENTS_ULTRAFLOW_MIDDLEWARE_UTILS_WIKA_DATA_TYPES_SYSTEM_DATA_H_ */
