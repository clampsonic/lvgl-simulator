﻿/**************************************************************************************
 *  \file       wikaXml_data.h
 *
 *  \brief      File Brief Description
 *
 *  \details    File Detailed Description
 *
 *  \author     Agrippino Luca
 *  \author     Bodini Andrea
 *
 *  \version    1.0
 *
 *  \date		30 nov 2021
 *
 *  \copyright  (C) Copyright 2021, Euromisure Sas di WIKA Italia Srl
***************************************************************************************/

#ifndef COMPONENTS_ULTRAFLOW_MIDDLEWARE_UTILS_WIKA_DATA_TYPES_WIKAXML_DATA_H_
#define COMPONENTS_ULTRAFLOW_MIDDLEWARE_UTILS_WIKA_DATA_TYPES_WIKAXML_DATA_H_

//--------------------------------------------------------------------------------------
#include <stdint.h>
//--------------------------------------------------------------------------------------

/********************************** LOCAL DEFINES *************************************/
#define SSID_LENGTH			64
#define NAME_LENGTH 		36
#define SOLID_NAME_LEN		21
#define IP_ADD_LENGTH		16
#define BT_PIN_LENGHT		7
#define BT_SERVICE_NUM		4
#define BT_UUID_LEN			37
#define VERSION_LEN			5
#define DATE_TIME_LENGTH	35
#define MAC_ADD_LEN			18
#define WIFI_PASS_LEN		64
#define WIFI_COUNTRY_LEN	3
#define NTP_TIMEZONE_LEN	64

#define CHANNEL_NUMBER			1
#define STREAM_NUMBER			2
#define NUMBER_OF_MEASUREMENTS	9
#define ANALOG_INPUTS_NUMBER_PER_CHANNEL	2

#define	SCHEDULE_LBL_SIZE		4

#define SERIAL_START_LINE_CHAR 	'$'
#define SERIAL_END_LINE_CHAR	'#'

// do not change this order!
typedef enum _ENABLE_MEASUREMENTS_ {
    USER_MEASURE_FLOW = 0,
    USER_MEASURE_VOLUME = 1,
    USER_MEASURE_MASSFLOW = 2,
    USER_MEASURE_MASS = 3,
    USER_MEASURE_ENERGY = 4,
    USER_MEASURE_DENSITY = 5,
    USER_MEASURE_TOTFLOW = 6,
    USER_MEASURE_TOTMASS = 7,
    USER_MEASURE_TOTENERGY = 8,
} sel_meter_t;

/*******************************************************************************************/
/************************ LANG PARAMETERS STRUCTURE ****************************************/
/*******************************************************************************************/


#define NUM_OF_SUPPORTED_LANGUAGES  3

#define DEFAULT_LANGUAGE            langEN

/**
 * /enum    wikaSupportedLang_t
 *          Enum representing the GUI supported languages.
 *          Naming scheme from ISO 639-1 standard
*/
typedef enum  _SUPPORTED_LANGS_ {
    langEN,
    langIT,
    langDE,
    langFR,
    langES,
} wikaSupportedLang_t;

/*******************************************************************************************/
/************************ SENSOR PARAMETERS STRUCTURE **************************************/
/*******************************************************************************************/
typedef struct _USS_SENSOR_T_ {
    uint8_t Type;
    uint8_t  Name[NAME_LENGTH];
    float Frequency;
    float AngleInRad;
    float WedgeSoundSpeed;
    float WedgeSsCorrectionFactor;
    float CircuitPropTimeInSec;
    float ReferenceTemperature;
    float WorkingTemperature;
    float PropagationTimeInSec;
    uint8_t TemperatureSource;
    float minTemperature;
    float maxTemperature;
}UssSensor_t;

typedef enum _USS_SENSOR_CONFIGURATION_ {
    USS_SENSOR_CFG_I = 1,
    USS_SENSOR_CFG_V = 2,
    USS_SENSOR_CFG_Z = 3,
    USS_SENSOR_CFG_W = 4,
    USS_SENSOR_CFG_OTHER = 0,
} uss_sensor_cfg_t;


/*******************************************************************************************/
/************************ EXTERNAL SENSORS (4/20 mA) STRUCTURES ****************************/
/*******************************************************************************************/
typedef enum _EXT_SENS_ALARM_TYPE_ {
    extSensAlarm_noError = 0,
    extSensAlarm_shortCircuit = 21,
    extSensAlarm_openCircuit = 3,
}extSensAlarmType_t;

typedef enum _EXT_SENSOR_LABEL_ {
    EXTSENS_AIN1_MAIN1 = 0,
    EXTSENS_AIN2_AUX1 = 1,
    EXTSENS_AIN3_MAIN2 = 2,
    EXTSENS_AIN4_AUX2 = 3,
    EXTSENS_USERDEFINED = 4,
}extSensorId_t;

typedef struct _EXT_SENSOR_STRUCT_ {
    uint8_t			  id;
    uint8_t      	Enable;
    uint8_t      	Type;
    float        	EndPoint4;
    float        	EndPoint20;
    extSensAlarmType_t  OutOfRangeAlarm;
    uint8_t      	Unit[NAME_LENGTH];
    float        	Value;
} AnalogInputs_t;

/*******************************************************************************************/
/************************ CHANNEL CONFIGURATION STRUCTURES *********************************/
/*******************************************************************************************/
typedef struct _USS_CHANNEL_ {
    uint8_t 			available;
    uint8_t				enable;
    UssSensor_t 		ussSensor;
}Channel_t;

/*******************************************************************************************/
/************************ DAQ CONFIGURATION STRUCTURES *************************************/
/*******************************************************************************************/
typedef struct _DAQ_CONFIG_ {
    uint32_t GapPulseStartAdcCapture;
    uint32_t NumberOfPulses;
    uint32_t UpsAndDnsGap;
    uint32_t Ups0AndUps1Gap;
    uint32_t  GainControl;
    float    MeterConstant;
    uint32_t UssXtFrequency;
    uint32_t AdcSamplingFrequency;
    uint32_t SignalSamplingFrequency;
    uint32_t  AdcOverSamplingRate;
    int32_t  DeltaTofOffset;
    int32_t  AbsTofAdditionalDelay;
    int32_t  CaptureDuration;
    int32_t  AlgorithmOption;
    int32_t  UlpBiasDelay;
    int32_t  StartPpgCount;
    int32_t  TurnOnAdcCount;
    int32_t  UserParam7_EnvCrossThresh;
    int32_t  UserParam8_EndpointT4mA;
    int32_t  UserParam9_EndpointT20mA;
    int32_t  UserParam10_EstimatedTemp;
    int32_t  StartPgaAndInBiasCount;
    int32_t  UserParam6;
    uint32_t LeaEnable;
    uint16_t F1;
    uint16_t F2;			//dummy value to align two valid values inside the same uint32. ignored
    uint32_t  PatternOption;
    uint32_t  CalibrationTableStatus;
}DaqConfig_t;

typedef struct _DAQ_STRUCT_ {
    Channel_t channels[CHANNEL_NUMBER];
    DaqConfig_t daqConfig;
}Daq_t;

/*******************************************************************************************/
/************************ PIPE CONFIGURATION STRUCTURES ************************************/
/*******************************************************************************************/
typedef struct _PIPE_STRUCT_ {
    // from user
    uint8_t 		id;
    uint8_t 		materialId;
    uint8_t 		Material[SOLID_NAME_LEN];
    float 			DiameterInMt;
    float 			ThicknessInMt;
    extSensorId_t 	tempSource;
    float			workingTemp;
    //from table
    uint16_t SoundSpeed;
    //from Schedule
    uint8_t sizeId;
    uint8_t pipeSize[SCHEDULE_LBL_SIZE];
    uint8_t scheduleNo[SCHEDULE_LBL_SIZE];
}Pipe_t;

/*******************************************************************************************/
/************************ LINING CONFIGURATION STRUCTURES **********************************/
/*******************************************************************************************/
typedef struct _LINING_STRUCT_ {
    // from user
    uint8_t Material;
    float ThicknessInMt;
    // from table
    uint16_t SoundSpeed;
}Lining_t;

/*******************************************************************************************/
/************************ LINING CONFIGURATION STRUCTURES **********************************/
/*******************************************************************************************/
typedef struct _FLUID_STRUCT_ {
    //from user
    uint8_t id;
    uint8_t Name[NAME_LENGTH];
    extSensorId_t tempSource;
    float WorkingTemperature;
    // from table
    uint16_t SoundSpeed;
    float AbsoluteDensity;
    float KinematicViscosity;
    float dynamicViscosity;
    float Enthalpy;

    // other fluids
    float specificGravity;
    float ssTempCoeff;
}Fluid_t;

/*******************************************************************************************/
/************************ EXTERNAL ENVIRONMENT STRUCTURE ***********************************/
/*******************************************************************************************/
typedef struct _EXT_ENVIRONMENT_STRUCT_ {
    Pipe_t pipe;
    Lining_t lining;
    Fluid_t fluid;
}ExternalEnvironment_t;

/*******************************************************************************************/
/************************ PRE CALCULATION RESULTS ******************************************/
/*******************************************************************************************/
typedef struct _PRE_CALC_RESULT_ {
    float mountingDistance;
    float pipePropagationTimeInSec;
    float liningPropagationTimeInSec;
    float fluidPropagationTime;
    float fluidAngle;
    float fluidLength;
    float internalPipeDiameter;
    float fluidDistance;
    float fluidArea;
    float fluidDensity;
    float deltaEnthalpy;
    float totalPropagationTime;
} PreCalcResult_t;

/*******************************************************************************************/
/************************ ENERGY MEASUREMENT CONFIGURATION *********************************/
/*******************************************************************************************/
typedef enum _ENERGY_MEASURE_TYPE_ {
    ENERGY_MEAS_TYPE_DISABLED = 0x00,
    ENERGY_MEAS_TYPE_COOLER = 0x01,
    ENERGY_MEAS_TYPE_HEATER = 0x02,
} energyMeasureType_t;

typedef struct _ENERGY_MEASUREMENT_SECTION_ {
    uint8_t				enable;
    uint8_t				lossEstimEnable;
    energyMeasureType_t energyMeasType;
    uint8_t				sendStream;
    extSensorId_t		sendTempSource;
    extSensorId_t		retTempSource;
    float				sendTempValue;
    float				retTempValue;
} EnergyMeas_t;

/*******************************************************************************************/
/************************ STREAM CONFIGURATION STRUCTURES **********************************/
/*******************************************************************************************/
typedef struct _MEASUREMENT_PARAMETERS_ {
    Daq_t daq;
    ExternalEnvironment_t externalEnvironment;
    AnalogInputs_t 		analogInput[ANALOG_INPUTS_NUMBER_PER_CHANNEL];
} measParameters_t;

typedef enum _SETPOINT_TYPE_ {
    SETPOINT_SPEED = 0x00,
    SETPOINT_FLOWRATE = 0x01
} setPointType_t;

typedef struct _CONFIGURATION_ {
    // Measure Configuration
    uint8_t 			      dualChannelEnable;
    uss_sensor_cfg_t 	  sensor_configuration;
    uint8_t 			      enableReynold;
    float               dTOFzeroOffsetInNs;

    // Energy Section
    energyMeasureType_t	energyMeasType;
    uint8_t 			      mainChannel;

    // User Measurement Section
    uint8_t 			      userMeasure[NUMBER_OF_MEASUREMENTS];
    uint8_t 			      auxiliaryFlow;
    uint8_t 			      auxiliaryVolume;
    uint8_t 			      auxiliaryMassFlow;

    // Setpoint Section
    setPointType_t 		  setPointType;
    uint8_t 			      setPointUnit[10];
    float				        setPointScaling;
    float 				      setPointCutoff;
    float				        setPointWarningThreshold;
    float				        setPointFullScale;
} Configuration_t;


typedef struct _STREAM_ {
    uint8_t id;
    uint8_t enable;
    uint8_t measureInProgress;
    Configuration_t configuration;
    measParameters_t parameters;
    PreCalcResult_t preCalculations;
} Stream_t;

typedef struct _MEASUREMENT_SESSION_ {
    uint8_t activeStream;
    Stream_t streams[STREAM_NUMBER];
    EnergyMeas_t energyMeasParams;
} MeasurementSession_t;

typedef struct _MEASUREMENT_DATA_ {
    uint8_t streamNumber;
    // RAW Data
    float fdelta_tof_in_us;
    float fabsolute_tups_in_us;
    float fabsolute_tdns_in_us;
    uint8_t ussError;

    // post measurement
    float FluidSpeed;
    float Flow;
    float Volume;
    float MassFlow;
    float Mass;
    float Energy;

    // Filtered Data
    float filteredTUpInS;
    float filteredTDownInS;
    float filteredDeltaTInS;

    float fpcb_temperature;
    float fpcb_rh;
} MeasurementData_t;

// device configuration
typedef struct _USER_DATA_ {
    uint8_t Company[2 * NAME_LENGTH];
    uint8_t VAT[NAME_LENGTH];
    uint8_t Address[2 * NAME_LENGTH];
    uint8_t Email[2 * NAME_LENGTH];
    uint8_t PlantID[NAME_LENGTH];
} UserData_t;

typedef struct _DISPLAY_STRUCT_ {
    uint8_t  Brightness;
    uint8_t  AutoLock;
    uint32_t AutoLockTimeoutInSec;
}Display_t;

//-------------- WIFI ---------------------

typedef enum _WIFI_MODE_ {
    WiFiMode_StationMode = 0,
    WifiMode_SoftAP = 1,
} wifiMode_t;

typedef enum _IP_MODE_ {
    IP_Mode_DHCP = 0,
    IP_Mode_Static = 1,
} ipMode_t;

typedef struct _WIFI_STRUCT_ {
    uint8_t 	WiFiEnable;
    wifiMode_t 	WiFiMode;
    uint8_t 	mac[MAC_ADD_LEN];
    uint8_t 	SSID[SSID_LENGTH];
    uint8_t		PSSWD[WIFI_PASS_LEN];
    ipMode_t	IPType;
    uint8_t 	IP[IP_ADD_LENGTH];
    uint8_t 	NetMask[IP_ADD_LENGTH];
    uint8_t 	Gateway[IP_ADD_LENGTH];
    uint8_t 	DNS1[IP_ADD_LENGTH];
    uint8_t 	DNS2[IP_ADD_LENGTH];
    uint8_t 	channel;
    uint8_t 	maxStaConn;
    uint8_t 	HostName[NAME_LENGTH];
    uint8_t 	WebServerMountingPoint[NAME_LENGTH];
    uint8_t		wifiRegion[WIFI_COUNTRY_LEN];
    uint8_t		wifiSecurity;
    uint8_t		hideSSID;
    uint8_t		WiFiConnected;
} Wifi_t;
//-----------------------------------------

//-------------- BLUETOOTH ---------------------
typedef enum _SERVICE_RIGHTS_ {
    BLE_SERVICE_W = 0,
    BLE_SERVICE_R = 1,
} BtServiceType_t;

typedef struct _BT_SERVICES_ {
    uint8_t 		Name[NAME_LENGTH];
    uint8_t 		UUID[BT_UUID_LEN];
    uint8_t			byteLength;
    uint8_t 		notifyEnable;
    BtServiceType_t	serviceType;
} BluetoothServices_t;

typedef struct _BT_STRUCT_ {
    uint8_t 			enable;
    uint8_t 			lastDeviceName[NAME_LENGTH];
    uint8_t 			BtPIN[BT_PIN_LENGHT];
    uint8_t 			mac[MAC_ADD_LEN];
    BluetoothServices_t ServicesList[BT_SERVICE_NUM];
}Bluetooth_t;
//----------------------------------------------


typedef struct _LOOP_PWR_EN_ {
    uint8_t LoopPower24;
}Power_t;

//-------------- RS485 ---------------------
typedef enum _RS485_BAUD_RATE_ {
    BR1200 = 1200,
    BR2400 = 2400,
    BR4800 = 4800,
    BR9600 = 9600,
    BR19200 = 19200,
    BR38400 = 38400,
    BR56000 = 56000,
    BR115200 = 115200,
} baudRate_t;

typedef enum _RS485_PARITY_ {
    NONE_PARITY = 0,
    ODD_PARITY = 1,
    EVEN_PARITY = 2
} Parity_t;

typedef enum _RS485_STOP_BITS_ {
    ONE = 1,
    TWO = 2
} StopBits_t;

typedef struct _RS485_INFO_ {
    uint8_t 	enable;
    baudRate_t 	speed;
    StopBits_t	stopBits;
    Parity_t	parity;
    uint64_t	dataThroughputInMS;
} RS485_t;
//------------------------------------------
typedef enum _MODBUS_TYPE_ {
    MODBUS_RTU = 0x01,
    MODBUS_TCP = 0x02
}modBusType_t;

typedef struct _MODBUS_INFO_ {
    uint8_t enable;
    modBusType_t type;
    uint8_t address[IP_ADD_LENGTH];
} ModBus_t;

typedef enum _PULSE_OUT_TECH_UNIT_ {
    PULSE_UNIT_ML = 0x00,
    PULSE_UNIT_L = 0x01,
    PULSE_UNIT_M3 = 0x02,
    PULSE_UNIT_GAL = 0x03
} pulseTechUnit_t;

typedef struct _PULSED_OUT_ {
    uint8_t 		enable;
    pulseTechUnit_t pulseTechUnit;
    uint32_t 		pulseVolume;
    uint32_t		pulseDuration;
} PulsedOut_t;

//--- MQTT CONNECTION ---------------------
typedef struct _MQTT_DATA_ {
    uint8_t 	enable;
    uint8_t		brokerIP[IP_ADD_LENGTH];
    uint8_t		brokerDNS[2 * NAME_LENGTH];
    uint32_t 	port;
    uint8_t		username[NAME_LENGTH];
    uint8_t		password[NAME_LENGTH];
    uint8_t		sslPath[NAME_LENGTH];
} MQTT_t;
//-----------------------------------------

//--- TIME SYNC ---
typedef struct _TIME_SYNC_STRUCT_ {
    uint8_t	ntpEnable;
    uint8_t	ntpIP[IP_ADD_LENGTH];
    uint8_t	ntpADD[NAME_LENGTH];
    uint8_t timeZone[NTP_TIMEZONE_LEN];
} NTP_t;

//--- System startup modes ---
typedef enum _SYSTEM_WORK_MODES_ {
    SYS_WRK_NORMAL_MODE = 0x00,		/*!< Standard Working mode */
    SYS_WRK_TESTING_MODE = 0x01,		/*!< GUI shows only measurement parameters */
    SYS_WRK_ENGINEER_MODE = 0x02,		/*!< GUI shows detailed information on the current process */
    SYS_WRK_PROGRAM_MODE = 0x03,		/*!< The system freezer communication peripherals to permit Gesture controller programming */
} sysWorkMode_t;
//-----------------------------------------

typedef enum _SYSTEM_UNITS_ {
    SYSUNITS_SI = 0x01,
    SYSUNITS_IMPERIAL = 0x02,
} sysUnits_t;

//--- System Settings Structure ---
typedef struct _SYSTEM_CONFIG_ {
    wikaSupportedLang_t Language;
    uint8_t 			DateTime[DATE_TIME_LENGTH];
    sysUnits_t 			Unit;
    uint8_t 			CurrentFW[VERSION_LEN];
    uint8_t 			warrantyStartDate[DATE_TIME_LENGTH];
    uint8_t 			warrantyExpired;
    sysWorkMode_t 		WorkingMode;
    uint8_t 			mainLedBrightness;
    uint8_t				RH;
    uint32_t 			temperature;
}System_t;
//-----------------------------------------

//-----------------------------------------
//-- MAIN SYSTEM CONFIGURATION STRUCTURE --
//-----------------------------------------
typedef struct _DEVICE_PARAMETERS_STRUCT_ {
    //--- MAIN INFO ---
    uint8_t 	name[NAME_LENGTH];
    uint8_t		serialNumber[NAME_LENGTH];
    uint8_t 	fwVersion[VERSION_LEN];
    uint8_t		daqFwVersion[VERSION_LEN];
    uint8_t 	mac[MAC_ADD_LEN];
    //---------------------------------
    //--- USER CONFIGURABLE SECTION ---
    //---------------------------------
    uint8_t 	sysPassword[NAME_LENGTH];
    UserData_t 	userData;
    Display_t 	display;
    uint8_t		validConfigFlag;
    //--- Connectivity
    Wifi_t 		wifi;
    Bluetooth_t bluetooth;
    RS485_t 	rs485;
    ModBus_t 	modBus;
    PulsedOut_t pulseOut;
    MQTT_t		mqtt;
    Power_t 	power;
    //--- System parameters
    System_t 	system;
    NTP_t		ntp;
}DeviceConfig_t;
//-----------------------------------------

//-----------------------------------------
//-- WIFI AP LIST DATA STRUCTURE ----------
//-----------------------------------------

/*************************************************
 * \struct 	wifi_ap_t
 * 			WiFi access-point structure.
 * 			It contains SSID name and RSSI.
 *************************************************/
typedef struct _WIFI_AP_LIST_ {
    uint8_t 	SSID[NAME_LENGTH];
    int8_t		rssi;
} wifi_ap_t;

/************************************/

#endif
