/*
 * diagnostic_data.h
 *
 *  Created on: 8 dic 2020
 *      Author: agrippino
 */

#ifndef COMPONENTS_ULTRAFLOW_MIDDLEWARE_UTILS_WIKA_DATA_TYPES_DIAGNOSTIC_DATA_H_
#define COMPONENTS_ULTRAFLOW_MIDDLEWARE_UTILS_WIKA_DATA_TYPES_DIAGNOSTIC_DATA_H_

typedef struct
{
	float measured_speed_sound;
	float flow_rate;  // *500
	float Re;
	float KRe;
	float volume;
	float pipe_angle;
	float lining_angle;
	float fluid_angle;
	float propagation_speed_sound_up_total_time;
	float propagation_speed_sound_down_total_time;
	float delta_t;

	float amplitude_average;   // ??
	float amplitude_max_up;    // ??
	float amplitude_max_down;  // ??
	float Qup; //(abs(amplitude_max_up-amplitude_max_ist)/amplitude_average) * 100
	float Qdown; //(abs(amplitude_max_down-amplitude_max_ist)/amplitude_average) * 100

	float send_temperature;
	float return_temperature;

}diagnostic_t;

#endif /* COMPONENTS_ULTRAFLOW_MIDDLEWARE_UTILS_WIKA_DATA_TYPES_DIAGNOSTIC_DATA_H_ */
