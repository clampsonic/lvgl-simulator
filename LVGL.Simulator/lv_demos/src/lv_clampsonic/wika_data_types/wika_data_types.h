﻿/*
 * wika_data_types.h
 *
 *  Created on: 8 dic 2020
 *      Author: agrippino
 */

#ifndef COMPONENTS_ULTRAFLOW_MIDDLEWARE_UTILS_WIKA_DATA_TYPES_WIKA_DATA_TYPES_H_
#define COMPONENTS_ULTRAFLOW_MIDDLEWARE_UTILS_WIKA_DATA_TYPES_WIKA_DATA_TYPES_H_

#include "diagnostic_data.h"
#include "gesture_data.h"
#include "storage_data.h"
#include "system_data.h"
#include "uss_packet.h"
#include "device_data.h"
#include "wikaXml_data.h"
#ifndef LVGL_SIMULATOR_ACTIVE
#include "led_data.h"
#endif // !

// generic defines
#define ENABLED  1
#define DISABLED 0

#ifdef LVGL_SIMULATOR_ACTIVE
void fakeDataStructCreate(MeasurementSession_t* fakeMeasurementSession, DeviceConfig_t* fakeDeviceConfig);
uint8_t u8WikaGeneric_get_sensor_cfg_as_char(uss_sensor_cfg_t uss_sens_cfg);
uint8_t* u8WikaGeneric_get_temp_source_string(extSensorId_t ext_sens_id);
uint8_t* u8WikaGeneric_get_energy_type_string(energyMeasureType_t nrg_meas_type, wikaSupportedLang_t lang);
uint8_t* vWikaGeneric_get_analog_in_type_str(uint8_t ext_sens_type, wikaSupportedLang_t lang);
#endif // LVGL_SIMULATOR_ACTIVE



#endif /* COMPONENTS_ULTRAFLOW_MIDDLEWARE_UTILS_WIKA_DATA_TYPES_WIKA_DATA_TYPES_H_ */
