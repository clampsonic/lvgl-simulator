/*
 * led_data.h
 *
 *  Created on: 20 set 2021
 *      Author: BodiniA
 */

#ifndef COMPONENTS_ULTRAFLOW_MIDDLEWARE_UTILS_WIKA_DATA_TYPES_LED_DATA_H_
#define COMPONENTS_ULTRAFLOW_MIDDLEWARE_UTILS_WIKA_DATA_TYPES_LED_DATA_H_

#include <stdint.h>
#include <stdio.h>
#include "TLC59116.h"

typedef enum{
	NORTH_ARCH_WHITE  = NORTH_ARCH_W,
	NORTH_ARCH_RED    = NORTH_ARCH_R,
	SOUTH_ARCH_WHITE  = SOUTH_ARCH_W,
	SOUTH_ARCH_RED    = SOUTH_ARCH_R,
	EAST_ARCH_WHITE   = EAST_ARCH_W,
	EAST_ARCH_RED     = EAST_ARCH_R,
	WEST_ARCH_WHITE   = WEST_ARCH_W,
	WEST_ARCH_RED     = WEST_ARCH_R,
	CENTER_LED_W	  = MAIN_LED,
	LOGO_LED_W		  = WIKA_LOGO,
	LCD_BACK1	  	  = LCD_BACKLIGHT_1,
	LCD_BACK2	  	  = LCD_BACKLIGHT_2,
} arc_type_t;

typedef struct{
	arc_type_t arc;
	uint8_t duty_cycle;
}arc_t;
/*-----------------------------------------*/


#endif /* COMPONENTS_ULTRAFLOW_MIDDLEWARE_UTILS_WIKA_DATA_TYPES_LED_DATA_H_ */
