/*
 * gesture_data.h
 *
 *  Created on: 3 feb 2021
 *      Author: agrippino
 */

#ifndef COMPONENTS_ULTRAFLOW_MIDDLEWARE_UTILS_WIKA_DATA_TYPES_GESTURE_DATA_H_
#define COMPONENTS_ULTRAFLOW_MIDDLEWARE_UTILS_WIKA_DATA_TYPES_GESTURE_DATA_H_

#include <stdint.h>

typedef enum{
	none,
	west2east,
	east2west,
	south2north,
	north2south,
	ClockWise,
	CounterClockWise,
	airwheel,
	touchNorth,
	touchSouth,
	touchEast,
	touchWest,
	touchCenter,
	tapNorth,
	tapSouth,
	tapEast,
	tapWest,
	tapCenter,
	doubleTapNorth,
	doubleTapSouth,
	doubleTapEast,
	doubleTapWest,
	doubleTapCenter,

	GESTURE_LENGTH
}gesture_event_t;

typedef struct{
	gesture_event_t gesture[GESTURE_LENGTH];
	int32_t airwheel_data;
}gesture_data_t;

#endif /* COMPONENTS_ULTRAFLOW_MIDDLEWARE_UTILS_WIKA_DATA_TYPES_GESTURE_DATA_H_ */
