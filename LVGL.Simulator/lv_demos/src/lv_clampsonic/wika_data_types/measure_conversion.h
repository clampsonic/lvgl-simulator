/**************************************************************************************
 *  \file       measure_conversion.h
 *
 *  \brief      Header file for measure units conversion
 *
 *  \details    Use this file for making measure units conversion between values
 *
 *  \author     Agrippino Luca
 *
 *  \version    0.0
 *
 *  \date		7 lug 2021
 *
 *  \copyright  (C) Copyright 2021, Euromisure Sas di WIKA Italia Srl
***************************************************************************************/


#ifndef COMPONENTS_ULTRAFLOW_MIDDLEWARE_MEASURE_LIB_MEASURE_CONVERSION_H_
#define COMPONENTS_ULTRAFLOW_MIDDLEWARE_MEASURE_LIB_MEASURE_CONVERSION_H_

/*
 * Angle conversion constant
 */
#define D2R_CONST            		0.01745329251 //  PI/180 =  0.01745329251
#define R2D_CONST            	   57.2957795131  //  180/PI = 57.2957795131

/*
 * Convert a value from degree to radians
 */
#define DEG_TO_RAD(X) (X*D2R_CONST)

/*
 * Convert a value from radians to degree
 */
#define RAD_TO_DEG(X) (X*R2D_CONST)

/*
 * Convert a value from kilo hertz to hertz
 */
#define KHZ_TO_HZ(X)  (X*1e3)

/*
 * Convert a value from hertz to kilo hertz
 */
#define HZ_TO_KHZ(X)  (X*1e-3)

/*
 * Convert a value from milli seconds to seconds
 */
#define MS_TO_SEC(X)  (X*1e-3)

/*
 * Convert a value from seconds to milli seconds
 */
#define SEC_TO_MS(X)  (X*1e3)

/*
 * Convert a value from micro seconds to seconds
 */
#define US_TO_SEC(X)  (X*1e-6)

/*
 * Convert a value from seconds to micro seconds
 */
#define SEC_TO_US(X)  (X*1e6)

/*
 * Convert a value from nano seconds to seconds
 */
#define NS_TO_SEC(X)  (X*1e-9)
/*
 * Convert a value from seconds to nano seconds
 */
#define SEC_TO_NS(X)  (X*1e9)

/*
 * Convert a value from pico seconds to seconds
 */
#define PS_TO_SEC(X)  (X*1e-12)

/*
 * Convert a value from seconds to pico seconds
 */
#define SEC_TO_PS(X)  (X*1e12)

/*
 * Convert a value from millimeter to meter
 */
#define MM_TO_M(X)  (X*1e-3)

/*
 * Convert a value from meter to millimeter
 */
#define M_TO_MM(X)  (X*1e3)

/*
 * Convert a value from 10^6[m^2/s] to m^2/s
 */
#define TO_M2S(X) (X*1e-6)

/*
 * Convert a value from from m^3/sec to l/h
 */
#define M3S_TO_LH(X) (X*3.6e6)

/*
 * Convert a value from Inches to mm
 */
#define IN_TO_MM(X) (X*25.4)


#endif /* COMPONENTS_ULTRAFLOW_MIDDLEWARE_MEASURE_LIB_MEASURE_CONVERSION_H_ */
