﻿/**************************************************************************************
 *  \file       wikaXml.c
 *  
 *  \brief      Simulator File to Contain Fake XML data
 *  
 *  \details    File Detailed Description
 *  
 *  \author     ***
 *  \author     ***
 *  
 *  \version    0.0
 *  
 *  \date		11 gen 2022
 *  
 *  \copyright  (C) Copyright 2021, Euromisure Sas di WIKA Italia Srl
***************************************************************************************/

/********************************** INCLUDE *******************************************/
#include <string.h>
#include <stdlib.h>
#include "wika_data_types.h"
#ifdef LVGL_SIMULATOR_ACTIVE
#include "..\measure_lib\measure_conversion.h"
#include "..\gui_design\dictionary\wikaGui_labels.h"
#include "..\..\ultraflow-hmi\components\ultraflow\middleware\shared_values_lib\shared_values_json_labels.h"
#endif // !LVGL_SIMULATOR_ACTIVE



/********************************** LOCAL VARIABLES ***********************************/


/********************************** FUNCTION PROTOTYPES *******************************/


/********************************** FUNCTIONS *****************************************/
void fakeDataStructCreate(MeasurementSession_t * fakeMeasurementSession, DeviceConfig_t *fakeDeviceConfig)
{
    // DeviceInfo
    strcpy((char*)fakeDeviceConfig->name, "CSN6A6514");
    strcpy((char*)fakeDeviceConfig->serialNumber, "21CS09US28");
    strcpy((char*)fakeDeviceConfig->mac, "c4:dd:57:6a:65:14");
    strcpy((char*)fakeDeviceConfig->fwVersion, "0.9");
    strcpy((char*)fakeDeviceConfig->system.warrantyStartDate, "30/09/2021");
    fakeDeviceConfig->system.warrantyExpired = 0;
    strcpy((char*)fakeDeviceConfig->sysPassword, "123456WiKa");
    fakeDeviceConfig->validConfigFlag = 1;

    strcpy((char*)fakeDeviceConfig->userData.Company, "Euromisure S.a.S di Wika italia Srl");
    strcpy((char*)fakeDeviceConfig->userData.VAT, "123456789");
    strcpy((char*)fakeDeviceConfig->userData.Address, "1234567890");
    strcpy((char*)fakeDeviceConfig->userData.Email, "plantManagerMail@company.com");
    strcpy((char*)fakeDeviceConfig->userData.PlantID, "123456");

    fakeDeviceConfig->display.Brightness = 255;
    fakeDeviceConfig->display.AutoLock = 0;
    fakeDeviceConfig->display.AutoLockTimeoutInSec = 5000;
    fakeDeviceConfig->system.mainLedBrightness = 60;

    fakeDeviceConfig->wifi.WiFiEnable = 1;
    fakeDeviceConfig->wifi.WiFiMode = 1;
    strcpy((char*)fakeDeviceConfig->wifi.mac, "c4:dd:57:6a:65:15");
    strcpy((char*)fakeDeviceConfig->wifi.SSID, "CSN6A6514");
    strcpy((char*)fakeDeviceConfig->wifi.PSSWD, "12345678");
    fakeDeviceConfig->wifi.IPType = (ipMode_t) 0;
    strcpy((char*)fakeDeviceConfig->wifi.IP, "192.168.100.1");
    strcpy((char*)fakeDeviceConfig->wifi.NetMask, "255.255.255.0");
    strcpy((char*)fakeDeviceConfig->wifi.Gateway, "192.168.100.1");
    strcpy((char*)fakeDeviceConfig->wifi.DNS1, "192.168.100.1");
    strcpy((char*)fakeDeviceConfig->wifi.DNS2, "8.8.8.8");
    strcpy((char*)fakeDeviceConfig->wifi.HostName, "CSN6A6514");
    fakeDeviceConfig->wifi.channel = 1;
    fakeDeviceConfig->wifi.maxStaConn = 2;
    strcpy((char*)fakeDeviceConfig->wifi.wifiRegion, "01");
    fakeDeviceConfig->wifi.wifiSecurity = 4;
    fakeDeviceConfig->wifi.hideSSID = 0;

    fakeDeviceConfig->bluetooth.enable = 0;
    strcpy((char*)fakeDeviceConfig->bluetooth.BtPIN, "123456");
    strcpy((char*)fakeDeviceConfig->bluetooth.lastDeviceName, "");
    strcpy((char*)fakeDeviceConfig->bluetooth.mac, "c4:dd:57:6a:65:16");

    strcpy((char*)fakeDeviceConfig->bluetooth.ServicesList[0].Name, "WiFi Config");
    strcpy((char*)fakeDeviceConfig->bluetooth.ServicesList[0].UUID, "46b664fa-5b64-4f1f-8220-a9b4034506da");
    fakeDeviceConfig->bluetooth.ServicesList[0].byteLength = 255;
    fakeDeviceConfig->bluetooth.ServicesList[0].notifyEnable = 0;
    fakeDeviceConfig->bluetooth.ServicesList[0].serviceType = 0;

    fakeDeviceConfig->rs485.enable = 0;
    fakeDeviceConfig->rs485.speed = BR38400;
    fakeDeviceConfig->rs485.stopBits = ONE;
    fakeDeviceConfig->rs485.parity = NONE_PARITY;
    fakeDeviceConfig->rs485.dataThroughputInMS = 0;

    fakeDeviceConfig->mqtt.enable = 0;
    strcpy((char*)fakeDeviceConfig->mqtt.brokerIP, "");
    strcpy((char*)fakeDeviceConfig->mqtt.brokerDNS, "");
    fakeDeviceConfig->mqtt.port = 0;
    strcpy((char*)fakeDeviceConfig->mqtt.username, "");
    strcpy((char*)fakeDeviceConfig->mqtt.password, "");
    strcpy((char*)fakeDeviceConfig->mqtt.sslPath, "");

    fakeDeviceConfig->modBus.enable = 0;
    strcpy((char*)fakeDeviceConfig->modBus.address, "192.168.8.221");

    fakeDeviceConfig->pulseOut.enable = 0;

    fakeDeviceConfig->system.Language = 0;
    // Attributes 2 and 3 are respectively temperature and RH of the internal sensor
    // there is no need to import them from a file
    fakeDeviceConfig->system.Unit = SYSUNITS_SI;
    fakeDeviceConfig->system.WorkingMode = 0;
    fakeDeviceConfig->ntp.ntpEnable = 0;
    strcpy((char*)fakeDeviceConfig->ntp.ntpIP, "162.159.200.1");
    strcpy((char*)fakeDeviceConfig->ntp.ntpADD, "pool.ntp.org");
    strcpy((char*)fakeDeviceConfig->ntp.timeZone, "CET");
    strcpy((char*)fakeDeviceConfig->system.DateTime, "12/11/2021 08:28:27");

    MeasurementSession_t* session = fakeMeasurementSession;

    session->streams[0].enable = 1;
    session->streams[0].id = 0;

    session->streams[0].configuration.sensor_configuration = USS_SENSOR_CFG_I;
    session->streams[0].configuration.enableReynold = 1;

    session->streams[1].enable = 0;
    session->streams[1].id = 1;

    session->streams[1].configuration.sensor_configuration = USS_SENSOR_CFG_V;
    session->streams[1].configuration.enableReynold = 0;

    uint8_t streamNum = 0;

    for (streamNum = 0; streamNum < 2; streamNum++) {

        /*
        * Flow="1"
        * Volume="0"
        * MassFlow="0"
        * Mass="0"
        * Energy="0"
        */
        session->streams[streamNum].configuration.userMeasure[USER_MEASURE_FLOW] = 1;
        session->streams[streamNum].configuration.userMeasure[USER_MEASURE_MASS] = 0;
        session->streams[streamNum].configuration.userMeasure[USER_MEASURE_MASSFLOW] = 0;
        session->streams[streamNum].configuration.userMeasure[USER_MEASURE_VOLUME] = 0;
        session->streams[streamNum].configuration.userMeasure[USER_MEASURE_ENERGY] = 0;
        session->streams[streamNum].configuration.userMeasure[USER_MEASURE_DENSITY] = 0;
        session->streams[streamNum].configuration.userMeasure[USER_MEASURE_TOTFLOW] = 0;
        session->streams[streamNum].configuration.userMeasure[USER_MEASURE_TOTENERGY] = 0;
        session->streams[streamNum].configuration.userMeasure[USER_MEASURE_TOTMASS] = 0;

        strcpy((char*) session->streams[streamNum].configuration.setPointUnit, "m/s");

        session->streams[streamNum].configuration.setPointScaling = 1;
        session->streams[streamNum].configuration.setPointCutoff = 0.0025;
        session->streams[streamNum].configuration.setPointWarningThreshold = 1.0;
        session->streams[streamNum].configuration.setPointFullScale = 1.1;

        uint8_t channelNum = 0;

        for (channelNum = 0; channelNum < CHANNEL_NUMBER; channelNum++) {

            session->streams[streamNum].parameters.daq.channels[channelNum].ussSensor.Type = 1;
            session->streams[streamNum].parameters.daq.channels[channelNum].ussSensor.Frequency = 1000.00;
            session->streams[streamNum].parameters.daq.channels[channelNum].ussSensor.AngleInRad = DEG_TO_RAD(43.0);
            session->streams[streamNum].parameters.daq.channels[channelNum].ussSensor.WedgeSoundSpeed = 2460.00;
            session->streams[streamNum].parameters.daq.channels[channelNum].ussSensor.WedgeSsCorrectionFactor = -2100.00;
            session->streams[streamNum].parameters.daq.channels[channelNum].ussSensor.PropagationTimeInSec = US_TO_SEC(0.01);
            session->streams[streamNum].parameters.daq.channels[channelNum].ussSensor.ReferenceTemperature = 25.00;
            session->streams[streamNum].parameters.daq.channels[channelNum].ussSensor.TemperatureSource = 0;
            session->streams[streamNum].parameters.daq.channels[channelNum].ussSensor.WorkingTemperature = 0.00;
            session->streams[streamNum].parameters.daq.channels[channelNum].ussSensor.CircuitPropTimeInSec = US_TO_SEC(0.00);
            session->streams[streamNum].parameters.daq.channels[channelNum].ussSensor.minTemperature = 0.00;
            session->streams[streamNum].parameters.daq.channels[channelNum].ussSensor.maxTemperature = 100.00;            
        }

        for (channelNum = 0; channelNum < ANALOG_INPUTS_NUMBER_PER_CHANNEL; channelNum++) {
            session->streams[streamNum].parameters.analogInput[channelNum].Enable = 1;
            session->streams[streamNum].parameters.analogInput[channelNum].Type = 1;
            session->streams[streamNum].parameters.analogInput[channelNum].EndPoint4 = 0.00;
            session->streams[streamNum].parameters.analogInput[channelNum].EndPoint20 = 50.00;
            session->streams[streamNum].parameters.analogInput[channelNum].OutOfRangeAlarm = 0;
            session->streams[streamNum].parameters.analogInput[channelNum].Value = 0;
            strcpy((char*)session->streams[streamNum].parameters.analogInput[channelNum].Unit, "°C");
        }

        // DAQ BOARD
        session->streams[streamNum].parameters.daq.daqConfig.F1 = 1320;
        session->streams[streamNum].parameters.daq.daqConfig.PatternOption = 0;
        session->streams[streamNum].parameters.daq.daqConfig.GapPulseStartAdcCapture = 240;
        session->streams[streamNum].parameters.daq.daqConfig.NumberOfPulses = 14;
        session->streams[streamNum].parameters.daq.daqConfig.UpsAndDnsGap = 1000;
        session->streams[streamNum].parameters.daq.daqConfig.Ups0AndUps1Gap = 25;
        session->streams[streamNum].parameters.daq.daqConfig.GainControl = 21;
        session->streams[streamNum].parameters.daq.daqConfig.MeterConstant = 12742000.0;
        session->streams[streamNum].parameters.daq.daqConfig.UssXtFrequency = 8000;
        session->streams[streamNum].parameters.daq.daqConfig.AdcSamplingFrequency = 200;
        session->streams[streamNum].parameters.daq.daqConfig.SignalSamplingFrequency = 4000;
        session->streams[streamNum].parameters.daq.daqConfig.AdcOverSamplingRate = 20;
        session->streams[streamNum].parameters.daq.daqConfig.DeltaTofOffset = 0;
        session->streams[streamNum].parameters.daq.daqConfig.AbsTofAdditionalDelay = 0;
        session->streams[streamNum].parameters.daq.daqConfig.CaptureDuration = 60;
        session->streams[streamNum].parameters.daq.daqConfig.AlgorithmOption = 0;
        session->streams[streamNum].parameters.daq.daqConfig.UlpBiasDelay = 3;
        session->streams[streamNum].parameters.daq.daqConfig.StartPpgCount = 10000;
        session->streams[streamNum].parameters.daq.daqConfig.TurnOnAdcCount = 5000;
        session->streams[streamNum].parameters.daq.daqConfig.StartPgaAndInBiasCount = 0;
        session->streams[streamNum].parameters.daq.daqConfig.UserParam6 = 0;
        session->streams[streamNum].parameters.daq.daqConfig.UserParam7_EnvCrossThresh = 120;
        session->streams[streamNum].parameters.daq.daqConfig.UserParam8_EndpointT4mA = 30;
        session->streams[streamNum].parameters.daq.daqConfig.UserParam9_EndpointT20mA = 3;
        session->streams[streamNum].parameters.daq.daqConfig.UserParam10_EstimatedTemp = 0;

        // EXTERNAL ENVIRONMENT
        session->streams[streamNum].parameters.externalEnvironment.pipe.id = 99;
        session->streams[streamNum].parameters.externalEnvironment.pipe.materialId = 2;
        strcpy((char*)session->streams[streamNum].parameters.externalEnvironment.pipe.Material, "304L Stainless Steel");
        session->streams[streamNum].parameters.externalEnvironment.pipe.sizeId = 0;
        session->streams[streamNum].parameters.externalEnvironment.pipe.DiameterInMt = MM_TO_M(168.3);
        session->streams[streamNum].parameters.externalEnvironment.pipe.ThicknessInMt = MM_TO_M(2.0);
        session->streams[streamNum].parameters.externalEnvironment.pipe.SoundSpeed = 3070;

        session->streams[streamNum].parameters.externalEnvironment.lining.Material = 0;
        session->streams[streamNum].parameters.externalEnvironment.lining.ThicknessInMt = MM_TO_M(0.00);
        session->streams[streamNum].parameters.externalEnvironment.lining.SoundSpeed = 0;

        session->streams[streamNum].parameters.externalEnvironment.fluid.id = 27;
        strcpy((char*)session->streams[streamNum].parameters.externalEnvironment.fluid.Name, "Water");
        session->streams[streamNum].parameters.externalEnvironment.fluid.WorkingTemperature = 24.00;
        session->streams[streamNum].parameters.externalEnvironment.fluid.SoundSpeed = 1493;
        session->streams[streamNum].parameters.externalEnvironment.fluid.ssTempCoeff = 0.00;
        session->streams[streamNum].parameters.externalEnvironment.fluid.AbsoluteDensity = 997.15;
        session->streams[streamNum].parameters.externalEnvironment.fluid.KinematicViscosity = 0.9228;
        session->streams[streamNum].parameters.externalEnvironment.fluid.dynamicViscosity = 0;
        session->streams[streamNum].parameters.externalEnvironment.fluid.Enthalpy = 100.6;
        session->streams[streamNum].parameters.externalEnvironment.fluid.specificGravity = 0.00;
    }
}
//----------------------------------------------------------------------------------------------------------
uint8_t u8WikaGeneric_get_sensor_cfg_as_char(uss_sensor_cfg_t uss_sens_cfg)
{
    uint8_t ret = '0';

    switch (uss_sens_cfg) {
    case USS_SENSOR_CFG_I:
        ret = 'I';
        break;
    case USS_SENSOR_CFG_V:
        ret = 'V';
        break;
    case USS_SENSOR_CFG_W:
        ret = 'W';
        break;
    case USS_SENSOR_CFG_Z:
        ret = 'Z';
        break;
    default:
        ret = '?';
        break;
    }
    return ret;
}
//----------------------------------------------------------------------------------------------------------
uint8_t* u8WikaGeneric_get_temp_source_string(extSensorId_t ext_sens_id)
{
    uint8_t* str_ret = NULL;
    switch (ext_sens_id) {
    case EXTSENS_AIN1_MAIN1:
        str_ret = u8WikaGUI_getLabel(LABELS_GENERIC_TEMP_SRC_AIN1, DEFAULT_LANGUAGE);
        break;
    case EXTSENS_AIN2_AUX1:
        str_ret = u8WikaGUI_getLabel(LABELS_GENERIC_TEMP_SRC_AIN2, DEFAULT_LANGUAGE);
        break;
    case EXTSENS_AIN3_MAIN2:
        str_ret = u8WikaGUI_getLabel(LABELS_GENERIC_TEMP_SRC_AIN3, DEFAULT_LANGUAGE);
        break;
    case EXTSENS_AIN4_AUX2:
        str_ret = u8WikaGUI_getLabel(LABELS_GENERIC_TEMP_SRC_AIN4, DEFAULT_LANGUAGE);
        break;
    case EXTSENS_USERDEFINED:
        str_ret = u8WikaGUI_getLabel(LABELS_GENERIC_TEMP_SRC_USR, DEFAULT_LANGUAGE);
        break;
    default:
        break;
    }

    return str_ret;
}

uint8_t* u8WikaGeneric_get_energy_type_string(energyMeasureType_t nrg_meas_type, wikaSupportedLang_t lang)
{
    uint8_t* str_ret = NULL;
    switch (nrg_meas_type) {
    case ENERGY_MEAS_TYPE_DISABLED:
        str_ret = u8WikaGUI_getLabel(LABELS_GENERIC_DISABLED, lang);
        break;
    case ENERGY_MEAS_TYPE_COOLER:
        str_ret = u8WikaGUI_getLabel(LABELS_ENERGY_COOLER, lang);
        break;
    case ENERGY_MEAS_TYPE_HEATER:
        str_ret = u8WikaGUI_getLabel(LABELS_ENERGY_HEATER, lang);
        break;

    default:
        break;
    }

    return str_ret;
}

void vWikaGeneric_get_active_meters(uint8_t* user_active_meters, uint8_t* str_array, size_t str_len, wikaSupportedLang_t lang)
{
    // Clear the array in which the strings will be stored
    memset(str_array, 0, str_len * sizeof(uint8_t));
    size_t curr_str_len = 0;

    if (*(user_active_meters + USER_MEASURE_FLOW) == true) {
        if ((curr_str_len + strlen((char*)u8WikaGUI_getLabel(LABELS_MEASUREMENT_FLOW, lang)) + 1) > str_len) {
            strncpy((char*)str_array, "Overflow\n", str_len);
        }
        else {
            strcat((char*)str_array, (char*)u8WikaGUI_getLabel(LABELS_MEASUREMENT_FLOW, lang));
            strcat((char*)str_array, "\n");
            curr_str_len = strlen((char*)str_array);
        }
    }

    if (*(user_active_meters + USER_MEASURE_VOLUME) == true) {
        if ((curr_str_len + strlen((char*)u8WikaGUI_getLabel(LABELS_MEASUREMENT_VOLUME, lang)) + 1) > str_len) {
            strncpy((char*)str_array, "Overflow\n", str_len);
        }
        else {
            strcat((char*)str_array, (char*)u8WikaGUI_getLabel(LABELS_MEASUREMENT_VOLUME, lang));
            strcat((char*)str_array, "\n");
            curr_str_len = strlen((char*)str_array);
        }
    }

    if (*(user_active_meters + USER_MEASURE_MASSFLOW) == true) {
        if ((curr_str_len + strlen((char*)u8WikaGUI_getLabel(LABELS_MEASUREMENT_MASSFLOW, lang)) + 1) > str_len) {
            strncpy((char*)str_array, "Overflow\n", str_len);
        }
        else {
            strcat((char*)str_array, (char*)u8WikaGUI_getLabel(LABELS_MEASUREMENT_MASSFLOW, lang));
            strcat((char*)str_array, "\n");
            curr_str_len = strlen((char*)str_array);
        }
    }

    if (*(user_active_meters + USER_MEASURE_MASS) == true) {
        if ((curr_str_len + strlen((char*)u8WikaGUI_getLabel(LABELS_MEASUREMENT_MASS, lang)) + 1) > str_len) {
            strncpy((char*)str_array, "Overflow\n", str_len);
        }
        else {
            strcat((char*)str_array, (char*)u8WikaGUI_getLabel(LABELS_MEASUREMENT_MASS, lang));
            strcat((char*)str_array, "\n");
            curr_str_len = strlen((char*)str_array);
        }
    }

    if (*(user_active_meters + USER_MEASURE_ENERGY) == true) {
        if ((curr_str_len + strlen((char*)u8WikaGUI_getLabel(LABELS_ENERGY, lang)) + 1) > str_len) {
            strncpy((char*)str_array, "Overflow\n", str_len);
        }
        else {
            strcat((char*)str_array, (char*)u8WikaGUI_getLabel(LABELS_ENERGY, lang));
            strcat((char*)str_array, "\n");
            curr_str_len = strlen((char*)str_array);
        }
    }

    if (*(user_active_meters + USER_MEASURE_TOTFLOW) == true) {
        if ((curr_str_len + strlen((char*)u8WikaGUI_getLabel(LABELS_MEASUREMENT_TOTALIZATION, lang)) + 1) > str_len) {
            strncpy((char*)str_array, "Overflow\n", str_len);
        }
        else {
            strcat((char*)str_array, (char*)u8WikaGUI_getLabel(LABELS_MEASUREMENT_TOTALIZATION, lang));
            strcat((char*)str_array, "\n");
            curr_str_len = strlen((char*)str_array);
        }
    }

    *(str_array + --curr_str_len) = "\0";

    // TODO: add density to user measurement
  //  if(*(user_active_meters + Density) == true) {
  //    if((curr_str_len + strlen((char *) u8WikaGUI_getLabel(LABELS_MEASUREMENT_DENSITY, lang)) +1 ) > str_len){
  //      strncpy((char *) str_array, "Overflow", str_len);
  //    }
  //    else {
  //      strcat((char *) str_array, (char *) u8WikaGUI_getLabel(LABELS_MEASUREMENT_DENSITY, lang));
  //      curr_str_len = strlen((char *) str_array);
  //    }
  //  }

}

uint8_t* vWikaGeneric_get_analog_in_type_str(uint8_t ext_sens_type, wikaSupportedLang_t lang)
{
    if (ext_sens_type == SHRLBL_VALUE_ANALOGIN_TYPE_PRESSURE) {
        return u8WikaGUI_getLabel(LABELS_GENERIC_PRESSURE, lang);
    }
    if (ext_sens_type == SHRLBL_VALUE_ANALOGIN_TYPE_TEMPERATURE) {
        return u8WikaGUI_getLabel(LABELS_GENERIC_TEMPERATURE, lang);
    }
    if (ext_sens_type == SHRLBL_VALUE_ANALOGIN_TYPE_PH) {
        return u8WikaGUI_getLabel(LABELS_GENERIC_PH, lang);
    }
    if (ext_sens_type == SHRLBL_VALUE_CUSTOM_ELEMENT_ID) {
        return u8WikaGUI_getLabel(LABELS_GENERIC_CUSTOM, lang);
    }
}

/********************************* EOF ************************************************/
