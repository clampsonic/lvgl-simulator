﻿/**************************************************************************************
 *  \file       wikaConfigManager.h
 *
 *  \brief      This module manage both the device and measurement configuration.
 *
 *  \details    This module contains the functions for manage the structures
 *  			of device configuration and measurement configuration.
 *				Use this module to obtains the device structures and measurement structures,
 *				and for load and save those structures from xml files.
 *
 *  \author     Agrippino Luca
 *
 *  \version    0.0
 *
 *  \date		6 lug 2021
 *
 *  \copyright  (C) Copyright 2021, Euromisure Sas di WIKA Italia Srl
***************************************************************************************/

#ifndef COMPONENTS_ULTRAFLOW_APPLICATION_CONFIG_MANAGER_WIKACONFIGMANAGER_H_
#define COMPONENTS_ULTRAFLOW_APPLICATION_CONFIG_MANAGER_WIKACONFIGMANAGER_H_

#include "../error/errorManager.h"
#include "../wika_data_types/wika_data_types.h"

#define DEVICENAME_BASE_STRING	"CSN"

/************* XML ATTRIBUTES IDS *************/
#define FLUID_TAB_WATER_ID			27
#define CUSTOM_ELEMENT_ID			99

#define CARBON_STEEL_PIPE_ID		0
#define FIRST_NON_STEEL_MATERIAL_ID	5

/*********************************************
 * \enum 	tableSelect_t
 * 			Value used to chose between the
 * 			different data tables
 *********************************************/
typedef enum _FLUID_TABLE_SELECT_ {
    TableSelect_WaterTable = 0x00,
    TableSelect_OtherFluidTable = 0x01,
    TableSelect_PipeSchedule = 0x02,
    TableSelect_SensorList = 0x03,
    TableSelect_SolidSoundSpd = 0x04,
} tableSelect_t;

/***************************************************************************************
 * 	\author		BodiniA
 *
 *	\brief		This function checks if the device is in its first startup.
 *
 *	\param[in]	none
 *
 *	\return		none
 ***************************************************************************************/
systemErrors_t tWikaConfigManager_checkFirstConfig(void);

/***************************************************************************************
 * 	\author		agrippino
 *
 *	\brief		Wrapper function for loading a measurement session XML file
 *
 *	\param[in]	filename: the name of the XML file
 *
 *	\return		The error code \see systemErrors_t
 ***************************************************************************************/
systemErrors_t tWikaConfigManager_loadMeasSession(char* filename);

/***************************************************************************************
 * 	\author		agrippino
 *
 *	\brief		Wrapper function for loading a device configuration XML file
 *
 *	\param[in]	filename: the name of the XML file
 *
 *	\return		The error code \see systemErrors_t
 ***************************************************************************************/
systemErrors_t tWikaConfigManager_loadDeviceConfig(char* filename);

/***************************************************************************************
 * 	\author		Andrea Bodini
 *
 *	\brief		Wrapper function for save a device configuration XML file
 *
 *	\param[in]	filename: the name of the XML file
 *
 *	\return		The error code \see systemErrors_t
 ***************************************************************************************/
systemErrors_t tWikaConfigManager_saveDeviceConfig(char* filename);

/***************************************************************************************
 * 	\author		agrippino
 *
 *	\brief		Add an installation to the measurements session
 *
 *	\param[in]	installation: the new installation
 *
 *	\return		The error code \see systemErrors_t
 ***************************************************************************************/
systemErrors_t tWikaConfigManager_addInstToSession(Stream_t* installation);

/***************************************************************************************
 * 	\author		agrippino
 *
 *	\brief		Remove an installation from the measurements session
 *
 *	\param[in]	installation: the installation to remove
 *
 *	\return		The error code \see systemErrors_t
 ***************************************************************************************/
systemErrors_t tWikaConfigManager_removeInstFromSession(Stream_t* installation);

/***************************************************************************************
 * 	\author		agrippino
 *
 *	\brief		Initialize the measurement session
 *
 *	\return		The error code \see systemErrors_t
 ***************************************************************************************/
systemErrors_t tWikaConfigManager_initMeasurementSession(void);

/***************************************************************************************
 * 	\author		agrippino
 *
 *	\brief		Return the current installation
 *
 *	\return		the current installation
 ***************************************************************************************/
Stream_t* tWikaConfigManager_getCurrentInstallation(void);

/***************************************************************************************
 * 	\author		agrippino
 *
 *	\brief		Get the device configuration structure
 *
 *	\return		the device configuration structure
 ***************************************************************************************/
DeviceConfig_t* tWikaConfigManager_getDeviceConfig(void);

/***************************************************************************************
 * 	\author		agrippino
 *
 *	\brief		Get the measurement session structure
 *
 *	\return		the measurement session structure
 ***************************************************************************************/
MeasurementSession_t* tWikaConfigManager_getMeasurementSession(void);

#endif /* COMPONENTS_ULTRAFLOW_APPLICATION_CONFIG_MANAGER_WIKACONFIGMANAGER_H_ */
