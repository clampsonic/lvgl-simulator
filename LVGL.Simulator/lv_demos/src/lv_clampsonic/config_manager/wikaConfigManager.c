﻿/**************************************************************************************
 *  \file       wikaConfigManager.c
 *
 *  \brief      Device Configuration Manager
 *
 *  \details    This section acts on the actual session configuration files and
 *  			variables.
 *
 *  \author     Agrippino Luca
 *  \author     Bodini Andrea
 *
 *  \version    1.0
 *
 *  \date		5 ott 2021
 *
 *  \copyright  (C) Copyright 2021, Euromisure Sas di WIKA Italia Srl
***************************************************************************************/


//--------------------------------
#include <string.h>
#include <stdlib.h>
//--------------------------------
#ifndef LVGL_SIMULATOR_ACTIVE
#include "esp_log.h"
#include "esp_system.h"
//--------------------------------
#include "wikaOs.h"
#include "wikaConfigManager.h"
#include "wikaHal.h"
#include "measure_utils.h"
#include "measure_conversion.h"
#include "wikaXml.h"
//--------------------------------
#else
#include "wikaConfigManager.h"
#include "..\measure_lib\measure_conversion.h"
#include "..\measure_lib\measure_utils.h"

#endif // !LVGL_SIMULATOR_ACTIVE


/********************************** LOCAL DEFINES ***********************************/
#define TAG_CONFIG_MANAGER	"CFG_MNGR"


/************************************************************************************
 * \enum	configFile_t
 * \brief	It describes the file type, removing the necessity of compare strings.
 ************************************************************************************/
typedef enum _CONFIG_FILE_ {
	configFile_none		= 0x00,
	configFile_factory	= 0x01,
	configFile_user		= 0x02,
} configFile_t;

/********************************** LOCAL VARIABLES ***********************************/
static MeasurementSession_t mSession;
static DeviceConfig_t deviceConfig;


/********************************** FUNCTION PROTOTYPES *******************************/
static systemErrors_t tWikaConfigManager_initExtEnvironment(ExternalEnvironment_t* extEnv);
static systemErrors_t tWikaConfigManager_initChannels(Daq_t* daq);
static systemErrors_t tWikaConfigManager_initAuxiliaryMeasure(Stream_t* installation);

void vWikaConfigManager_createDevName(uint8_t * mac, uint8_t * devName);

/********************************** FUNCTIONS *****************************************/

/***************************************************************************************
 * 	\author		BodiniA
 *
 *	\brief		This function creates the device Name from the MAC address
 *
 *	\param[in]	mac		-	the MAC address chosen for the naming, in uint8_t format.
 *	\param[out]	devName	-	the string containing the device Name
 *
 *	\return		none
 ***************************************************************************************/
void vWikaConfigManager_createDevName(uint8_t * mac, uint8_t * devName)
{
	uint8_t macLen = 6;
	uint8_t tempString[8];

	memset(&tempString, 0, sizeof(tempString));
	memset(devName, 0, NAME_LENGTH);

	strcpy((char *) devName,(char *) DEVICENAME_BASE_STRING);
	// The Devicename is now Clampsonic
	// We append the last 6 chars of the MAC address.
	sprintf((char *) tempString, "%X%X%X", *(mac+(macLen-3)),*(mac+(macLen-2)),*(mac+(macLen-1)));

	strcat((char *)devName,(char *) tempString);
}

/*
systemErrors_t tWikaConfigManager_checkFirstConfig(void)
{
	systemErrors_t err = WIKA_NO_ERROR;

	configFile_t devFile = configFile_none;

	// FUSE3 MAC
	uint8_t stdMacAddress[6];
	uint8_t stdStrMacAddress[MAC_ADD_LEN];

	// Wifi
	uint8_t wifiMacAddress[6];
	uint8_t wifiStrMacAddress[MAC_ADD_LEN];

	// Bluetooth
	uint8_t btMacAddress[6];
	uint8_t btStrMacAddress[MAC_ADD_LEN];

	// STD
	memset(&stdMacAddress, 0, sizeof(stdMacAddress));

	// WiFi
	memset(&wifiMacAddress, 0, sizeof(wifiMacAddress));

	// Bluetooth
	memset(&btMacAddress, 0, sizeof(btMacAddress));

	err = tWikaHal_checkFileExistence(USER_CONFIG_FULLPATH);
	if( err == WIKA_FILE_NOT_EXIST )
	{
		// The userConfig file does not exist.
		// First we load the factoryConfig file.
		err = tWikaConfigManager_loadDeviceConfig(FACTORY_CONFIG_FULLPATH);
		check(err,"Cannot Load Factory Config File");
		devFile = configFile_factory;
	}
	else
	{
		// The userConfig exists.
		err = tWikaConfigManager_loadDeviceConfig(USER_CONFIG_FULLPATH);
		check(err,"Cannot Load User Config File");
		devFile = configFile_user;
	}

	// Check the MAC address written in the file, if it is different
	// from the one stored in the local device's fuses, we have two possibilities:
	//	1)	the factoryConfig is the default one, stored in production.
	// 	2)	the device configuration is loaded from the webserver

	err = esp_efuse_mac_get_default(&stdMacAddress[0]);

	// there are two different MACs, for SoftAP and Station
	if( deviceConfig.wifi.WiFiMode == WifiMode_SoftAP )
	{
		err = esp_read_mac(&wifiMacAddress[0], ESP_MAC_WIFI_SOFTAP);
	}
	else
	{
		err = esp_read_mac(&wifiMacAddress[0], ESP_MAC_WIFI_STA);
	}

	// Read Bluetooth MAC Address
	err = esp_read_mac(&btMacAddress[0], ESP_MAC_BT);

	// Copy the given addresses as string into the variables
	sprintf((char *) &stdStrMacAddress[0], MACSTR, MAC2STR(&stdMacAddress[0]));
	sprintf((char *) &wifiStrMacAddress[0], MACSTR, MAC2STR(&wifiMacAddress[0]));
	sprintf((char *) &btStrMacAddress[0], MACSTR, MAC2STR(&btMacAddress[0]));

	// The hard coded MAIN MAC address must be the same as in the XML file.
	if(	strcmp((char *) deviceConfig.mac, 	 (char *) stdStrMacAddress	) != 0 )
	{
		// The MAC saved in the file is different than device's
		ESP_LOGW(TAG_CONFIG_MANAGER, "Found a different MAC address!");

		// Copy the device's MAC in the structure.
		strcpy((char *) deviceConfig.mac,	 		(char *) stdStrMacAddress);
		strcpy((char *) deviceConfig.wifi.mac, 		(char *) wifiStrMacAddress);
		strcpy((char *) deviceConfig.bluetooth.mac, (char *) btStrMacAddress);

		// Create the device name
		vWikaConfigManager_createDevName(&stdMacAddress[0], deviceConfig.name);

		// Copy the device name as the Hostname
		strcpy((char *) deviceConfig.wifi.HostName, (char *) deviceConfig.name);

		// If the WiFi is set as SoftAP, then change the SSID to the factory settings.
		if(deviceConfig.wifi.WiFiMode == WifiMode_SoftAP)
		{
			// Copy the device name as the device-linked Wifi name
			strcpy((char *) deviceConfig.wifi.SSID, (char *) deviceConfig.name);
		}

		switch(devFile){

			case configFile_user:
			{
				// Read the measurement section of the XML user file.
				err = tWikaConfigManager_loadMeasSession(USER_CONFIG_FULLPATH);

				// We tell to the SystemSM to save the current configuration as the "userParameters.xml"
				err = WIKA_SAVE_USERFILE;
				break;
			}

			case configFile_factory:{
				// Copy the device name as the device-linked Wifi name
				strcpy((char *) deviceConfig.wifi.SSID, (char *) deviceConfig.name);

				// Then, load the measurement config
				err = tWikaConfigManager_loadMeasSession(FACTORY_CONFIG_FULLPATH);

				// We tell to the SystemSM to save the current configuration as the "factoryParameters.xml"
				err = WIKA_SAVE_FACTORYFILE;
				break;
			}

			case configFile_none:
			default:{
				ESP_LOGE(TAG_CONFIG_MANAGER, "Wrong switch-case status!!");
				err = WIKA_GENERIC_ERROR;
				break;
			}
		}
	}
	else
	{
		switch(devFile){
			case configFile_user:{
				err = WIKA_NO_ERROR;
				break;
			}

			case configFile_factory:{
				err = tWikaConfigManager_loadMeasSession(FACTORY_CONFIG_FULLPATH);
				err = WIKA_SAVE_USERFILE;
				break;
			}
			case configFile_none:
			default:{
				ESP_LOGE(TAG_CONFIG_MANAGER, "Wrong switch-case status!!");
				err = WIKA_GENERIC_ERROR;
				break;
			}
		}
	}

	return err;

}
*/
/*
systemErrors_t tWikaConfigManager_loadMeasSession(char* filename)
{
	systemErrors_t err = WIKA_NO_ERROR;

	memset(&mSession,0,sizeof(mSession));

	if(pdTRUE == tWikaOs_takeDataAccessMutex())
	{
		// load xml file
		err = tWikaXml_loadConfiguration(filename, ConfigSelect_MeasConfig, &mSession);
		vWikaOs_giveDataAccessMutex();
	}

	return err;
}
*/
/*
systemErrors_t tWikaConfigManager_loadDeviceConfig(char* filename)
{
	systemErrors_t err = WIKA_NO_ERROR;

	memset(&deviceConfig,0,sizeof(deviceConfig));

	if(pdTRUE == tWikaOs_takeDataAccessMutex())
	{
		err = tWikaXml_loadConfiguration(filename, ConfigSelect_DeviceConf, &deviceConfig);
		vWikaOs_giveDataAccessMutex();
	}

	return err;
}


systemErrors_t tWikaConfigManager_saveDeviceConfig(char* filename)
{
	systemErrors_t err = WIKA_NO_ERROR;

	if(pdTRUE == tWikaOs_takeDataAccessMutex())
	{
		tWikaXml_saveDeviceConfiguration(filename, &deviceConfig, &mSession);
		vWikaOs_giveDataAccessMutex();
	}

	return err;
}

systemErrors_t tWikaConfigManager_addInstToSession(Stream_t* installation)
{
	systemErrors_t err = WIKA_NO_ERROR;

	// check if there is a free space to add installation
	// add installation

	return err;
}

systemErrors_t tWikaConfigManager_removeInstFromSession(Stream_t* installation)
{
	systemErrors_t err = WIKA_NO_ERROR;

	uint8_t inst;
	for(inst=0;inst<STREAM_NUMBER;inst++)
	{
		// check if installation exist in this session
		if(mSession.streams[inst].id == installation->id)
		{
			// remove (clear) installation
		}
	}

	return err;
}
*/
systemErrors_t tWikaConfigManager_initMeasurementSession(void)
{
	systemErrors_t err = WIKA_NO_ERROR;

	if(0)
	{
		uint8_t inst;
		for(inst=0;inst<STREAM_NUMBER;inst++)
		{
			if( mSession.streams[inst].enable == 1 )
			{
				mSession.streams[inst].id = inst;

				err = tWikaConfigManager_initExtEnvironment(&mSession.streams[inst].parameters.externalEnvironment);

				err = tWikaConfigManager_initChannels(&mSession.streams[inst].parameters.daq);

				err = tMeasUtils_preMeasurementCalculation(&mSession.streams[inst]);

				err = tWikaConfigManager_initAuxiliaryMeasure(&mSession.streams[inst]);
			}
			else
			{
				;// installation not enabled
			}
			//vWikaOs_giveDataAccessMutex();
		}
	}

    fakeDataStructCreate(&mSession, &deviceConfig);

    tMeasUtils_preMeasurementCalculation(&mSession.streams[0]);
    tMeasUtils_preMeasurementCalculation(&mSession.streams[1]);

	return err;
}

Stream_t* tWikaConfigManager_getCurrentInstallation(void)
{
	return &(mSession.streams[mSession.activeStream]);
}

static systemErrors_t tWikaConfigManager_initExtEnvironment(ExternalEnvironment_t* extEnv)
{
	systemErrors_t err = WIKA_NO_ERROR;

	tableSelect_t tableToRead;

	if(extEnv->fluid.id == FLUID_TAB_WATER_ID)
	{
		tableToRead = TableSelect_WaterTable;

		//err = tWikaXml_loadProperties(tableToRead, &extEnv->fluid.WorkingTemperature, &extEnv->fluid);
		//check(err, "Cannot Load Water XML Parameters!");

	}
	else if(extEnv->fluid.id != CUSTOM_ELEMENT_ID)
	{
		tableToRead = TableSelect_OtherFluidTable;

		//err = tWikaXml_loadProperties(tableToRead, &extEnv->fluid.id, &extEnv->fluid);
		//check(err, "Cannot Load Other Fluid XML Parameters!");
	}

	// Load Pipe Material sound speed
	if(extEnv->pipe.materialId != CUSTOM_ELEMENT_ID)
	{
		//err = tWikaXml_loadProperties(TableSelect_SolidSoundSpd, &extEnv->pipe.materialId, &extEnv->pipe);
		//check(err, "Cannot Load Pipe Material XML Parameters!");
	}

	// Load Pipe dimensions
	if(extEnv->pipe.id != CUSTOM_ELEMENT_ID)
	{
		//err = tWikaXml_loadProperties(TableSelect_PipeSchedule, &extEnv->pipe.id, &extEnv->pipe);
		//check(err, "Cannot Load Pipe Schedule XML Parameters!");
	}

	// add table info for lining to mSession
	// get lining name and use this to find the sound speed in the lining table
	// when the sound speed is available, copy this value into the lining struct
	extEnv->lining.SoundSpeed = strtol( "0", NULL, 10); // uint16_t

	return err;
}

static systemErrors_t tWikaConfigManager_initChannels(Daq_t* daq)
{
	systemErrors_t err = WIKA_NO_ERROR;

	uint8_t chNum = 0;

	for(chNum = 0; chNum < CHANNEL_NUMBER; chNum++)
	{
		if(daq->channels[chNum].ussSensor.Type != CUSTOM_ELEMENT_ID)
		{

			//err = tWikaXml_loadProperties(TableSelect_SensorList, &daq->channels[chNum].ussSensor.Type, &daq->channels[chNum].ussSensor);

			if(err != WIKA_NO_ERROR)
			{
				//ESP_LOGE("ConfigMngr","Cannot Load Sensors Ch:%d XML Parameters!",chNum);
			}
			//check(err, "Cannot Load Sensors XML Parameters!");
		}
	}

	return err;
}

static systemErrors_t tWikaConfigManager_initAuxiliaryMeasure(Stream_t* installation)
{
	systemErrors_t err = WIKA_NO_ERROR;

	return err;
}


DeviceConfig_t* tWikaConfigManager_getDeviceConfig(void)
{
	return &deviceConfig;
}

MeasurementSession_t* tWikaConfigManager_getMeasurementSession(void)
{
	return &mSession;
}
