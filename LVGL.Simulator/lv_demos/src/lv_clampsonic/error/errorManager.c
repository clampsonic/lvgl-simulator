﻿/*
 * errorManager.c
 *
 *  Created on: 12 nov 2020
 *      Author: agrippino
 */

#ifndef LVGL_SIMULATOR_ACTIVE
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_log.h"
#include "errorManager.h"

static const char *ERROR_TAG = "WIKA Error!";


void
check(systemErrors_t err, const char* errorMessage){

	if( err != WIKA_NO_ERROR ){
		ESP_LOGE(ERROR_TAG, " >> %s",errorMessage);
//		while(1){
//
//			vTaskDelay(1 / portTICK_PERIOD_MS);
//		}
	}
	else
		return;
}
#endif // !LVGL_SIMULATOR_ACTIVE
