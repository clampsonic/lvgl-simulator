﻿/**************************************************************************************
 *  \file       wikaGraphic.h
 *
 *  \brief      Wrapper module for the GUI
 *
 *  \details    This module wrap the code that create and manage the GUI
 *
 *  \author     Luca Agrippino
 *
 *  \version    0.0
 *
 *  \date		2 ago 2021
 *
 *  \copyright  (C) Copyright 2021, Euromisure Sas di WIKA Italia Srl
***************************************************************************************/

#ifndef COMPONENTS_ULTRAFLOW_APPLICATION_GUI_WIKAGRAPHIC_H_
#define COMPONENTS_ULTRAFLOW_APPLICATION_GUI_WIKAGRAPHIC_H_

//--------------------------------------------------------------------------------------
#include <stdint.h>
#ifndef LVGL_SIMULATOR_ACTIVE
//--------------------------------------------------------------------------------------
#include "lvgl.h"
//--------------------------------------------------------------------------------------
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
//--------------------------------------------------------------------------------------
#include "errorManager.h"
#include "guiDesign.h"
#include "wika_GenericFunctions.h"
#include "wikaHal.h"
#include "wikaConfigManager.h"
#include "wikaXml_data.h"
#include "wika_data_types.h"
#include "wikaOs.h"
#include "wika_events.h"
//--------------------------------------------------------------------------------------
#else
#define WIKA_SCREEN_X_SIZE  480
#define WIKA_SCREEN_Y_SIZE  320
//--------------------------------------------------------------------------------------
#include "lvgl/lvgl.h"
//--------------------------------------------------------------------------------------
#include "..\error\errorManager.h"
#include "..\gui_design\guiDesign.h"
#include "..\wika_data_types\wika_data_types.h"
#include "..\config_manager\wikaConfigManager.h"
#include "..\..\ultraflow-hmi\components\ultraflow\middleware\shared_values_lib\shared_values_json_labels.h"
//--------------------------------------------------------------------------------------
#endif // !LVGL_SIMULATOR_ACTIVE

#define STATUS_BAR_HEIGHT   	24

#define SEP_LINE_L_X        	(WIKA_SCREEN_X_SIZE/3)
#define SEP_LINE_Y1         	(2)
#define SEP_LINE_Y2         	(STATUS_BAR_HEIGHT -5)
#define SEP_LINE_R_X        	(WIKA_SCREEN_X_SIZE*2/3)

#define TUS_PRINT_FORMAT                    "%5.2f us"  /*!< max 5 digits with 2 decimal points, with us as unit */
#define TNS_PRINT_FORMAT                    "%5.3f ns"  /*!< max 5 digits with 3 decimal points, with ns as unit */
#define SIGNAL_LVL_PRINT_FORMAT             "%5.1f mV"  /*!< max 5 digits with 1 decimal points, with mV as unit */
#define AGC_GAIN_PRINT_FORMAT               "%3.1f dB"  /*!< max 3 digits with 1 decimal points, with dB as unit */
#define TEMPERATURE_PRINT_FORMAT            "%3.1f °C"  /*!< max 3 digits with 1 decimal points, with °C as unit */
#define DISTANCE_MM_PRINT_FORMAT            "%6.2f mm"  /*!< max 6 digits with 2 decimal points, with mm as unit */
#define SPEED_MS_PRINT_FORMAT               "%6.4f m/s" /*!< max 6 digits with 4 decimal points, with m/s as unit */
#define FLUIDSPD_MS_SHORT_PRINT_FORMAT      "%4.3f m/s" /*!< max 6 digits with 4 decimal points, with m/s as unit */

#define FREQUENCY_KHZ_PRINT_FORMAT          "%4.1f kHz" /*!< max 4 digits with 1 decimal points, with kHz as unit */
#define SOUNDSPEED_MS_PRINT_FORMAT          "%5d m/s"   /*!< max 5 digits with 1 decimal points, with m/s as unit */
#define TOTALIZATION_FLOW_M3_PRINT_FORMAT   "%09d m3"   /*!< max 8 digits without decimal points, with m3 as unit */
#define FLOWSPEED_MS_PRINT_FORMAT           "%4.3f m/s" /*!< max 4 digits with 3 decimal points, with m/s as unit */
#define MAINFIELD_NOUNIT_PRINT_FORMAT       "%6.0f"    /*!< max 7 digits with 0 decimal point, without units */

/*******************************************************************************
 * \enum	tabScreenLbl_t
 * 			Labels to identify the Tabview tabs.
 *******************************************************************************/
typedef enum _TAB_SCREEN_ENUM_ {
    MAIN_TAB_LBL_STREAM1    = 0,
    MAIN_TAB_LBL_STREAM2    = 1,
    MAIN_TAB_LBL_INFO       = 2,
    MAIN_TAB_LBL_CONFIG     = 3,
    MAIN_TAB_LBL_OTHER      = 4,
} tabScreenLbl_t;

typedef enum _RGB_COLOR_NAME_ {
    WIKA_RGB_COLOR_BLUE,
    WIKA_RGB_COLOR_LIGHT_GREY,
    WIKA_RGB_COLOR_WHITE,
    WIKA_RGB_COLOR_DARK_GREY,
    WIKA_RGB_COLOR_BLACK,
    WIKA_RGB_COLOR_LIGHT_BLUE,
    WIKA_RGB_COLOR_GREY,
    WIKA_RGB_COLOR_ORANGE,
    WIKA_RGB_COLOR_GREEN,
    WIKA_RGB_COLOR_RED,
} rgbColorNames_t;

typedef enum _START_STOP_MEAS_POPUP_ {
    SS_POPUP_START_MEASURE = 0,
    SS_POPUP_STOP_MEASURE = 1,
} startStopMeasPopup_t;

/***************************************************************************************
 * 	\author		BodiniA
 *
 *	\brief		This function returns the element linked to the screen label.
 *
 *	\param[in]	guiScreen	-	the label of the screen we are looking into.
 *
 *	\return		\see lv_obj_t pointer to the screen object element.
 ***************************************************************************************/
lv_obj_t* tWikaGraphic_GetScreen(const guiAppScreensLabels_t guiScreen);

/***************************************************************************************
 * 	\author		BodiniA
 *
 *	\brief		This function is used to focus the cursor on the buttor array at the
 *				top of the tabview widget.
 *				It returns the highest parent of the object to distinguish in which
 *				level of the screen we are working into.
 *
 *	\param[in]	none
 *
 *	\return		\see lv_obj_t pointer to parent element.
 ***************************************************************************************/
lv_obj_t* tWikaGraphic_focusToTabButton(void);

/***************************************************************************************
 * 	\author		BodiniA
 *
 *	\brief		Returns the label of the current tabview tab.
 *
 *	\param[in]	none
 *
 *	\return		\see tabScreenLbl_t
 ***************************************************************************************/
tabScreenLbl_t tWikaGraphic_getCurrentTab(void);

/***************************************************************************************
 * 	\author		agrippino
 *
 *	\brief		Initialize the GUI
 *
 *	\param[in]	gesture_indev: the input device associated with this GUI
 *
 *	\return		The error code \see systemErrors_t
 ***************************************************************************************/
#ifndef LVGL_SIMULATOR_ACTIVE
void vWikaGraphic_initializeGui(lv_indev_t* gesture_indev);
#else
void vWikaGraphic_initializeGui();
#endif

/***************************************************************************************
 * 	\author		agrippino
 *
 *	\brief		Load the welcome screen on the internal buffer and show it on LCD
 *
 *
 *	\return		The error code \see systemErrors_t
 ***************************************************************************************/
void vWikaGraphic_loadWelcomeScreen(void);

/***************************************************************************************
 * 	\author		agrippino
 *
 *	\brief		Load the main screen on the internal buffer and show it on LCD
 *
 *
 *	\return		The error code \see systemErrors_t
 ***************************************************************************************/
void vWikaGraphic_loadMainScreen(void);
void vWikaGraphic_reloadMainScreen(void);

/***************************************************************************************
 * 	\author		agrippino
 *
 *	\brief		Load the restart screen on the internal buffer and show it on LCD
 *
 *
 *	\return		The error code \see systemErrors_t
 ***************************************************************************************/
void vWikaGraphic_loadRestartScreen(void);

/***************************************************************************************
 * 	\author		agrippino
 *
 *	\brief		Load the wireless pop-up on the internal buffer and show it on LCD
 *
 *
 *	\return		The error code \see systemErrors_t
 ***************************************************************************************/
void tWikaGraphic_loadWirelessPopup(void);

/***************************************************************************************
 * 	\author		agrippino
 *
 *	\brief		Load the test screen on the internal buffer and show it on LCD
 *
 *
 *	\return		The error code \see systemErrors_t
 ***************************************************************************************/
void vWikaGraphic_loadTestScreen(void);

/***************************************************************************************
 * 	\author		BodiniA
 *
 *	\brief		Load Firmware update screen on the LCD
 *
 *	\param[in]	none
 *
 *	\return		none
 ***************************************************************************************/
void vWikaGraphic_loadFwUpdateScreen(void);

/***************************************************************************************
 * 	\author		BodiniA
 *
 *	\brief		Manage the measurement page Meter limits.
 *				Given the device config and measurement session, the function evaluate
 *				itself the warning/full-scale values through the given set-point value.
 *
 *	\param[in]	*devConfig			-	\see DeviceConfig_t
 *	\param[in]	*measSession		-	\see MeasurementSession_t
 *	\param[in]	streamNumber		-	current stream number
 *	\param[in]	*lowFlowSpeedCutoff	-	flow speed cutoff value set by the user
 *	\param[in]	*maxFlowSpeedWarnThresh	-	flow speed warning threshold value set by the user
 *	\param[in]	*maxFlowSpeed		-	flow speed full-scale value set by the user
 *	\param[in]	*FlowrateCutoff		-	low flowrate cutoff value set by the user
 *	\param[in]	*maxFlowRateWarnThresh	-	flow rate warning threshold value set by the user
 *	\param[in]	*maxFlowRate		-	max flowrate threshold value set by the user
 *
 *	\return		function return
 ***************************************************************************************/
void vWikaGraphic_ManageMeterLimits(MeasurementSession_t* measSession,
    const uint8_t streamNumber,
    int32_t* lowFlowSpeedCutoff, int32_t* maxFlowSpeedWarnThresh, int32_t* maxFlowSpeed,
    int32_t* FlowrateCutoff, int32_t* maxFlowRateWarnThresh, int32_t* maxFlowRate);


/***************************************************************************************
 * 	\author		BodiniA
 *
 *	\brief		Returns a given WIKA color into the lv_color_t format.
 *				\see rgbColorNames_t
 *
 *	\param[in]	rgbColorName - \see rgbColorNames_t
 *
 *	\return		the selected color in lv_color_t type.
 ***************************************************************************************/
lv_color_t tWikaGuiDesign_getColor(const rgbColorNames_t rgbColorName);


void restorePageGroup(int16_t tab);

#endif /* COMPONENTS_ULTRAFLOW_APPLICATION_GUI_WIKAGRAPHIC_H_ */
