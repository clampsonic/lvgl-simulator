﻿/**************************************************************************************
 *  \file       wikaGraphic.c
 *
 *  \brief      File Brief Description
 *
 *  \details    File Detailed Description
 *
 *  \author     Bodini Andrea
 *
 *  \version    0.1
 *
 *  \date		17 nov 2021
 *
 *  \copyright  (C) Copyright 2021, Euromisure Sas di WIKA Italia Srl
***************************************************************************************/

//--------------------------------------------------------------------------------------
#include <string.h>
#include <math.h>
#include <stdbool.h>
#include "wikaGraphic.h"
//--------------------------------------------------------------------------------------
#ifndef LVGL_SIMULATOR_ACTIVE
#include "esp_log.h"
//--------------------------------------------------------------------------------------
#include "wika_data_types.h"
#include "wikaOs.h"
#include "wikaConfigManager.h"
#include "wika_data_types.h"
#include "measure_conversion.h"
#include "wikaHal.h"
//--------------------------------------------------------------------------------------
#include "lvgl_helpers.h"
//--------------------------------------------------------------------------------------
#else
#include "../wika_data_types/wika_data_types.h"
#include "../config_manager/wikaConfigManager.h"
#include "../measure_lib/measure_conversion.h"
#endif // !LVGL_SIMULATOR_ACTIVE

//----- REFRESH TIMERS TIMEOUT -------------------------
#define TESTING_SCREEN_UPDATE_TIMEOUT_MS			200
#define MAIN_SCREEN_WAVEFORM_UPDATE_TIMEOUT_MS		1000
#define MAIN_SCREEN_DEVICEINFO_UPDATE_TIMEOUT_MS	5000
#define MAIN_SCREEN_MEASURETAB_UPDATE_TIMEOUT_MS	500
#define MAIN_SCREEN_MEASURESTAT_UPDATE_TIMEOUT_MS	500
#define MAIN_SCREEN_STATUSICONS_UPDATE_TIMEOUT_MS	1000
#define MAIN_SCREEN_DATETIME_UPDATE_TIMEOUT_MS		1000
#define FWUPDATE_SCREEN_STATS_UPDATE_TIMEOUT_MS		100

#define REPEAT_INFINITE_TIMES						(-1)
#define METER_SCALING_FACTOR						100

#define ANIMATION_FADEOUT_TIMEOUT_IN_MS				50
#define ANIMATION_FADEIN_TIMEOUT_IN_MS				0

/******************************************************************************************
 ********************************* LOCAL VARIABLES ****************************************
 ******************************************************************************************/
#ifndef LVGL_SIMULATOR_ACTIVE
static EXT_RAM_ATTR gui_objs guiObjects;
#else
static gui_objs guiObjects;
#endif

static rgbColor_t wikaBlue 		= { 22, 72, 154 };
static rgbColor_t wikaLightGrey	= { 233, 233, 233 };
static rgbColor_t wikaWhite 	= { 255, 255, 255 };
static rgbColor_t wikaDarkGrey 	= {76, 75, 73};
static rgbColor_t wikaBlack 	= { 0, 0, 0 };
static rgbColor_t wikaGreen 	= { 0, 163, 109};
static rgbColor_t wikaOrange 	= { 254, 153, 0 };
static rgbColor_t wikaGrey 		= { 166, 163, 158 };
static rgbColor_t wikaLightBlue	= { 52, 128, 207};
static rgbColor_t wikaRed       = { 198, 40, 40 };



/******************************************************************************************
 *********************** LOCAL FUNCTION PROTOTYPES ****************************************
 ******************************************************************************************/
static void updateMeasureTab(void);
static void updateStatusIcons(void);
static void updateTestingScreen(void);
static void updateDateTime(void);
static void updateWaveFormGraphTile();
static void updateDeviceInfoField(void);
static void updateMeasureStat(void);
static void vDestroyAllTimers(void);


/***************************************************************************************
 * 	\author		BodiniA
 *
 *	\brief		Update statistics and labels on the FW Update screen.
 *				The screen shows a current status label, a load bar and the
 *				current uploaded size of the shared file versus the total file size.
 *
 *	\param[in]	none
 *
 *	\return		none
 ***************************************************************************************/
static void vWikaGraphic_ManageFwUpdateScreen( void );

/******************************************************************************************/
static void vDestroyAllTimers(void)
{
	uint8_t u8number_of_timers = sizeof(timerStruct_objs) / sizeof(lv_timer_t *);
	uint8_t u8counter = 0;

	lv_timer_t **currentTimer = (lv_timer_t **) &guiObjects.timers_objs;

	for(u8counter = 0; u8counter < u8number_of_timers; u8counter++){
		if(*(currentTimer + u8counter) != NULL){
			lv_timer_pause(*(currentTimer + u8counter));
		}
	}
}


lv_color_t tWikaGuiDesign_getColor(const rgbColorNames_t rgbColorName)
{
    rgbColor_t *selColor;
    switch (rgbColorName) {
    case WIKA_RGB_COLOR_BLUE:
        selColor = &wikaBlue;
        break;
    case WIKA_RGB_COLOR_LIGHT_GREY:
        selColor = &wikaLightGrey;
        break;
    case WIKA_RGB_COLOR_WHITE:
        selColor = &wikaWhite;
        break;
    case WIKA_RGB_COLOR_DARK_GREY:
        selColor = &wikaDarkGrey;
        break;
    case WIKA_RGB_COLOR_BLACK:
        selColor = &wikaBlack;
        break;
    case WIKA_RGB_COLOR_LIGHT_BLUE:
    	selColor = &wikaLightBlue;
		break;
    case WIKA_RGB_COLOR_GREY:
    	selColor = &wikaGrey;
		break;
    case WIKA_RGB_COLOR_ORANGE:
    	selColor = &wikaOrange;
		break;
    case WIKA_RGB_COLOR_GREEN:
    	selColor = &wikaGreen;
		break;
    case WIKA_RGB_COLOR_RED:
        selColor = &wikaRed;
        break;
    default:
        selColor = &wikaWhite;
        break;
    }

    return lv_color_make(selColor->Red, selColor->Green, selColor->Blue);
}

lv_obj_t *tWikaGraphic_GetScreen(const guiAppScreensLabels_t guiScreenLbl)
{
	lv_obj_t *returnTab;
	switch(guiScreenLbl){
	case GUI_SCR_LBL_WELCOME:
		returnTab = guiObjects.screen_Welcome;
		break;
	case GUI_SCR_LBL_RESTART:
		returnTab = guiObjects.screen_Restart;
		break;
	case GUI_SCR_LBL_MAIN:
		returnTab = guiObjects.screen_Main;
		break;
	case GUI_SCR_LBL_TEST:
		returnTab = guiObjects.screen_Testing;
		break;
	case GUI_SCR_LBL_FWUPDATE:
		returnTab = guiObjects.screen_FwUpdate;
		break;
	case GUI_SCR_LBL_POPUP_ENVIRONMENT:
	case GUI_SCR_LBL_POPUP_NETWORK:
		returnTab = guiObjects.popup_genericPopup;
		break;
	default:
		returnTab = guiObjects.screen_Welcome;
		break;
	}
	return returnTab;
}

tabScreenLbl_t tWikaGraphic_getCurrentTab(void){
	return lv_tabview_get_tab_act(guiObjects.mainScreenObjs.tabView);
}

lv_obj_t *tWikaGraphic_focusToTabButton(void){

	lv_obj_t * obj = lv_group_get_focused(guiObjects.guiHelpersObjs.guiGroup);

	restorePageGroup(MAIN_TAB_LBL_CONFIG);

	lv_tabview_set_act(guiObjects.mainScreenObjs.tabView, MAIN_TAB_LBL_CONFIG, LV_ANIM_OFF);

	return obj->parent->parent;
}

void restorePageGroup(int16_t tab)
{
    size_t size = 0;
    lv_obj_t** objs = NULL;
    uint32_t i;

    lv_group_remove_all_objs(guiObjects.guiHelpersObjs.guiGroup);

//    if (tab == MAIN_TAB_LBL_CONFIG) {
//        size = sizeof(settingTabActiveObjs);
//        objs = (lv_obj_t**)&settingTabActiveObjs;
//    }

    lv_group_add_obj(guiObjects.guiHelpersObjs.guiGroup, guiObjects.mainScreenObjs.tabView);

    for (i = 0; i < size / sizeof(lv_obj_t*); i++) {
        if (objs[i] == NULL) continue;
        lv_group_add_obj(guiObjects.guiHelpersObjs.guiGroup, objs[i]);
    }
}

#ifndef LVGL_SIMULATOR_ACTIVE
void vWikaGraphic_initializeGui(lv_indev_t* gesture_indev)
#else
void vWikaGraphic_initializeGui()
#endif
{
	memset(&guiObjects, 0, sizeof(guiObjects));

	// create a group for this input device
	guiObjects.guiHelpersObjs.guiGroup = lv_group_create();

#ifndef LVGL_SIMULATOR_ACTIVE
    // set this group to gesture_indev input device
    lv_indev_set_group(gesture_indev, guiObjects.guiHelpersObjs.guiGroup);
#endif    

    // setup welcome screen
    guiObjects.screen_Welcome	= lv_obj_create(NULL);

    // setup restart screen
    guiObjects.screen_Restart	= lv_obj_create(NULL);

    // setup main screen
    guiObjects.screen_Main		= lv_obj_create(NULL);

    // setup testing screen
    guiObjects.screen_Testing 	= lv_obj_create(NULL);

    // setup firmware update screen
    guiObjects.screen_FwUpdate 	= lv_obj_create(NULL);

    setupPopUpStructures(&guiObjects);

    // setup popup main structure
    guiObjects.popup_genericPopup= lv_obj_create(NULL);
}

void vWikaGraphic_loadWelcomeScreen(void)
{
	// Being the first screen it must be inited before its call
	vSetupWelcomeScreen_init(&guiObjects.welcomeScreenObjs, guiObjects.screen_Welcome);

	// load screen
	lv_scr_load(guiObjects.screen_Welcome);

	// need to activate the focus on tableview
	lv_event_send(guiObjects.mainScreenObjs.tabView, LV_EVENT_REFRESH, NULL);
}

void vWikaGraphic_reloadMainScreen(void)
{
	// tabviewContentRefresh_cb triggered in setupMainScreen
	lv_event_send(guiObjects.mainScreenObjs.tabView, LV_EVENT_VALUE_CHANGED, NULL);
}

void vWikaGraphic_loadMainScreen(void)
{
	// Clear all previously started animations
	lv_anim_del_all();

	// Setup the screen objects
	vSetupMainScreen_init(&guiObjects, guiObjects.screen_Main);

#ifndef LVGL_SIMULATOR_ACTIVE

	// Update the measure values
	guiObjects.timers_objs.updateMeasureTab_tmr = lv_timer_create((lv_timer_cb_t) updateMeasureTab, MAIN_SCREEN_MEASURETAB_UPDATE_TIMEOUT_MS, NULL);

	// Create the WaveForm Update Chart timer
	guiObjects.timers_objs.updateWaveFormGraph_tmr   = lv_timer_create((lv_timer_cb_t)updateWaveFormGraphTile, MAIN_SCREEN_WAVEFORM_UPDATE_TIMEOUT_MS, NULL);
	lv_timer_pause(guiObjects.timers_objs.updateWaveFormGraph_tmr);

	// Create the Measure Stat Update Timer
	guiObjects.timers_objs.updateMeasStat_tmr   = lv_timer_create((lv_timer_cb_t)updateMeasureStat, MAIN_SCREEN_MEASURESTAT_UPDATE_TIMEOUT_MS, NULL);
	lv_timer_pause(guiObjects.timers_objs.updateMeasStat_tmr);

	// Update the Notify Icons
	guiObjects.timers_objs.updateNotifyIco_tmr  = lv_timer_create((lv_timer_cb_t)updateStatusIcons, MAIN_SCREEN_STATUSICONS_UPDATE_TIMEOUT_MS, NULL);

	// Udate the Date/time
	guiObjects.timers_objs.updateDateTime_tmr  = lv_timer_create((lv_timer_cb_t)updateDateTime, MAIN_SCREEN_DATETIME_UPDATE_TIMEOUT_MS, NULL);

	// Jump between hostname and IP address
	guiObjects.timers_objs.updateCenterField_tmr =  lv_timer_create((lv_timer_cb_t)updateDeviceInfoField, MAIN_SCREEN_DEVICEINFO_UPDATE_TIMEOUT_MS, NULL);

#endif // !LVGL_SIMULATOR_ACTIVE


	// load screen
	lv_scr_load_anim(guiObjects.screen_Main, LV_SCR_LOAD_ANIM_FADE_ON, ANIMATION_FADEOUT_TIMEOUT_IN_MS, ANIMATION_FADEIN_TIMEOUT_IN_MS, true);
}

void vWikaGraphic_loadRestartScreen(void)
{
	// Clear all previously started animations
	lv_anim_del_all();

	vDestroyAllTimers();

	// Setup the screen
	vSetupRestartScreen_init(&guiObjects.restartScreenObjs, guiObjects.screen_Restart);

	// load screen
	lv_scr_load_anim(guiObjects.screen_Restart, LV_SCR_LOAD_ANIM_FADE_ON, ANIMATION_FADEOUT_TIMEOUT_IN_MS, ANIMATION_FADEIN_TIMEOUT_IN_MS, true);
}

void vWikaGraphic_loadFwUpdateScreen(void)
{
	// Clear all previously started animations
	lv_anim_del_all();

	vDestroyAllTimers();

	// Update the Notify Icons
	guiObjects.timers_objs.updateFwStats_tmr  = lv_timer_create((lv_timer_cb_t) vWikaGraphic_ManageFwUpdateScreen, FWUPDATE_SCREEN_STATS_UPDATE_TIMEOUT_MS, NULL);

	// Setup the screen
	vFwUpdateScreen_init(&guiObjects.fwUpdateScreenObjs, guiObjects.screen_FwUpdate);

	// load screen
	lv_scr_load_anim(guiObjects.screen_FwUpdate, LV_SCR_LOAD_ANIM_FADE_ON, ANIMATION_FADEOUT_TIMEOUT_IN_MS, ANIMATION_FADEIN_TIMEOUT_IN_MS, true);
}

void vWikaGraphic_loadTestScreen( void )
{
	// Clear all previously started animations
	lv_anim_del_all();

	vDestroyAllTimers();

	// Setup the screen objects
    vSetupTestingScreen_init(&guiObjects.testingScreenObjs, guiObjects.screen_Testing);

	// setup task for update (if need) some values in this screen
    guiObjects.testingScreenObjs.updateValues_tmr = lv_timer_create((lv_timer_cb_t) updateTestingScreen, TESTING_SCREEN_UPDATE_TIMEOUT_MS, NULL);

	lv_timer_ready(guiObjects.testingScreenObjs.updateValues_tmr);
	lv_timer_set_repeat_count(guiObjects.testingScreenObjs.updateValues_tmr, REPEAT_INFINITE_TIMES);

	// load the screen
	lv_scr_load_anim(guiObjects.screen_Testing, LV_SCR_LOAD_ANIM_FADE_ON, ANIMATION_FADEOUT_TIMEOUT_IN_MS, ANIMATION_FADEIN_TIMEOUT_IN_MS, true);
}

static void updateTestingScreen(void)
{
	/*
	MeasurementData_t guiData;
	BaseType_t ret;

	// TODO: ADD STREAM 2 LABELS
	ret = tWikaOs_peekProcessedUssData(&guiData, 0);
	if( ret == pdTRUE )
	{
		lv_label_set_text_fmt(guiObjects.testingScreenObjs.deltaT_value_lbl	, "%4.3f"	, SEC_TO_NS(guiData.filteredDeltaT) );
		lv_label_set_text_fmt(guiObjects.testingScreenObjs.tUp_value_lbl	, "%3.6f"	, SEC_TO_US(guiData.filteredTUp)	);
		lv_label_set_text_fmt(guiObjects.testingScreenObjs.tDown_value_lbl	, "%3.6f"	, SEC_TO_US(guiData.filteredTDown)  );
		lv_label_set_text_fmt(guiObjects.testingScreenObjs.error_code_lbl	, "%d"		, guiData.ussError   );

		// other data
		lv_label_set_text_fmt(guiObjects.testingScreenObjs.flow_value_lbl   , "%6.3f"	, M3S_TO_LH(guiData.Flow) );
		lv_label_set_text_fmt(guiObjects.testingScreenObjs.speed_value_lbl  , "%4.6f"	, guiData.FluidSpeed );
	}
	else
	{
		;
	}
	*/
}



static void updateMeasureTab(void)
{
#ifndef LVGL_SIMULATOR_ACTIVE
	MeasurementData_t guiData;
	BaseType_t ret;

	tabScreenLbl_t currentTab = tWikaGraphic_getCurrentTab();

	ret = tWikaOs_peekProcessedUssData(currentTab, &guiData);
	if( ret == pdTRUE )
	{
		if((currentTab == MAIN_TAB_LBL_STREAM1) && (guiData.streamNumber == DAQ_STREAM_1))
		{
			lv_event_send(guiObjects.mainScreenObjs.tab_stream1_objs.tabScreen_tileView_tile1.obj_main_tileView_tile, LV_EVENT_REFRESH, &guiData);
		}

		if((currentTab == MAIN_TAB_LBL_STREAM2) && (guiData.streamNumber == DAQ_STREAM_2))
		{
			lv_event_send(guiObjects.mainScreenObjs.tab_stream2_objs.tabScreen_tileView_tile1.obj_main_tileView_tile, LV_EVENT_REFRESH, &guiData);
		}
	}
	else
	{
		;
	}
#endif
}

static void updateMeasureStat(void)
{
#ifndef LVGL_SIMULATOR_ACTIVE
	BaseType_t queueStat = pdFALSE;
	tabScreenLbl_t currentTab = tWikaGraphic_getCurrentTab();
	static UssStreamStatPacket_t statPckt;

	queueStat = tWikaOs_receiveUssStatPacket(&statPckt, 0);
	if(queueStat == pdTRUE)
	{
		// Data Received.
		if((currentTab == MAIN_TAB_LBL_STREAM1) && (statPckt.streamNumber == DAQ_STREAM_1) ){
			lv_event_send(	guiObjects.mainScreenObjs.tab_stream1_objs.tabScreen_tileView_tile2.obj_statContainer,
							LV_EVENT_REFRESH,
							&statPckt);
		}
		if((currentTab == MAIN_TAB_LBL_STREAM2) && (statPckt.streamNumber == DAQ_STREAM_2) ){
			lv_event_send(	guiObjects.mainScreenObjs.tab_stream2_objs.tabScreen_tileView_tile2.obj_statContainer,
							LV_EVENT_REFRESH,
							&statPckt);
		}
	}
#endif
}

static void updateWaveFormGraphTile(void)
{
#ifndef LVGL_SIMULATOR_ACTIVE
	UssStreamPlot_t plotPacket;

	memset(&plotPacket, 0, sizeof(plotPacket));

	tWikaOs_receiveUssWaveformData(&plotPacket, 0);

	tabScreenLbl_t currentTab = tWikaGraphic_getCurrentTab();

	if(tWikaOs_receiveDaqPlotEvent(EVENT_DAQ_PLOT_WAVEFORM_UPDATED, 0) != false)
	{
		// Data Received.
		if((currentTab == MAIN_TAB_LBL_STREAM1) && (plotPacket.streamNumber == DAQ_STREAM_1) ){
			lv_event_send(	guiObjects.mainScreenObjs.tab_stream1_objs.tabScreen_tileView_tile2.chart_UpsDnsWaveform,
							LV_EVENT_REFRESH, &plotPacket);
		}
		if((currentTab == MAIN_TAB_LBL_STREAM2) && (plotPacket.streamNumber == DAQ_STREAM_2) ){
			lv_event_send(	guiObjects.mainScreenObjs.tab_stream2_objs.tabScreen_tileView_tile2.chart_UpsDnsWaveform,
							LV_EVENT_REFRESH, &plotPacket);
		}
	}
	else
	{
		if(currentTab == MAIN_TAB_LBL_STREAM1){
			tWikaOs_sendEventToSystem(EVENT_SYS_FSM_REQ_PLOT_WAVEFORM_STREAM1);
		}
		if(currentTab == MAIN_TAB_LBL_STREAM2){
			tWikaOs_sendEventToSystem(EVENT_SYS_FSM_REQ_PLOT_WAVEFORM_STREAM2);
		}
	}
#endif
}

static void vWikaGraphic_ManageFwUpdateScreen(void)
{
#ifndef LVGL_SIMULATOR_ACTIVE
    fwUpdateDataStats_t tfw_data_stats;
    memset(&tfw_data_stats, 0, sizeof(fwUpdateDataStats_t));
    BaseType_t tqueue_ret = pdFALSE;

    tqueue_ret = tWikaOs_peekFwUpdateStat(&tfw_data_stats, 0);

    if (tqueue_ret == pdTRUE) {
        lv_event_send(guiObjects.fwUpdateScreenObjs.fwUpdatePhase_lbl, LV_EVENT_REFRESH, &tfw_data_stats.tcurrent_phase);
        lv_event_send(guiObjects.fwUpdateScreenObjs.progressBar_bar, LV_EVENT_REFRESH, &tfw_data_stats.u8percentage);
        lv_event_send(guiObjects.fwUpdateScreenObjs.fwUpdateDataStats_lbl, LV_EVENT_REFRESH, &tfw_data_stats);
    }
    if ((tfw_data_stats.tcurrent_phase == FW_UPDT_PHASE_COMPLETED) || (tfw_data_stats.tcurrent_phase < 0)) {
        lv_timer_pause(guiObjects.timers_objs.updateFwStats_tmr);
    }
#endif
}

static inline void updateStatusIcons(void)
{
	lv_event_send(guiObjects.mainScreenObjs.statusBar_objs.statusBar_notifyIcons_cont, LV_EVENT_REFRESH, NULL);
}

static inline void updateDateTime(void)
{
	lv_event_send(guiObjects.mainScreenObjs.statusBar_objs.statusBar_dateTime_cont, LV_EVENT_REFRESH, NULL);
}

static inline void updateDeviceInfoField(void)
{
	lv_event_send(guiObjects.mainScreenObjs.statusBar_objs.statusBar_hostname_lbl, LV_EVENT_REFRESH, NULL);
}

void vWikaGraphic_ManageMeterLimits(MeasurementSession_t* measSession,
    const uint8_t streamNumber,
    int32_t* lowFlowSpeedCutoff, int32_t* maxFlowSpeedWarnThresh, int32_t* maxFlowSpeed,
    int32_t* FlowrateCutoff, int32_t* maxFlowRateWarnThresh, int32_t* maxFlowRate)
{
    if (measSession->streams[streamNumber].configuration.setPointType == SETPOINT_SPEED)
    {
        // Flowspeed
        *lowFlowSpeedCutoff = (int32_t)trunc((measSession->streams[streamNumber].configuration.setPointCutoff) * METER_SCALING_FACTOR);
        *maxFlowSpeedWarnThresh = (int32_t)trunc((measSession->streams[streamNumber].configuration.setPointWarningThreshold) * METER_SCALING_FACTOR);
        *maxFlowSpeed = (int32_t)trunc((measSession->streams[streamNumber].configuration.setPointFullScale) * METER_SCALING_FACTOR);

        *FlowrateCutoff = (int32_t)M3S_TO_LH((float)*lowFlowSpeedCutoff * (measSession->streams[streamNumber].preCalculations.fluidArea) / METER_SCALING_FACTOR);
        *maxFlowRateWarnThresh = (int32_t)M3S_TO_LH((float)*maxFlowSpeedWarnThresh * (measSession->streams[streamNumber].preCalculations.fluidArea) / METER_SCALING_FACTOR);
        *maxFlowRate = (int32_t)M3S_TO_LH((float)*maxFlowSpeed * (measSession->streams[streamNumber].preCalculations.fluidArea) / METER_SCALING_FACTOR);
    }
    else {
        // Flowrate
        *FlowrateCutoff = (int32_t)M3S_TO_LH(measSession->streams[streamNumber].configuration.setPointCutoff);
        *maxFlowRateWarnThresh = (int32_t)M3S_TO_LH(measSession->streams[streamNumber].configuration.setPointWarningThreshold);
        *maxFlowRate = (int32_t)M3S_TO_LH(measSession->streams[streamNumber].configuration.setPointFullScale);

        if (measSession->streams[streamNumber].preCalculations.fluidArea != 0) {
            *lowFlowSpeedCutoff = (int32_t)trunc((measSession->streams[streamNumber].configuration.setPointCutoff / (measSession->streams[streamNumber].preCalculations.fluidArea)) * METER_SCALING_FACTOR);
            *maxFlowSpeedWarnThresh = (int32_t)trunc((measSession->streams[streamNumber].configuration.setPointWarningThreshold / (measSession->streams[streamNumber].preCalculations.fluidArea)) * METER_SCALING_FACTOR);
            *maxFlowSpeed = (int32_t)trunc((measSession->streams[streamNumber].configuration.setPointFullScale / (measSession->streams[streamNumber].preCalculations.fluidArea)) * METER_SCALING_FACTOR);
        }
    }
}

/********************************* EOF ************************************************/
