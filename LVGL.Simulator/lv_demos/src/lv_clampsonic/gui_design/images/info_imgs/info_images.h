﻿/**************************************************************************************
 *  \file       info_images.h
 *
 *  \brief      Header file for Info screen images/icons
 *
 *  \details    This file is used for declaring the images used into the
 *              Info screen
 *
 *  \author     BodiniA
 *
 *  \version    0.1
 *
 *  \date		15 jun 2022
 *
 *  \copyright  (C) Copyright 2022, Euromisure Sas di WIKA Italia Srl
***************************************************************************************/

#ifndef COMPONENTS_ULTRAFLOW_APPLICATION_GUI_GUI_DESIGN_IMAGES_INFO_IMGS_INFOIMAGES_H_
#define COMPONENTS_ULTRAFLOW_APPLICATION_GUI_GUI_DESIGN_IMAGES_INFO_IMGS_INFOIMAGES_H_

#include <stdio.h>
#ifndef LVGL_SIMULATOR_ACTIVE
#include "lvgl.h"
#include "guider_fonts/guider_fonts.h"
#else
#include "lvgl/lvgl.h"
#endif // !LVGL_SIMULATOR_ACTIVE


/***************************************************************************
 ************* GENERIC DEFINITIONS *****************************************
 ***************************************************************************/

LV_IMG_DECLARE(setpoints_blue);
LV_IMG_DECLARE(setpoints_white);
LV_IMG_DECLARE(signalstat_blue);
LV_IMG_DECLARE(time_blue);

#endif /* COMPONENTS_ULTRAFLOW_APPLICATION_GUI_GUI_DESIGN_IMAGES_INFO_IMGS_INFOIMAGES_H_ */
