﻿/**************************************************************************************
 *  \file       info_buttons.h
 *
 *  \brief      Header file Button images in Info screen
 *
 *  \details    This file is used for declaring the images used into the
 *              Info screen, in detail the button matrixes
 *
 *  \author     BodiniA
 *
 *  \version    0.1
 *
 *  \date		14 jun 2022
 *
 *  \copyright  (C) Copyright 2022, Euromisure Sas di WIKA Italia Srl
***************************************************************************************/

#ifndef COMPONENTS_ULTRAFLOW_APPLICATION_GUI_GUI_DESIGN_IMAGES_BUTTONS_INFOBUTTONS_H_
#define COMPONENTS_ULTRAFLOW_APPLICATION_GUI_GUI_DESIGN_IMAGES_BUTTONS_INFOBUTTONS_H_

#include <stdio.h>
#ifndef LVGL_SIMULATOR_ACTIVE
#include "lvgl.h"
#include "guider_fonts/guider_fonts.h"
#else
#include "lvgl/lvgl.h"
#endif // !LVGL_SIMULATOR_ACTIVE


/***************************************************************************
 ************* GENERIC DEFINITIONS *****************************************
 ***************************************************************************/
#define STAT_BUTTON_NUMBER  10

#define BTN_HEIGHT  50

#define BTN11_WIDTH 209
#define BTN12_WIDTH 120

#define BTN21_WIDTH 150
#define BTN22_WIDTH 101

#define BTN31_WIDTH 117
#define BTN32_WIDTH 120

#define BTN41_WIDTH 100
#define BTN42_WIDTH 155

#define BTN51_WIDTH 126
#define BTN52_WIDTH 219

LV_IMG_DECLARE(btn11);
LV_IMG_DECLARE(btn12);
LV_IMG_DECLARE(btn21);
LV_IMG_DECLARE(btn22);
LV_IMG_DECLARE(btn31);
LV_IMG_DECLARE(btn32);
LV_IMG_DECLARE(btn41);
LV_IMG_DECLARE(btn42);
LV_IMG_DECLARE(btn51);
LV_IMG_DECLARE(btn52);
LV_IMG_DECLARE(btn1x);
LV_IMG_DECLARE(btnx2);

#endif /* COMPONENTS_ULTRAFLOW_APPLICATION_GUI_GUI_DESIGN_IMAGES_BUTTONS_INFOBUTTONS_H_ */
