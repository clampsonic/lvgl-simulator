﻿/**************************************************************************************
 *  \file       setupSettingsTab.c
 *  
 *  \brief      Settings Tab Main File
 *  
 *  \details    This file contains all the methods and variables linked to the
 *              settings Tab of che LVGL UI.
 *  
 *  \author     Bodini Andrea
 *  
 *  \version    0.1
 *  
 *  \date		20 lug 2022
 *  
 *  \copyright  (C) Copyright 2021, Euromisure Sas di WIKA Italia Srl
***************************************************************************************/

/********************************** INCLUDE *******************************************/
#include "guiDesign.h"
#include "math.h"
#include <stdlib.h>
#include <string.h>
#ifndef LVGL_SIMULATOR_ACTIVE
#include "wikaGraphic.h"
#include "esp_log.h"
#else
#include "..\wika_graphic\wikaGraphic.h"
#endif // !LVGL_SIMULATOR_ACTIVE

/********************************** DEFINES *******************************************/


/********************************** LOCAL VARIABLES ***********************************/


/********************************** FUNCTION PROTOTYPES *******************************/


/********************************** FUNCTIONS *****************************************/
// TAB MAIN CONSTRUCTOR
void vSetupSettingsTab_init(settingsTabObjs_t* objects, lv_obj_t* parent)
{
    // Prevent the object being scrollable.
    lv_obj_set_scrollbar_mode(parent, LV_SCROLLBAR_MODE_OFF);
    lv_obj_clear_flag(parent, LV_OBJ_FLAG_SCROLLABLE);

    // Create a container to simplify the object context creation
    objects->obj_container = lv_obj_create(parent);
    lv_obj_set_size(objects->obj_container, lv_pct(PCT_FULL_SIZE), lv_pct(PCT_FULL_SIZE));
    lv_obj_set_style_bg_opa(objects->obj_container, LV_OPA_0, LV_PART_MAIN);
    lv_obj_set_style_bg_color(objects->obj_container, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_GREEN), LV_PART_MAIN);
    lv_obj_set_style_radius(objects->obj_container, 0, LV_PART_MAIN);
    lv_obj_set_style_border_width(objects->obj_container, 0, LV_PART_MAIN);
    lv_obj_set_style_pad_all(objects->obj_container, 0, LV_PART_MAIN);

    lv_style_init(&objects->style_img_bg);
    lv_style_set_opa(&objects->style_img_bg, 5);
    lv_style_set_height(&objects->style_img_bg, LV_SIZE_CONTENT);

    objects->img_bg_right = lv_img_create(objects->obj_container);
    lv_img_set_src(objects->img_bg_right, &btnx2);
    lv_obj_set_align(objects->img_bg_right, LV_ALIGN_BOTTOM_RIGHT);

    lv_obj_add_style(objects->img_bg_right, &objects->style_img_bg, LV_PART_MAIN);

    lv_style_init(&objects->style_obj_info_container);
    lv_style_set_border_color(&objects->style_obj_info_container, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_DARK_GREY));
    lv_style_set_border_width(&objects->style_obj_info_container, 2);
    lv_style_set_width(&objects->style_obj_info_container, lv_pct(PCT_FULL_SIZE));
    lv_style_set_bg_opa(&objects->style_obj_info_container, LV_OPA_0);
    lv_style_set_bg_color(&objects->style_obj_info_container, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_DARK_GREY));
    lv_style_set_radius(&objects->style_obj_info_container, 10);
    lv_style_set_max_height(&objects->style_obj_info_container, lv_pct(45));

    objects->obj_wifi_info_container = lv_obj_create(parent);
    lv_obj_align(objects->obj_wifi_info_container, LV_ALIGN_TOP_MID, 0, 13);
    lv_obj_add_style(objects->obj_wifi_info_container, &objects->style_obj_info_container, LV_PART_MAIN);

    lv_style_init(&objects->style_container_info_label);
    lv_style_set_text_font(&objects->style_container_info_label, &wika_montserrat_20_semibold_symbols);
    lv_style_set_text_align(&objects->style_container_info_label, LV_TEXT_ALIGN_CENTER);
    lv_style_set_text_color(&objects->style_container_info_label, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_DARK_GREY));
    lv_style_set_bg_opa(&objects->style_container_info_label, LV_OPA_COVER);
    lv_style_set_bg_color(&objects->style_container_info_label, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_LIGHT_GREY));
    lv_style_set_pad_hor(&objects->style_container_info_label, 5);

    lv_obj_t* lbl_wifi_settings = lv_label_create(parent);
    lv_label_set_text(lbl_wifi_settings, "WiFi - Settings");
    lv_obj_align(lbl_wifi_settings, LV_ALIGN_TOP_LEFT, 15, 0);
    lv_obj_add_style(lbl_wifi_settings, &objects->style_container_info_label, LV_PART_MAIN);


    objects->obj_web_info_container = lv_obj_create(parent);
    lv_obj_align_to(objects->obj_web_info_container, objects->obj_wifi_info_container, LV_ALIGN_OUT_BOTTOM_LEFT, 0, 13);
    lv_obj_add_style(objects->obj_web_info_container, &objects->style_obj_info_container, LV_PART_MAIN);

    lv_obj_t* lbl_web_settings = lv_label_create(parent);
    lv_label_set_text(lbl_web_settings, "Web Interface - Settings");
    lv_obj_align_to(lbl_web_settings, objects->obj_web_info_container, LV_ALIGN_OUT_TOP_LEFT, 15, 3);
    lv_obj_add_style(lbl_web_settings, &objects->style_container_info_label, LV_PART_MAIN);
}



/********************************* EOF ************************************************/
