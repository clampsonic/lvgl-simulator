﻿#include <stdbool.h>
#include "guiDesign.h"

#ifndef LVGL_SIMULATOR_ACTIVE
#include "wikaGraphic.h"
#include "esp_log.h"
#else
#include "..\wika_graphic\wikaGraphic.h"
#endif

#define LABELS_SPAN                 25
#define HMI_FW_V_LABEL_Y_POSITION   95
#define DAQ_FW_V_LABEL_Y_POSITION   (HMI_FW_V_LABEL_Y_POSITION + LABELS_SPAN)
#define REL_FW_V_LABEL_Y_POSITION   (DAQ_FW_V_LABEL_Y_POSITION + LABELS_SPAN)

#define MESSAGE_INIT_FILESYSTEM		"Initialize FileSystem\0"
#define MESSAGE_INIT_CONFIGURATION	"Load User Configuration\0"
#define MESSAGE_INIT_PERIPHERALS	"Initialize Peripherals\0"
#define MESSAGE_INIT_GUI			"Loading GUI\0"
#define MESSAGE_SYSTEM_STARTUP		"System Startup\0"

static welcomeScreen_objs * welcomeScreenObjs;

static const char* firmwareVerLabels[3] = {
    "HMI FW v.1.0\0",
    "DAQ FW v.0.9\0",
    "REL: 2021.00.01.00\0"
};

static void setBarValue(void* bar, int barValue);

static void setStartupStatusLabel(lv_event_t* event);


void vSetupWelcomeScreen_init(welcomeScreen_objs * screenObjs,lv_obj_t* parent)
{
	welcomeScreenObjs = screenObjs;

    ///////////////////////////////////////////////////////////////////////////
    //////////////////////// BACKGROUND IMAGE /////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////
    // Create the Welcome Screen Image
	welcomeScreenObjs->wika_logo_img = lv_img_create(parent);

	lv_obj_set_style_bg_color(parent, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_LIGHT_GREY), LV_PART_MAIN);

	lv_obj_set_style_opa(welcomeScreenObjs->wika_logo_img, LV_OPA_COVER, LV_PART_MAIN);

    // Associate img object to the image structure
    lv_img_set_src(welcomeScreenObjs->wika_logo_img, &wika_logo);

    // Set position of the image
    lv_obj_set_pos(welcomeScreenObjs->wika_logo_img, 180, 51);
    lv_img_set_pivot(welcomeScreenObjs->wika_logo_img, 0, 0);
    lv_img_set_angle(welcomeScreenObjs->wika_logo_img, 0);

    ///////////////////////////////////////////////////////////////////////////
    /////////////////////////// LOADING BAR ///////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////

    //LV_IMG_DECLARE(img_skew_strip);
    static lv_style_t style_loadBar;

    lv_style_init(&style_loadBar);

    lv_style_set_bg_img_src(&style_loadBar, &img_skew_strip);
    lv_style_set_bg_img_tiled(&style_loadBar, true);
    lv_style_set_bg_img_opa(&style_loadBar, LV_OPA_30);
    lv_style_set_bg_opa(&style_loadBar, LV_OPA_COVER);
    lv_style_set_bg_color(&style_loadBar, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_BLUE));

    // Create the bar
    welcomeScreenObjs->progressBar_bar = lv_bar_create(parent);
    lv_obj_add_style(welcomeScreenObjs->progressBar_bar, &style_loadBar, LV_PART_INDICATOR);

    lv_obj_set_size(welcomeScreenObjs->progressBar_bar, 260, 10);
    lv_obj_center(welcomeScreenObjs->progressBar_bar);
    lv_bar_set_mode(welcomeScreenObjs->progressBar_bar, LV_BAR_MODE_NORMAL);
    lv_bar_set_start_value(welcomeScreenObjs->progressBar_bar, 0, LV_ANIM_OFF);

    lv_anim_init(&welcomeScreenObjs->barAnimation_anim);
    lv_anim_set_exec_cb(&welcomeScreenObjs->barAnimation_anim, setBarValue);
    lv_anim_set_time(&welcomeScreenObjs->barAnimation_anim, 5000);
    lv_anim_set_var(&welcomeScreenObjs->barAnimation_anim, welcomeScreenObjs->progressBar_bar);
    lv_anim_set_values(&welcomeScreenObjs->barAnimation_anim, 0, 100);

    lv_anim_set_repeat_count(&welcomeScreenObjs->barAnimation_anim, 0);
    lv_anim_start(&welcomeScreenObjs->barAnimation_anim);

    ///////////////////////////////////////////////////////////////////////////
    //////////////////////// LOADING PHASE LABEL //////////////////////////////
    ///////////////////////////////////////////////////////////////////////////

    static lv_style_t style_textLabels;
    lv_style_init(&style_textLabels);

    lv_style_set_text_font(&style_textLabels, &lv_font_montserrat_18);

    welcomeScreenObjs->loadPhase_lbl = lv_label_create(parent);
    lv_obj_align(welcomeScreenObjs->loadPhase_lbl, LV_ALIGN_CENTER, 0, -25);
    lv_obj_set_width(welcomeScreenObjs->loadPhase_lbl, 250);
    lv_obj_set_style_text_align(welcomeScreenObjs->loadPhase_lbl, LV_TEXT_ALIGN_CENTER, 0);
    lv_label_set_long_mode(welcomeScreenObjs->loadPhase_lbl, LV_LABEL_LONG_WRAP);
    lv_label_set_text(welcomeScreenObjs->loadPhase_lbl, MESSAGE_INIT_FILESYSTEM);

    lv_obj_add_style(welcomeScreenObjs->loadPhase_lbl, &style_textLabels, LV_STATE_DEFAULT);
    lv_obj_add_event_cb(welcomeScreenObjs->loadPhase_lbl, setStartupStatusLabel, LV_EVENT_REFRESH, NULL);

	///////////////////////////////////////////////////////////////////////////
	////////////////////////// FIRMWARE VERSION ///////////////////////////////
	///////////////////////////////////////////////////////////////////////////

	welcomeScreenObjs->hmiFWv_lbl = lv_label_create(parent);
	lv_obj_align(welcomeScreenObjs->hmiFWv_lbl, LV_ALIGN_CENTER, 0, HMI_FW_V_LABEL_Y_POSITION);
	lv_obj_set_width(welcomeScreenObjs->hmiFWv_lbl, 250);
	lv_obj_set_style_text_align(welcomeScreenObjs->hmiFWv_lbl, LV_TEXT_ALIGN_CENTER, 0);
	lv_label_set_long_mode(welcomeScreenObjs->hmiFWv_lbl, LV_LABEL_LONG_WRAP);
	// TODO: add firmware version
	lv_label_set_text(welcomeScreenObjs->hmiFWv_lbl, firmwareVerLabels[0]);

	///////////////////////////////////////////////////////////////////////////
	////////////////////////// DAQ FIRMWARE VERSION ///////////////////////////
	///////////////////////////////////////////////////////////////////////////

	welcomeScreenObjs->daqFWv_lbl = lv_label_create(parent);
	lv_obj_align(welcomeScreenObjs->daqFWv_lbl, LV_ALIGN_CENTER, 0, DAQ_FW_V_LABEL_Y_POSITION);
	lv_obj_set_width(welcomeScreenObjs->daqFWv_lbl, 250);
	lv_obj_set_style_text_align(welcomeScreenObjs->daqFWv_lbl, LV_TEXT_ALIGN_CENTER, 0);
	lv_label_set_long_mode(welcomeScreenObjs->daqFWv_lbl, LV_LABEL_LONG_WRAP);
	// TODO: add firmware version
	lv_label_set_text(welcomeScreenObjs->daqFWv_lbl, firmwareVerLabels[1]);

	///////////////////////////////////////////////////////////////////////////
	////////////////////////// FIRMWARE RELEASE ///////////////////////////////
	///////////////////////////////////////////////////////////////////////////

	welcomeScreenObjs->fwRel_lbl = lv_label_create(parent);
	lv_obj_align(welcomeScreenObjs->fwRel_lbl, LV_ALIGN_CENTER, 0, REL_FW_V_LABEL_Y_POSITION);
	lv_obj_set_width(welcomeScreenObjs->fwRel_lbl, 250);
	lv_obj_set_style_text_align(welcomeScreenObjs->fwRel_lbl, LV_TEXT_ALIGN_CENTER, 0);
	lv_label_set_long_mode(welcomeScreenObjs->fwRel_lbl, LV_LABEL_LONG_WRAP);
	// TODO: add firmware version
	lv_label_set_text(welcomeScreenObjs->fwRel_lbl, firmwareVerLabels[2]);
}

static void setBarValue(void* bar, int barValue)
{
    lv_bar_set_value(bar, barValue, LV_ANIM_OFF);
	lv_event_send(welcomeScreenObjs->loadPhase_lbl, LV_EVENT_REFRESH, &barValue);
}

static void setStartupStatusLabel(lv_event_t* event)
{
    lv_obj_t* obj = lv_event_get_target(event);

    int loadBarValue = (int)lv_bar_get_value(welcomeScreenObjs->progressBar_bar);

    if (loadBarValue <= 20){
    	lv_label_set_text(obj, MESSAGE_INIT_FILESYSTEM );
    }
    else if (loadBarValue <= 40) {
    	lv_label_set_text(obj, MESSAGE_INIT_CONFIGURATION );
    }
    else if (loadBarValue <= 60) {
    	lv_label_set_text(obj, MESSAGE_INIT_PERIPHERALS );
    }
    else if (loadBarValue <= 80) {
    	lv_label_set_text(obj, MESSAGE_INIT_GUI );
    }
    else {
    	lv_label_set_text(obj, MESSAGE_SYSTEM_STARTUP );
    }
}

/********************************* EOF ************************************************/
