﻿/**************************************************************************************
 *  \file       setupMeasureTab.c
 *
 *  \brief      File Brief Description
 *
 *  \details    File Detailed Description
 *
 *  \author     Bodini Andrea
 *
 *  \version    1.0
 *
 *  \date   7 gen 2022
 *
 *  \copyright  (C) Copyright 2021, Euromisure Sas di WIKA Italia Srl
***************************************************************************************/

/********************************** INCLUDE *******************************************/
#include <math.h>
#ifndef LVGL_SIMULATOR_ACTIVE
#include "wikaGraphic.h"
#include "esp_log.h"
#include "measure_conversion.h"
#else
#include "..\wika_graphic\wikaGraphic.h"
#include "..\measure_lib\measure_conversion.h"
#endif // !LVGL_SIMULATOR_ACTIVE

#include "guiDesign.h"

/********************************** LOCAL VARIABLES ***********************************/

#define TILE_PADDING_SIZE               0
#define HEAD_TITLE_PAD                  5
#define HEAD_TITLE_LEFT_PAD             20

#define VERTICAL_PADDING_BETWEEN_ELEMS  6

#define MAX_MAIN_LBLS_WIDTH             320
#define MAX_BAR_WIDTH                   291
#define MAX_BAR_HEIGHT                  18
#define MAX_STATUS_LBL_WIDTH            190

#define BAR_MIN_VALUE                   -100
#define BAR_MAX_VALUE                   +100

#define BTN12_TITLE_OFFS_X              18
#define BTN22_TITLE_OFFS_X              7
#define BTN32_TITLE_OFFS_X              18
#define BTN42_TITLE_OFFS_X              30
#define BTN52_TITLE_OFFS_X              55

#define BTNXX_TITLE_OFFS_Y              -12
#define BTNXX_VALUE_OFFS_Y              12
#define BTNXX_VALUE_PAD_RIGHT           -5

// Children ID mnemonics
#define CHILD_ID_LABEL_TITLE            0
#define CHILD_ID_LABEL_MAIN_VALUE       1
#define CHILD_ID_LABEL_MAIN_UNIT        2
#define CHILD_ID_BAR                    3
#define CHILD_ID_LABEL_FLUID            6
#define CHILD_ID_LABEL_CHANNEL          7
#define CHILD_ID_LED_STATUS             8
#define CHILD_ID_LABEL_STATUS           9
#define CHILD_ID_OBJ12                  10
#define CHILD_ID_OBJ22                  11
#define CHILD_ID_OBJ32                  12
#define CHILD_ID_OBJ42                  13
#define CHILD_ID_OBJ52                  14

#define BAR_CHILD_ID_ZERO_LINE          0
#define BAR_CHILD_ID_NEG_WARN_LINE      1
#define BAR_CHILD_ID_POS_WARN_LINE      2

// Variables containing stream setpoints value.
static int32_t lowFlowSpeedCutoff[STREAM_NUMBER];
static int32_t maxFlowSpeedWarnThresh[STREAM_NUMBER];
static int32_t maxFlowSpeed[STREAM_NUMBER];
static int32_t flowRateCutoff[STREAM_NUMBER];
static int32_t maxFlowRateWarnThresh[STREAM_NUMBER];
static int32_t maxFlowRate[STREAM_NUMBER];

/********************************** FUNCTION PROTOTYPES *******************************/
static void vSetupMeasureTab_measTile_init(measureTile_obj* objs);
void vSetupMeasureTab_enable_elements(lv_obj_t* object);
void vSetupMeasureTab_disable_elements(lv_obj_t* object);
void vSetupMeasureTab_set_btn_title(lv_obj_t* btn, const char* string);
void vSetupMeasureTab_update_btn_value(lv_obj_t* btn, const float value, const char* format);

// EVENTS CALLBACK
static void vSetupMeasureTab_set_elements(lv_event_t* event);
static void vSetupMeasureTab_update_data(lv_event_t* event);

/********************************** FUNCTIONS *****************************************/
void vSetupMeasureTab_init(measureTab_objs* measTab, lv_obj_t* parent, streamNumber_t stream)
{
    // Create measure type label
    measTab->tabScreen_tileView_container = lv_tileview_create(parent);

    // TILEVIEW TILE style
    lv_style_init(&measTab->style_tiles);

    lv_style_set_pad_all(&measTab->style_tiles, TILE_PADDING_SIZE);
    lv_style_set_bg_color(&measTab->style_tiles, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_LIGHT_GREY));
    lv_style_set_size(&measTab->style_tiles, lv_pct(100));
    lv_style_set_border_width(&measTab->style_tiles, 0);
    lv_style_set_base_dir(&measTab->style_tiles, LV_BASE_DIR_RTL);

    // SCROLLBAR style
    lv_style_init(&measTab->style_scrollbar);
    lv_style_set_bg_color(&measTab->style_scrollbar, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_BLUE));
    lv_style_set_bg_opa(&measTab->style_scrollbar, LV_OPA_COVER);
    lv_style_set_width(&measTab->style_scrollbar, 4);
    lv_style_set_pad_all(&measTab->style_scrollbar, 1);
    lv_style_set_border_side(&measTab->style_scrollbar, LV_BORDER_SIDE_NONE);

    // Add Styles to the objects
    lv_obj_add_style(measTab->tabScreen_tileView_container, &measTab->style_tiles, LV_PART_MAIN);
    lv_obj_add_style(measTab->tabScreen_tileView_container, &measTab->style_scrollbar, LV_PART_SCROLLBAR);
    lv_obj_add_style(measTab->tabScreen_tileView_container, &measTab->style_scrollbar, LV_STATE_SCROLLED | LV_PART_SCROLLBAR);

    // Tile 1 - Stream X
    measTab->tabScreen_tileView_tile1.obj_main_tileView_tile = lv_tileview_add_tile(measTab->tabScreen_tileView_container, 0, 0, LV_DIR_BOTTOM);
    vSetupMeasureTab_measTile_init(&measTab->tabScreen_tileView_tile1);

    // Tile 2 - Stream X
    measTab->tabScreen_tileView_tile2.obj_main_tileView_tile = lv_tileview_add_tile(measTab->tabScreen_tileView_container, 0, 1, LV_DIR_TOP);
    vSetupGraphTile_init(&measTab->tabScreen_tileView_tile2, stream);
}

static void vSetupMeasureTab_measTile_init(measureTile_obj* objs)
{
    // The tiles will be scrolled at the button click.
    lv_obj_set_scrollbar_mode(objs->obj_main_tileView_tile, LV_SCROLLBAR_MODE_OFF);
    lv_obj_clear_flag(objs->obj_main_tileView_tile, LV_OBJ_FLAG_SCROLLABLE);

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Header Label - Meter selected
    objs->lbl_title = lv_label_create(objs->obj_main_tileView_tile);

    lv_style_init(&objs->style_lbl_header);
    lv_style_set_text_font(&objs->style_lbl_header, &wika_montserrat_32_semibold);
    lv_style_set_text_align(&objs->style_lbl_header, LV_TEXT_ALIGN_LEFT);
    lv_style_set_text_color(&objs->style_lbl_header, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_BLACK));
    lv_style_set_text_opa(&objs->style_lbl_header, LV_OPA_COVER);
    lv_style_set_align(&objs->style_lbl_header, LV_ALIGN_TOP_LEFT);
    lv_style_set_x(&objs->style_lbl_header, HEAD_TITLE_LEFT_PAD);
    lv_style_set_y(&objs->style_lbl_header, HEAD_TITLE_PAD);
    lv_style_set_pad_bottom(&objs->style_lbl_header, HEAD_TITLE_PAD);
    lv_style_set_border_width(&objs->style_lbl_header, 2);
    lv_style_set_border_side(&objs->style_lbl_header, LV_BORDER_SIDE_BOTTOM);
    lv_style_set_width(&objs->style_lbl_header, MAX_MAIN_LBLS_WIDTH);

    lv_obj_add_style(objs->lbl_title, &objs->style_lbl_header, LV_PART_MAIN);

    lv_label_set_text(objs->lbl_title, "Msr");

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Create Main measurement label
    objs->lbl_main_indicator_value = lv_label_create(objs->obj_main_tileView_tile);

    lv_style_init(&objs->style_lbl_main_value);
    lv_style_set_text_font(&objs->style_lbl_main_value, &wika_dseg_64_regular);
    lv_style_set_text_align(&objs->style_lbl_main_value, LV_TEXT_ALIGN_RIGHT);
    lv_style_set_text_color(&objs->style_lbl_main_value, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_BLACK));
    lv_style_set_text_opa(&objs->style_lbl_main_value, LV_OPA_COVER);
    lv_style_set_width(&objs->style_lbl_main_value, MAX_MAIN_LBLS_WIDTH);
    lv_style_set_height(&objs->style_lbl_main_value, LV_SIZE_CONTENT);
    lv_style_set_max_width(&objs->style_lbl_main_value, MAX_MAIN_LBLS_WIDTH);

    lv_obj_add_style(objs->lbl_main_indicator_value, &objs->style_lbl_main_value, LV_PART_MAIN);

    lv_obj_align_to(objs->lbl_main_indicator_value, objs->lbl_title, LV_ALIGN_OUT_BOTTOM_LEFT, 0, VERTICAL_PADDING_BETWEEN_ELEMS);
    lv_label_set_text_fmt(objs->lbl_main_indicator_value, MAINFIELD_NOUNIT_PRINT_FORMAT, 0.0);
    lv_label_set_long_mode(objs->lbl_main_indicator_value, LV_LABEL_LONG_DOT);

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Create main measurement unit
    objs->lbl_main_indicator_unit = lv_label_create(objs->obj_main_tileView_tile);

    lv_style_init(&objs->style_lbl_main_unit);
    lv_style_set_text_font(&objs->style_lbl_main_unit, &wika_montserrat_24_semibold);
    lv_style_set_text_align(&objs->style_lbl_main_unit, LV_TEXT_ALIGN_RIGHT);
    lv_style_set_text_color(&objs->style_lbl_main_unit, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_BLACK));
    lv_style_set_text_opa(&objs->style_lbl_main_unit, LV_OPA_COVER);
    lv_style_set_height(&objs->style_lbl_main_unit, LV_SIZE_CONTENT);
    lv_style_set_width(&objs->style_lbl_main_unit, MAX_MAIN_LBLS_WIDTH);

    lv_obj_add_style(objs->lbl_main_indicator_unit, &objs->style_lbl_main_unit, LV_PART_MAIN);
    lv_obj_align_to(objs->lbl_main_indicator_unit, objs->lbl_main_indicator_value, LV_ALIGN_OUT_BOTTOM_RIGHT, 0, VERTICAL_PADDING_BETWEEN_ELEMS);
    lv_label_set_text(objs->lbl_main_indicator_unit, "m3/s");

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Create fluid bar
    lv_style_init(&objs->style_bar_item);
    lv_style_set_width(&objs->style_bar_item, MAX_BAR_WIDTH);
    lv_style_set_bg_color(&objs->style_bar_item, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_LIGHT_GREY));
    lv_style_set_bg_opa(&objs->style_bar_item, LV_OPA_COVER);
    lv_style_set_border_color(&objs->style_bar_item, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_BLACK));
    lv_style_set_border_width(&objs->style_bar_item, 2);
    lv_style_set_border_side(&objs->style_bar_item, LV_BORDER_SIDE_LEFT | LV_BORDER_SIDE_RIGHT);
    lv_style_set_pad_all(&objs->style_bar_item, 3);
    lv_style_set_height(&objs->style_bar_item, MAX_BAR_HEIGHT);
    lv_style_set_base_dir(&objs->style_bar_item, LV_BASE_DIR_LTR);

    lv_style_init(&objs->style_bar_bar_ok);
    lv_style_set_bg_color(&objs->style_bar_bar_ok, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_DARK_GREY));
    lv_style_set_bg_opa(&objs->style_bar_bar_ok, LV_OPA_COVER);
    lv_style_set_bg_img_src(&objs->style_bar_bar_ok, &img_skew_strip);
    lv_style_set_bg_img_tiled(&objs->style_bar_bar_ok, true);
    lv_style_set_bg_img_opa(&objs->style_bar_bar_ok, LV_OPA_30);

    lv_style_init(&objs->style_bar_bar_warn);
    lv_style_set_bg_color(&objs->style_bar_bar_warn, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_ORANGE));
    lv_style_set_bg_opa(&objs->style_bar_bar_warn, LV_OPA_COVER);
    lv_style_set_bg_img_src(&objs->style_bar_bar_warn, &img_skew_strip);
    lv_style_set_bg_img_tiled(&objs->style_bar_bar_warn, true);
    lv_style_set_bg_img_opa(&objs->style_bar_bar_warn, LV_OPA_30);

    lv_style_init(&objs->style_bar_bar_err);
    lv_style_set_bg_color(&objs->style_bar_bar_err, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_RED));
    lv_style_set_bg_opa(&objs->style_bar_bar_err, LV_OPA_COVER);
    lv_style_set_bg_img_src(&objs->style_bar_bar_err, &img_skew_strip);
    lv_style_set_bg_img_tiled(&objs->style_bar_bar_err, true);
    lv_style_set_bg_img_opa(&objs->style_bar_bar_err, LV_OPA_30);

    objs->bar_fluid_bar = lv_bar_create(objs->obj_main_tileView_tile);
    lv_bar_set_mode(objs->bar_fluid_bar, LV_SLIDER_MODE_RANGE);
    lv_bar_set_range(objs->bar_fluid_bar, BAR_MIN_VALUE, BAR_MAX_VALUE);
    lv_obj_remove_style_all(objs->bar_fluid_bar);
    lv_bar_set_value(objs->bar_fluid_bar, BAR_MAX_VALUE, LV_ANIM_ON);
    lv_bar_set_start_value(objs->bar_fluid_bar, BAR_MIN_VALUE, LV_ANIM_ON);

    lv_obj_align_to(objs->bar_fluid_bar, objs->lbl_main_indicator_unit, LV_ALIGN_OUT_BOTTOM_LEFT, 0, VERTICAL_PADDING_BETWEEN_ELEMS);

    lv_obj_add_style(objs->bar_fluid_bar, &objs->style_bar_item, LV_PART_MAIN);
    lv_obj_add_style(objs->bar_fluid_bar, &objs->style_bar_bar_ok, LV_PART_INDICATOR | LV_STATE_DEFAULT);
    lv_obj_add_style(objs->bar_fluid_bar, &objs->style_bar_bar_warn, LV_PART_INDICATOR | LV_STATE_USER_1);
    lv_obj_add_style(objs->bar_fluid_bar, &objs->style_bar_bar_err, LV_PART_INDICATOR | LV_STATE_USER_2);

    objs->line_zero_point = lv_line_create(objs->bar_fluid_bar);

    static lv_point_t zero_line_points[2] = { {0, 0}, {0, MAX_BAR_HEIGHT} };
    lv_line_set_points(objs->line_zero_point, zero_line_points, 2);
    lv_obj_align(objs->line_zero_point, LV_ALIGN_CENTER, 0, 0);

    lv_style_init(&objs->style_bar_lines);
    lv_style_set_line_color(&objs->style_bar_lines, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_BLACK));
    lv_style_set_line_width(&objs->style_bar_lines, 1);

    lv_obj_add_style(objs->line_zero_point, &objs->style_bar_lines, LV_PART_MAIN);

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Line
    lv_style_init(&objs->style_lines);

    lv_style_set_line_width(&objs->style_lines, 2);
    lv_style_set_line_color(&objs->style_lines, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_BLACK));
    lv_style_set_line_rounded(&objs->style_lines, true);

    objs->line_bar_detail_sep = lv_line_create(objs->obj_main_tileView_tile);

    static lv_point_t line_points[2] = { {0, 0}, {220, 0} };
    lv_line_set_points(objs->line_bar_detail_sep, line_points, 2);

    lv_obj_add_style(objs->line_bar_detail_sep, &objs->style_lines, LV_PART_MAIN);
    lv_obj_align_to(objs->line_bar_detail_sep, objs->bar_fluid_bar, LV_ALIGN_OUT_BOTTOM_LEFT, 0, VERTICAL_PADDING_BETWEEN_ELEMS);

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Stream details
    objs->ico_fluid = lv_img_create(objs->obj_main_tileView_tile);
    lv_img_set_src(objs->ico_fluid, &main_scr_fluid_img);
    lv_obj_align_to(objs->ico_fluid, objs->line_bar_detail_sep, LV_ALIGN_OUT_BOTTOM_LEFT, 0, VERTICAL_PADDING_BETWEEN_ELEMS);
    lv_obj_update_layout(objs->ico_fluid);

    // Init label style
    lv_style_init(&objs->style_lbl_details);
    lv_style_set_text_font(&objs->style_lbl_details, &wika_montserrat_18_semibold_symbols);
    lv_style_set_text_line_space(&objs->style_lbl_details, 0);
    lv_style_set_text_letter_space(&objs->style_lbl_details, 0);
    lv_style_set_max_height(&objs->style_lbl_details, LV_SIZE_CONTENT);
    lv_style_set_translate_y(&objs->style_lbl_details, -lv_font_montserrat_18.base_line / 2);

    // Fluid Label
    objs->lbl_fluid = lv_label_create(objs->obj_main_tileView_tile);

    lv_obj_align_to(objs->lbl_fluid, objs->ico_fluid, LV_ALIGN_OUT_RIGHT_MID, VERTICAL_PADDING_BETWEEN_ELEMS, 0);

    lv_obj_add_style(objs->lbl_fluid, &objs->style_lbl_details, LV_PART_MAIN);

    lv_label_set_text(objs->lbl_fluid, "*****");

    // Channel label
    objs->lbl_channel = lv_label_create(objs->obj_main_tileView_tile);
    lv_obj_align_to(objs->lbl_channel, objs->lbl_fluid, LV_ALIGN_OUT_BOTTOM_LEFT, 0, VERTICAL_PADDING_BETWEEN_ELEMS);
    lv_obj_add_style(objs->lbl_channel, &objs->style_lbl_details, LV_PART_MAIN);
    lv_label_set_text(objs->lbl_channel, "**********");

    // Stream status
    objs->led_status = lv_obj_create(objs->obj_main_tileView_tile);

    lv_obj_align_to(objs->led_status, objs->lbl_channel, LV_ALIGN_OUT_BOTTOM_LEFT, 0, VERTICAL_PADDING_BETWEEN_ELEMS);
    lv_obj_set_size(objs->led_status, lv_obj_get_width(objs->ico_fluid), lv_obj_get_height(objs->ico_fluid));
    lv_obj_set_x(objs->led_status, lv_obj_get_x(objs->ico_fluid));
    lv_obj_set_style_border_width(objs->led_status, 1, LV_PART_MAIN);
    lv_obj_set_style_border_color(objs->led_status, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_BLACK), LV_PART_MAIN);

    lv_style_init(&objs->style_led_stat_ok);
    lv_style_set_bg_color(&objs->style_led_stat_ok, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_GREEN));
    lv_style_set_bg_opa(&objs->style_led_stat_ok, LV_OPA_COVER);

    lv_style_init(&objs->style_led_stat_nok);
    lv_style_set_bg_color(&objs->style_led_stat_nok, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_RED));
    lv_style_set_bg_opa(&objs->style_led_stat_nok, LV_OPA_COVER);

    lv_style_init(&objs->style_led_stat_off);
    lv_style_set_bg_color(&objs->style_led_stat_off, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_DARK_GREY));
    lv_style_set_bg_opa(&objs->style_led_stat_off, LV_OPA_50);

    lv_obj_add_style(objs->led_status, &objs->style_led_stat_off, LV_PART_MAIN | LV_STATE_DISABLED);
    lv_obj_add_style(objs->led_status, &objs->style_led_stat_ok, LV_PART_MAIN | LV_STATE_USER_1);
    lv_obj_add_style(objs->led_status, &objs->style_led_stat_nok, LV_PART_MAIN | LV_STATE_USER_2);

    lv_obj_add_state(objs->led_status, LV_STATE_DISABLED);

    lv_obj_set_scrollbar_mode(objs->led_status, LV_SCROLLBAR_MODE_OFF);
    lv_obj_clear_flag(objs->led_status, LV_OBJ_FLAG_SCROLLABLE);

    lv_obj_set_style_pad_all(objs->led_status, 0, LV_PART_MAIN);

    objs->lbl_status = lv_label_create(objs->obj_main_tileView_tile);
    lv_obj_align_to(objs->lbl_status, objs->led_status, LV_ALIGN_OUT_RIGHT_TOP, 0, 0);
    lv_obj_set_x(objs->lbl_status, lv_obj_get_x(objs->lbl_fluid));
    lv_obj_add_style(objs->lbl_status, &objs->style_lbl_details, LV_PART_MAIN);
    lv_label_set_text(objs->lbl_status, "-----");
    lv_label_set_long_mode(objs->lbl_status, LV_LABEL_LONG_SCROLL_CIRCULAR);
    lv_obj_set_width(objs->lbl_status, MAX_STATUS_LBL_WIDTH);

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Right data containers
    // IMG12 ----------------------------------------------------------------------------------------
    objs->obj_12.img_bg = lv_img_create(objs->obj_main_tileView_tile);
    lv_img_set_src(objs->obj_12.img_bg, &btn12);
    lv_obj_align(objs->obj_12.img_bg, LV_ALIGN_TOP_RIGHT, 0, 0 * BTN_HEIGHT);

    objs->obj_12.lbl_title = lv_label_create(objs->obj_12.img_bg);
    lv_label_set_text(objs->obj_12.lbl_title, "Meas");

    objs->obj_12.lbl_value = lv_label_create(objs->obj_12.img_bg);
    lv_label_set_text_fmt(objs->obj_12.lbl_value, TEMPERATURE_PRINT_FORMAT, 0.0);

    // IMG22 ----------------------------------------------------------------------------------------
    objs->obj_22.img_bg = lv_img_create(objs->obj_main_tileView_tile);
    lv_img_set_src(objs->obj_22.img_bg, &btn22);
    lv_obj_align(objs->obj_22.img_bg, LV_ALIGN_TOP_RIGHT, 0, 1 * (BTN_HEIGHT + 2));

    objs->obj_22.lbl_title = lv_label_create(objs->obj_22.img_bg);
    lv_label_set_text(objs->obj_22.lbl_title, "Meas");

    objs->obj_22.lbl_value = lv_label_create(objs->obj_22.img_bg);
    lv_label_set_text_fmt(objs->obj_22.lbl_value, TEMPERATURE_PRINT_FORMAT, 0.0);

    // IMG32 ----------------------------------------------------------------------------------------
    objs->obj_32.img_bg = lv_img_create(objs->obj_main_tileView_tile);
    lv_img_set_src(objs->obj_32.img_bg, &btn32);
    lv_obj_align(objs->obj_32.img_bg, LV_ALIGN_TOP_RIGHT, 0, 2 * (BTN_HEIGHT + 2));

    objs->obj_32.lbl_title = lv_label_create(objs->obj_32.img_bg);
    lv_label_set_text(objs->obj_32.lbl_title, "Meas");

    objs->obj_32.lbl_value = lv_label_create(objs->obj_32.img_bg);
    lv_label_set_text_fmt(objs->obj_32.lbl_value, TEMPERATURE_PRINT_FORMAT, 0.0);

    // IMG42 ----------------------------------------------------------------------------------------
    objs->obj_42.img_bg = lv_img_create(objs->obj_main_tileView_tile);
    lv_img_set_src(objs->obj_42.img_bg, &btn42);
    lv_obj_align(objs->obj_42.img_bg, LV_ALIGN_TOP_RIGHT, 0, 3 * (BTN_HEIGHT + 2));

    objs->obj_42.lbl_title = lv_label_create(objs->obj_42.img_bg);
    lv_label_set_text(objs->obj_42.lbl_title, "Meas");

    objs->obj_42.lbl_value = lv_label_create(objs->obj_42.img_bg);
    lv_label_set_text_fmt(objs->obj_42.lbl_value, FLOWSPEED_MS_PRINT_FORMAT, 0.0);

    // IMG52 ----------------------------------------------------------------------------------------
    objs->obj_52.img_bg = lv_img_create(objs->obj_main_tileView_tile);
    lv_img_set_src(objs->obj_52.img_bg, &btn52);
    lv_obj_align(objs->obj_52.img_bg, LV_ALIGN_TOP_RIGHT, 0, 4 * (BTN_HEIGHT + 2));

    objs->obj_52.lbl_title = lv_label_create(objs->obj_52.img_bg);
    lv_label_set_text(objs->obj_52.lbl_title, "Meas");

    objs->obj_52.lbl_value = lv_label_create(objs->obj_52.img_bg);
    lv_label_set_text_fmt(objs->obj_52.lbl_value, TOTALIZATION_FLOW_M3_PRINT_FORMAT, 0);

    //-----------------------------------------------------------------------------------------------

    // Create styles
    // Disabled
    lv_style_init(&objs->style_img_bg_disabled);
    lv_style_set_text_opa(&objs->style_img_bg_disabled, LV_OPA_0);
    lv_style_set_img_opa(&objs->style_img_bg_disabled, LV_OPA_30);
    lv_style_set_border_side(&objs->style_img_bg_disabled, LV_BORDER_SIDE_NONE);

    // Default
    lv_style_init(&objs->style_img_bg);
    lv_style_set_text_opa(&objs->style_img_bg, LV_OPA_COVER);
    lv_style_set_img_opa(&objs->style_img_bg, LV_OPA_COVER);
    lv_style_set_border_side(&objs->style_img_bg, LV_BORDER_SIDE_NONE);
    lv_style_set_text_align(&objs->style_img_bg, LV_TEXT_ALIGN_RIGHT);
    lv_style_set_text_font(&objs->style_img_bg, &lv_font_montserrat_16);
    lv_style_set_text_color(&objs->style_img_bg, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_LIGHT_GREY));

    lv_style_init(&objs->style_lbl_title);
    lv_style_set_base_dir(&objs->style_lbl_title, LV_BASE_DIR_LTR);
    lv_style_set_text_letter_space(&objs->style_lbl_title, 0);
    lv_style_set_text_line_space(&objs->style_lbl_title, 0);
    lv_style_set_text_align(&objs->style_lbl_title, LV_TEXT_ALIGN_RIGHT);
    lv_style_set_text_font(&objs->style_lbl_title, &wika_montserrat_18_semibold_symbols);
    lv_style_set_text_color(&objs->style_lbl_title, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_WHITE));
    lv_style_set_align(&objs->style_lbl_title, LV_ALIGN_RIGHT_MID);
    lv_style_set_x(&objs->style_lbl_title, BTNXX_VALUE_PAD_RIGHT);
    lv_style_set_y(&objs->style_lbl_title, BTNXX_TITLE_OFFS_Y);

    lv_style_init(&objs->style_lbl_value);
    lv_style_set_base_dir(&objs->style_lbl_value, LV_BASE_DIR_LTR);
    lv_style_set_text_letter_space(&objs->style_lbl_value, 0);
    lv_style_set_text_line_space(&objs->style_lbl_value, 0);
    lv_style_set_align(&objs->style_lbl_value, LV_ALIGN_RIGHT_MID);
    lv_style_set_x(&objs->style_lbl_value, BTNXX_VALUE_PAD_RIGHT);
    lv_style_set_y(&objs->style_lbl_value, BTNXX_VALUE_OFFS_Y);
    lv_style_set_text_color(&objs->style_lbl_value, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_LIGHT_GREY));

    // Apply styles
    lv_obj_add_style(objs->obj_12.img_bg, &objs->style_img_bg, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_add_style(objs->obj_12.lbl_title, &objs->style_lbl_title, LV_PART_MAIN);
    lv_obj_add_style(objs->obj_12.lbl_value, &objs->style_lbl_value, LV_PART_MAIN);

    lv_obj_add_style(objs->obj_22.img_bg, &objs->style_img_bg, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_add_style(objs->obj_22.lbl_title, &objs->style_lbl_title, LV_PART_MAIN);
    lv_obj_add_style(objs->obj_22.lbl_value, &objs->style_lbl_value, LV_PART_MAIN);

    lv_obj_add_style(objs->obj_32.img_bg, &objs->style_img_bg, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_add_style(objs->obj_32.lbl_title, &objs->style_lbl_title, LV_PART_MAIN);
    lv_obj_add_style(objs->obj_32.lbl_value, &objs->style_lbl_value, LV_PART_MAIN);

    lv_obj_add_style(objs->obj_42.img_bg, &objs->style_img_bg, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_add_style(objs->obj_42.lbl_title, &objs->style_lbl_title, LV_PART_MAIN);
    lv_obj_add_style(objs->obj_42.lbl_value, &objs->style_lbl_value, LV_PART_MAIN);

    lv_obj_add_style(objs->obj_52.img_bg, &objs->style_img_bg, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_add_style(objs->obj_52.lbl_title, &objs->style_lbl_title, LV_PART_MAIN);
    lv_obj_add_style(objs->obj_52.lbl_value, &objs->style_lbl_value, LV_PART_MAIN);

    lv_obj_add_style(objs->obj_12.img_bg, &objs->style_img_bg_disabled, LV_PART_MAIN | LV_STATE_DISABLED);

    lv_obj_add_style(objs->obj_22.img_bg, &objs->style_img_bg_disabled, LV_PART_MAIN | LV_STATE_DISABLED);

    lv_obj_add_style(objs->obj_32.img_bg, &objs->style_img_bg_disabled, LV_PART_MAIN | LV_STATE_DISABLED);

    lv_obj_add_style(objs->obj_42.img_bg, &objs->style_img_bg_disabled, LV_PART_MAIN | LV_STATE_DISABLED);

    lv_obj_add_style(objs->obj_52.img_bg, &objs->style_img_bg_disabled, LV_PART_MAIN | LV_STATE_DISABLED);

    lv_obj_add_state(objs->obj_12.img_bg, LV_STATE_DISABLED);

    lv_obj_add_state(objs->obj_22.img_bg, LV_STATE_DISABLED);

    lv_obj_add_state(objs->obj_32.img_bg, LV_STATE_DISABLED);

    lv_obj_add_state(objs->obj_42.img_bg, LV_STATE_DISABLED);

    lv_obj_add_state(objs->obj_52.img_bg, LV_STATE_DISABLED);

    // Add the tile refresh callback
    lv_obj_add_event_cb(objs->obj_main_tileView_tile, vSetupMeasureTab_set_elements, LV_EVENT_VALUE_CHANGED, NULL);
    lv_obj_add_event_cb(objs->obj_main_tileView_tile, vSetupMeasureTab_update_data, LV_EVENT_REFRESH, NULL);

    lv_event_send(objs->obj_main_tileView_tile, LV_EVENT_VALUE_CHANGED, NULL);
}

static void vSetupMeasureTab_set_elements(lv_event_t* event)
{
    lv_obj_t* tileView = lv_event_get_target(event);

    tabScreenLbl_t curr_stream = tWikaGraphic_getCurrentTab();

    /////////////////////////////////////////////////////
    // Children ID:
    // 0 - lbl_title
    // 1 - lbl_main_indicator_value
    // 2 - lbl_main_indicator_unit
    // 3 - bar_fluid_bar
    // 4 - line_bar_detail_sep
    // 5 - ico_fluid
    // 6 - lbl_fluid
    // 7 - lbl_channel
    // 8 - led_status
    // 9 - lbl_status
    // 10- obj_12.img_bg
    // 11- obj_22.img_bg
    // 12- obj_32.img_bg
    // 13- obj_42.img_bg
    // 14- obj_52.img_bg
    /////////////////////////////////////////////////////

    lv_obj_t* lbl_title = lv_obj_get_child(tileView, CHILD_ID_LABEL_TITLE);
    lv_obj_t* main_indicator_unit = lv_obj_get_child(tileView, CHILD_ID_LABEL_MAIN_UNIT);

    MeasurementSession_t* curr_meas_t = tWikaConfigManager_getMeasurementSession();

    vWikaGraphic_ManageMeterLimits(curr_meas_t, curr_stream,
        &lowFlowSpeedCutoff[curr_stream], &maxFlowSpeedWarnThresh[curr_stream], &maxFlowSpeed[curr_stream],
        &flowRateCutoff[curr_stream], &maxFlowRateWarnThresh[curr_stream], &maxFlowRate[curr_stream]);

    lv_obj_t* bar = lv_obj_get_child(tileView, CHILD_ID_BAR);

    if (maxFlowRate[curr_stream] != 0) {
        lv_bar_set_range(bar, -maxFlowRate[curr_stream], maxFlowRate[curr_stream]);
    }

    if (curr_meas_t->streams[curr_stream].configuration.userMeasure[USER_MEASURE_ENERGY] == true) {
        // Energy Measurement Active
        lv_label_set_text(lbl_title, "Energy");
    }
    else if (curr_meas_t->streams[curr_stream].configuration.userMeasure[USER_MEASURE_MASSFLOW] == true) {
        // MassFlow Measurement Active
        lv_label_set_text(lbl_title, "MassFlow");
    }
    else if (curr_meas_t->streams[curr_stream].configuration.userMeasure[USER_MEASURE_FLOW] == true) {
        // Flow Measurement Active
        lv_label_set_text(lbl_title, "Flow");
        lv_label_set_text(main_indicator_unit, "l/h");

        lv_obj_t* tempObj = lv_obj_get_child(tileView, CHILD_ID_OBJ32);

        lv_obj_t* speedObj = lv_obj_get_child(tileView, CHILD_ID_OBJ42);

        lv_obj_t* totalizationObj = lv_obj_get_child(tileView, CHILD_ID_OBJ52);

        vSetupMeasureTab_enable_elements(tempObj);
        vSetupMeasureTab_enable_elements(speedObj);
        vSetupMeasureTab_enable_elements(totalizationObj);

        vSetupMeasureTab_set_btn_title(tempObj, "Temp");
        vSetupMeasureTab_set_btn_title(speedObj, "Fluid Speed");
        vSetupMeasureTab_set_btn_title(totalizationObj, "Totalization");

        lv_obj_t* channel = lv_obj_get_child(tileView, CHILD_ID_LABEL_CHANNEL);
        lv_obj_t* fluid = lv_obj_get_child(tileView, CHILD_ID_LABEL_FLUID);
        lv_obj_t* status = lv_obj_get_child(tileView, CHILD_ID_LABEL_STATUS);

        // Check if the Stream is enabled
        if (curr_meas_t->streams[curr_stream].enable == true) {
            lv_label_set_text_fmt(fluid, "%s", curr_meas_t->streams[curr_stream].parameters.externalEnvironment.fluid.Name);

            if (curr_meas_t->streams[curr_stream].configuration.dualChannelEnable == true) {
                lv_label_set_text(channel, "Dual Channel");
            }
            else {
                if (curr_meas_t->streams[curr_stream].configuration.mainChannel == 0) {
                    lv_label_set_text(channel, "Single Channel - Main");
                }
                else {
                    lv_label_set_text(channel, "Single Channel - Aux");
                }
            }
        }
        // If not, write disabled
        else {
            lv_label_set_text(channel, (char*)u8WikaGUI_getLabel(LABELS_GENERIC_DISABLED, DEFAULT_LANGUAGE));
            lv_label_set_text(fluid, "-----");
            lv_label_set_text(status, "-----");
        }
    }
}

static void vSetupMeasureTab_update_data(lv_event_t* event)
{
    static uint8_t current_error_id = 0;

    MeasurementData_t* guiData = lv_event_get_param(event);
    MeasurementSession_t* curr_meas_t = tWikaConfigManager_getMeasurementSession();

    lv_obj_t* tileView = lv_event_get_target(event);

    tabScreenLbl_t curr_stream = tWikaGraphic_getCurrentTab();

    lv_obj_t* main_value = lv_obj_get_child(tileView, CHILD_ID_LABEL_MAIN_VALUE);

    lv_obj_t* tempObj = lv_obj_get_child(tileView, CHILD_ID_OBJ32);
    lv_obj_t* speedObj = lv_obj_get_child(tileView, CHILD_ID_OBJ42);
    lv_obj_t* totalObj = lv_obj_get_child(tileView, CHILD_ID_OBJ52);

    lv_obj_t* status = lv_obj_get_child(tileView, CHILD_ID_LABEL_STATUS);
    lv_obj_t* led = lv_obj_get_child(tileView, CHILD_ID_LED_STATUS);
    lv_obj_t* bar = lv_obj_get_child(tileView, CHILD_ID_BAR);

    if (guiData->ussError != WIKA_NO_ERROR) {
        if (lv_obj_get_state(led) != LV_STATE_USER_2) {
            lv_obj_clear_state(led, LV_STATE_USER_1);
            lv_obj_add_state(led, LV_STATE_USER_2);
        }

        if (current_error_id != guiData->ussError) {
            lv_label_set_text_fmt(main_value, "Err%03d", guiData->ussError);
            lv_label_set_text(status, (char*)u8WikaHal_DAQ_decode_error_value(guiData->ussError));
            current_error_id = guiData->ussError;
        }
        vSetupMeasureTab_update_btn_value(speedObj, 0.0, FLUIDSPD_MS_SHORT_PRINT_FORMAT);
        vSetupMeasureTab_update_btn_value(tempObj, 0.0, TEMPERATURE_PRINT_FORMAT);
    }
    else {
        current_error_id = WIKA_NO_ERROR;

        if (lv_obj_get_state(led) != LV_STATE_USER_1) {
            lv_obj_clear_state(led, LV_STATE_USER_2);
            lv_obj_add_state(led, LV_STATE_USER_1);
            lv_label_set_text(status, "Measuring");
        }

        // Setup bar value
        if (guiData->Flow < 0) {
            lv_bar_set_value(bar, 0, LV_ANIM_ON);
            lv_bar_set_start_value(bar, (int32_t)M3S_TO_LH(guiData->Flow), LV_ANIM_ON);
        }
        else {
            lv_bar_set_value(bar, (int32_t)M3S_TO_LH(guiData->Flow), LV_ANIM_ON);
            lv_bar_set_start_value(bar, 0, LV_ANIM_ON);
        }

        if (abs(M3S_TO_LH(guiData->Flow)) > maxFlowRate[curr_stream]) {
            lv_obj_clear_state(bar, LV_STATE_USER_1);
            lv_obj_add_state(bar, LV_STATE_USER_2);
        }
        else if (abs(M3S_TO_LH(guiData->Flow)) > maxFlowRateWarnThresh[curr_stream]) {
            lv_obj_clear_state(bar, LV_STATE_USER_2);
            lv_obj_add_state(bar, LV_STATE_USER_1);
        }
        else {
            lv_obj_clear_state(bar, LV_STATE_USER_2);
            lv_obj_clear_state(bar, LV_STATE_USER_1);
        }

        if (curr_meas_t->streams[curr_stream].configuration.userMeasure[USER_MEASURE_ENERGY] == true) {
            // Energy Measurement
            lv_label_set_text_fmt(main_value, MAINFIELD_NOUNIT_PRINT_FORMAT, guiData->Energy);
        }
        else if (curr_meas_t->streams[curr_stream].configuration.userMeasure[USER_MEASURE_MASSFLOW] == true) {
            // MassFlow Measurement
            lv_label_set_text_fmt(main_value, MAINFIELD_NOUNIT_PRINT_FORMAT, guiData->MassFlow);
        }
        else if (curr_meas_t->streams[curr_stream].configuration.userMeasure[USER_MEASURE_FLOW] == true) {
            // Flow Measurement
            lv_label_set_text_fmt(main_value, MAINFIELD_NOUNIT_PRINT_FORMAT, M3S_TO_LH(guiData->Flow));
            vSetupMeasureTab_update_btn_value(speedObj, guiData->FluidSpeed, FLUIDSPD_MS_SHORT_PRINT_FORMAT);
            vSetupMeasureTab_update_btn_value(tempObj, guiData->fpcb_temperature, TEMPERATURE_PRINT_FORMAT);
        }
    }
}

inline void vSetupMeasureTab_set_btn_title(lv_obj_t* btn, const char* string)
{
    lv_obj_t* label = lv_obj_get_child(btn, 0);
    lv_label_set_text_fmt(label, string);
}

inline void vSetupMeasureTab_update_btn_value(lv_obj_t* btn, const float value, const char* format)
{
    lv_obj_t* label = lv_obj_get_child(btn, 1);
    lv_label_set_text_fmt(label, format, value);
}

inline void vSetupMeasureTab_enable_elements(lv_obj_t* object)
{
    uint8_t child_count = lv_obj_get_child_cnt(object);
    uint8_t counter = 0;
    for (counter = 0; counter < child_count; counter++) {
        lv_obj_clear_state(lv_obj_get_child(object, counter), LV_STATE_DISABLED);
    }
    lv_obj_clear_state(object, LV_STATE_DISABLED);
}

inline void vSetupMeasureTab_disable_elements(lv_obj_t* object)
{
    uint8_t child_count = lv_obj_get_child_cnt(object);
    uint8_t counter = 0;
    for (counter = 0; counter < child_count; counter++) {
        lv_obj_add_state(lv_obj_get_child(object, counter), LV_STATE_DISABLED);
    }
    lv_obj_add_state(object, LV_STATE_DISABLED);
}

/********************************* EOF ************************************************/
