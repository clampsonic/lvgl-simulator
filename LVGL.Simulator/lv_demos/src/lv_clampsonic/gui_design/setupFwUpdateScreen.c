﻿/**************************************************************************************
 *  \file       setupFwUpdateScreen.c
 *
 *  \brief      Methods for Firmware Update page
 *
 *  \details    File Detailed Description
 *
 *  \author     Bodini Andrea
 *
 *  \version    0.1
 *
 *  \date		25 mag 2022
 *
 *  \copyright  (C) Copyright 2021, Euromisure Sas di WIKA Italia Srl
***************************************************************************************/

/********************************** INCLUDE *******************************************/
#ifndef LVGL_SIMULATOR_ACTIVE
#include "wikaGraphic.h"
#else
#include "..\wika_graphic\wikaGraphic.h"
#endif // !LVGL_SIMULATOR_ACTIVE

#define MESSAGE_DOWNLOAD_FW	"Downloading Firmware\0"
#define MESSAGE_FLASHING_FW	"Flashing New Firmware\0"
#define MESSAGE_CHECKING_FW	"Checking New Firmware\0"
#define MESSAGE_COMPLETE_FW	"Update Process Completed!!\0"
#define MESSAGE_FAILED___FW	"Update Process Failed!!\0"
#define MESSAGE_UNKNOWN__FW	"WTF?!?!\0"



/********************************** LOCAL VARIABLES ***********************************/
static fwUpdateScreen_objs *fwUpdtScreenObjs;

/********************************** FUNCTION PROTOTYPES *******************************/
static void vFwUpdateScreen_setBarValue(lv_event_t* event);

static void vFwUpdateScreen_setStatusLabel(lv_event_t *event);

static void vFwUpdateScreen_setDataProgressLabel(lv_event_t* event);

/********************************** FUNCTIONS *****************************************/

void vFwUpdateScreen_init(fwUpdateScreen_objs *screenObjs, lv_obj_t *parent)
{
	fwUpdtScreenObjs = screenObjs;

	///////////////////////////////////////////////////////////////////////////
	//////////////////////// BACKGROUND IMAGE /////////////////////////////////
	///////////////////////////////////////////////////////////////////////////
	// Create the Logo
	fwUpdtScreenObjs->wika_logo_img = lv_img_create(parent);

	// Associate img object to the image structure
	lv_img_set_src(fwUpdtScreenObjs->wika_logo_img, &wika_logo);

	lv_obj_set_style_opa(fwUpdtScreenObjs->wika_logo_img, LV_OPA_COVER, LV_PART_MAIN);

	// Set position of the image
	lv_obj_set_pos(fwUpdtScreenObjs->wika_logo_img, 180, 51);
	lv_img_set_pivot(fwUpdtScreenObjs->wika_logo_img, 0, 0);
	lv_img_set_angle(fwUpdtScreenObjs->wika_logo_img, 0);

	///////////////////////////////////////////////////////////////////////////
	/////////////////////////// LOADING BAR ///////////////////////////////////
	///////////////////////////////////////////////////////////////////////////

	static lv_style_t style_loadBar;

	lv_style_init(&style_loadBar);

	lv_style_set_bg_img_src(&style_loadBar, &img_skew_strip);
	lv_style_set_bg_img_tiled(&style_loadBar, true);
	lv_style_set_bg_img_opa(&style_loadBar, LV_OPA_30);
	lv_style_set_bg_opa(&style_loadBar, LV_OPA_COVER);
	lv_style_set_bg_color(&style_loadBar, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_BLUE));

	// Create the bar
	fwUpdtScreenObjs->progressBar_bar = lv_bar_create(parent);
	lv_obj_add_style(fwUpdtScreenObjs->progressBar_bar, &style_loadBar, LV_PART_INDICATOR);

	lv_obj_set_size(fwUpdtScreenObjs->progressBar_bar, 260, 10);
	lv_obj_center(fwUpdtScreenObjs->progressBar_bar);
	lv_bar_set_mode(fwUpdtScreenObjs->progressBar_bar, LV_BAR_MODE_NORMAL);
	lv_bar_set_start_value(fwUpdtScreenObjs->progressBar_bar, 0, LV_ANIM_OFF);

	///////////////////////////////////////////////////////////////////////////
	//////////////////////// LOADING PHASE LABEL //////////////////////////////
	///////////////////////////////////////////////////////////////////////////

	static lv_style_t style_textLabels;
	lv_style_init(&style_textLabels);

	lv_style_set_text_font(&style_textLabels, &lv_font_montserrat_18);

	fwUpdtScreenObjs->fwUpdatePhase_lbl = lv_label_create(parent);
	lv_obj_align(fwUpdtScreenObjs->fwUpdatePhase_lbl, LV_ALIGN_CENTER, 0, -25);
	lv_obj_set_width(fwUpdtScreenObjs->fwUpdatePhase_lbl, 260);
	lv_obj_set_style_text_align(fwUpdtScreenObjs->fwUpdatePhase_lbl, LV_TEXT_ALIGN_CENTER, 0);
	lv_label_set_long_mode(fwUpdtScreenObjs->fwUpdatePhase_lbl, LV_LABEL_LONG_SCROLL_CIRCULAR);

	lv_obj_add_style(fwUpdtScreenObjs->fwUpdatePhase_lbl, &style_textLabels, LV_STATE_DEFAULT);

	lv_label_set_text(fwUpdtScreenObjs->fwUpdatePhase_lbl, "Firmware Update Starting");

	///////////////////////////////////////////////////////////////////////////
	////////////////////////// FIRMWARE UPLOAD STAT ///////////////////////////
	///////////////////////////////////////////////////////////////////////////

	fwUpdtScreenObjs->fwUpdateDataStats_lbl = lv_label_create(parent);
	lv_obj_align(fwUpdtScreenObjs->fwUpdateDataStats_lbl, LV_ALIGN_CENTER, 0, 25);
	lv_obj_set_width(fwUpdtScreenObjs->fwUpdateDataStats_lbl, 250);
	lv_obj_set_style_text_align(fwUpdtScreenObjs->fwUpdateDataStats_lbl, LV_TEXT_ALIGN_CENTER, 0);
	lv_label_set_long_mode(fwUpdtScreenObjs->fwUpdateDataStats_lbl, LV_LABEL_LONG_SCROLL_CIRCULAR);
	lv_obj_add_style(fwUpdtScreenObjs->fwUpdateDataStats_lbl, &style_textLabels, LV_STATE_DEFAULT);
	lv_label_set_text(fwUpdtScreenObjs->fwUpdateDataStats_lbl, "");

	///////////////////////////////////////////////////////////////////////////
	////////////////////////// CUSTOM MESSAGE /////////////////////////////////
	///////////////////////////////////////////////////////////////////////////
	fwUpdtScreenObjs->customMessage_lbl = lv_label_create(parent);
	lv_obj_align(fwUpdtScreenObjs->customMessage_lbl, LV_ALIGN_CENTER, 0, 60);
	lv_obj_set_width(fwUpdtScreenObjs->customMessage_lbl, LV_SIZE_CONTENT);
	lv_obj_set_style_text_align(fwUpdtScreenObjs->customMessage_lbl, LV_TEXT_ALIGN_CENTER, 0);
	lv_label_set_long_mode(fwUpdtScreenObjs->customMessage_lbl, LV_LABEL_LONG_SCROLL_CIRCULAR);
	lv_obj_add_style(fwUpdtScreenObjs->customMessage_lbl, &style_textLabels, LV_STATE_DEFAULT);
	lv_label_set_text(fwUpdtScreenObjs->customMessage_lbl, "PLEASE, DO NOT REMOVE POWER PLUG!");


	///////////////////////////////////////////////////////////////////////////
	////////////////////////// EVENT CALLBACKS ////////////////////////////////
	///////////////////////////////////////////////////////////////////////////
	lv_obj_add_event_cb(fwUpdtScreenObjs->fwUpdateDataStats_lbl, vFwUpdateScreen_setDataProgressLabel, LV_EVENT_REFRESH, NULL);
	lv_obj_add_event_cb(fwUpdtScreenObjs->fwUpdatePhase_lbl, vFwUpdateScreen_setStatusLabel, LV_EVENT_REFRESH, NULL);
	lv_obj_add_event_cb(fwUpdtScreenObjs->progressBar_bar, vFwUpdateScreen_setBarValue, LV_EVENT_REFRESH, NULL);
}

static void vFwUpdateScreen_setBarValue(lv_event_t *event)
{
	lv_obj_t *bar = lv_event_get_target(event);

	uint8_t *p_bar_value = lv_event_get_param(event);

    lv_bar_set_value(bar, *p_bar_value, LV_ANIM_OFF);
}

static void vFwUpdateScreen_setStatusLabel(lv_event_t *event)
{
    lv_obj_t *p_obj = lv_event_get_target(event);
    fwUpdatePhase_t *p_update_phase = lv_event_get_param(event);

    if(*p_update_phase == FW_UPDT_PHASE_DOWNLOAD_FIRMWARE){

    	lv_label_set_text(p_obj, MESSAGE_DOWNLOAD_FW);

    } else if(*p_update_phase == FW_UPDT_PHASE_COPY_DATA){

    	lv_label_set_text(p_obj, MESSAGE_FLASHING_FW);

    } else if(*p_update_phase == FW_UPDT_PHASE_CHECK_DATA){

    	lv_label_set_text(p_obj, MESSAGE_CHECKING_FW);

    } else if(*p_update_phase == FW_UPDT_PHASE_COMPLETED){

    	lv_label_set_text(p_obj, MESSAGE_COMPLETE_FW);

    } else if(*p_update_phase == FW_UPDT_PHASE_ERROR){

    	lv_label_set_text(p_obj, MESSAGE_FAILED___FW);

    } else {

    	lv_label_set_text(p_obj, MESSAGE_UNKNOWN__FW);

    }

}

static void vFwUpdateScreen_setDataProgressLabel(lv_event_t* event)
{
    lv_obj_t *p_obj = lv_event_get_target(event);
    fwUpdateDataStats_t *p_data_stats = lv_event_get_param(event);

    int itot_size_in_kB = (int) (p_data_stats->i32total_len / 1024);
    int icurr_size_in_kB = (int) (p_data_stats->i32curr_len / 1024);

    uint8_t u8data_stats_str [25];
    memset(&u8data_stats_str, 0, sizeof(u8data_stats_str));
    sprintf((char *) u8data_stats_str, "%d / %d kB", icurr_size_in_kB, itot_size_in_kB);

    if(p_data_stats->i32total_len != 0){
    	lv_label_set_text(p_obj, (char *) &u8data_stats_str[0]);
    } else {
    	lv_label_set_text(p_obj, "");
    }

}

/********************************* EOF ************************************************/
