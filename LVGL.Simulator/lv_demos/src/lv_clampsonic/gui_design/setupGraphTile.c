﻿/**************************************************************************************
 *  \file       setupGraphTile.c
 *  
 *  \brief      File Brief Description
 *  
 *  \details    File Detailed Description
 *  
 *  \author     ***
 *  \author     ***
 *  
 *  \version    0.0
 *  
 *  \date		4 feb 2022
 *  
 *  \copyright  (C) Copyright 2021, Euromisure Sas di WIKA Italia Srl
***************************************************************************************/

/********************************** INCLUDE *******************************************/
#ifndef LVGL_SIMULATOR_ACTIVE
#include "wikaGraphic.h"
#include "esp_log.h"
#else
#include "..\wika_graphic\wikaGraphic.h"
#endif // !LVGL_SIMULATOR_ACTIVE

#include "guiDesign.h"


/********************************** LOCAL VARIABLES ***********************************/
static graphTile_obj * localGraphTileObjs[2];

/********************************** FUNCTION PROTOTYPES *******************************/
static void updateChartData(lv_event_t * event);
static void updateStatData(lv_event_t * event);
static void statLabelsUpdateDimensions_cb(lv_event_t* event);

/********************************** FUNCTIONS *****************************************/
void vSetupGraphTile_init(graphTile_obj * tile_objs, streamNumber_t stream)
{
	localGraphTileObjs[stream] = tile_objs;
	lv_obj_set_style_pad_all(tile_objs->obj_main_tileView_tile, 0, LV_PART_MAIN);

	// Create the Chart as a child of the tileView container
	tile_objs->chart_UpsDnsWaveform = lv_chart_create(tile_objs->obj_main_tileView_tile);
	lv_obj_set_size(tile_objs->chart_UpsDnsWaveform, lv_pct(88), lv_pct(88));
	lv_obj_align(tile_objs->chart_UpsDnsWaveform, LV_ALIGN_TOP_RIGHT, 0, 0);
	lv_chart_set_range(tile_objs->chart_UpsDnsWaveform, LV_CHART_AXIS_PRIMARY_Y, -2048, 2048);

	lv_chart_set_type(tile_objs->chart_UpsDnsWaveform, LV_CHART_TYPE_SCATTER);
	lv_chart_set_update_mode(tile_objs->chart_UpsDnsWaveform, LV_CHART_UPDATE_MODE_CIRCULAR);

	// Do not display points on the chart
	lv_obj_set_style_size(tile_objs->chart_UpsDnsWaveform, 0, LV_PART_INDICATOR);

	// Set chart ticks
	lv_chart_set_axis_tick(tile_objs->chart_UpsDnsWaveform, LV_CHART_AXIS_PRIMARY_Y, 10, 5, 6, 5, true, 65);
	lv_chart_set_axis_tick(tile_objs->chart_UpsDnsWaveform, LV_CHART_AXIS_PRIMARY_X, 10, 5, 10, 1, true, 30);

	// Init the chart data series
	tile_objs->series_upsWaveform = lv_chart_add_series(tile_objs->chart_UpsDnsWaveform, lv_palette_main(LV_PALETTE_RED), LV_CHART_AXIS_PRIMARY_Y);
	tile_objs->series_dnsWaveform = lv_chart_add_series(tile_objs->chart_UpsDnsWaveform, lv_palette_main(LV_PALETTE_GREEN), LV_CHART_AXIS_PRIMARY_Y);

	lv_obj_add_event_cb(tile_objs->chart_UpsDnsWaveform, updateChartData, LV_EVENT_REFRESH, NULL);

	// Create the Legend.
	tile_objs->lbl_UpsLegend = lv_label_create(tile_objs->chart_UpsDnsWaveform);
	tile_objs->lbl_DnsLegend = lv_label_create(tile_objs->chart_UpsDnsWaveform);

	lv_obj_set_style_bg_color(tile_objs->lbl_UpsLegend, lv_palette_main(LV_PALETTE_RED), LV_PART_MAIN);
	lv_obj_set_style_bg_color(tile_objs->lbl_DnsLegend, lv_palette_main(LV_PALETTE_GREEN), LV_PART_MAIN);

	lv_style_init(&tile_objs->style_legend);

	lv_style_set_text_font(&tile_objs->style_legend, &lv_font_montserrat_16);
	lv_style_set_border_width(&tile_objs->style_legend, 0);
	lv_style_set_pad_all(&tile_objs->style_legend, 2);
	lv_style_set_bg_opa(&tile_objs->style_legend, LV_OPA_80);
	lv_style_set_text_color(&tile_objs->style_legend, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_WHITE));
	lv_style_set_height(&tile_objs->style_legend, LV_SIZE_CONTENT);
	lv_style_set_width(&tile_objs->style_legend, LV_SIZE_CONTENT);

	lv_obj_add_style(tile_objs->lbl_UpsLegend, &tile_objs->style_legend, LV_PART_MAIN);
	lv_obj_add_style(tile_objs->lbl_DnsLegend, &tile_objs->style_legend, LV_PART_MAIN);

	lv_label_set_text(tile_objs->lbl_UpsLegend, "UPS Plot");
	lv_label_set_text(tile_objs->lbl_DnsLegend, "DNS Plot");

	lv_obj_align(tile_objs->lbl_UpsLegend, LV_ALIGN_TOP_RIGHT, -5, 0);
	lv_obj_align_to(tile_objs->lbl_DnsLegend, tile_objs->lbl_UpsLegend, LV_ALIGN_OUT_BOTTOM_MID, 0, 5);

	// Create the stat box
	tile_objs->obj_statContainer = lv_obj_create(tile_objs->chart_UpsDnsWaveform);

	lv_obj_set_layout(tile_objs->obj_statContainer, LV_LAYOUT_FLEX);
	lv_obj_set_flex_flow(tile_objs->obj_statContainer, LV_FLEX_FLOW_ROW_WRAP);

	lv_obj_set_size(tile_objs->obj_statContainer, lv_pct(38), lv_pct(35));
	lv_obj_align(tile_objs->obj_statContainer, LV_ALIGN_BOTTOM_RIGHT, 0, 0);

	lv_obj_set_style_pad_all(tile_objs->obj_statContainer, 5, LV_PART_MAIN);

	lv_obj_set_style_pad_row(tile_objs->obj_statContainer, 2, LV_PART_MAIN);
	lv_obj_set_style_pad_column(tile_objs->obj_statContainer, 2, LV_PART_MAIN);

	lv_obj_set_style_bg_color(tile_objs->obj_statContainer, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_DARK_GREY), LV_PART_MAIN);
	lv_obj_set_style_bg_opa(tile_objs->obj_statContainer, LV_OPA_60, LV_PART_MAIN);
	lv_obj_set_style_border_width(tile_objs->obj_statContainer, 0, LV_PART_MAIN);

	lv_obj_set_scrollbar_mode(tile_objs->obj_statContainer, LV_SCROLLBAR_MODE_OFF);
	lv_obj_clear_flag(tile_objs->obj_statContainer, LV_OBJ_FLAG_SCROLLABLE);

	// Create the labels
	tile_objs->lbl_Temp = lv_label_create(tile_objs->obj_statContainer);
	tile_objs->lbl_Temp_val = lv_label_create(tile_objs->obj_statContainer);

	tile_objs->lbl_Gain = lv_label_create(tile_objs->obj_statContainer);
	tile_objs->lbl_Gain_val = lv_label_create(tile_objs->obj_statContainer);

	tile_objs->lbl_SigLvl = lv_label_create(tile_objs->obj_statContainer);
	tile_objs->lbl_SigLvl_val = lv_label_create(tile_objs->obj_statContainer);

	lv_label_set_text(tile_objs->lbl_Temp, "T [°C]:");
	lv_label_set_text(tile_objs->lbl_Gain, "G [dB]:");
	lv_label_set_text(tile_objs->lbl_SigLvl, "SLV [mV]:");

	lv_label_set_text(tile_objs->lbl_Temp_val, "0");
	lv_label_set_text(tile_objs->lbl_Gain_val, "0");
	lv_label_set_text(tile_objs->lbl_SigLvl_val, "0");

	lv_obj_set_style_text_align(tile_objs->lbl_Temp,LV_TEXT_ALIGN_LEFT, LV_PART_MAIN);
	lv_obj_set_style_text_align(tile_objs->lbl_Gain,LV_TEXT_ALIGN_LEFT, LV_PART_MAIN);
	lv_obj_set_style_text_align(tile_objs->lbl_SigLvl,LV_TEXT_ALIGN_LEFT, LV_PART_MAIN);

	lv_obj_set_style_text_align(tile_objs->lbl_Temp_val,LV_TEXT_ALIGN_RIGHT, LV_PART_MAIN);
	lv_obj_set_style_text_align(tile_objs->lbl_Gain_val,LV_TEXT_ALIGN_RIGHT, LV_PART_MAIN);
	lv_obj_set_style_text_align(tile_objs->lbl_SigLvl_val,LV_TEXT_ALIGN_RIGHT, LV_PART_MAIN);

	// Create the style
	lv_style_init(&tile_objs->style_StatText);
	lv_style_set_text_font(&tile_objs->style_StatText, &lv_font_montserrat_16);
	lv_style_set_text_color(&tile_objs->style_StatText, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_WHITE));
	lv_style_set_height(&tile_objs->style_StatText, LV_SIZE_CONTENT);
	lv_style_set_pad_all(&tile_objs->style_StatText, 0);

	lv_obj_add_style(tile_objs->lbl_Temp, &tile_objs->style_StatText, LV_PART_MAIN);
	lv_obj_add_style(tile_objs->lbl_Gain, &tile_objs->style_StatText, LV_PART_MAIN);
	lv_obj_add_style(tile_objs->lbl_SigLvl, &tile_objs->style_StatText, LV_PART_MAIN);

	lv_obj_add_style(tile_objs->lbl_Temp_val, &tile_objs->style_StatText, LV_PART_MAIN);
	lv_obj_add_style(tile_objs->lbl_Gain_val, &tile_objs->style_StatText, LV_PART_MAIN);
	lv_obj_add_style(tile_objs->lbl_SigLvl_val, &tile_objs->style_StatText, LV_PART_MAIN);

	lv_obj_add_event_cb(tile_objs->obj_statContainer, updateStatData, LV_EVENT_REFRESH, NULL);
	lv_obj_add_event_cb(tile_objs->obj_statContainer, statLabelsUpdateDimensions_cb, LV_EVENT_DRAW_MAIN_END, NULL);
}


static void statLabelsUpdateDimensions_cb(lv_event_t* event)
{
    lv_obj_t* target = lv_event_get_target(event);
    // Get Container Width
    lv_coord_t containerWidth = lv_obj_get_content_width(target);
    lv_coord_t width_head = 0;
    lv_coord_t width_value = 0;

    lv_obj_t* elementsList[][2] = {
        {localGraphTileObjs[0]->lbl_Temp	,   localGraphTileObjs[0]->lbl_Temp_val		},
		{localGraphTileObjs[0]->lbl_Gain	,   localGraphTileObjs[0]->lbl_Gain_val		},
		{localGraphTileObjs[0]->lbl_SigLvl	,   localGraphTileObjs[0]->lbl_SigLvl_val	},
		{localGraphTileObjs[1]->lbl_Temp	,   localGraphTileObjs[1]->lbl_Temp_val		},
		{localGraphTileObjs[1]->lbl_Gain	,   localGraphTileObjs[1]->lbl_Gain_val		},
		{localGraphTileObjs[1]->lbl_SigLvl	,   localGraphTileObjs[1]->lbl_SigLvl_val	},
    };

    uint8_t numberOfElements = (sizeof(elementsList)/sizeof(lv_obj_t *))/2;
    uint8_t counter;

    for (counter = 0; counter < numberOfElements; counter++) {
        width_head = lv_obj_get_width(elementsList[counter][0]);
        width_value = lv_obj_get_width(elementsList[counter][1]);

        if ((width_head + width_value + 3) != containerWidth)
        {
            lv_obj_set_width(elementsList[counter][1], (containerWidth - width_head - 3));
        }
        else
        {
        	lv_obj_set_style_text_opa(elementsList[counter][0], LV_OPA_COVER, LV_PART_MAIN);
			lv_obj_set_style_text_opa(elementsList[counter][1], LV_OPA_COVER, LV_PART_MAIN);
			lv_obj_remove_event_cb(target, statLabelsUpdateDimensions_cb);
        }
    }
}

static void updateStatData(lv_event_t * event)
{
	UssStreamStatPacket_t * localStatStruct = lv_event_get_param(event);

	lv_label_set_text_fmt(localGraphTileObjs[localStatStruct->streamNumber]->lbl_Temp_val, "%.2f", localStatStruct->ussStat.payload.temperature);
	lv_label_set_text_fmt(localGraphTileObjs[localStatStruct->streamNumber]->lbl_Gain_val, "%.2f", localStatStruct->ussStat.payload.gain);
	lv_label_set_text_fmt(localGraphTileObjs[localStatStruct->streamNumber]->lbl_SigLvl_val, "%.2f", localStatStruct->ussStat.payload.signalLevel);
}

static void updateChartData(lv_event_t * event)
{
	lv_obj_t * chartObj = lv_event_get_target(event);

	UssStreamPlot_t * localPlotStruct = lv_event_get_param(event);

#ifndef LVGL_SIMULATOR_ACTIVE
    uint16_t plotMaxLen = max(localPlotStruct->upsPlot.plotSize, localPlotStruct->dnsPlot.plotSize);
#else
    uint16_t plotMaxLen = localPlotStruct->upsPlot.plotSize;
#endif // !LVGL_SIMULATOR_ACTIVE	

	int16_t cnt = 0;

	uint8_t str = localPlotStruct->streamNumber;

	if(localPlotStruct->upsPlot.plotSize == localPlotStruct->dnsPlot.plotSize){
		lv_chart_set_point_count(chartObj, plotMaxLen);

		for(cnt = 0; cnt < plotMaxLen; cnt++){
			 lv_chart_set_next_value2(chartObj, localGraphTileObjs[str]->series_upsWaveform, cnt, (lv_coord_t) localPlotStruct->upsPlot.plotWaveform[cnt]);
			 lv_chart_set_next_value2(chartObj, localGraphTileObjs[str]->series_dnsWaveform, cnt, (lv_coord_t) localPlotStruct->dnsPlot.plotWaveform[cnt]);
		}

		lv_chart_set_range(chartObj, LV_CHART_AXIS_PRIMARY_X, 0, cnt);
	}

	lv_chart_refresh(chartObj);

}


/********************************* EOF ************************************************/
