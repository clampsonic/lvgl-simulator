﻿
#ifndef GUIDER_FONTS_H
#define GUIDER_FONTS_H
#ifdef __cplusplus
extern "C" {
#endif

    //#include "lv_font.h"

    LV_FONT_DECLARE(lv_font_ariblk_24)
        //LV_FONT_DECLARE(lv_font_simsun_12)
    LV_FONT_DECLARE(lv_font_ariblk_16)
    LV_FONT_DECLARE(lv_font_ariblk_12)
    LV_FONT_DECLARE(lv_font_arial_12)
    LV_FONT_DECLARE(lv_font_ariblk_14)
    LV_FONT_DECLARE(lv_font_ariblk_20)
    LV_FONT_DECLARE(lv_font_ariblk_26)
    LV_FONT_DECLARE(lv_font_ariblk_30)
    LV_FONT_DECLARE(lv_font_ariblk_18)

#ifdef __cplusplus
}
#endif
#endif
