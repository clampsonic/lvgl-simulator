﻿/**************************************************************************************
 *  \file       wikaGui_labels.c
 *
 *  \brief      File Containing all the GUI Labels
 *
 *  \details    This file contains all the labels in every supported language
 *              by the device.
 *
 *  \author     BodiniA
 *
 *  \version    0.0
 *
 *  \date		14 giu 2022
 *
 *  \copyright  (C) Copyright 2021, Euromisure Sas di WIKA Italia Srl
***************************************************************************************/

/********************************** INCLUDE *******************************************/
#include "../dictionary/wikaGui_labels.h"

/********************************** LOCAL VARIABLES ***********************************/
////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////// ANALOG INPUTS STRINGS ///////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
static wikaDictionaryLabel_t analog_inputs_t = {
    // langEN
    .langEN = "Analog Inputs\0",
    // langIT
    .langIT = "Input Analogici\0",
    // langDE
    .langDE = "Sticazzen\0"
};

////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////// CONNECTIVITY SECTION STRINGS ///////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
static wikaDictionaryLabel_t connectivity_t = {
    // langEN
    .langEN = "Connectivity\0",
    // langIT
    .langIT = "Connettività\0",
    // langDE
    .langDE = "Sticazzen\0"
};

////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////// MEASUREMENT SECTION STRINGS ///////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
static wikaDictionaryLabel_t measurement_t = {
    // langEN
    .langEN = "Measurement\0",
    // langIT
    .langIT = "Misura\0",
    // langDE
    .langDE = "Sticazzen\0"
};

static wikaDictionaryLabel_t measurement_speed_t = {
    // langEN
    .langEN = "Speed\0",
    // langIT
    .langIT = "Velocità\0",
    // langDE
    .langDE = "Sticazzen\0"
};

static wikaDictionaryLabel_t measurement_meter_t = {
    // langEN
    .langEN = "Meter:\0",
    // langIT
    .langIT = "Misure:\0",
    // langDE
    .langDE = "Sticazzen\0"
};

static wikaDictionaryLabel_t measurement_flow_t = {
    // langEN
    .langEN = "Flow\0",
    // langIT
    .langIT = "Portata\0",
    // langDE
    .langDE = "Sticazzen\0"
};

static wikaDictionaryLabel_t measurement_reynolds_corr_t = {
    // langEN
    .langEN = "Reynolds Corr.\0",
    // langIT
    .langIT = "Corr. Reynolds\0",
    // langDE
    .langDE = "Sticazzen\0"
};

static wikaDictionaryLabel_t measurement_mass_t = {
    // langEN
    .langEN = "Mass\0",
    // langIT
    .langIT = "Massa\0",
    // langDE
    .langDE = "Sticazzen\0"
};

static wikaDictionaryLabel_t measurement_volume_t = {
    // langEN
    .langEN = "Volume\0",
    // langIT
    .langIT = "Volume\0",
    // langDE
    .langDE = "Sticazzen\0"
};

static wikaDictionaryLabel_t measurement_massflow_t = {
    // langEN
    .langEN = "Massflow\0",
    // langIT
    .langIT = "Port. Massica\0",
    // langDE
    .langDE = "Sticazzen\0"
};

static wikaDictionaryLabel_t measurement_totalization_t = {
    // langEN
    .langEN = "Totalization\0",
    // langIT
    .langIT = "Totalizz.\0",
    // langDE
    .langDE = "Sticazzen\0"
};

static wikaDictionaryLabel_t measurement_accurate_t = {
    // langEN
    .langEN = "Accurate\0",
    // langIT
    .langIT = "Accurata\0",
    // langDE
    .langDE = "Sticazzen\0"
};

static wikaDictionaryLabel_t measurement_density_t = {
    // langEN
    .langEN = "Density\0",
    // langIT
    .langIT = "Densità\0",
    // langDE
    .langDE = "Sticazzen\0"
};


////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////// TIMING SECTION STRINGS //////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
static wikaDictionaryLabel_t timing_t = {
    // langEN
    .langEN = "Timing\0",
    // langIT
    .langIT = "Tempi\0",
    // langDE
    .langDE = "Sticazzen\0"
};

static wikaDictionaryLabel_t timing_tups_t = {
    // langEN
    .langEN = "tUPS\0",
    // langIT
    .langIT = "tUPS\0",
    // langDE
    .langDE = "Sticazzen\0"
};

static wikaDictionaryLabel_t timing_tdns_t = {
    // langEN
    .langEN = "tDNS\0",
    // langIT
    .langIT = "tDNS\0",
    // langDE
    .langDE = "Sticazzen\0"
};
static wikaDictionaryLabel_t timing_dtof_t = {
    // langEN
    .langEN = "DTOF\0",
    // langIT
    .langIT = "DTOF\0",
    // langDE
    .langDE = "Sticazzen\0"
};
static wikaDictionaryLabel_t timing_zero_flow_off_t = {
    // langEN
    .langEN = "Zero Offset\0",
    // langIT
    .langIT = "Zero Offset\0",
    // langDE
    .langDE = "Sticazzen\0"
};

////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////// SETPOINT SECTION STRINGS //////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
static wikaDictionaryLabel_t setpoint_t = {
    // langEN
    .langEN = "Setpoint\0",
    // langIT
    .langIT = "Soglie\0",
    // langDE
    .langDE = "Sticazzen\0"
};

static wikaDictionaryLabel_t setpoint_type_speed_t = {
    // langEN
    .langEN = "Speed\0",
    // langIT
    .langIT = "Velocità\0",
    // langDE
    .langDE = "Sticazzen\0"
};

static wikaDictionaryLabel_t setpoint_type_flowrate_t = {
    // langEN
    .langEN = "Flow\0",
    // langIT
    .langIT = "Portata\0",
    // langDE
    .langDE = "Sticazzen\0"
};

static wikaDictionaryLabel_t setpoint_cutoff_t = {
    // langEN
    .langEN = "Cutoff\0",
    // langIT
    .langIT = "Taglio\0",
    // langDE
    .langDE = "Sticazzen\0"
};

static wikaDictionaryLabel_t setpoint_warning_t = {
    // langEN
    .langEN = "Warning\0",
    // langIT
    .langIT = "Attenzione\0",
    // langDE
    .langDE = "Sticazzen\0"
};

static wikaDictionaryLabel_t setpoint_fullscale_t = {
    // langEN
    .langEN = "Fullscale\0",
    // langIT
    .langIT = "Fondo Scala\0",
    // langDE
    .langDE = "Sticazzen\0"
};

////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////// SIGNAL STATS SECTION STRINGS //////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////

static wikaDictionaryLabel_t signal_t = {
    // langEN
    .langEN = "Signal\0",
    // langIT
    .langIT = "Segnale\0",
    // langDE
    .langDE = "Sticazzen\0"
};

static wikaDictionaryLabel_t signal_level_t = {
    // langEN
    .langEN = "Signal Level\0",
    // langIT
    .langIT = "Ampiezza\0",
    // langDE
    .langDE = "Sticazzen\0"
};

static wikaDictionaryLabel_t signal_agc_gain_t = {
    // langEN
    .langEN = "AGC Gain\0",
    // langIT
    .langIT = "Amplif. AGC\0",
    // langDE
    .langDE = "Sticazzen\0"
};

static wikaDictionaryLabel_t signal_fluid_atten_t = {
    // langEN
    .langEN = "Fluid Atten.\0",
    // langIT
    .langIT = "Atten. Fluido\0",
    // langDE
    .langDE = "Sticazzen\0"
};

static wikaDictionaryLabel_t signal_quality_t = {
    // langEN
    .langEN = "Quality\0",
    // langIT
    .langIT = "Qualità\0",
    // langDE
    .langDE = "Sticazzen\0"
};

////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////// LINING INFO SECTION STRINGS ///////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
static wikaDictionaryLabel_t lining_t = {
    // langEN
    .langEN = "Lining\0",
    // langIT
    .langIT = "Rivestim.\0",
    // langDE
    .langDE = "Sticazzen\0"
};

////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////// FLUID INFO SECTION STRINGS ////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////

static wikaDictionaryLabel_t fluid_t = {
    // langEN
    .langEN = "Fluid\0",
    // langIT
    .langIT = "Fluido\0",
    // langDE
    .langDE = "Sticazzen\0"
};

////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////// PIPE INFO SECTION STRINGS /////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////

static wikaDictionaryLabel_t pipe_t = {
    // langEN
    .langEN = "Pipe\0",
    // langIT
    .langIT = "Tubazione\0",
    // langDE
    .langDE = "Sticazzen\0"
};


static wikaDictionaryLabel_t pipe_ext_diameter_t = {
    // langEN
    .langEN = "Ext. Diam.\0",
    // langIT
    .langIT = "Diam. Est.\0",
    // langDE
    .langDE = "Sticazzen\0"
};

////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////// PIPE INFO SECTION STRINGS /////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
static wikaDictionaryLabel_t sensor_t = {
    // langEN
    .langEN = "Sensor\0",
    // langIT
    .langIT = "Sensori\0",
    // langDE
    .langDE = "Sticazzen\0"
};

static wikaDictionaryLabel_t sensor_config_t = {
    // langEN
    .langEN = "Configuration\0",
    // langIT
    .langIT = "Configurazione\0",
    // langDE
    .langDE = "Sticazzen\0"
};

static wikaDictionaryLabel_t sensor_distance_t = {
    // langEN
    .langEN = "Distance\0",
    // langIT
    .langIT = "Distanza\0",
    // langDE
    .langDE = "Sticazzen\0"
};

static wikaDictionaryLabel_t sensor_frequency_t = {
    // langEN
    .langEN = "Frequency\0",
    // langIT
    .langIT = "Frequenza\0",
    // langDE
    .langDE = "Sticazzen\0"
};

////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////// ENERGY INFO SECTION STRINGS ///////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
static wikaDictionaryLabel_t energy_t = {
    // langEN
    .langEN = "Energy\0",
    // langIT
    .langIT = "Energia\0",
    // langDE
    .langDE = "Sticazzen\0"
};

static wikaDictionaryLabel_t energy_type_heater_t = {
    // langEN
    .langEN = "Heater\0",
    // langIT
    .langIT = "Riscaldam.",
    // langDE
    .langDE = "Sticazzen\0"
};

static wikaDictionaryLabel_t energy_type_cooler_t = {
    // langEN
    .langEN = "Cooler\0",
    // langIT
    .langIT = "Raffreddam.\0",
    // langDE
    .langDE = "Sticazzen\0"
};

static wikaDictionaryLabel_t energy_direction_cooler_t = {
    // langEN
    .langEN = "Direction\0",
    // langIT
    .langIT = "Direzione\0",
    // langDE
    .langDE = "Sticazzen\0"
};

static wikaDictionaryLabel_t energy_send_t = {
    // langEN
    .langEN = "Send\0",
    // langIT
    .langIT = "Mandata\0",
    // langDE
    .langDE = "Sticazzen\0"
};

static wikaDictionaryLabel_t energy_return_t = {
    // langEN
    .langEN = "Return\0",
    // langIT
    .langIT = "Ritorno\0",
    // langDE
    .langDE = "Sticazzen\0"
};

////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////// GENERIC LABELS STRINGS ////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
static wikaDictionaryLabel_t generic_type_t = {
    // langEN
    .langEN = "Type\0",
    // langIT
    .langIT = "Tipo\0",
    // langDE
    .langDE = "Sticazzen\0"
};

static wikaDictionaryLabel_t generic_temp_source_t = {
    // langEN
    .langEN = "Temp. Src\0",
    // langIT
    .langIT = "Sorg. Temp.\0",
    // langDE
    .langDE = "Sticazzen\0"
};

static wikaDictionaryLabel_t generic_temperature_t = {
    // langEN
    .langEN = "Temp.\0",
    // langIT
    .langIT = "Temp.\0",
    // langDE
    .langDE = "Sticazzen\0"
};

static wikaDictionaryLabel_t generic_pressure_t = {
    // langEN
    .langEN = "Press.\0",
    // langIT
    .langIT = "Press.\0",
    // langDE
    .langDE = "Sticazzen\0"
};

static wikaDictionaryLabel_t generic_ph_t = {
    // langEN
    .langEN = "PH\0",
    // langIT
    .langIT = "PH\0",
    // langDE
    .langDE = "PH\0"
};

static wikaDictionaryLabel_t generic_soundspeed_t = {
    // langEN
    .langEN = "Sound Speed\0",
    // langIT
    .langIT = "Vel. Sonica\0",
    // langDE
    .langDE = "Sticazzen\0"
};

static wikaDictionaryLabel_t generic_disabled_t = {
    // langEN
    .langEN = "Disabled\0",
    // langIT
    .langIT = "Disab.\0",
    // langDE
    .langDE = "Sticazzen\0"
};

static wikaDictionaryLabel_t generic_enabled_t = {
    // langEN
    .langEN = "Enabled\0",
    // langIT
    .langIT = "Abil.\0",
    // langDE
    .langDE = "Sticazzen\0"
};

static wikaDictionaryLabel_t generic_material_t = {
    // langEN
    .langEN = "Material\0",
    // langIT
    .langIT = "Materiale\0",
    // langDE
    .langDE = "Sticazzen\0"
};

static wikaDictionaryLabel_t generic_thickness_t = {
    // langEN
    .langEN = "Thickness\0",
    // langIT
    .langIT = "Spessore\0",
    // langDE
    .langDE = "Sticazzen\0"
};

static wikaDictionaryLabel_t generic_custom_t = {
    // langEN
    .langEN = "Custom\0",
    // langIT
    .langIT = "Custom\0",
    // langDE
    .langDE = "Sticazzen\0"
};

static wikaDictionaryLabel_t generic_unit_t = {
    // langEN
    .langEN = "Unit\0",
    // langIT
    .langIT = "Unità\0",
    // langDE
    .langDE = "Sticazzen\0"
};

static wikaDictionaryLabel_t generic_endpoints_t = {
    // langEN
    .langEN = "End Points\0",
    // langIT
    .langIT = "Estremi\0",
    // langDE
    .langDE = "Sticazzen\0"
};

static wikaDictionaryLabel_t generic_curr_read_t = {
    // langEN
    .langEN = "Curr.Read\0",
    // langIT
    .langIT = "Valore\0",
    // langDE
    .langDE = "Sticazzen\0"
};

static wikaDictionaryLabel_t generic_temp_source_ain1_t = {
    // langEN
    .langEN = "AIN1 - MAIN1\0"
};
static wikaDictionaryLabel_t generic_temp_source_ain2_t = {
    // langEN
    .langEN = "AIN2 - AUX1\0"
};
static wikaDictionaryLabel_t generic_temp_source_ain3_t = {
    // langEN
    .langEN = "AIN3 - MAIN2\0"
};
static wikaDictionaryLabel_t generic_temp_source_ain4_t = {
    // langEN
    .langEN = "AIN4 - AUX2\0"
};
static wikaDictionaryLabel_t generic_temp_source_usrdef_t = {
    // langEN
    .langEN = "USER DEF\0"
};

////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////

wikaDictionaryLabel_t* labelsPointer[] = {
    // Analog Section
    &analog_inputs_t,

    // Timing Section
    &timing_t,
    &timing_tups_t,
    &timing_tdns_t,
    &timing_dtof_t,
    &timing_zero_flow_off_t,

    // Connectivity Section
    &connectivity_t,

    // Measurement Section
    &measurement_t,
    &measurement_reynolds_corr_t,
    &measurement_meter_t,
    &measurement_speed_t,
    &measurement_flow_t,
    &measurement_volume_t,
    &measurement_mass_t,
    &measurement_massflow_t,
    &measurement_totalization_t,
    &measurement_density_t,
    &measurement_accurate_t,

    // Setpoint Section
    &setpoint_t,
    &setpoint_type_speed_t,
    &setpoint_type_flowrate_t,
    &setpoint_cutoff_t,
    &setpoint_warning_t,
    &setpoint_fullscale_t,

    // Lining Section
    &lining_t,
    &generic_material_t,
    &generic_thickness_t,

    // Signal Section
    &signal_t,
    &signal_level_t,
    &signal_agc_gain_t,
    &signal_fluid_atten_t,
    &signal_quality_t,

    // Fluid Section
    &fluid_t,

    // Pipe Section
    &pipe_t,
    &generic_material_t,
    &pipe_ext_diameter_t,
    &generic_thickness_t,

    // Sensor Section
    &sensor_t,
    &sensor_config_t,
    &sensor_distance_t,
    &sensor_frequency_t,

    // Energy Section
    &energy_t,
    &energy_type_cooler_t,
    &energy_type_heater_t,
    &energy_direction_cooler_t,
    &energy_send_t,
    &energy_return_t,

    // Generic Labels
    &generic_type_t,
    &generic_temp_source_t,
    &generic_temp_source_ain1_t,
    &generic_temp_source_ain2_t,
    &generic_temp_source_ain3_t,
    &generic_temp_source_ain4_t,
    &generic_temp_source_usrdef_t,
    &generic_temperature_t,
    &generic_soundspeed_t,
    &generic_disabled_t,
    &generic_enabled_t,
    &generic_pressure_t,
    &generic_ph_t,
    &generic_custom_t,
    &generic_unit_t,
    &generic_endpoints_t,
    &generic_curr_read_t,
};

/********************************** FUNCTION PROTOTYPES *******************************/


/********************************** FUNCTIONS *****************************************/
uint8_t* u8WikaGUI_getLabel(wikaGuiLabels_t label, wikaSupportedLang_t lang)
{
    switch (lang) {
    case langEN:
        return labelsPointer[label]->langEN;
    case langIT:
        return labelsPointer[label]->langIT;
    case langDE:
        return labelsPointer[label]->langDE;
    default:
        return labelsPointer[label]->langEN;
    }
}



/********************************* EOF ************************************************/
