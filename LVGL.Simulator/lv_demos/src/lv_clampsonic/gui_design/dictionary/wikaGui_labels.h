﻿/**************************************************************************************
 *  \file       wikaGui_labels.h
 *
 *  \brief      Header file containing all the labels used in the GUI
 *
 *  \details    This file is contains enums and the strings being used in the GUI,
 *              enabling the possibility to introduce multi-language support.
 *
 *  \author     BodiniA
 *
 *  \version    0.1
 *
 *  \date		15 jun 2022
 *
 *  \copyright  (C) Copyright 2022, Euromisure Sas di WIKA Italia Srl
***************************************************************************************/

#ifndef COMPONENTS_ULTRAFLOW_APPLICATION_GUI_GUI_DESIGN_LABELS_WIKAGUILABELS_H_
#define COMPONENTS_ULTRAFLOW_APPLICATION_GUI_GUI_DESIGN_LABELS_WIKAGUILABELS_H_

#include <stdio.h>
#ifndef LVGL_SIMULATOR_ACTIVE
#include "lvgl.h"
#include "wika_data_types.h"
#else
#include "lvgl/lvgl.h"
#include "../../wika_data_types/wika_data_types.h"
#endif // !LVGL_SIMULATOR_ACTIVE


/***************************************************************************
 ************* GENERIC DEFINITIONS *****************************************
 ***************************************************************************/

#define LABEL_MAX_LEN           20

typedef enum _LABELS_ {
    LABELS_ANALOGIN,
    LABELS_TIMING,
    LABELS_TIMING_TUPS,
    LABELS_TIMING_TDNS,
    LABELS_TIMING_DTOF,
    LABELS_TIMING_ZEROFLOW_OFF,
    LABELS_CONNECTIVITY,
    LABELS_MEASUREMENT,
    LABELS_MEASUREMENT_REYNOLDS,
    LABELS_MEASUREMENT_METER,
    LABELS_MEASUREMENT_SPEED,
    LABELS_MEASUREMENT_FLOW,
    LABELS_MEASUREMENT_VOLUME,
    LABELS_MEASUREMENT_MASS,
    LABELS_MEASUREMENT_MASSFLOW,
    LABELS_MEASUREMENT_TOTALIZATION,
    LABELS_MEASUREMENT_DENSITY,
    LABELS_MEASUREMENT_ACCURATE,
    LABELS_SETPOINT,
    LABELS_SETPOINT_TYPE_SPEED,
    LABELS_SETPOINT_TYPE_FLOW,
    LABELS_SETPOINT_CUTOFF,
    LABELS_SETPOINT_WARNING,
    LABELS_SETPOINT_FULLSCALE,
    LABELS_LINING,
    LABELS_LINING_MATERIAL,
    LABELS_LINING_THICKNESS,
    LABELS_SIGNAL,
    LABELS_SIGNAL_LEVEL,
    LABELS_SIGNAL_AGC_GAIN,
    LABELS_SIGNAL_FLUID_ATTEN,
    LABELS_SIGNAL_QUALITY,
    LABELS_FLUID,
    LABELS_PIPE,
    LABELS_PIPE_MATERIAL,
    LABELS_PIPE_EXT_DIAMETER,
    LABELS_PIPE_THICKNESS,
    LABELS_SENSOR,
    LABELS_SENSOR_CONFIGURATION,
    LABELS_SENSOR_DISTANCE,
    LABELS_SENSOR_FREQUENCY,
    LABELS_ENERGY,
    LABELS_ENERGY_COOLER,
    LABELS_ENERGY_HEATER,
    LABELS_ENERGY_DIRECTION,
    LABELS_ENERGY_SEND,
    LABELS_ENERGY_RETURN,
    LABELS_GENERIC_TYPE,
    LABELS_GENERIC_TEMP_SRC,
    LABELS_GENERIC_TEMP_SRC_AIN1,
    LABELS_GENERIC_TEMP_SRC_AIN2,
    LABELS_GENERIC_TEMP_SRC_AIN3,
    LABELS_GENERIC_TEMP_SRC_AIN4,
    LABELS_GENERIC_TEMP_SRC_USR,
    LABELS_GENERIC_TEMPERATURE,
    LABELS_GENERIC_SOUNDSPEED,
    LABELS_GENERIC_DISABLED,
    LABELS_GENERIC_ENABLED,
    LABELS_GENERIC_PRESSURE,
    LABELS_GENERIC_PH,
    LABELS_GENERIC_CUSTOM,
    LABELS_GENERIC_UNIT,
    LABELS_GENERIC_ENDPOINTS,
    LABELS_GENERIC_CURR_READ,
} wikaGuiLabels_t;

typedef struct _DICTIONARY_LABEL_STRUCT_ {
    // add field when increasing the number of supported languages
    uint8_t langEN[LABEL_MAX_LEN];
    uint8_t langIT[LABEL_MAX_LEN];
    uint8_t langDE[LABEL_MAX_LEN];
    uint8_t langFR[LABEL_MAX_LEN];
    uint8_t langES[LABEL_MAX_LEN];
} wikaDictionaryLabel_t;


uint8_t* u8WikaGUI_getLabel(wikaGuiLabels_t label, wikaSupportedLang_t lang);

#endif /* COMPONENTS_ULTRAFLOW_APPLICATION_GUI_GUI_DESIGN_LABELS_WIKAGUILABELS_H_ */
