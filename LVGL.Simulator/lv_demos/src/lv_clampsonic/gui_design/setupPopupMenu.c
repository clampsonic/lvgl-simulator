﻿/**************************************************************************************
 *  \file       setupPopupMenu.c
 *
 *  \brief      File Brief Description
 *
 *  \details    File Detailed Description
 *
 *  \author     Bodini Andrea
 *
 *  \version    0.0
 *
 *  \date		12 gen 2022
 *
 *  \copyright  (C) Copyright 2021, Euromisure Sas di WIKA Italia Srl
***************************************************************************************/

/********************************** INCLUDE *******************************************/
#ifndef LVGL_SIMULATOR_ACTIVE
#include "wikaGraphic.h"
#include "wikaConfigManager.h"
#include "wika_data_types.h"
#include "wikaOs.h"
#include "wikaGraphic.h"
#include "measure_conversion.h"
#else
#include "..\wika_graphic\wikaGraphic.h"
#include "..\config_manager\wikaConfigManager.h"
#include "..\wika_data_types\wika_data_types.h"
#include "..\measure_lib\measure_conversion.h"
#endif // !LVGL_SIMULATOR_ACTIVE
#include "guiDesign.h"


#define SCROLL_DELTA	200



/********************************** LOCAL VARIABLES ***********************************/
//wifiPopupActive_objs wifiPopupActiveObjs;
//wifiPopupStatic_objs wifiPopupStaticObjs;


static gui_objs* guiObjs;

static guiHelper_objs*      guiHelpersObjs;
static envRecapPopup_objs*  envRecapPopUpObjs;
static startStopMeasurePopup_objs* ssDaqPopUpObjs;

/********************************** FUNCTION PROTOTYPES *******************************/
// Standard PopUp Callbacks
static void envRecapPopupKeyEvent_cb(lv_event_t* event);
static void ssDaqPopupKeyEvent_cb(lv_event_t* event);
static void closePopupEvt_cb(lv_event_t* event);

// Specific Popup Setup Functions
static void setupEnvRecapPopUp(lv_obj_t* parent, uint8_t streamNumber);
static void envPopupUpdateDimensions_cb(lv_event_t* event);

static void setupStartStopMeasurePopUp(lv_obj_t * parent, startStopMeasPopup_t cmd);

//static void opa_anim(void* bg, lv_anim_value_t v);
//static void wifiSw_event_cb(lv_obj_t* btn, lv_event_t evt);
//static void wifiMode_event_cb(lv_obj_t* btn, lv_event_t evt);
//static void bluetoothSw_event_cb(lv_obj_t* btn, lv_event_t evt);
//static void applyBtn_event_cb(lv_obj_t* btn, lv_event_t evt);
//
//static void close_ready_cb(lv_anim_t* a);
//static void start_auto_close(lv_obj_t* obj, uint16_t delay, uint16_t animTime);


/********************************** FUNCTIONS *****************************************/
void setupPopUpStructures(gui_objs * guiObjects) {
    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    // PopUp generic Style
    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    lv_style_init(&guiObjects->style_genPopUp);
    lv_style_set_bg_color(&guiObjects->style_genPopUp, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_LIGHT_GREY));
    lv_style_set_border_color(&guiObjects->style_genPopUp, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_BLUE));
    lv_style_set_border_width(&guiObjects->style_genPopUp, 3);
    lv_style_set_bg_opa(&guiObjects->style_genPopUp, LV_OPA_COVER);
    lv_style_set_shadow_width(&guiObjects->style_genPopUp, 55);
    lv_style_set_shadow_color(&guiObjects->style_genPopUp, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_WHITE));
    lv_style_set_shadow_spread(&guiObjects->style_genPopUp, 55);
    lv_style_set_shadow_opa(&guiObjects->style_genPopUp, LV_OPA_80);
    lv_style_set_pad_all(&guiObjects->style_genPopUp, 0);
    lv_style_set_align(&guiObjects->style_genPopUp, LV_ALIGN_CENTER);
    lv_style_set_width(&guiObjects->style_genPopUp, lv_pct(90));
    lv_style_set_height(&guiObjects->style_genPopUp, lv_pct(95));

    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Close button Styles and properties
    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    lv_style_init(&guiObjects->style_closeBtn);
    lv_style_set_radius(&guiObjects->style_closeBtn, LV_RADIUS_CIRCLE);
    lv_style_set_bg_color(&guiObjects->style_closeBtn, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_BLUE));
    //lv_style_set_bg_img_src(&guiObjects->style_closeBtn, LV_SYMBOL_CLOSE);

    lv_style_set_bg_opa(&guiObjects->style_closeBtn, LV_OPA_100);
    lv_style_set_bg_color(&guiObjects->style_closeBtn, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_BLUE));

    lv_style_set_border_opa(&guiObjects->style_closeBtn, LV_OPA_40);
    lv_style_set_border_width(&guiObjects->style_closeBtn, 2);
    lv_style_set_border_color(&guiObjects->style_closeBtn, lv_palette_main(LV_PALETTE_GREY));

    lv_style_set_shadow_width(&guiObjects->style_closeBtn, 8);
    lv_style_set_shadow_color(&guiObjects->style_closeBtn, lv_palette_main(LV_PALETTE_GREY));
    lv_style_set_shadow_ofs_y(&guiObjects->style_closeBtn, 8);

    lv_style_set_outline_opa(&guiObjects->style_closeBtn, LV_OPA_COVER);
    lv_style_set_outline_color(&guiObjects->style_closeBtn, lv_palette_main(LV_PALETTE_BLUE));

    lv_style_set_text_color(&guiObjects->style_closeBtn, lv_color_white());
    lv_style_set_pad_all(&guiObjects->style_closeBtn, 10);

    /*Init the pressed style*/
    lv_style_init(&guiObjects->style_closeBtn_pr);

    /*Ad a large outline when pressed*/
    lv_style_set_outline_width(&guiObjects->style_closeBtn_pr, 10);
    lv_style_set_outline_opa(&guiObjects->style_closeBtn_pr, LV_OPA_TRANSP);

    lv_style_set_bg_color(&guiObjects->style_closeBtn_pr, lv_palette_darken(LV_PALETTE_BLUE, 2));
    lv_style_set_bg_grad_color(&guiObjects->style_closeBtn_pr, lv_palette_darken(LV_PALETTE_BLUE, 4));

    /*Add a transition to the the outline*/
    static lv_style_transition_dsc_t trans;
    static lv_style_prop_t props[] = { LV_STYLE_OUTLINE_WIDTH, LV_STYLE_OUTLINE_OPA, 0 };
    lv_style_transition_dsc_init(&trans, props, lv_anim_path_linear, 500, 0, NULL);

    lv_style_set_transition(&guiObjects->style_closeBtn_pr, &trans);

    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Title Style
    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    lv_style_init(&guiObjects->style_popupTitle);
    lv_style_set_text_align(&guiObjects->style_popupTitle, LV_TEXT_ALIGN_CENTER);
    lv_style_set_text_font(&guiObjects->style_popupTitle, &lv_font_montserrat_22);
    lv_style_set_text_color(&guiObjects->style_popupTitle, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_WHITE));
    lv_style_set_bg_color(&guiObjects->style_popupTitle, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_BLUE));
    lv_style_set_bg_opa(&guiObjects->style_popupTitle, LV_OPA_COVER);
    lv_style_set_width(&guiObjects->style_popupTitle, lv_pct(100));
    lv_style_set_height(&guiObjects->style_popupTitle, LV_SIZE_CONTENT);
    lv_style_set_pad_all(&guiObjects->style_popupTitle, 5);
    lv_style_set_border_width(&guiObjects->style_popupTitle, 0);
    lv_style_set_align(&guiObjects->style_popupTitle, LV_ALIGN_TOP_MID);
    //lv_style_set_radius(&guiObjects->style_popupTitle, 0);

    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Data Container
    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    lv_style_init(&guiObjects->style_dataContainer);
    lv_style_set_bg_color(&guiObjects->style_dataContainer, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_WHITE));
    lv_style_set_bg_opa(&guiObjects->style_dataContainer, LV_OPA_0);
    lv_style_set_width(&guiObjects->style_dataContainer, lv_pct(100));
    lv_style_set_height(&guiObjects->style_dataContainer, lv_pct(76));
    lv_style_set_pad_all(&guiObjects->style_dataContainer, 4);
    lv_style_set_pad_right(&guiObjects->style_dataContainer, 12);
    lv_style_set_border_width(&guiObjects->style_dataContainer, 0);
    lv_style_set_align(&guiObjects->style_dataContainer, LV_ALIGN_CENTER);
    lv_style_set_translate_y(&guiObjects->style_dataContainer, 0);
    lv_style_set_radius(&guiObjects->style_dataContainer, 0);

    lv_style_set_layout(&guiObjects->style_dataContainer, LV_LAYOUT_FLEX);
    lv_style_set_flex_flow(&guiObjects->style_dataContainer, LV_FLEX_FLOW_ROW_WRAP);
    lv_style_set_flex_grow(&guiObjects->style_dataContainer, 0);
    lv_style_set_pad_row(&guiObjects->style_dataContainer, 2);
    lv_style_set_pad_column(&guiObjects->style_dataContainer, 2);
}

void createGenericPopUp(gui_objs * guiStruct, guiAppScreensLabels_t screenLbl, uint8_t streamNumber)
{
    guiObjs = guiStruct;
    guiHelpersObjs = &guiObjs->guiHelpersObjs;
    envRecapPopUpObjs = &guiObjs->envRecapPopUpObjs;
    ssDaqPopUpObjs = &guiObjs->ssDaqPopUpObjs;

    static guiAppScreensLabels_t lastScreen = 0;

    // Clear all previously started animations
    lv_anim_del_all();

    // CREATE LABEL CONTAINER
	guiObjs->popup_genericPopup = lv_obj_create(guiObjs->screen_Main);

	lv_obj_set_scrollbar_mode(guiObjs->popup_genericPopup, LV_SCROLLBAR_MODE_OFF);
	lv_obj_clear_flag(guiObjs->popup_genericPopup, LV_OBJ_FLAG_SCROLLABLE);

	lv_obj_add_style(guiObjs->popup_genericPopup, &guiObjs->style_genPopUp, LV_PART_MAIN);

	// Init Popup Objects

	// TITLE LABEL
	guiObjs->lbl_popUpTitle = lv_label_create(guiObjs->popup_genericPopup);
	lv_obj_add_style(guiObjs->lbl_popUpTitle, &guiObjs->style_popupTitle, LV_PART_MAIN);

	// DATA CONTAINER
	guiObjs->obj_dataContainer = lv_obj_create(guiObjs->popup_genericPopup);
	lv_obj_add_style(guiObjs->obj_dataContainer, &guiObjs->style_dataContainer, LV_PART_MAIN);

    // SCROLLBAR
	static lv_style_t style_scrollBar;
	lv_style_init(&style_scrollBar);

	lv_style_set_bg_color(&style_scrollBar, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_BLUE));

    switch (screenLbl) {
    case GUI_SCR_LBL_POPUP_ENVIRONMENT:

    	lv_obj_set_flex_align(guiObjs->obj_dataContainer, LV_FLEX_ALIGN_CENTER, LV_FLEX_ALIGN_SPACE_AROUND, LV_FLEX_ALIGN_START);

    	lv_obj_set_scrollbar_mode(guiObjs->obj_dataContainer, LV_SCROLLBAR_MODE_AUTO);

		lv_obj_add_style(guiObjs->obj_dataContainer, &style_scrollBar, LV_PART_SCROLLBAR );

    	lv_label_set_text_fmt(guiObjs->lbl_popUpTitle, "Stream %d Parameters", (streamNumber + 1));

    	setupEnvRecapPopUp(guiObjs->obj_dataContainer, streamNumber);

        // Set Background Symbol for the button
		lv_style_set_bg_img_src(&guiObjs->style_closeBtn, LV_SYMBOL_CLOSE);

		// Key management event
		lv_obj_add_event_cb(guiObjs->popup_genericPopup, envRecapPopupKeyEvent_cb, LV_EVENT_KEY, NULL);
        break;
    case GUI_SCR_LBL_POPUP_NETWORK:

    	lv_label_set_text(guiObjs->lbl_popUpTitle, "Network Parameters");

    	break;
    case GUI_SCR_LBL_POPUP_STOP_MEASUREMENT:
    case GUI_SCR_LBL_POPUP_START_MEASUREMENT:

    	lv_obj_set_flex_align(guiObjs->obj_dataContainer, LV_FLEX_ALIGN_CENTER, LV_FLEX_ALIGN_SPACE_EVENLY, LV_FLEX_ALIGN_CENTER);

		// Remove the scrollbar
		lv_obj_set_scrollbar_mode(guiObjs->obj_dataContainer, LV_SCROLLBAR_MODE_OFF);
		lv_obj_clear_flag(guiObjs->obj_dataContainer, LV_OBJ_FLAG_SCROLLABLE);

    	lv_label_set_text_fmt(guiObjs->lbl_popUpTitle, "Stream %d DAQ Manager", (streamNumber + 1));

    	if(screenLbl == GUI_SCR_LBL_POPUP_START_MEASUREMENT)
    	{
    		setupStartStopMeasurePopUp(guiObjs->obj_dataContainer, SS_POPUP_START_MEASURE);
    	}
    	else
    	{
    		setupStartStopMeasurePopUp(guiObjs->obj_dataContainer, SS_POPUP_STOP_MEASURE);
    	}

    	lastScreen = screenLbl;

    	// Key management event
		lv_obj_add_event_cb(guiObjs->popup_genericPopup, ssDaqPopupKeyEvent_cb, LV_EVENT_KEY, &lastScreen);

    	// Set Background Symbol for the button
		lv_style_set_bg_img_src(&guiObjs->style_closeBtn, LV_SYMBOL_OK);
		break;
    default:
    	break;
    }

    // CLOSE POPUP BUTTON
    guiObjs->btn_closePopUp = lv_btn_create(guiObjs->popup_genericPopup);

    // Key management Section ********************************************
    // Remove all the component associated to the input device and
    // add only the PopUp as a receiver.
    lv_group_remove_all_objs(guiHelpersObjs->guiGroup);

    // TODO: check if needed
    lv_group_add_obj(guiHelpersObjs->guiGroup, guiObjs->popup_genericPopup);
    //
    lv_group_add_obj(guiHelpersObjs->guiGroup, guiObjs->btn_closePopUp);
    //********************************************************************

    // Enable events bubbling
    lv_obj_add_flag(guiObjs->btn_closePopUp, LV_OBJ_FLAG_EVENT_BUBBLE);

    lv_obj_remove_style_all(guiObjs->btn_closePopUp);
    lv_obj_add_style(guiObjs->btn_closePopUp, &guiObjs->style_closeBtn, 0);
    lv_obj_add_style(guiObjs->btn_closePopUp, &guiObjs->style_closeBtn_pr, LV_STATE_PRESSED);
    lv_obj_set_size(guiObjs->btn_closePopUp, LV_SIZE_CONTENT, LV_SIZE_CONTENT);

    lv_obj_align(guiObjs->btn_closePopUp, LV_ALIGN_BOTTOM_MID, 0, -8);

	// Button Clicked event
	lv_obj_add_event_cb(guiObjs->popup_genericPopup, closePopupEvt_cb, LV_EVENT_CLICKED, NULL);
}

/***************************************************************************************
************** POPUP STANDARD CALLBACKS ************************************************
****************************************************************************************/

static void closePopupEvt_cb(lv_event_t* event)
{
    /*The original target of the event. Can be the buttons or the container*/
    lv_obj_t* target = lv_event_get_target(event);

    /*The current target is always the container as the event is added to it*/
    lv_obj_t* cont = lv_event_get_current_target(event);

    /*If container was clicked do nothing*/
    if (target == cont) return;

    lv_obj_del_delayed(cont, 550);
}


static void envRecapPopupKeyEvent_cb(lv_event_t* event)
{
    const uint32_t* key_event;
    key_event = lv_event_get_param(event);
    lv_coord_t currScroll = lv_obj_get_scroll_y(guiObjs->obj_dataContainer);

    switch (*key_event)
    {
    case LV_KEY_ENTER:
    case LV_KEY_BACKSPACE:
        lv_obj_add_state(guiObjs->btn_closePopUp, LV_STATE_PRESSED);
        lv_event_send(guiObjs->btn_closePopUp, LV_EVENT_CLICKED, NULL);
        lv_event_send(guiObjs->mainScreenObjs.tabView, LV_EVENT_REFRESH, NULL);
        // send led event
        //tWikaOs_sendLedEvent(EVENT_TAP_CENTER);
        break;
    case LV_KEY_UP:
    	lv_obj_scroll_to_y(guiObjs->obj_dataContainer, (currScroll - SCROLL_DELTA ), LV_ANIM_OFF);
    	// send led event
		//tWikaOs_sendLedEvent(EVENT_SOUTH_2_NORTH);
    	break;
    case LV_KEY_DOWN:
    	lv_obj_scroll_to_y(guiObjs->obj_dataContainer, (currScroll + SCROLL_DELTA ), LV_ANIM_OFF);
    	// send led event
		//tWikaOs_sendLedEvent(EVENT_NORTH_2_SOUTH);
    	break;
    }
}

static void ssDaqPopupKeyEvent_cb(lv_event_t* event)
{
    const uint32_t* key_event = lv_event_get_param(event);

    int16_t tab = lv_tabview_get_tab_act(guiObjs->mainScreenObjs.tabView);

    guiAppScreensLabels_t * currScreen = lv_event_get_user_data(event);

    //EventBits_t eventToSend = 0;

    switch (*key_event)
    {
    case LV_KEY_ENTER:
    case LV_KEY_BACKSPACE:
        lv_obj_add_state(guiObjs->btn_closePopUp, LV_STATE_PRESSED);

        lv_event_send(guiObjs->btn_closePopUp, LV_EVENT_CLICKED, NULL);
        lv_event_send(guiObjs->mainScreenObjs.tabView, LV_EVENT_REFRESH, NULL);

        // send led event
        //tWikaOs_sendLedEvent(EVENT_TAP_CENTER);

        if(*currScreen == GUI_SCR_LBL_POPUP_START_MEASUREMENT)
        {
        	if(tab == 0)
        	{
        		//eventToSend = EVENT_SYS_FSM_START_MEAS_STREAM1;
        	}
        	else
        	{
        		//eventToSend = EVENT_SYS_FSM_START_MEAS_STREAM2;
        	}
        }
        else if(*currScreen == GUI_SCR_LBL_POPUP_STOP_MEASUREMENT)
        {
        	if(tab == 0)
			{
				//eventToSend = EVENT_SYS_FSM_STOP_MEAS_STREAM1;
			}
			else
			{
				//eventToSend = EVENT_SYS_FSM_STOP_MEAS_STREAM2;
			}
        }
        // send system event
        //tWikaOs_sendEventToSystem(eventToSend);
        break;
    case LV_KEY_DOWN:
    	lv_event_send(guiObjs->btn_closePopUp, LV_EVENT_CLICKED, NULL);
		lv_event_send(guiObjs->mainScreenObjs.tabView, LV_EVENT_REFRESH, NULL);
  //  	// send led event
		//tWikaOs_sendLedEvent(EVENT_SOUTH_2_NORTH);
    	break;
    }
}

/***************************************************************************************
************** POPUP SETUP FUNCTIONS ***************************************************
****************************************************************************************/

static void setupEnvRecapPopUp(lv_obj_t* parent, uint8_t streamNumber) {
    MeasurementSession_t* localMeasSession = tWikaConfigManager_getMeasurementSession();

    // Label containers
    lv_style_init(&envRecapPopUpObjs->style_label_continers);
    lv_style_set_width(&envRecapPopUpObjs->style_label_continers, lv_pct(100));
    lv_style_set_height(&envRecapPopUpObjs->style_label_continers, LV_SIZE_CONTENT);
    lv_style_set_radius(&envRecapPopUpObjs->style_label_continers, 0);
    lv_style_set_pad_all(&envRecapPopUpObjs->style_label_continers, 0);
    lv_style_set_pad_top(&envRecapPopUpObjs->style_label_continers, 1);
    lv_style_set_pad_bottom(&envRecapPopUpObjs->style_label_continers, 4);
    lv_style_set_bg_color(&envRecapPopUpObjs->style_label_continers, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_DARK_GREY));
    lv_style_set_bg_opa(&envRecapPopUpObjs->style_label_continers, LV_OPA_0);

    lv_style_set_border_color(&envRecapPopUpObjs->style_label_continers, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_DARK_GREY));
    lv_style_set_border_width(&envRecapPopUpObjs->style_label_continers, 2);
    lv_style_set_border_side(&envRecapPopUpObjs->style_label_continers, LV_BORDER_SIDE_BOTTOM);

    lv_style_set_layout(&envRecapPopUpObjs->style_label_continers, LV_LAYOUT_FLEX);
	lv_style_set_flex_flow(&envRecapPopUpObjs->style_label_continers, LV_FLEX_FLOW_ROW_WRAP);
	lv_style_set_flex_grow(&envRecapPopUpObjs->style_label_continers, 0);
	lv_style_set_pad_row(&envRecapPopUpObjs->style_label_continers, 0);
	lv_style_set_pad_column(&envRecapPopUpObjs->style_label_continers, 2);

    // Label head style init
    lv_style_init(&envRecapPopUpObjs->style_text_head);

    lv_style_set_text_align(&envRecapPopUpObjs->style_text_head, LV_TEXT_ALIGN_LEFT);
    lv_style_set_text_font(&envRecapPopUpObjs->style_text_head, &lv_font_montserrat_18);
    lv_style_set_height(&envRecapPopUpObjs->style_text_head, LV_SIZE_CONTENT);
    lv_style_set_width(&envRecapPopUpObjs->style_text_head, LV_SIZE_CONTENT);
    lv_style_set_bg_color(&envRecapPopUpObjs->style_text_head, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_BLUE));
    lv_style_set_bg_opa(&envRecapPopUpObjs->style_text_head, LV_OPA_0);
    lv_style_set_text_opa(&envRecapPopUpObjs->style_text_head, LV_OPA_0);

    // Label value style init
    lv_style_init(&envRecapPopUpObjs->style_text_value);
    lv_style_set_text_font(&envRecapPopUpObjs->style_text_value, &lv_font_montserrat_18);
    lv_style_set_text_align(&envRecapPopUpObjs->style_text_value, LV_TEXT_ALIGN_RIGHT);
    lv_style_set_height(&envRecapPopUpObjs->style_text_value, LV_SIZE_CONTENT);
    lv_style_set_width(&envRecapPopUpObjs->style_text_value, LV_SIZE_CONTENT);
    lv_style_set_bg_color(&envRecapPopUpObjs->style_text_value, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_BLUE));
    lv_style_set_bg_opa(&envRecapPopUpObjs->style_text_value, LV_OPA_0);
    lv_style_set_text_opa(&envRecapPopUpObjs->style_text_value, LV_OPA_0);

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////// PIPE PROPERTIES /////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Pipe Material
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    envRecapPopUpObjs->lbl_containers[0] = lv_obj_create(parent);
    lv_obj_add_style(envRecapPopUpObjs->lbl_containers[0], &envRecapPopUpObjs->style_label_continers, LV_PART_MAIN);

    envRecapPopUpObjs->lbl_pipeMaterial_head = lv_label_create(envRecapPopUpObjs->lbl_containers[0]);
    lv_label_set_text(envRecapPopUpObjs->lbl_pipeMaterial_head, "Pipe Material:");
    lv_obj_add_style(envRecapPopUpObjs->lbl_pipeMaterial_head, &envRecapPopUpObjs->style_text_head, LV_PART_MAIN);

    envRecapPopUpObjs->lbl_pipeMaterial_value = lv_label_create(envRecapPopUpObjs->lbl_containers[0]);
    lv_label_set_text(envRecapPopUpObjs->lbl_pipeMaterial_value, (char *) localMeasSession->streams[streamNumber].parameters.externalEnvironment.pipe.Material);
    lv_label_set_long_mode(envRecapPopUpObjs->lbl_pipeMaterial_value, LV_LABEL_LONG_SCROLL_CIRCULAR);
    lv_obj_add_style(envRecapPopUpObjs->lbl_pipeMaterial_value, &envRecapPopUpObjs->style_text_value, LV_PART_MAIN);

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Pipe Schedule Label
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    envRecapPopUpObjs->lbl_containers[1] = lv_obj_create(parent);
	lv_obj_add_style(envRecapPopUpObjs->lbl_containers[1], &envRecapPopUpObjs->style_label_continers, LV_PART_MAIN);

    envRecapPopUpObjs->lbl_pipeScheduleLbl_head = lv_label_create(envRecapPopUpObjs->lbl_containers[1]);
    lv_label_set_text(envRecapPopUpObjs->lbl_pipeScheduleLbl_head, "Schedule Label:");
    lv_obj_add_style(envRecapPopUpObjs->lbl_pipeScheduleLbl_head, &envRecapPopUpObjs->style_text_head, LV_PART_MAIN);

    envRecapPopUpObjs->lbl_pipeScheduleLbl_value = lv_label_create(envRecapPopUpObjs->lbl_containers[1]);
    lv_label_set_text(envRecapPopUpObjs->lbl_pipeScheduleLbl_value, (char *) localMeasSession->streams[streamNumber].parameters.externalEnvironment.pipe.pipeSize);
    lv_label_set_long_mode(envRecapPopUpObjs->lbl_pipeScheduleLbl_value, LV_LABEL_LONG_SCROLL_CIRCULAR);
    lv_obj_add_style(envRecapPopUpObjs->lbl_pipeScheduleLbl_value, &envRecapPopUpObjs->style_text_value, LV_PART_MAIN);

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Pipe Schedule Number
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    envRecapPopUpObjs->lbl_containers[2] = lv_obj_create(parent);
	lv_obj_add_style(envRecapPopUpObjs->lbl_containers[2], &envRecapPopUpObjs->style_label_continers, LV_PART_MAIN);

    envRecapPopUpObjs->lbl_pipeScheduleNum_head = lv_label_create(envRecapPopUpObjs->lbl_containers[2]);
    lv_label_set_text(envRecapPopUpObjs->lbl_pipeScheduleNum_head, "Schedule Number:");
    lv_obj_add_style(envRecapPopUpObjs->lbl_pipeScheduleNum_head, &envRecapPopUpObjs->style_text_head, LV_PART_MAIN);

    envRecapPopUpObjs->lbl_pipeScheduleNum_value = lv_label_create(envRecapPopUpObjs->lbl_containers[2]);
    lv_label_set_text(envRecapPopUpObjs->lbl_pipeScheduleNum_value, (char *) localMeasSession->streams[streamNumber].parameters.externalEnvironment.pipe.scheduleNo);
    lv_label_set_long_mode(envRecapPopUpObjs->lbl_pipeScheduleNum_value, LV_LABEL_LONG_SCROLL_CIRCULAR);
    lv_obj_add_style(envRecapPopUpObjs->lbl_pipeScheduleNum_value, &envRecapPopUpObjs->style_text_value, LV_PART_MAIN);

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Pipe Diameter
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    envRecapPopUpObjs->lbl_containers[3] = lv_obj_create(parent);
	lv_obj_add_style(envRecapPopUpObjs->lbl_containers[3], &envRecapPopUpObjs->style_label_continers, LV_PART_MAIN);

    envRecapPopUpObjs->lbl_pipeDiameter_head = lv_label_create(envRecapPopUpObjs->lbl_containers[3]);
    lv_label_set_text(envRecapPopUpObjs->lbl_pipeDiameter_head, "Pipe Ext. Diameter:");
    lv_obj_add_style(envRecapPopUpObjs->lbl_pipeDiameter_head, &envRecapPopUpObjs->style_text_head, LV_PART_MAIN);

    envRecapPopUpObjs->lbl_pipeDiameter_value = lv_label_create(envRecapPopUpObjs->lbl_containers[3]);
    lv_label_set_text_fmt(envRecapPopUpObjs->lbl_pipeDiameter_value,"%.2f [mm]", (float) M_TO_MM(localMeasSession->streams[streamNumber].parameters.externalEnvironment.pipe.DiameterInMt));
    lv_label_set_long_mode(envRecapPopUpObjs->lbl_pipeDiameter_value, LV_LABEL_LONG_SCROLL_CIRCULAR);
    lv_obj_add_style(envRecapPopUpObjs->lbl_pipeDiameter_value, &envRecapPopUpObjs->style_text_value, LV_PART_MAIN);

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Pipe Thickness
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    envRecapPopUpObjs->lbl_containers[4] = lv_obj_create(parent);
	lv_obj_add_style(envRecapPopUpObjs->lbl_containers[4], &envRecapPopUpObjs->style_label_continers, LV_PART_MAIN);

    envRecapPopUpObjs->lbl_pipeThickness_head = lv_label_create(envRecapPopUpObjs->lbl_containers[4]);
    lv_label_set_text(envRecapPopUpObjs->lbl_pipeThickness_head, "Pipe Thickness:");
    lv_obj_add_style(envRecapPopUpObjs->lbl_pipeThickness_head, &envRecapPopUpObjs->style_text_head, LV_PART_MAIN);

    envRecapPopUpObjs->lbl_pipeThickness_value = lv_label_create(envRecapPopUpObjs->lbl_containers[4]);
    lv_label_set_text_fmt(envRecapPopUpObjs->lbl_pipeThickness_value, "%.2f [mm]", (float) M_TO_MM(localMeasSession->streams[streamNumber].parameters.externalEnvironment.pipe.ThicknessInMt));
    lv_label_set_long_mode(envRecapPopUpObjs->lbl_pipeThickness_value, LV_LABEL_LONG_SCROLL_CIRCULAR);
    lv_obj_add_style(envRecapPopUpObjs->lbl_pipeThickness_value, &envRecapPopUpObjs->style_text_value, LV_PART_MAIN);

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Pipe SoundSpeed
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    envRecapPopUpObjs->lbl_containers[5] = lv_obj_create(parent);
	lv_obj_add_style(envRecapPopUpObjs->lbl_containers[5], &envRecapPopUpObjs->style_label_continers, LV_PART_MAIN);

    envRecapPopUpObjs->lbl_pipeSoundSpeed_head = lv_label_create(envRecapPopUpObjs->lbl_containers[5]);
    lv_label_set_text(envRecapPopUpObjs->lbl_pipeSoundSpeed_head, "Pipe Sound Speed:");
    lv_obj_add_style(envRecapPopUpObjs->lbl_pipeSoundSpeed_head, &envRecapPopUpObjs->style_text_head, LV_PART_MAIN);

    envRecapPopUpObjs->lbl_pipeSoundSpeed_value = lv_label_create(envRecapPopUpObjs->lbl_containers[5]);
    lv_label_set_text_fmt(envRecapPopUpObjs->lbl_pipeSoundSpeed_value, "%.2f [m/s]", (float) localMeasSession->streams[streamNumber].parameters.externalEnvironment.pipe.SoundSpeed);
    lv_label_set_long_mode(envRecapPopUpObjs->lbl_pipeSoundSpeed_value, LV_LABEL_LONG_SCROLL_CIRCULAR);
    lv_obj_add_style(envRecapPopUpObjs->lbl_pipeSoundSpeed_value, &envRecapPopUpObjs->style_text_value, LV_PART_MAIN);

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////// LINING PROPERTIES ///////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Lining Material
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    envRecapPopUpObjs->lbl_containers[6] = lv_obj_create(parent);
	lv_obj_add_style(envRecapPopUpObjs->lbl_containers[6], &envRecapPopUpObjs->style_label_continers, LV_PART_MAIN);

	envRecapPopUpObjs->lbl_liningMaterial_head = lv_label_create(envRecapPopUpObjs->lbl_containers[6]);
    lv_label_set_text(envRecapPopUpObjs->lbl_liningMaterial_head, "Lining Material:");
    lv_obj_add_style(envRecapPopUpObjs->lbl_liningMaterial_head, &envRecapPopUpObjs->style_text_head, LV_PART_MAIN);

    envRecapPopUpObjs->lbl_liningMaterial_value = lv_label_create(envRecapPopUpObjs->lbl_containers[6]);

    if (localMeasSession->streams[streamNumber].parameters.externalEnvironment.lining.Material == 0) {
        lv_label_set_text(envRecapPopUpObjs->lbl_liningMaterial_value, "None");
    }
    else {
        lv_label_set_text_fmt(envRecapPopUpObjs->lbl_liningMaterial_value,"%d",localMeasSession->streams[streamNumber].parameters.externalEnvironment.lining.Material);
    }
    lv_label_set_long_mode(envRecapPopUpObjs->lbl_liningMaterial_value, LV_LABEL_LONG_SCROLL_CIRCULAR);
    lv_obj_add_style(envRecapPopUpObjs->lbl_liningMaterial_value, &envRecapPopUpObjs->style_text_value, LV_PART_MAIN);

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Lining Thickness
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    envRecapPopUpObjs->lbl_containers[7] = lv_obj_create(parent);
	lv_obj_add_style(envRecapPopUpObjs->lbl_containers[7], &envRecapPopUpObjs->style_label_continers, LV_PART_MAIN);

    envRecapPopUpObjs->lbl_liningThickness_head = lv_label_create(envRecapPopUpObjs->lbl_containers[7]);
    lv_label_set_text(envRecapPopUpObjs->lbl_liningThickness_head, "Lining Thickness:");
    lv_obj_add_style(envRecapPopUpObjs->lbl_liningThickness_head, &envRecapPopUpObjs->style_text_head, LV_PART_MAIN);

    envRecapPopUpObjs->lbl_liningThickness_value = lv_label_create(envRecapPopUpObjs->lbl_containers[7]);
    lv_label_set_text_fmt(envRecapPopUpObjs->lbl_liningThickness_value, "%.2f [mm]", (float) M_TO_MM(localMeasSession->streams[streamNumber].parameters.externalEnvironment.lining.ThicknessInMt));
    lv_label_set_long_mode(envRecapPopUpObjs->lbl_liningThickness_value, LV_LABEL_LONG_SCROLL_CIRCULAR);
    lv_obj_add_style(envRecapPopUpObjs->lbl_liningThickness_value, &envRecapPopUpObjs->style_text_value, LV_PART_MAIN);


    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Lining SoundSpeed
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    envRecapPopUpObjs->lbl_containers[8] = lv_obj_create(parent);
    lv_obj_add_style(envRecapPopUpObjs->lbl_containers[8], &envRecapPopUpObjs->style_label_continers, LV_PART_MAIN);

    envRecapPopUpObjs->lbl_liningSoundSpeed_head = lv_label_create(envRecapPopUpObjs->lbl_containers[8]);
    lv_label_set_text(envRecapPopUpObjs->lbl_liningSoundSpeed_head, "Lining Sound Speed:");
    lv_obj_add_style(envRecapPopUpObjs->lbl_liningSoundSpeed_head, &envRecapPopUpObjs->style_text_head, LV_PART_MAIN);

    envRecapPopUpObjs->lbl_liningSoundSpeed_value = lv_label_create(envRecapPopUpObjs->lbl_containers[8]);
    lv_label_set_text_fmt(envRecapPopUpObjs->lbl_liningSoundSpeed_value, "%.2f [m/s]", (float) localMeasSession->streams[streamNumber].parameters.externalEnvironment.lining.SoundSpeed);
    lv_label_set_long_mode(envRecapPopUpObjs->lbl_liningSoundSpeed_value, LV_LABEL_LONG_SCROLL_CIRCULAR);
    lv_obj_add_style(envRecapPopUpObjs->lbl_liningSoundSpeed_value, &envRecapPopUpObjs->style_text_value, LV_PART_MAIN);

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////// FLUID PROPERTIES ////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Fluid SoundSpeed
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    envRecapPopUpObjs->lbl_containers[9] = lv_obj_create(parent);
	lv_obj_add_style(envRecapPopUpObjs->lbl_containers[9], &envRecapPopUpObjs->style_label_continers, LV_PART_MAIN);

    envRecapPopUpObjs->lbl_fluidSoundSpeed_head = lv_label_create(envRecapPopUpObjs->lbl_containers[9]);
	lv_label_set_text(envRecapPopUpObjs->lbl_fluidSoundSpeed_head, "Fluid Sound Speed:");
	lv_obj_add_style(envRecapPopUpObjs->lbl_fluidSoundSpeed_head, &envRecapPopUpObjs->style_text_head, LV_PART_MAIN);

	envRecapPopUpObjs->lbl_fluidSoundSpeed_value = lv_label_create(envRecapPopUpObjs->lbl_containers[9]);
	lv_label_set_text_fmt(envRecapPopUpObjs->lbl_fluidSoundSpeed_value, "%.2f [m/s]", (float) localMeasSession->streams[streamNumber].parameters.externalEnvironment.fluid.SoundSpeed);
	lv_label_set_long_mode(envRecapPopUpObjs->lbl_fluidSoundSpeed_value, LV_LABEL_LONG_SCROLL_CIRCULAR);
	lv_obj_add_style(envRecapPopUpObjs->lbl_fluidSoundSpeed_value, &envRecapPopUpObjs->style_text_value, LV_PART_MAIN);

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Fluid SoundSpeed Coefficient
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	envRecapPopUpObjs->lbl_containers[10] = lv_obj_create(parent);
	lv_obj_add_style(envRecapPopUpObjs->lbl_containers[10], &envRecapPopUpObjs->style_label_continers, LV_PART_MAIN);

	envRecapPopUpObjs->lbl_fluidSoundSpeedCoeff_head = lv_label_create(envRecapPopUpObjs->lbl_containers[10]);
	lv_label_set_text(envRecapPopUpObjs->lbl_fluidSoundSpeedCoeff_head, "Fluid Sound Speed Coeff:");
	lv_obj_add_style(envRecapPopUpObjs->lbl_fluidSoundSpeedCoeff_head, &envRecapPopUpObjs->style_text_head, LV_PART_MAIN);

	envRecapPopUpObjs->lbl_fluidSoundSpeedCoeff_value = lv_label_create(envRecapPopUpObjs->lbl_containers[10]);
	lv_label_set_text_fmt(envRecapPopUpObjs->lbl_fluidSoundSpeedCoeff_value, "%.2f [m/s]", (float) localMeasSession->streams[streamNumber].parameters.externalEnvironment.fluid.ssTempCoeff);
	lv_label_set_long_mode(envRecapPopUpObjs->lbl_fluidSoundSpeedCoeff_value, LV_LABEL_LONG_SCROLL_CIRCULAR);
	lv_obj_add_style(envRecapPopUpObjs->lbl_fluidSoundSpeedCoeff_value, &envRecapPopUpObjs->style_text_value, LV_PART_MAIN);

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Fluid Absolute Density
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	envRecapPopUpObjs->lbl_containers[11] = lv_obj_create(parent);
	lv_obj_add_style(envRecapPopUpObjs->lbl_containers[11], &envRecapPopUpObjs->style_label_continers, LV_PART_MAIN);

	envRecapPopUpObjs->lbl_fluidAbsDensity_head = lv_label_create(envRecapPopUpObjs->lbl_containers[11]);
	lv_label_set_text(envRecapPopUpObjs->lbl_fluidAbsDensity_head, "Fluid Abs. Density:");
	lv_obj_add_style(envRecapPopUpObjs->lbl_fluidAbsDensity_head, &envRecapPopUpObjs->style_text_head, LV_PART_MAIN);

	envRecapPopUpObjs->lbl_fluidAbsDensity_value = lv_label_create(envRecapPopUpObjs->lbl_containers[11]);
	lv_label_set_text_fmt(envRecapPopUpObjs->lbl_fluidAbsDensity_value, "%5.2f [m/s]", (float) localMeasSession->streams[streamNumber].parameters.externalEnvironment.fluid.AbsoluteDensity);
	lv_label_set_long_mode(envRecapPopUpObjs->lbl_fluidAbsDensity_value, LV_LABEL_LONG_SCROLL_CIRCULAR);
	lv_obj_add_style(envRecapPopUpObjs->lbl_fluidAbsDensity_value, &envRecapPopUpObjs->style_text_value, LV_PART_MAIN);

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Fluid Kinematic Viscosity
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	envRecapPopUpObjs->lbl_containers[12] = lv_obj_create(parent);
	lv_obj_add_style(envRecapPopUpObjs->lbl_containers[12], &envRecapPopUpObjs->style_label_continers, LV_PART_MAIN);

	envRecapPopUpObjs->lbl_fluidKinViscosity_head = lv_label_create(envRecapPopUpObjs->lbl_containers[12]);
	lv_label_set_text(envRecapPopUpObjs->lbl_fluidKinViscosity_head, "Fluid Kin. Viscosity:");
	lv_obj_add_style(envRecapPopUpObjs->lbl_fluidKinViscosity_head, &envRecapPopUpObjs->style_text_head, LV_PART_MAIN);

	envRecapPopUpObjs->lbl_fluidKinViscosity_value = lv_label_create(envRecapPopUpObjs->lbl_containers[12]);
	lv_label_set_text_fmt(envRecapPopUpObjs->lbl_fluidKinViscosity_value, "%5.4f [m/s]", (float) localMeasSession->streams[streamNumber].parameters.externalEnvironment.fluid.KinematicViscosity);
	lv_label_set_long_mode(envRecapPopUpObjs->lbl_fluidKinViscosity_value, LV_LABEL_LONG_SCROLL_CIRCULAR);
	lv_obj_add_style(envRecapPopUpObjs->lbl_fluidKinViscosity_value, &envRecapPopUpObjs->style_text_value, LV_PART_MAIN);

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Fluid Enthalpy
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	envRecapPopUpObjs->lbl_containers[13] = lv_obj_create(parent);
	lv_obj_add_style(envRecapPopUpObjs->lbl_containers[13], &envRecapPopUpObjs->style_label_continers, LV_PART_MAIN);

	envRecapPopUpObjs->lbl_fluidEnthpy_head = lv_label_create(envRecapPopUpObjs->lbl_containers[13]);
	lv_label_set_text(envRecapPopUpObjs->lbl_fluidEnthpy_head, "Fluid Kin. Viscosity:");
	lv_obj_add_style(envRecapPopUpObjs->lbl_fluidEnthpy_head, &envRecapPopUpObjs->style_text_head, LV_PART_MAIN);

	envRecapPopUpObjs->lbl_fluidEnthpy_value = lv_label_create(envRecapPopUpObjs->lbl_containers[13]);
	lv_label_set_text_fmt(envRecapPopUpObjs->lbl_fluidEnthpy_value, "%5.2f [m/s]", (float) localMeasSession->streams[streamNumber].parameters.externalEnvironment.fluid.Enthalpy);
	lv_label_set_long_mode(envRecapPopUpObjs->lbl_fluidEnthpy_value, LV_LABEL_LONG_SCROLL_CIRCULAR);
	lv_obj_add_style(envRecapPopUpObjs->lbl_fluidEnthpy_value, &envRecapPopUpObjs->style_text_value, LV_PART_MAIN);

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Fluid Specific Gravity
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	envRecapPopUpObjs->lbl_containers[14] = lv_obj_create(parent);
	lv_obj_add_style(envRecapPopUpObjs->lbl_containers[14], &envRecapPopUpObjs->style_label_continers, LV_PART_MAIN);

	envRecapPopUpObjs->lbl_fluidSpecGrvty_head = lv_label_create(envRecapPopUpObjs->lbl_containers[14]);
	lv_label_set_text(envRecapPopUpObjs->lbl_fluidSpecGrvty_head, "Fluid Kin. Viscosity:");
	lv_obj_add_style(envRecapPopUpObjs->lbl_fluidSpecGrvty_head, &envRecapPopUpObjs->style_text_head, LV_PART_MAIN);

	envRecapPopUpObjs->lbl_fluidSpecGrvty_value = lv_label_create(envRecapPopUpObjs->lbl_containers[14]);
	lv_label_set_text_fmt(envRecapPopUpObjs->lbl_fluidSpecGrvty_value, "%5.2f [m/s]", (float) localMeasSession->streams[streamNumber].parameters.externalEnvironment.fluid.specificGravity);
	lv_label_set_long_mode(envRecapPopUpObjs->lbl_fluidSpecGrvty_value, LV_LABEL_LONG_SCROLL_CIRCULAR);
	lv_obj_add_style(envRecapPopUpObjs->lbl_fluidSpecGrvty_value, &envRecapPopUpObjs->style_text_value, LV_PART_MAIN);

    lv_obj_add_event_cb(parent, envPopupUpdateDimensions_cb, LV_EVENT_DRAW_MAIN_END, NULL);
}

static void envPopupUpdateDimensions_cb(lv_event_t* event)
{
    lv_obj_t* target = lv_event_get_target(event);
    // Get Container Width
    lv_coord_t containerWidth = lv_obj_get_content_width(target);
    lv_coord_t width_head = 0;
    lv_coord_t width_value = 0;

    lv_obj_t* elementsList[][2] = {
        {envRecapPopUpObjs->lbl_pipeMaterial_head        ,   envRecapPopUpObjs->lbl_pipeMaterial_value       },
        {envRecapPopUpObjs->lbl_pipeDiameter_head        ,   envRecapPopUpObjs->lbl_pipeDiameter_value       },
        {envRecapPopUpObjs->lbl_pipeThickness_head       ,   envRecapPopUpObjs->lbl_pipeThickness_value      },
        {envRecapPopUpObjs->lbl_pipeSoundSpeed_head      ,   envRecapPopUpObjs->lbl_pipeSoundSpeed_value     },
        {envRecapPopUpObjs->lbl_pipeScheduleLbl_head     ,   envRecapPopUpObjs->lbl_pipeScheduleLbl_value    },
        {envRecapPopUpObjs->lbl_pipeScheduleNum_head     ,   envRecapPopUpObjs->lbl_pipeScheduleNum_value    },
        {envRecapPopUpObjs->lbl_liningMaterial_head      ,   envRecapPopUpObjs->lbl_liningMaterial_value     },
        {envRecapPopUpObjs->lbl_liningThickness_head     ,   envRecapPopUpObjs->lbl_liningThickness_value    },
        {envRecapPopUpObjs->lbl_liningSoundSpeed_head    ,   envRecapPopUpObjs->lbl_liningSoundSpeed_value   },
		{envRecapPopUpObjs->lbl_fluidSoundSpeed_head	 ,	envRecapPopUpObjs->lbl_fluidSoundSpeed_value	},
		{envRecapPopUpObjs->lbl_fluidSoundSpeedCoeff_head,	envRecapPopUpObjs->lbl_fluidSoundSpeedCoeff_value},
		{envRecapPopUpObjs->lbl_fluidAbsDensity_head	 ,	envRecapPopUpObjs->lbl_fluidAbsDensity_value	},
		{envRecapPopUpObjs->lbl_fluidKinViscosity_head	 ,	envRecapPopUpObjs->lbl_fluidKinViscosity_value	},
		{envRecapPopUpObjs->lbl_fluidEnthpy_head		 ,	envRecapPopUpObjs->lbl_fluidEnthpy_value		},
		{envRecapPopUpObjs->lbl_fluidSpecGrvty_head		 ,	envRecapPopUpObjs->lbl_fluidSpecGrvty_value		},
    };

    uint8_t numberOfElements = (sizeof(elementsList)/sizeof(lv_obj_t *))/2;
    uint8_t counter;

    for (counter = 0; counter < numberOfElements; counter++) {
        width_head = lv_obj_get_width(elementsList[counter][0]);
        width_value = lv_obj_get_width(elementsList[counter][1]);

        if ((width_head + width_value + 6) != containerWidth)
        {
            lv_obj_set_width(elementsList[counter][1], (containerWidth - width_head - 6));
        }
        else
        {
        	lv_obj_set_style_text_opa(elementsList[counter][0], LV_OPA_COVER, LV_PART_MAIN);
			lv_obj_set_style_text_opa(elementsList[counter][1], LV_OPA_COVER, LV_PART_MAIN);
			lv_obj_remove_event_cb(target, envPopupUpdateDimensions_cb);
        }
        // Force the complete redraw of the object.
		lv_obj_invalidate(target);
    }
}

static void setupStartStopMeasurePopUp(lv_obj_t * parent, startStopMeasPopup_t cmd)
{
	lv_obj_t * messageLabel = lv_label_create(parent);

	lv_obj_set_size(messageLabel, lv_pct(80), lv_pct(50));
	lv_obj_set_style_text_align(messageLabel, LV_TEXT_ALIGN_CENTER , LV_PART_MAIN);
	lv_obj_set_style_text_font(messageLabel, &lv_font_montserrat_20, LV_PART_MAIN);
	lv_obj_set_style_text_color(messageLabel, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_BLUE), LV_PART_MAIN);
	lv_obj_set_style_text_letter_space(messageLabel, 0, LV_PART_MAIN);
	lv_label_set_long_mode(messageLabel, LV_LABEL_LONG_WRAP);
	lv_obj_set_style_bg_color(messageLabel, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_DARK_GREY), LV_PART_MAIN);
	lv_obj_set_style_bg_opa(messageLabel, LV_OPA_0, LV_PART_MAIN);

	if(cmd == SS_POPUP_START_MEASURE)
	{
		lv_label_set_text(messageLabel, "Do you want to START the Measurement?");
	}
	else
	{
		lv_label_set_text(messageLabel, "Do you want to STOP the Measurement?");
	}

	lv_obj_t * commonMessage = lv_label_create(parent);

	lv_obj_set_size(commonMessage, lv_pct(80), LV_SIZE_CONTENT);
	lv_obj_set_style_text_align(commonMessage, LV_TEXT_ALIGN_CENTER , LV_PART_MAIN);
	lv_obj_set_style_text_font(commonMessage, &lv_font_montserrat_20, LV_PART_MAIN);
	lv_obj_set_style_text_color(commonMessage, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_WHITE), LV_PART_MAIN);
	lv_obj_set_style_text_letter_space(commonMessage, 0, LV_PART_MAIN);
	lv_obj_set_style_bg_color(commonMessage, lv_palette_main(LV_PALETTE_RED), LV_PART_MAIN);
	lv_obj_set_style_bg_opa(commonMessage, LV_OPA_COVER, LV_PART_MAIN);
	lv_label_set_long_mode(commonMessage, LV_LABEL_LONG_WRAP);
	lv_label_set_text(commonMessage, "SWIPE UP to EXIT\nTAP to CONFIRM");

}

/********************************* EOF ************************************************/
