﻿/**************************************************************************************
 *  \file       setupRestartScreen.c
 *
 *  \brief      Restart screen
 *
 *  \details    This file contains the methods and parameters for the
 *  			restart screen.
 *
 *  \author     Bodini Andrea
 *
 *  \version    1.0
 *
 *  \date		11 gen 2022
 *
 *  \copyright  (C) Copyright 2021, Euromisure Sas di WIKA Italia Srl
***************************************************************************************/

/********************************** INCLUDE *******************************************/
#ifndef LVGL_SIMULATOR_ACTIVE
#include "wikaGraphic.h"
#else
#include "..\wika_graphic\wikaGraphic.h"
#endif // !LVGL_SIMULATOR_ACTIVE

#define MESSAGE_DEINIT_PERIPH		"Deinit Peripherals\0"
#define MESSAGE_DEINIT_DAQ			"Deinit DAQ\0"
#define MESSAGE_DEINIT_PROCEDURES	"Deinit Procedures\0"
#define MESSAGE_DEINIT_PARAMETERS	"Saving Parameters\0"
#define MESSAGE_SYSTEM_REBOOT		"System Reboot\0"



/********************************** LOCAL VARIABLES ***********************************/
static restartScreen_objs *restartScreenObjs;

/********************************** FUNCTION PROTOTYPES *******************************/
static void setBarValue(void *bar, int barValue);

static void setStatusLabel(lv_event_t *event);

/********************************** FUNCTIONS *****************************************/

void vSetupRestartScreen_init	(restartScreen_objs *screenObjs, lv_obj_t *parent)
{
	restartScreenObjs = screenObjs;

	///////////////////////////////////////////////////////////////////////////
	//////////////////////// BACKGROUND IMAGE /////////////////////////////////
	///////////////////////////////////////////////////////////////////////////
	// Create the Logo
	restartScreenObjs->wika_logo_img = lv_img_create(parent);

	// Associate img object to the image structure
	lv_img_set_src(restartScreenObjs->wika_logo_img, &wika_logo);

	lv_obj_set_style_opa(restartScreenObjs->wika_logo_img, LV_OPA_COVER, LV_PART_MAIN);

	// Set position of the image
	lv_obj_set_pos(restartScreenObjs->wika_logo_img, 180, 51);
	lv_img_set_pivot(restartScreenObjs->wika_logo_img, 0, 0);
	lv_img_set_angle(restartScreenObjs->wika_logo_img, 0);

	///////////////////////////////////////////////////////////////////////////
	/////////////////////////// LOADING BAR ///////////////////////////////////
	///////////////////////////////////////////////////////////////////////////

	//LV_IMG_DECLARE(img_skew_strip);
	static lv_style_t style_loadBar;

	lv_style_init(&style_loadBar);

	lv_style_set_bg_img_src(&style_loadBar, &img_skew_strip);
	lv_style_set_bg_img_tiled(&style_loadBar, true);
	lv_style_set_bg_img_opa(&style_loadBar, LV_OPA_30);
	lv_style_set_bg_opa(&style_loadBar, LV_OPA_COVER);
	lv_style_set_bg_color(&style_loadBar, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_BLUE));

	// Create the bar
	restartScreenObjs->progressBar_bar = lv_bar_create(parent);
	lv_obj_add_style(restartScreenObjs->progressBar_bar, &style_loadBar, LV_PART_INDICATOR);

	lv_obj_set_size(restartScreenObjs->progressBar_bar, 260, 10);
	lv_obj_center(restartScreenObjs->progressBar_bar);
	lv_bar_set_mode(restartScreenObjs->progressBar_bar, LV_BAR_MODE_NORMAL);
	lv_bar_set_start_value(restartScreenObjs->progressBar_bar, 0, LV_ANIM_OFF);

	lv_anim_init(&restartScreenObjs->barAnimation_anim);
	lv_anim_set_exec_cb(&restartScreenObjs->barAnimation_anim, setBarValue);
	lv_anim_set_time(&restartScreenObjs->barAnimation_anim, 5000);
	lv_anim_set_var(&restartScreenObjs->barAnimation_anim, restartScreenObjs->progressBar_bar);
	lv_anim_set_values(&restartScreenObjs->barAnimation_anim, 100, 0);

	lv_anim_set_repeat_count(&restartScreenObjs->barAnimation_anim, 0);
	lv_anim_start(&restartScreenObjs->barAnimation_anim);

	///////////////////////////////////////////////////////////////////////////
	//////////////////////// LOADING PHASE LABEL //////////////////////////////
	///////////////////////////////////////////////////////////////////////////

	static lv_style_t style_textLabels;
	lv_style_init(&style_textLabels);

	lv_style_set_text_font(&style_textLabels, &lv_font_montserrat_18);

	restartScreenObjs->restartPhase_lbl = lv_label_create(parent);
	lv_obj_align(restartScreenObjs->restartPhase_lbl, LV_ALIGN_CENTER, 0, -25);
	lv_obj_set_width(restartScreenObjs->restartPhase_lbl, 250);
	lv_obj_set_style_text_align(restartScreenObjs->restartPhase_lbl, LV_TEXT_ALIGN_CENTER, 0);
	lv_label_set_long_mode(restartScreenObjs->restartPhase_lbl, LV_LABEL_LONG_WRAP);

	lv_obj_add_style(restartScreenObjs->restartPhase_lbl, &style_textLabels, LV_STATE_DEFAULT);
	lv_obj_add_event_cb(restartScreenObjs->restartPhase_lbl, setStatusLabel, LV_EVENT_REFRESH, NULL);
}

static void setBarValue(void* bar, int barValue)
{
    lv_bar_set_value(bar, barValue, LV_ANIM_OFF);
	lv_event_send(restartScreenObjs->restartPhase_lbl, LV_EVENT_REFRESH, &barValue);
}

static void setStatusLabel(lv_event_t* event)
{
    lv_obj_t* obj = lv_event_get_target(event);

    int loadBarValue = (int)lv_bar_get_value(restartScreenObjs->progressBar_bar);

    if (loadBarValue <= 20){
    	lv_label_set_text(obj, MESSAGE_SYSTEM_REBOOT);
    }
    else if (loadBarValue <= 40) {
    	lv_label_set_text(obj, MESSAGE_DEINIT_PARAMETERS);
    }
    else if (loadBarValue <= 60) {
    	lv_label_set_text(obj, MESSAGE_DEINIT_PROCEDURES);
    }
    else if (loadBarValue <= 80) {
    	lv_label_set_text(obj, MESSAGE_DEINIT_DAQ);
    }
    else {
    	lv_label_set_text(obj, MESSAGE_DEINIT_PERIPH);;
    }
}

/********************************* EOF ************************************************/
