﻿/**************************************************************************************
 *  \file       setupStatusTab.c
 *
 *  \brief      Status Tab Methods and management
 *
 *  \details    This file contains all the methods and variables linked to the
 *  			management of the Status tab in the MAIN screen of the LVGL interface.
 *  			This tab will contain detailed informations about the measurement process,
 *  			and it will be divided in two tiles, one for each stream.
 *
 *  \author     BodiniA
 *
 *  \version    0.1
 *
 *  \date		7 giu 2022
 *
 *  \copyright  (C) Copyright 2021, Euromisure Sas di WIKA Italia Srl
***************************************************************************************/

/********************************** INCLUDE *******************************************/
#include "guiDesign.h"
#include "math.h"
#include <stdlib.h>
#include <string.h>
#ifndef LVGL_SIMULATOR_ACTIVE
#include "wikaGraphic.h"
#include "esp_log.h"
#else
#include "..\wika_graphic\wikaGraphic.h"
#endif // !LVGL_SIMULATOR_ACTIVE

/********************************** LOCAL VARIABLES ***********************************/
#define TILE_VIEW_HOR_PAD   3
#define TILE_VIEW_VER_PAD   4
#define TILE_VIEW_COL_NUM   0
#define TILE_VIEW_ROW_NUM   (STAT_BUTTON_NUMBER - 1)
#define HEADER_WHITE_BORDER_SIZE_IN_PX  3    

#define BTN_LBL_LEFT_X_OFFS     7
#define BTN_LBL_RIGHT_X_OFFS    -7
#define HDR_LBL_Y_OFFS          10
#define HDR_IMG_HEIGHT          LV_SIZE_CONTENT

#define SUBMENU_ICO_WIDTH   28

#define TABLE_ELEMENT_NUM_OF_COLS   3   /*<! Number of columns, default for every screen */

#define TABLE_ELEMENT_COL_LABEL     0
#define TABLE_ELEMENT_COL_STREAM1   1
#define TABLE_ELEMENT_COL_STREAM2   2

#define TABLE_HEADER_STREAM1_LABEL  "Stream 1"
#define TABLE_HEADER_STREAM2_LABEL  "Stream 2"
#define TABLE_HEADER_NULL_LABEL     "\0"
#define TABLE_ELEMENT_HEADER_ROW    0

#define TABLE_HEADER_AN1_LABEL      "AN1"
#define TABLE_HEADER_AN2_LABEL      "AN2"
#define TABLE_HEADER_AN3_LABEL      "AN3"
#define TABLE_HEADER_AN4_LABEL      "AN4"

// TIMING TILE PROPERTIES
#define TABLE_TIMING_TILE_ROW_CNT         4

#define TABLE_TIMING_TILE_TUPS_ROW        0
#define TABLE_TIMING_TILE_TDNS_ROW        1
#define TABLE_TIMING_TILE_DTOF_ROW        2
#define TABLE_TIMING_TILE_ZOFF_ROW        3

// SETPOINT TILE PROPERTIES
#define TABLE_SETPOINT_TILE_ROW_CNT       4

#define TABLE_SETPOINT_TILE_TYPE_ROW      0
#define TABLE_SETPOINT_TILE_CUTOFF_ROW    1
#define TABLE_SETPOINT_TILE_WARNING_ROW   2
#define TABLE_SETPOINT_TILE_FULLSCALE_ROW 3

// FLUID STATS TILE PROPERTIES
#define TABLE_SIGNAL_TILE_ROW_CNT         4

#define TABLE_SIGNAL_TILE_LEVEL_ROW       0
#define TABLE_SIGNAL_TILE_AGC_GAIN_ROW    1
#define TABLE_SIGNAL_TILE_FLUID_ATTN_ROW  2
#define TABLE_SIGNAL_TILE_QUALITY_ROW     3

// PIPE INFO TILE PROPERTIES
#define TABLE_PIPE_TILE_ROW_CNT           5

#define TABLE_PIPE_TILE_MATERIAL_ROW        0
#define TABLE_PIPE_TILE_EXT_DIAMETER_ROW    1
#define TABLE_PIPE_TILE_THICKNESS_ROW       2
#define TABLE_PIPE_TILE_TEMP_SOURCE_ROW     3
#define TABLE_PIPE_TILE_TEMPERATURE_ROW     4

// LINING INFO TILE PROPERTIES
#define TABLE_LINING_TILE_ROW_CNT           3

#define TABLE_LINING_TILE_MATERIAL_ROW      0
#define TABLE_LINING_TILE_THICKNESS_ROW     1
#define TABLE_LINING_TILE_SOUNDSPEED_ROW    2

// SENSOR INFO TILE PROPERTIES
#define TABLE_SENSOR_TILE_ROW_CNT           4

#define TABLE_SENSOR_TILE_CFG_ROW           0
#define TABLE_SENSOR_TILE_DISTANCE_ROW      1
#define TABLE_SENSOR_TILE_TYPE_ROW          2
#define TABLE_SENSOR_TILE_FREQ_ROW          3

// FLUID INFO TILE PROPERTIES
#define TABLE_FLUID_TILE_ROW_CNT            4

#define TABLE_FLUID_TILE_TYPE_ROW           0
#define TABLE_FLUID_TILE_TEMP_SRC_ROW       1
#define TABLE_FLUID_TILE_TEMP_ROW           2
#define TABLE_FLUID_TILE_SOUNDSPEED_ROW     3

// MEASUREMENT INFO TILE PROPERTIES
#define TABLE_MEASUREMENT_TILE_ROW_CNT      3

#define TABLE_MEAS_TILE_REY_CORR_ROW        0
#define TABLE_MEAS_TILE_ACCURATE_ROW        1
#define TABLE_MEAS_TILE_METER_ROW           2

// ENERGY MEAS INFO TILE PROPERTIES
#define TABLE_ENERGY_TILE_ROW_CNT           4

#define TABLE_ENERGY_TILE_TYPE_ROW            0
#define TABLE_ENERGY_TILE_LOSS_EVAL_ROW       1
#define TABLE_ENERGY_TILE_SEND_ROW            2
#define TABLE_ENERGY_TILE_SENDRET_TEMPSRC_ROW 3

// ANALOG IN INFO TILE PROPERTIES
#define TABLE_ANIN_TILE_ROW_CNT         5
#define TABLE_ANIN_TILE_COL_CNT         5

#define TABLE_ANIN_TILE_ID_ROW          0
#define TABLE_ANIN_TILE_TYPE_ROW        1
#define TABLE_ANIN_TILE_UNIT_ROW        2
#define TABLE_ANIN_TILE_ENDPT_ROW       3
#define TABLE_ANIN_TILE_CURRVAL_ROW     4

#define TABLE_ELEMENT_LABEL_COL         0
#define TABLE_ELEMENT_ANIN1_COL         1
#define TABLE_ELEMENT_ANIN2_COL         2
#define TABLE_ELEMENT_ANIN3_COL         3
#define TABLE_ELEMENT_ANIN4_COL         4

static statusTab_objs* pStatTabObj_obj;

static void* pTileUpdatecallbackHandler;

enum _BUTTONS_VS_TILES_ {
    BTN11_TILE_ANALOG_INPUTS = 0,
    BTN12_TILE_TIMING = 1,
    BTN21_TILE_ENERGY = 2,
    BTN22_TILE_SIGNAL = 3,
    BTN31_TILE_LINING = 4,
    BTN32_TILE_PIPE = 5,
    BTN41_TILE_FLUID = 6,
    BTN42_TILE_SETPOINT = 7,
    BTN51_TILE_SENSOR = 8,
    BTN52_TILE_MEASUREMENT = 9
};

/********************************** FUNCTION PROTOTYPES *******************************/
// Object constructors
static void vSetupStatusTab_create_button(lv_obj_t* button, uint8_t btnNum, lv_coord_t btnWidth, lv_obj_t* label);
static void vSetupStatusTab_initLandingPage(infoLandingPageTile_t* infoTileObj);

// Detail Tiles
// Main Tile Constructor
static void vSetupStatusTab_initDetailTile(infoDetailPageTile_t* detailPageObj);
// Detail Tiles Constructors
static void vSetupStatusTab_drawTimingTile(infoDetailPageTile_t* detailPageObj);
static void vSetupStatusTab_drawSetpointTile(infoDetailPageTile_t* detailPageObj);
static void vSetupStatusTab_drawSignalStatTile(infoDetailPageTile_t* detailPageObj);
static void vSetupStatusTab_drawPipeInfoTile(infoDetailPageTile_t* detailPageObj);
static void vSetupStatusTab_drawSensorInfoTile(infoDetailPageTile_t* detailPageObj);
static void vSetupStatusTab_drawFluidInfoTile(infoDetailPageTile_t* detailPageObj);
static void vSetupStatusTab_drawEnergyInfoTile(infoDetailPageTile_t* detailPageObj);
static void vSetupStatusTab_drawLiningInfoTile(infoDetailPageTile_t* detailPageObj);
static void vSetupStatusTab_drawMeasurementInfoTile(infoDetailPageTile_t* detailPageObj);
static void vSetupStatusTab_draw_AnIn_InfoTile(infoDetailPageTile_t* detailPageObj);

// Event Handlers
static void vSetupStatusTab_landingPageNavigation(lv_event_t* event);
static void vSetupStatusTab_updateBtnStyle(lv_event_t* event);
static void vSetupStatusTab_updateCentralImage(lv_event_t* event);
static void vSetupStatusTab_updateDetailTile(lv_event_t* event);
static void vSetupStatusTab_updateTimingTableValues(lv_event_t* event);

// Other functions
static uint16_t u16SetupStatusTab_getImgScalingPct(lv_coord_t currentSize_in_px, lv_coord_t desiredSize_in_px);
static void vSetupStatusTab_del_children(lv_obj_t* parent, uint8_t start_del_cnt);
static void vSetupStatusTab_setup_header(infoDetailPageTile_t* detailPageObj, lv_obj_t* lbl_header[], uint8_t col_cnt, lv_coord_t col_width);
static void vSetupStatusTab_setup_elements(infoDetailPageTile_t* detailPageObj, void* elements[], lv_obj_t* row_container[], lv_obj_t* grad_sep_line[], uint8_t row_count, uint8_t col_count);

/********************************** FUNCTIONS *****************************************/
// TILE MAIN CONSTRUCTOR
void vSetupStatusTab_init(statusTab_objs* statTabObj, lv_obj_t* parent)
{
    pStatTabObj_obj = statTabObj;

    lv_obj_set_style_pad_ver(parent, TILE_VIEW_VER_PAD, LV_PART_MAIN);
    lv_obj_set_style_pad_hor(parent, TILE_VIEW_HOR_PAD, LV_PART_MAIN);

    // Prevent the object being scrollable.
    lv_obj_set_scrollbar_mode(parent, LV_SCROLLBAR_MODE_OFF);
    lv_obj_clear_flag(parent, LV_OBJ_FLAG_SCROLLABLE);

    statTabObj->info_tileView_container = lv_tileview_create(parent);
    // Size, position, alignment of Tileview container
    lv_obj_set_size(statTabObj->info_tileView_container, lv_pct(PCT_FULL_SIZE), lv_pct(PCT_FULL_SIZE));
    lv_obj_set_style_pad_all(statTabObj->info_tileView_container, 0, LV_PART_MAIN);
    lv_obj_set_style_border_width(statTabObj->info_tileView_container, 0, LV_PART_MAIN);

    lv_obj_set_scrollbar_mode(statTabObj->info_tileView_container, LV_SCROLLBAR_MODE_OFF);
    lv_obj_clear_flag(statTabObj->info_tileView_container, LV_OBJ_FLAG_SCROLLABLE);

    // Set a callback which dinamically updates the detail Tile
    lv_obj_add_event_cb(statTabObj->info_tileView_container, vSetupStatusTab_updateDetailTile, LV_EVENT_REFRESH, &statTabObj->tile_detail);

    // Create the "landing" info Tile. This will contain all the link to different subsections
    statTabObj->tile_info.obj_main_tileView_tile = lv_tileview_add_tile(statTabObj->info_tileView_container, 0, 0, LV_DIR_BOTTOM);
    vSetupStatusTab_initLandingPage(&statTabObj->tile_info);

    // Set the Landing Page as the active tile.
    lv_obj_set_tile(statTabObj->info_tileView_container, statTabObj->tile_info.obj_main_tileView_tile, LV_ANIM_OFF);

    // Set a callback to manage the button navigation
    lv_obj_add_event_cb(statTabObj->tile_info.obj_main_tileView_tile, vSetupStatusTab_landingPageNavigation, LV_EVENT_KEY, NULL);

    // Create the detail tile. Its children will be updated dinamically.
    statTabObj->tile_detail.obj_main_tileView_tile = lv_tileview_add_tile(statTabObj->info_tileView_container, 0, 1, LV_DIR_BOTTOM);

    // Prevent Tile Scroll
    lv_obj_set_scrollbar_mode(statTabObj->tile_detail.obj_main_tileView_tile, LV_SCROLLBAR_MODE_OFF);
    lv_obj_clear_flag(statTabObj->tile_detail.obj_main_tileView_tile, LV_OBJ_FLAG_SCROLLABLE);

    vSetupStatusTab_initDetailTile(&statTabObj->tile_detail);
}

static inline uint16_t u16SetupStatusTab_getImgScalingPct(lv_coord_t currentSize_in_px, lv_coord_t desiredSize_in_px)
{
    return (desiredSize_in_px * 256) / currentSize_in_px;
}

static void vSetupStatusTab_submenu_ico_set(lv_obj_t* image)
{
    lv_obj_align(image, LV_ALIGN_LEFT_MID, 5, 0);
    lv_obj_update_layout(image);

    lv_img_set_zoom(image,
        u16SetupStatusTab_getImgScalingPct(lv_obj_get_height(image),
            SUBMENU_ICO_WIDTH)
    );
    lv_img_set_size_mode(image, LV_IMG_SIZE_MODE_REAL);
    lv_obj_set_height(image, SUBMENU_ICO_WIDTH + 1);
}

/////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////// LANDING TILE FUNCTIONS /////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
static void vSetupStatusTab_initLandingPage(infoLandingPageTile_t* infoTileObj)
{
    uint8_t counter = 0;

    lv_coord_t buttonSizeArray[STAT_BUTTON_NUMBER] = {
        BTN11_WIDTH, BTN12_WIDTH,
        BTN21_WIDTH, BTN22_WIDTH,
        BTN31_WIDTH, BTN32_WIDTH,
        BTN41_WIDTH, BTN42_WIDTH,
        BTN51_WIDTH, BTN52_WIDTH
    };

    // Prevent the object being scrollable.
    // The tiles will be scrolled at the button click.
    lv_obj_set_scrollbar_mode(infoTileObj->obj_main_tileView_tile, LV_SCROLLBAR_MODE_OFF);
    lv_obj_clear_flag(infoTileObj->obj_main_tileView_tile, LV_OBJ_FLAG_SCROLLABLE);

    // Set Background color
    lv_obj_set_style_bg_color(infoTileObj->obj_main_tileView_tile, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_LIGHT_GREY), LV_PART_MAIN);
    lv_obj_set_style_bg_opa(infoTileObj->obj_main_tileView_tile, LV_OPA_COVER, LV_PART_MAIN);

    // Init Tile Styles
    lv_style_init(&infoTileObj->style_button_labels_left_side);
    lv_style_set_text_font(&infoTileObj->style_button_labels_left_side, &wika_montserrat_18_semibold_symbols);
    lv_style_set_text_align(&infoTileObj->style_button_labels_left_side, LV_TEXT_ALIGN_LEFT);
    lv_style_set_align(&infoTileObj->style_button_labels_left_side, LV_ALIGN_LEFT_MID);
    lv_style_set_x(&infoTileObj->style_button_labels_left_side, BTN_LBL_LEFT_X_OFFS);

    lv_style_init(&infoTileObj->style_button_labels_right_side);
    lv_style_set_text_font(&infoTileObj->style_button_labels_right_side, &wika_montserrat_18_semibold_symbols);
    lv_style_set_text_align(&infoTileObj->style_button_labels_right_side, LV_TEXT_ALIGN_RIGHT);
    lv_style_set_align(&infoTileObj->style_button_labels_right_side, LV_ALIGN_RIGHT_MID);
    lv_style_set_x(&infoTileObj->style_button_labels_right_side, BTN_LBL_RIGHT_X_OFFS);

    lv_style_init(&infoTileObj->style_text_checked);
    lv_style_set_text_color(&infoTileObj->style_text_checked, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_BLUE));
    lv_style_set_text_opa(&infoTileObj->style_text_checked, LV_OPA_COVER);

    lv_style_init(&infoTileObj->style_text_released);
    lv_style_set_text_color(&infoTileObj->style_text_released, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_WHITE));
    lv_style_set_text_opa(&infoTileObj->style_text_released, LV_OPA_COVER);

    lv_style_init(&infoTileObj->style_buttons_released);
    lv_style_set_border_side(&infoTileObj->style_buttons_released, LV_BORDER_SIDE_NONE);

    lv_style_init(&infoTileObj->style_buttons_pressed);
    lv_style_set_text_color(&infoTileObj->style_buttons_pressed, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_BLUE));
    lv_style_set_img_opa(&infoTileObj->style_buttons_pressed, LV_OPA_30);
    lv_style_set_border_side(&infoTileObj->style_buttons_pressed, LV_BORDER_SIDE_NONE);

    for (counter = 0; counter < STAT_BUTTON_NUMBER; counter++) {
        infoTileObj->btn_info[counter] = lv_imgbtn_create(infoTileObj->obj_main_tileView_tile);
        infoTileObj->btn_info_label[counter] = lv_label_create(infoTileObj->btn_info[counter]);

        vSetupStatusTab_create_button(infoTileObj->btn_info[counter], counter, buttonSizeArray[counter], infoTileObj->btn_info_label[counter]);

        lv_obj_add_style(infoTileObj->btn_info[counter], &infoTileObj->style_buttons_released, LV_PART_MAIN);
        lv_obj_add_style(infoTileObj->btn_info[counter], &infoTileObj->style_buttons_pressed, LV_PART_MAIN | LV_STATE_CHECKED);

        // EVEN buttons are on the LEFT side,
        // ODD buttons are on the RIGHT side.
        if (counter % 2 == 0) {
            lv_obj_add_style(infoTileObj->btn_info_label[counter], &infoTileObj->style_button_labels_left_side, LV_PART_MAIN);
        }
        else {
            lv_obj_add_style(infoTileObj->btn_info_label[counter], &infoTileObj->style_button_labels_right_side, LV_PART_MAIN);
        }

        lv_obj_add_style(infoTileObj->btn_info_label[counter], &infoTileObj->style_text_checked, LV_PART_MAIN | LV_STATE_CHECKED);
        lv_obj_add_style(infoTileObj->btn_info_label[counter], &infoTileObj->style_text_released, LV_PART_MAIN);
    }

    infoTileObj->centralImage = lv_img_create(infoTileObj->obj_main_tileView_tile);
    lv_img_set_src(infoTileObj->centralImage, &time_blue);
    lv_obj_center(infoTileObj->centralImage);
    lv_obj_add_event_cb(infoTileObj->centralImage, vSetupStatusTab_updateCentralImage, LV_EVENT_VALUE_CHANGED, NULL);
}

static void vSetupStatusTab_create_button(lv_obj_t* button, uint8_t btnNum, lv_coord_t btnWidth, lv_obj_t* label)
{
    // Add callback to every button status change.
    lv_obj_add_event_cb(button, vSetupStatusTab_updateBtnStyle, LV_EVENT_FOCUSED, NULL);
    lv_obj_add_event_cb(button, vSetupStatusTab_updateBtnStyle, LV_EVENT_CLICKED, NULL);
    lv_obj_add_event_cb(button, vSetupStatusTab_updateBtnStyle, LV_EVENT_RELEASED, NULL);

    switch (btnNum) {
    case BTN11_TILE_ANALOG_INPUTS:
        lv_imgbtn_set_src(button, LV_IMGBTN_STATE_RELEASED, NULL, &btn11, NULL);
        lv_obj_align(button, LV_ALIGN_TOP_LEFT, 0, 0 * BTN_HEIGHT);
        lv_label_set_text(label, (char*)u8WikaGUI_getLabel(LABELS_ANALOGIN, DEFAULT_LANGUAGE));
        break;
    case BTN12_TILE_TIMING:
        lv_imgbtn_set_src(button, LV_IMGBTN_STATE_RELEASED, NULL, &btn12, NULL);
        lv_obj_align(button, LV_ALIGN_TOP_RIGHT, 0, 0 * BTN_HEIGHT);
        lv_label_set_text(label, (char*)u8WikaGUI_getLabel(LABELS_TIMING, DEFAULT_LANGUAGE));
        lv_event_send(button, LV_EVENT_FOCUSED, NULL);
        break;
    case BTN21_TILE_ENERGY:
        lv_imgbtn_set_src(button, LV_IMGBTN_STATE_RELEASED, NULL, &btn21, NULL);
        lv_obj_align(button, LV_ALIGN_TOP_LEFT, 0, 1 * (BTN_HEIGHT + 2));
        lv_label_set_text(label, (char*)u8WikaGUI_getLabel(LABELS_ENERGY, DEFAULT_LANGUAGE));
        break;
    case BTN22_TILE_SIGNAL:
        lv_imgbtn_set_src(button, LV_IMGBTN_STATE_RELEASED, NULL, &btn22, NULL);
        lv_obj_align(button, LV_ALIGN_TOP_RIGHT, 0, 1 * (BTN_HEIGHT + 2));
        lv_label_set_text(label, (char*)u8WikaGUI_getLabel(LABELS_SIGNAL, DEFAULT_LANGUAGE));
        break;
    case BTN31_TILE_LINING:
        lv_imgbtn_set_src(button, LV_IMGBTN_STATE_RELEASED, NULL, &btn31, NULL);
        lv_obj_align(button, LV_ALIGN_TOP_LEFT, 0, 2 * (BTN_HEIGHT + 2));
        lv_label_set_text(label, (char*)u8WikaGUI_getLabel(LABELS_LINING, DEFAULT_LANGUAGE));
        break;
    case BTN32_TILE_PIPE:
        lv_imgbtn_set_src(button, LV_IMGBTN_STATE_RELEASED, NULL, &btn32, NULL);
        lv_obj_align(button, LV_ALIGN_TOP_RIGHT, 0, 2 * (BTN_HEIGHT + 2));
        lv_label_set_text(label, (char*)u8WikaGUI_getLabel(LABELS_PIPE, DEFAULT_LANGUAGE));
        break;
    case BTN41_TILE_FLUID:
        lv_imgbtn_set_src(button, LV_IMGBTN_STATE_RELEASED, NULL, &btn41, NULL);
        lv_obj_align(button, LV_ALIGN_TOP_LEFT, 0, 3 * (BTN_HEIGHT + 2));
        lv_label_set_text(label, (char*)u8WikaGUI_getLabel(LABELS_FLUID, DEFAULT_LANGUAGE));
        break;
    case BTN42_TILE_SETPOINT:
        lv_imgbtn_set_src(button, LV_IMGBTN_STATE_RELEASED, NULL, &btn42, NULL);
        lv_obj_align(button, LV_ALIGN_TOP_RIGHT, 0, 3 * (BTN_HEIGHT + 2));
        lv_label_set_text(label, (char*)u8WikaGUI_getLabel(LABELS_SETPOINT, DEFAULT_LANGUAGE));
        break;
    case BTN51_TILE_SENSOR:
        lv_imgbtn_set_src(button, LV_IMGBTN_STATE_RELEASED, NULL, &btn51, NULL);
        lv_obj_align(button, LV_ALIGN_TOP_LEFT, 0, 4 * (BTN_HEIGHT + 2));
        lv_label_set_text(label, (char*)u8WikaGUI_getLabel(LABELS_SENSOR, DEFAULT_LANGUAGE));
        break;
    case BTN52_TILE_MEASUREMENT:
        lv_imgbtn_set_src(button, LV_IMGBTN_STATE_RELEASED, NULL, &btn52, NULL);
        lv_obj_align(button, LV_ALIGN_TOP_RIGHT, 0, 4 * (BTN_HEIGHT + 2));
        lv_label_set_text(label, (char*)u8WikaGUI_getLabel(LABELS_MEASUREMENT, DEFAULT_LANGUAGE));
        break;
    default:
        break;
    }

    lv_obj_set_width(button, btnWidth);
}

/////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////// DETAIL TILE FUNCTIONS //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
static void vSetupStatusTab_initDetailTile(infoDetailPageTile_t* detailPageObj)
{
    lv_obj_set_style_pad_all(detailPageObj->obj_main_tileView_tile, 0, LV_PART_MAIN);
    lv_obj_set_style_border_width(detailPageObj->obj_main_tileView_tile, 0, LV_PART_MAIN);
    lv_obj_set_style_border_color(detailPageObj->obj_main_tileView_tile, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_ORANGE), LV_PART_MAIN);

    lv_style_init(&detailPageObj->style_header_text);
    lv_style_set_text_align(&detailPageObj->style_header_text, LV_TEXT_ALIGN_LEFT);
    lv_style_set_text_color(&detailPageObj->style_header_text, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_WHITE));
    lv_style_set_text_font(&detailPageObj->style_header_text, &wika_montserrat_20_semibold_symbols);
    lv_style_set_align(&detailPageObj->style_header_text, LV_ALIGN_TOP_LEFT);
    lv_style_set_x(&detailPageObj->style_header_text, BTN_LBL_LEFT_X_OFFS);
    lv_style_set_y(&detailPageObj->style_header_text, HDR_LBL_Y_OFFS);

    // Prevent the object being scrollable.
    // The tiles will be scrolled at the button click.
    lv_obj_set_scrollbar_mode(detailPageObj->obj_main_tileView_tile, LV_SCROLLBAR_MODE_OFF);
    lv_obj_clear_flag(detailPageObj->obj_main_tileView_tile, LV_OBJ_FLAG_SCROLLABLE);

    // Set Background color
    lv_obj_set_style_bg_color(detailPageObj->obj_main_tileView_tile, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_LIGHT_GREY), LV_PART_MAIN);
    lv_obj_set_style_bg_opa(detailPageObj->obj_main_tileView_tile, LV_OPA_COVER, LV_PART_MAIN);

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Create Header Container
    detailPageObj->header_container = lv_obj_create(detailPageObj->obj_main_tileView_tile);
    lv_obj_set_style_bg_opa(detailPageObj->header_container, LV_OPA_COVER, LV_PART_MAIN);
    lv_obj_set_style_bg_color(detailPageObj->header_container, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_GREY), LV_PART_MAIN);
    lv_obj_align(detailPageObj->header_container, LV_ALIGN_TOP_RIGHT, 0, 0);
    lv_obj_set_width(detailPageObj->header_container, lv_pct(PCT_FULL_SIZE));
    lv_obj_set_height(detailPageObj->header_container, btn11.header.h);
    lv_obj_set_style_radius(detailPageObj->header_container, 0, LV_PART_MAIN);
    lv_obj_set_style_border_width(detailPageObj->header_container, 0, LV_PART_MAIN);
    lv_obj_set_style_border_side(detailPageObj->header_container, LV_BORDER_SIDE_NONE, LV_PART_MAIN);
    lv_obj_set_style_pad_all(detailPageObj->header_container, 0, LV_PART_MAIN);
    lv_obj_update_layout(detailPageObj->header_container);
    lv_obj_set_scrollbar_mode(detailPageObj->header_container, LV_SCROLLBAR_MODE_OFF);
    lv_obj_clear_flag(detailPageObj->header_container, LV_OBJ_FLAG_SCROLLABLE);

    detailPageObj->img_head_container_bg = lv_img_create(detailPageObj->header_container);
    lv_img_set_src(detailPageObj->img_head_container_bg, &btn11);
    lv_obj_set_style_img_recolor(detailPageObj->img_head_container_bg, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_WHITE), LV_PART_MAIN);
    lv_obj_set_style_img_recolor_opa(detailPageObj->img_head_container_bg, LV_OPA_COVER, LV_PART_MAIN);
    lv_obj_align(detailPageObj->img_head_container_bg, LV_ALIGN_TOP_LEFT, HEADER_WHITE_BORDER_SIZE_IN_PX, 0);
    lv_obj_set_style_pad_all(detailPageObj->img_head_container_bg, 0, LV_PART_MAIN);

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Header Image/Text
    detailPageObj->submenu_header_bg_img = lv_img_create(detailPageObj->obj_main_tileView_tile);
    lv_img_set_src(detailPageObj->submenu_header_bg_img, &btn1x);
    lv_obj_set_height(detailPageObj->submenu_header_bg_img, HDR_IMG_HEIGHT);
    lv_obj_align(detailPageObj->submenu_header_bg_img, LV_ALIGN_TOP_LEFT, 0, 0);

    // Create Header Label
    detailPageObj->submenu_header_lbl = lv_label_create(detailPageObj->submenu_header_bg_img);
    lv_obj_add_style(detailPageObj->submenu_header_lbl, &detailPageObj->style_header_text, LV_PART_MAIN);   

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Init Data Container Style
    lv_style_init(&detailPageObj->style_data_container);
    lv_style_set_bg_opa(&detailPageObj->style_data_container,  LV_OPA_90);
    lv_style_set_bg_color(&detailPageObj->style_data_container, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_WHITE));
    lv_style_set_align(&detailPageObj->style_data_container, LV_ALIGN_TOP_LEFT);
    lv_style_set_height(&detailPageObj->style_data_container, BTN_HEIGHT);
    lv_style_set_radius(&detailPageObj->style_data_container, 0);
    lv_style_set_border_width(&detailPageObj->style_data_container, 0);
    lv_style_set_width(&detailPageObj->style_data_container, lv_pct(PCT_FULL_SIZE));
    lv_style_set_pad_all(&detailPageObj->style_data_container, 0);
    lv_style_set_text_font(&detailPageObj->style_data_container, &lv_font_montserrat_16);

    // Create the data container
    detailPageObj->data_full_row_container = lv_obj_create(detailPageObj->obj_main_tileView_tile);
    lv_obj_set_width(detailPageObj->data_full_row_container, lv_pct(PCT_FULL_SIZE));
    lv_obj_set_height(detailPageObj->data_full_row_container, lv_obj_get_height(detailPageObj->obj_main_tileView_tile) - BTN_HEIGHT);
    lv_obj_set_style_border_width(detailPageObj->data_full_row_container, 0, LV_PART_MAIN);
    lv_obj_set_style_pad_all(detailPageObj->data_full_row_container, 0, LV_PART_MAIN);
    lv_obj_set_style_radius(detailPageObj->data_full_row_container, 0, LV_PART_MAIN);
    lv_obj_set_style_opa(detailPageObj->data_full_row_container, LV_OPA_COVER, LV_PART_MAIN);
    lv_obj_set_style_bg_opa(detailPageObj->data_full_row_container, LV_OPA_0, LV_PART_MAIN);
    lv_obj_align(detailPageObj->data_full_row_container, LV_ALIGN_BOTTOM_MID, 0, 0);

    // Init Data Label Text Style
    lv_style_init(&detailPageObj->style_data_label_text);
    lv_style_set_text_align(&detailPageObj->style_data_label_text, LV_TEXT_ALIGN_LEFT);
    lv_style_set_text_color(&detailPageObj->style_data_label_text, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_BLACK));

    // Init Data Data Text Style
    lv_style_init(&detailPageObj->style_data_data_text);
    lv_style_set_text_align(&detailPageObj->style_data_data_text, LV_TEXT_ALIGN_RIGHT);
    lv_style_set_text_color(&detailPageObj->style_data_data_text, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_DARK_GREY));

    // Init Data Separator Line Style
    lv_style_init(&detailPageObj->style_data_separator_line);
    lv_style_set_width(&detailPageObj->style_data_separator_line, lv_pct(PCT_FULL_SIZE));
    lv_style_set_pad_all(&detailPageObj->style_data_separator_line, 0);
    lv_style_set_border_width(&detailPageObj->style_data_separator_line, 0);
    lv_style_set_bg_color(&detailPageObj->style_data_separator_line, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_WHITE));
    lv_style_set_bg_grad_color(&detailPageObj->style_data_separator_line, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_GREY));
    lv_style_set_bg_grad_dir(&detailPageObj->style_data_separator_line, LV_GRAD_DIR_HOR);
    lv_style_set_bg_opa(&detailPageObj->style_data_separator_line, LV_OPA_COVER);
    lv_style_set_align(&detailPageObj->style_data_separator_line, LV_ALIGN_BOTTOM_MID);
    lv_style_set_height(&detailPageObj->style_data_separator_line, 1);

    // Init Stream Header Style
    lv_style_init(&detailPageObj->style_stream_titles);
    lv_style_set_bg_opa(&detailPageObj->style_stream_titles, LV_OPA_0);
    lv_style_set_text_font(&detailPageObj->style_stream_titles, &wika_montserrat_18_semibold_symbols);
    lv_style_set_text_color(&detailPageObj->style_stream_titles, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_WHITE));
    lv_style_set_text_align(&detailPageObj->style_stream_titles, LV_TEXT_ALIGN_RIGHT);
    lv_style_set_align(&detailPageObj->style_stream_titles, LV_ALIGN_LEFT_MID);
    lv_style_set_border_width(&detailPageObj->style_stream_titles, 0);
}

static void vSetupStatusTab_updateDetailTile(lv_event_t* event)
{
    infoDetailPageTile_t* detailTileObj = (infoDetailPageTile_t*)lv_event_get_user_data(event);
    uint8_t childNumber = *((uint8_t*)lv_event_get_param(event));

    switch (childNumber) {
    case BTN11_TILE_ANALOG_INPUTS:
        printf("Analog IN - child:%d\n", childNumber);
        vSetupStatusTab_draw_AnIn_InfoTile(detailTileObj);
        break;
    case BTN12_TILE_TIMING:
        printf("Timing - child:%d\n", childNumber);
        vSetupStatusTab_drawTimingTile(detailTileObj);
        break;
    case BTN21_TILE_ENERGY:
        printf("Energy - child:%d\n", childNumber);
        vSetupStatusTab_drawEnergyInfoTile(detailTileObj);
        break;
    case BTN22_TILE_SIGNAL:
        printf("Signal - child:%d\n", childNumber);
        vSetupStatusTab_drawSignalStatTile(detailTileObj);
        break;
    case BTN31_TILE_LINING:
        printf("Lining - child:%d\n", childNumber);
        vSetupStatusTab_drawLiningInfoTile(detailTileObj);
        break;
    case BTN32_TILE_PIPE:
        printf("Pipe - child:%d\n", childNumber);
        vSetupStatusTab_drawPipeInfoTile(detailTileObj);
        break;
    case BTN41_TILE_FLUID:
        printf("Fluid - child:%d\n", childNumber);
        vSetupStatusTab_drawFluidInfoTile(detailTileObj);
        break;
    case BTN42_TILE_SETPOINT:
        printf("Setpoint - child:%d\n", childNumber);
        vSetupStatusTab_drawSetpointTile(detailTileObj);
        break;
    case BTN51_TILE_SENSOR:
        printf("Sensor - child:%d\n", childNumber);
        vSetupStatusTab_drawSensorInfoTile(detailTileObj);
        break;
    case BTN52_TILE_MEASUREMENT:
        printf("Measurement - child:%d\n", childNumber);
        vSetupStatusTab_drawMeasurementInfoTile(detailTileObj);
        break;

    default:
        printf("Other!? - child:%d\n", childNumber);
        break;
    }
}

/////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////// DETAIL TILES CONSTRUCTORS ////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////

static void vSetupStatusTab_drawSetpointTile(infoDetailPageTile_t* detailPageObj)
{
    // Remove the callback set by previous clicked detail tile.
    if (pTileUpdatecallbackHandler != NULL) lv_obj_remove_event_dsc(detailPageObj->data_full_row_container, pTileUpdatecallbackHandler);

    MeasurementSession_t* curr_meas_session = tWikaConfigManager_getMeasurementSession();

    vSetupStatusTab_del_children(detailPageObj->data_full_row_container, 0);
    vSetupStatusTab_del_children(detailPageObj->header_container, 1);

    lv_obj_t* headContainer[STREAM_NUMBER];
    lv_obj_t* rowContainer[TABLE_SETPOINT_TILE_ROW_CNT];
    lv_obj_t* gradLine[TABLE_SETPOINT_TILE_ROW_CNT];
    lv_obj_t* tableElements[TABLE_SETPOINT_TILE_ROW_CNT][TABLE_ELEMENT_NUM_OF_COLS];

    vSetupStatusTab_setup_elements(detailPageObj, (void*)&tableElements[0], &rowContainer[0], &gradLine[0], TABLE_SETPOINT_TILE_ROW_CNT, TABLE_ELEMENT_NUM_OF_COLS);

    vSetupStatusTab_setup_header(detailPageObj, &headContainer[0], STREAM_NUMBER, lv_obj_get_width(tableElements[0][0]));

    // Update Table elements
    // Setpoint Type
    lv_label_set_text(tableElements[TABLE_SETPOINT_TILE_TYPE_ROW][TABLE_ELEMENT_COL_LABEL], (char*)u8WikaGUI_getLabel(LABELS_GENERIC_TYPE, DEFAULT_LANGUAGE));

    // Cutoff
    lv_label_set_text(tableElements[TABLE_SETPOINT_TILE_CUTOFF_ROW][TABLE_ELEMENT_COL_LABEL], (char*)u8WikaGUI_getLabel(LABELS_SETPOINT_CUTOFF, DEFAULT_LANGUAGE));

    // Warning
    lv_label_set_text(tableElements[TABLE_SETPOINT_TILE_WARNING_ROW][TABLE_ELEMENT_COL_LABEL], (char*)u8WikaGUI_getLabel(LABELS_SETPOINT_WARNING, DEFAULT_LANGUAGE));

    // Full-Scale
    lv_label_set_text(tableElements[TABLE_SETPOINT_TILE_FULLSCALE_ROW][TABLE_ELEMENT_COL_LABEL], (char*)u8WikaGUI_getLabel(LABELS_SETPOINT_FULLSCALE, DEFAULT_LANGUAGE));

    if (curr_meas_session->streams[DAQ_STREAM_1].configuration.setPointType == SETPOINT_SPEED) {
        lv_label_set_text(tableElements[TABLE_SETPOINT_TILE_TYPE_ROW][TABLE_ELEMENT_COL_STREAM1], (char*)u8WikaGUI_getLabel(LABELS_SETPOINT_TYPE_SPEED, DEFAULT_LANGUAGE));
        lv_label_set_text_fmt(tableElements[TABLE_SETPOINT_TILE_CUTOFF_ROW][TABLE_ELEMENT_COL_STREAM1],
            SPEED_MS_PRINT_FORMAT, curr_meas_session->streams[DAQ_STREAM_1].configuration.setPointCutoff);

        lv_label_set_text_fmt(tableElements[TABLE_SETPOINT_TILE_WARNING_ROW][TABLE_ELEMENT_COL_STREAM1],
            SPEED_MS_PRINT_FORMAT, curr_meas_session->streams[DAQ_STREAM_1].configuration.setPointWarningThreshold);

        lv_label_set_text_fmt(tableElements[TABLE_SETPOINT_TILE_FULLSCALE_ROW][TABLE_ELEMENT_COL_STREAM1],
            SPEED_MS_PRINT_FORMAT, curr_meas_session->streams[DAQ_STREAM_1].configuration.setPointFullScale);

    }
    else {
        lv_label_set_text(tableElements[TABLE_SETPOINT_TILE_TYPE_ROW][TABLE_ELEMENT_COL_STREAM1], (char*)u8WikaGUI_getLabel(LABELS_SETPOINT_TYPE_FLOW, DEFAULT_LANGUAGE));

    }

    if (curr_meas_session->streams[DAQ_STREAM_2].configuration.setPointType == SETPOINT_SPEED) {
        lv_label_set_text(tableElements[TABLE_SETPOINT_TILE_TYPE_ROW][TABLE_ELEMENT_COL_STREAM2], (char*)u8WikaGUI_getLabel(LABELS_SETPOINT_TYPE_SPEED, DEFAULT_LANGUAGE));
        lv_label_set_text_fmt(tableElements[TABLE_SETPOINT_TILE_CUTOFF_ROW][TABLE_ELEMENT_COL_STREAM2],
            SPEED_MS_PRINT_FORMAT, curr_meas_session->streams[DAQ_STREAM_2].configuration.setPointCutoff);

        lv_label_set_text_fmt(tableElements[TABLE_SETPOINT_TILE_WARNING_ROW][TABLE_ELEMENT_COL_STREAM2],
            SPEED_MS_PRINT_FORMAT, curr_meas_session->streams[DAQ_STREAM_2].configuration.setPointWarningThreshold);

        lv_label_set_text_fmt(tableElements[TABLE_SETPOINT_TILE_FULLSCALE_ROW][TABLE_ELEMENT_COL_STREAM2],
            SPEED_MS_PRINT_FORMAT, curr_meas_session->streams[DAQ_STREAM_2].configuration.setPointFullScale);
    }
    else {
        lv_label_set_text(tableElements[TABLE_SETPOINT_TILE_TYPE_ROW][TABLE_ELEMENT_COL_STREAM2], (char*)u8WikaGUI_getLabel(LABELS_SETPOINT_TYPE_FLOW, DEFAULT_LANGUAGE));
    }

    lv_coord_t start_point = lv_obj_get_x(tableElements[0][TABLE_ELEMENT_COL_STREAM1]);
    lv_obj_set_x(headContainer[DAQ_STREAM_1], start_point);
    lv_label_set_text(headContainer[DAQ_STREAM_1], TABLE_HEADER_STREAM1_LABEL);

    start_point = lv_obj_get_x(tableElements[0][TABLE_ELEMENT_COL_STREAM2]);
    lv_obj_set_x(headContainer[DAQ_STREAM_2], start_point);
    lv_label_set_text(headContainer[DAQ_STREAM_2], TABLE_HEADER_STREAM2_LABEL);
    
    // Update Header Text
    lv_label_set_text(detailPageObj->submenu_header_lbl, (char*)u8WikaGUI_getLabel(LABELS_SETPOINT, DEFAULT_LANGUAGE));
}

static void vSetupStatusTab_drawTimingTile(infoDetailPageTile_t* detailPageObj)
{
    // Remove the callback set by previous clicked detail tile.
    if (pTileUpdatecallbackHandler != NULL) lv_obj_remove_event_dsc(detailPageObj->data_full_row_container, pTileUpdatecallbackHandler);

    MeasurementSession_t* curr_meas_session = tWikaConfigManager_getMeasurementSession();

    vSetupStatusTab_del_children(detailPageObj->data_full_row_container, 0);
    vSetupStatusTab_del_children(detailPageObj->header_container, 1);

    lv_obj_t *headContainer[STREAM_NUMBER];
    lv_obj_t *rowContainer[TABLE_TIMING_TILE_ROW_CNT];
    lv_obj_t *gradLine[TABLE_TIMING_TILE_ROW_CNT];
    lv_obj_t *tableElements[TABLE_TIMING_TILE_ROW_CNT][TABLE_ELEMENT_NUM_OF_COLS];

    vSetupStatusTab_setup_elements(detailPageObj, (void*)&tableElements[0], &rowContainer[0], &gradLine[0], TABLE_TIMING_TILE_ROW_CNT, TABLE_ELEMENT_NUM_OF_COLS);

    vSetupStatusTab_setup_header(detailPageObj, &headContainer[0], STREAM_NUMBER, lv_obj_get_width(tableElements[0][0]));

    // tUPS
    lv_label_set_text(tableElements[TABLE_TIMING_TILE_TUPS_ROW][TABLE_ELEMENT_COL_LABEL], (char*)u8WikaGUI_getLabel(LABELS_TIMING_TUPS, DEFAULT_LANGUAGE));
    lv_label_set_text_fmt(tableElements[TABLE_TIMING_TILE_TUPS_ROW][TABLE_ELEMENT_COL_STREAM1], TUS_PRINT_FORMAT, 0.0);
    lv_label_set_text_fmt(tableElements[TABLE_TIMING_TILE_TUPS_ROW][TABLE_ELEMENT_COL_STREAM2], TUS_PRINT_FORMAT, 0.0);

    // tDNS
    lv_label_set_text(tableElements[TABLE_TIMING_TILE_TDNS_ROW][TABLE_ELEMENT_COL_LABEL], (char*)u8WikaGUI_getLabel(LABELS_TIMING_TDNS, DEFAULT_LANGUAGE));
    lv_label_set_text_fmt(tableElements[TABLE_TIMING_TILE_TDNS_ROW][TABLE_ELEMENT_COL_STREAM1], TUS_PRINT_FORMAT, 0.0);
    lv_label_set_text_fmt(tableElements[TABLE_TIMING_TILE_TDNS_ROW][TABLE_ELEMENT_COL_STREAM2], TUS_PRINT_FORMAT, 0.0);

    // dTOF
    lv_label_set_text(tableElements[TABLE_TIMING_TILE_DTOF_ROW][TABLE_ELEMENT_COL_LABEL], (char*)u8WikaGUI_getLabel(LABELS_TIMING_DTOF, DEFAULT_LANGUAGE));
    lv_label_set_text_fmt(tableElements[TABLE_TIMING_TILE_DTOF_ROW][TABLE_ELEMENT_COL_STREAM1], TNS_PRINT_FORMAT, 0.0);
    lv_label_set_text_fmt(tableElements[TABLE_TIMING_TILE_DTOF_ROW][TABLE_ELEMENT_COL_STREAM2], TNS_PRINT_FORMAT, 0.0);

    // ZeroOffset
    lv_label_set_text(tableElements[TABLE_TIMING_TILE_ZOFF_ROW][TABLE_ELEMENT_COL_LABEL], (char*)u8WikaGUI_getLabel(LABELS_TIMING_ZEROFLOW_OFF, DEFAULT_LANGUAGE));
    lv_label_set_text_fmt(  tableElements[TABLE_TIMING_TILE_ZOFF_ROW][TABLE_ELEMENT_COL_STREAM1],
                            TNS_PRINT_FORMAT,
                            curr_meas_session->streams[DAQ_STREAM_1].configuration.dTOFzeroOffsetInNs);
    lv_label_set_text_fmt(  tableElements[TABLE_TIMING_TILE_ZOFF_ROW][TABLE_ELEMENT_COL_STREAM2],
                            TNS_PRINT_FORMAT,
                            curr_meas_session->streams[DAQ_STREAM_2].configuration.dTOFzeroOffsetInNs);

    lv_coord_t start_point = lv_obj_get_x(tableElements[TABLE_TIMING_TILE_TUPS_ROW][TABLE_ELEMENT_COL_STREAM1]);
    lv_obj_set_x(headContainer[DAQ_STREAM_1], start_point);
    lv_label_set_text(headContainer[DAQ_STREAM_1], TABLE_HEADER_STREAM1_LABEL);

    start_point = lv_obj_get_x(tableElements[TABLE_TIMING_TILE_TUPS_ROW][TABLE_ELEMENT_COL_STREAM2]);
    lv_obj_set_x(headContainer[DAQ_STREAM_2], start_point);
    lv_label_set_text(headContainer[DAQ_STREAM_2], TABLE_HEADER_STREAM2_LABEL);

    // Update Header Text
    lv_label_set_text(detailPageObj->submenu_header_lbl, (char*)u8WikaGUI_getLabel(LABELS_TIMING, DEFAULT_LANGUAGE));
}

static void vSetupStatusTab_drawSignalStatTile(infoDetailPageTile_t* detailPageObj)
{
    /// Remove the callback set by previous clicked detail tile.
    if (pTileUpdatecallbackHandler != NULL) lv_obj_remove_event_dsc(detailPageObj->data_full_row_container, pTileUpdatecallbackHandler);

    vSetupStatusTab_del_children(detailPageObj->data_full_row_container, 0);
    vSetupStatusTab_del_children(detailPageObj->header_container, 1);

    lv_obj_t* headContainer[STREAM_NUMBER];
    lv_obj_t* rowContainer[TABLE_SIGNAL_TILE_ROW_CNT];
    lv_obj_t* gradLine[TABLE_SIGNAL_TILE_ROW_CNT];
    lv_obj_t* tableElements[TABLE_SIGNAL_TILE_ROW_CNT][TABLE_ELEMENT_NUM_OF_COLS];

    vSetupStatusTab_setup_elements(detailPageObj, (void*)&tableElements[0], &rowContainer[0], &gradLine[0], TABLE_SIGNAL_TILE_ROW_CNT, TABLE_ELEMENT_NUM_OF_COLS);

    vSetupStatusTab_setup_header(detailPageObj, &headContainer[0], STREAM_NUMBER, lv_obj_get_width(tableElements[0][0]));

    // Update Table elements
    // Signal Level
    lv_label_set_text(tableElements[TABLE_SIGNAL_TILE_LEVEL_ROW][TABLE_ELEMENT_COL_LABEL], (char*)u8WikaGUI_getLabel(LABELS_SIGNAL_LEVEL, DEFAULT_LANGUAGE));
    lv_label_set_text_fmt(tableElements[TABLE_SIGNAL_TILE_LEVEL_ROW][TABLE_ELEMENT_COL_STREAM1], SIGNAL_LVL_PRINT_FORMAT, 0.0);
    lv_label_set_text_fmt(tableElements[TABLE_SIGNAL_TILE_LEVEL_ROW][TABLE_ELEMENT_COL_STREAM2], SIGNAL_LVL_PRINT_FORMAT, 0.0);
    // AGC Gain
    lv_label_set_text(tableElements[TABLE_SIGNAL_TILE_AGC_GAIN_ROW][TABLE_ELEMENT_COL_LABEL], (char*)u8WikaGUI_getLabel(LABELS_SIGNAL_AGC_GAIN, DEFAULT_LANGUAGE));
    lv_label_set_text_fmt(tableElements[TABLE_SIGNAL_TILE_AGC_GAIN_ROW][TABLE_ELEMENT_COL_STREAM1], "00.0 dB");
    lv_label_set_text_fmt(tableElements[TABLE_SIGNAL_TILE_AGC_GAIN_ROW][TABLE_ELEMENT_COL_STREAM2], "00.0 dB");
    // Fluid Attenuation
    lv_label_set_text(tableElements[TABLE_SIGNAL_TILE_FLUID_ATTN_ROW][TABLE_ELEMENT_COL_LABEL], (char*)u8WikaGUI_getLabel(LABELS_SIGNAL_FLUID_ATTEN, DEFAULT_LANGUAGE));
    lv_label_set_text_fmt(tableElements[TABLE_SIGNAL_TILE_FLUID_ATTN_ROW][TABLE_ELEMENT_COL_STREAM1], "00.0 dB");
    lv_label_set_text_fmt(tableElements[TABLE_SIGNAL_TILE_FLUID_ATTN_ROW][TABLE_ELEMENT_COL_STREAM2], "00.0 dB");
    // Signal Quality
    lv_label_set_text(tableElements[TABLE_SIGNAL_TILE_QUALITY_ROW][TABLE_ELEMENT_COL_LABEL], (char*)u8WikaGUI_getLabel(LABELS_SIGNAL_QUALITY, DEFAULT_LANGUAGE));
    lv_label_set_text_fmt(tableElements[TABLE_SIGNAL_TILE_QUALITY_ROW][TABLE_ELEMENT_COL_STREAM1], "poor");
    lv_label_set_text_fmt(tableElements[TABLE_SIGNAL_TILE_QUALITY_ROW][TABLE_ELEMENT_COL_STREAM2], "poor");

    lv_coord_t start_point = lv_obj_get_x(tableElements[0][TABLE_ELEMENT_COL_STREAM1]);
    lv_obj_set_x(headContainer[DAQ_STREAM_1], start_point);
    lv_label_set_text(headContainer[DAQ_STREAM_1], TABLE_HEADER_STREAM1_LABEL);

    start_point = lv_obj_get_x(tableElements[0][TABLE_ELEMENT_COL_STREAM2]);
    lv_obj_set_x(headContainer[DAQ_STREAM_2], start_point);
    lv_label_set_text(headContainer[DAQ_STREAM_2], TABLE_HEADER_STREAM2_LABEL);
    // TODO: Add refresh data callback
    // Add data refresh callback to update the values
    //lv_obj_add_event_cb(detailPageObj->dataTable, vSetupStatusTab_updateTimingTableValues, LV_EVENT_REFRESH, NULL);

    // Update Header Text
    lv_label_set_text(detailPageObj->submenu_header_lbl, (char*)u8WikaGUI_getLabel(LABELS_SIGNAL, DEFAULT_LANGUAGE));
}

static void vSetupStatusTab_drawPipeInfoTile(infoDetailPageTile_t* detailPageObj)
{
    // Remove the callback set by previous clicked detail tile.
    if (pTileUpdatecallbackHandler != NULL) lv_obj_remove_event_dsc(detailPageObj->data_full_row_container, pTileUpdatecallbackHandler);

    vSetupStatusTab_del_children(detailPageObj->data_full_row_container, 0);
    vSetupStatusTab_del_children(detailPageObj->header_container, 1);

    // Current Measurement Session Pointer
    MeasurementSession_t* measSession = tWikaConfigManager_getMeasurementSession();

    lv_obj_t* headContainer[STREAM_NUMBER];
    lv_obj_t* rowContainer[TABLE_PIPE_TILE_ROW_CNT];
    lv_obj_t* gradLine[TABLE_PIPE_TILE_ROW_CNT];
    lv_obj_t* tableElements[TABLE_PIPE_TILE_ROW_CNT][TABLE_ELEMENT_NUM_OF_COLS];

    vSetupStatusTab_setup_elements(detailPageObj, (void*)&tableElements[0], &rowContainer[0], &gradLine[0], TABLE_PIPE_TILE_ROW_CNT, TABLE_ELEMENT_NUM_OF_COLS);

    vSetupStatusTab_setup_header(detailPageObj, &headContainer[0], STREAM_NUMBER, lv_obj_get_width(tableElements[0][0]));

    // Update Table elements
    // Material
    lv_label_set_text(tableElements[TABLE_PIPE_TILE_MATERIAL_ROW][TABLE_ELEMENT_COL_LABEL],
        (char*)u8WikaGUI_getLabel(LABELS_PIPE_MATERIAL, DEFAULT_LANGUAGE));

    lv_label_set_text(tableElements[TABLE_PIPE_TILE_MATERIAL_ROW][TABLE_ELEMENT_COL_STREAM1],
        (char*)measSession->streams[DAQ_STREAM_1].parameters.externalEnvironment.pipe.Material);

    lv_label_set_text(tableElements[TABLE_PIPE_TILE_MATERIAL_ROW][TABLE_ELEMENT_COL_STREAM2],
        (char*)measSession->streams[DAQ_STREAM_2].parameters.externalEnvironment.pipe.Material);

    // External Diameter
    lv_label_set_text(tableElements[TABLE_PIPE_TILE_EXT_DIAMETER_ROW][TABLE_ELEMENT_COL_LABEL],
        (char*)u8WikaGUI_getLabel(LABELS_PIPE_EXT_DIAMETER, DEFAULT_LANGUAGE));

    lv_label_set_text_fmt(tableElements[TABLE_PIPE_TILE_EXT_DIAMETER_ROW][TABLE_ELEMENT_COL_STREAM1],
        DISTANCE_MM_PRINT_FORMAT,
        M_TO_MM(measSession->streams[DAQ_STREAM_1].parameters.externalEnvironment.pipe.DiameterInMt));

    lv_label_set_text_fmt(tableElements[TABLE_PIPE_TILE_EXT_DIAMETER_ROW][TABLE_ELEMENT_COL_STREAM2],
        DISTANCE_MM_PRINT_FORMAT,
        M_TO_MM(measSession->streams[DAQ_STREAM_2].parameters.externalEnvironment.pipe.DiameterInMt));

    // Thickness
    lv_label_set_text(tableElements[TABLE_PIPE_TILE_THICKNESS_ROW][TABLE_ELEMENT_COL_LABEL],
        (char*)u8WikaGUI_getLabel(LABELS_PIPE_THICKNESS, DEFAULT_LANGUAGE));

    lv_label_set_text_fmt(tableElements[TABLE_PIPE_TILE_THICKNESS_ROW][TABLE_ELEMENT_COL_STREAM1],
        DISTANCE_MM_PRINT_FORMAT,
        M_TO_MM(measSession->streams[DAQ_STREAM_1].parameters.externalEnvironment.pipe.ThicknessInMt));

    lv_label_set_text_fmt(tableElements[TABLE_PIPE_TILE_THICKNESS_ROW][TABLE_ELEMENT_COL_STREAM2],
        DISTANCE_MM_PRINT_FORMAT,
        M_TO_MM(measSession->streams[DAQ_STREAM_2].parameters.externalEnvironment.pipe.ThicknessInMt));

    // Temperature Source
    lv_label_set_text(tableElements[TABLE_PIPE_TILE_TEMP_SOURCE_ROW][TABLE_ELEMENT_COL_LABEL],
        (char*)u8WikaGUI_getLabel(LABELS_GENERIC_TEMP_SRC, DEFAULT_LANGUAGE));

    lv_label_set_text(tableElements[TABLE_PIPE_TILE_TEMP_SOURCE_ROW][TABLE_ELEMENT_COL_STREAM1],
        (char*)u8WikaGeneric_get_temp_source_string(measSession->streams[DAQ_STREAM_1].parameters.externalEnvironment.pipe.tempSource));

    lv_label_set_text(tableElements[TABLE_PIPE_TILE_TEMP_SOURCE_ROW][TABLE_ELEMENT_COL_STREAM2],
        (char*)u8WikaGeneric_get_temp_source_string(measSession->streams[DAQ_STREAM_2].parameters.externalEnvironment.pipe.tempSource));

    // Temperature
    lv_label_set_text(tableElements[TABLE_PIPE_TILE_TEMPERATURE_ROW][TABLE_ELEMENT_COL_LABEL],
        (char*)u8WikaGUI_getLabel(LABELS_GENERIC_TEMPERATURE, DEFAULT_LANGUAGE));
    // TODO: update the values at init.
    lv_label_set_text_fmt(tableElements[TABLE_PIPE_TILE_TEMPERATURE_ROW][TABLE_ELEMENT_COL_STREAM1],
        TEMPERATURE_PRINT_FORMAT,
        measSession->streams[DAQ_STREAM_1].parameters.externalEnvironment.pipe.workingTemp);

    lv_label_set_text_fmt(tableElements[TABLE_PIPE_TILE_TEMPERATURE_ROW][TABLE_ELEMENT_COL_STREAM2],
        TEMPERATURE_PRINT_FORMAT,
        measSession->streams[DAQ_STREAM_2].parameters.externalEnvironment.pipe.workingTemp);

    lv_coord_t start_point = lv_obj_get_x(tableElements[0][TABLE_ELEMENT_COL_STREAM1]);
    lv_obj_set_x(headContainer[DAQ_STREAM_1], start_point);
    lv_label_set_text(headContainer[DAQ_STREAM_1], TABLE_HEADER_STREAM1_LABEL);

    start_point = lv_obj_get_x(tableElements[0][TABLE_ELEMENT_COL_STREAM2]);
    lv_obj_set_x(headContainer[DAQ_STREAM_2], start_point);
    lv_label_set_text(headContainer[DAQ_STREAM_2], TABLE_HEADER_STREAM2_LABEL);

    // TODO: add temperature refresh callback
    // Add data refresh callback to update the values
    //lv_obj_add_event_cb(detailPageObj->dataTable, vSetupStatusTab_updateTimingTableValues, LV_EVENT_REFRESH, NULL);

    // Update Header Text
    lv_label_set_text(detailPageObj->submenu_header_lbl, (char*)u8WikaGUI_getLabel(LABELS_PIPE, DEFAULT_LANGUAGE));
}

static void vSetupStatusTab_drawSensorInfoTile(infoDetailPageTile_t* detailPageObj)
{
    // Remove the callback set by previous clicked detail tile.
    if (pTileUpdatecallbackHandler != NULL) lv_obj_remove_event_dsc(detailPageObj->data_full_row_container, pTileUpdatecallbackHandler);

    vSetupStatusTab_del_children(detailPageObj->data_full_row_container, 0);
    vSetupStatusTab_del_children(detailPageObj->header_container, 1);

    // Current Measurement Session Pointer
    MeasurementSession_t* measSession = tWikaConfigManager_getMeasurementSession();

    lv_obj_t* headContainer[STREAM_NUMBER];
    lv_obj_t* rowContainer[TABLE_SENSOR_TILE_ROW_CNT];
    lv_obj_t* gradLine[TABLE_SENSOR_TILE_ROW_CNT];
    lv_obj_t* tableElements[TABLE_SENSOR_TILE_ROW_CNT][TABLE_ELEMENT_NUM_OF_COLS];

    vSetupStatusTab_setup_elements(detailPageObj, (void*)&tableElements[0], &rowContainer[0], &gradLine[0], TABLE_SENSOR_TILE_ROW_CNT, TABLE_ELEMENT_NUM_OF_COLS);

    vSetupStatusTab_setup_header(detailPageObj, &headContainer[0], STREAM_NUMBER, lv_obj_get_width(tableElements[0][0]));

    // Update Table elements
    // Configuration
    lv_label_set_text(tableElements[TABLE_SENSOR_TILE_CFG_ROW][TABLE_ELEMENT_COL_LABEL],
        (char*)u8WikaGUI_getLabel(LABELS_SENSOR_CONFIGURATION, DEFAULT_LANGUAGE));

    lv_label_set_text_fmt(tableElements[TABLE_SENSOR_TILE_CFG_ROW][TABLE_ELEMENT_COL_STREAM1],
        "%c",
        (char)u8WikaGeneric_get_sensor_cfg_as_char(measSession->streams[DAQ_STREAM_1].configuration.sensor_configuration));

    lv_label_set_text_fmt(tableElements[TABLE_SENSOR_TILE_CFG_ROW][TABLE_ELEMENT_COL_STREAM2],
        "%c",
        (char)u8WikaGeneric_get_sensor_cfg_as_char(measSession->streams[DAQ_STREAM_2].configuration.sensor_configuration));

    // Distance
    lv_label_set_text(tableElements[TABLE_SENSOR_TILE_DISTANCE_ROW][TABLE_ELEMENT_COL_LABEL],
        (char*)u8WikaGUI_getLabel(LABELS_SENSOR_DISTANCE, DEFAULT_LANGUAGE));

    lv_label_set_text_fmt(tableElements[TABLE_SENSOR_TILE_DISTANCE_ROW][TABLE_ELEMENT_COL_STREAM1],
        DISTANCE_MM_PRINT_FORMAT,
        M_TO_MM(measSession->streams[DAQ_STREAM_1].preCalculations.mountingDistance));

    lv_label_set_text_fmt(tableElements[TABLE_SENSOR_TILE_DISTANCE_ROW][TABLE_ELEMENT_COL_STREAM2],
        DISTANCE_MM_PRINT_FORMAT,
        M_TO_MM(measSession->streams[DAQ_STREAM_2].preCalculations.mountingDistance));

    // Type
    lv_label_set_text(tableElements[TABLE_SENSOR_TILE_TYPE_ROW][TABLE_ELEMENT_COL_LABEL],
        (char*)u8WikaGUI_getLabel(LABELS_GENERIC_TYPE, DEFAULT_LANGUAGE));

    lv_label_set_text(tableElements[TABLE_SENSOR_TILE_TYPE_ROW][TABLE_ELEMENT_COL_STREAM1],
        (char*)measSession->streams[DAQ_STREAM_1].parameters.daq.channels[0].ussSensor.Name);

    lv_label_set_text(tableElements[TABLE_SENSOR_TILE_TYPE_ROW][TABLE_ELEMENT_COL_STREAM2],
        (char*)measSession->streams[DAQ_STREAM_2].parameters.daq.channels[0].ussSensor.Name);

    // Frequency
    lv_label_set_text(tableElements[TABLE_SENSOR_TILE_FREQ_ROW][TABLE_ELEMENT_COL_LABEL],
        (char*)u8WikaGUI_getLabel(LABELS_SENSOR_FREQUENCY, DEFAULT_LANGUAGE));

    lv_label_set_text_fmt(tableElements[TABLE_SENSOR_TILE_FREQ_ROW][TABLE_ELEMENT_COL_STREAM1],
        FREQUENCY_KHZ_PRINT_FORMAT,
        measSession->streams[DAQ_STREAM_1].parameters.daq.channels[0].ussSensor.Frequency);

    lv_label_set_text_fmt(tableElements[TABLE_SENSOR_TILE_FREQ_ROW][TABLE_ELEMENT_COL_STREAM2],
        FREQUENCY_KHZ_PRINT_FORMAT,
        measSession->streams[DAQ_STREAM_2].parameters.daq.channels[0].ussSensor.Frequency);

    lv_coord_t start_point = lv_obj_get_x(tableElements[0][TABLE_ELEMENT_COL_STREAM1]);
    lv_obj_set_x(headContainer[DAQ_STREAM_1], start_point);
    lv_label_set_text(headContainer[DAQ_STREAM_1], TABLE_HEADER_STREAM1_LABEL);

    start_point = lv_obj_get_x(tableElements[0][TABLE_ELEMENT_COL_STREAM2]);
    lv_obj_set_x(headContainer[DAQ_STREAM_2], start_point);
    lv_label_set_text(headContainer[DAQ_STREAM_2], TABLE_HEADER_STREAM2_LABEL);


    // TODO: add temperature refresh callback
    // Add data refresh callback to update the values
    //lv_obj_add_event_cb(detailPageObj->dataTable, vSetupStatusTab_updateTimingTableValues, LV_EVENT_REFRESH, NULL);

    // Update Header Text
    lv_label_set_text(detailPageObj->submenu_header_lbl, (char*)u8WikaGUI_getLabel(LABELS_SENSOR, DEFAULT_LANGUAGE));
}

static void vSetupStatusTab_drawFluidInfoTile(infoDetailPageTile_t* detailPageObj)
{
    // Remove the callback set by previous clicked detail tile.
    if (pTileUpdatecallbackHandler != NULL) lv_obj_remove_event_dsc(detailPageObj->data_full_row_container, pTileUpdatecallbackHandler);

    vSetupStatusTab_del_children(detailPageObj->data_full_row_container, 0);
    vSetupStatusTab_del_children(detailPageObj->header_container, 1);

    // Current Measurement Session Pointer
    MeasurementSession_t* measSession = tWikaConfigManager_getMeasurementSession();

    lv_obj_t* headContainer[STREAM_NUMBER];
    lv_obj_t* rowContainer[TABLE_FLUID_TILE_ROW_CNT];
    lv_obj_t* gradLine[TABLE_FLUID_TILE_ROW_CNT];
    lv_obj_t* tableElements[TABLE_FLUID_TILE_ROW_CNT][TABLE_ELEMENT_NUM_OF_COLS];

    vSetupStatusTab_setup_elements(detailPageObj, (void*)&tableElements[0], &rowContainer[0], &gradLine[0], TABLE_FLUID_TILE_ROW_CNT, TABLE_ELEMENT_NUM_OF_COLS);

    vSetupStatusTab_setup_header(detailPageObj, &headContainer[0], STREAM_NUMBER, lv_obj_get_width(tableElements[0][0]));

    // Update Table elements
    // Type
    lv_label_set_text(tableElements[TABLE_FLUID_TILE_TYPE_ROW][TABLE_ELEMENT_COL_LABEL],
        (char*)u8WikaGUI_getLabel(LABELS_GENERIC_TYPE, DEFAULT_LANGUAGE));

    lv_label_set_text(tableElements[TABLE_FLUID_TILE_TYPE_ROW][TABLE_ELEMENT_COL_STREAM1],
        (char*)measSession->streams[DAQ_STREAM_1].parameters.externalEnvironment.fluid.Name);

    lv_label_set_text(tableElements[TABLE_FLUID_TILE_TYPE_ROW][TABLE_ELEMENT_COL_STREAM2],
        (char*)measSession->streams[DAQ_STREAM_2].parameters.externalEnvironment.fluid.Name);

    // Temperature Source
    lv_label_set_text(tableElements[TABLE_FLUID_TILE_TEMP_SRC_ROW][TABLE_ELEMENT_COL_LABEL],
        (char*)u8WikaGUI_getLabel(LABELS_GENERIC_TEMP_SRC, DEFAULT_LANGUAGE));

    lv_label_set_text(tableElements[TABLE_FLUID_TILE_TEMP_SRC_ROW][TABLE_ELEMENT_COL_STREAM1],
        (char*)u8WikaGeneric_get_temp_source_string(measSession->streams[DAQ_STREAM_1].parameters.externalEnvironment.fluid.tempSource));

    lv_label_set_text(tableElements[TABLE_FLUID_TILE_TEMP_SRC_ROW][TABLE_ELEMENT_COL_STREAM2],
        (char*)u8WikaGeneric_get_temp_source_string(measSession->streams[DAQ_STREAM_2].parameters.externalEnvironment.fluid.tempSource));

    // Temperature
    lv_label_set_text(tableElements[TABLE_FLUID_TILE_TEMP_ROW][TABLE_ELEMENT_COL_LABEL],
        (char*)u8WikaGUI_getLabel(LABELS_GENERIC_TEMPERATURE, DEFAULT_LANGUAGE));

    lv_label_set_text_fmt(tableElements[TABLE_FLUID_TILE_TEMP_ROW][TABLE_ELEMENT_COL_STREAM1],
        TEMPERATURE_PRINT_FORMAT,
        measSession->streams[DAQ_STREAM_1].parameters.externalEnvironment.fluid.WorkingTemperature);

    lv_label_set_text_fmt(tableElements[TABLE_FLUID_TILE_TEMP_ROW][TABLE_ELEMENT_COL_STREAM2],
        TEMPERATURE_PRINT_FORMAT,
        measSession->streams[DAQ_STREAM_2].parameters.externalEnvironment.fluid.WorkingTemperature);

    // Sound speed
    lv_label_set_text(tableElements[TABLE_FLUID_TILE_SOUNDSPEED_ROW][TABLE_ELEMENT_COL_LABEL],
        (char*)u8WikaGUI_getLabel(LABELS_GENERIC_SOUNDSPEED, DEFAULT_LANGUAGE));

    lv_label_set_text_fmt(tableElements[TABLE_FLUID_TILE_SOUNDSPEED_ROW][TABLE_ELEMENT_COL_STREAM1],
        SOUNDSPEED_MS_PRINT_FORMAT,
        measSession->streams[DAQ_STREAM_1].parameters.externalEnvironment.fluid.SoundSpeed);

    lv_label_set_text_fmt(tableElements[TABLE_FLUID_TILE_SOUNDSPEED_ROW][TABLE_ELEMENT_COL_STREAM2],
        SOUNDSPEED_MS_PRINT_FORMAT,
        measSession->streams[DAQ_STREAM_2].parameters.externalEnvironment.fluid.SoundSpeed);

    // TODO: add temperature refresh callback
    // Add data refresh callback to update the values
    //lv_obj_add_event_cb(detailPageObj->dataTable, vSetupStatusTab_updateTimingTableValues, LV_EVENT_REFRESH, NULL);

    lv_coord_t start_point = lv_obj_get_x(tableElements[0][TABLE_ELEMENT_COL_STREAM1]);
    lv_obj_set_x(headContainer[DAQ_STREAM_1], start_point);
    lv_label_set_text(headContainer[DAQ_STREAM_1], TABLE_HEADER_STREAM1_LABEL);

    start_point = lv_obj_get_x(tableElements[0][TABLE_ELEMENT_COL_STREAM2]);
    lv_obj_set_x(headContainer[DAQ_STREAM_2], start_point);
    lv_label_set_text(headContainer[DAQ_STREAM_2], TABLE_HEADER_STREAM2_LABEL);

    // Update Header Text
    lv_label_set_text(detailPageObj->submenu_header_lbl, (char*)u8WikaGUI_getLabel(LABELS_FLUID, DEFAULT_LANGUAGE));
}

static void vSetupStatusTab_drawLiningInfoTile(infoDetailPageTile_t* detailPageObj)
{
    // Remove the callback set by previous clicked detail tile.
    if (pTileUpdatecallbackHandler != NULL) lv_obj_remove_event_dsc(detailPageObj->data_full_row_container, pTileUpdatecallbackHandler);

    vSetupStatusTab_del_children(detailPageObj->data_full_row_container, 0);
    vSetupStatusTab_del_children(detailPageObj->header_container, 1);

    // Current Measurement Session Pointer
    MeasurementSession_t* measSession = tWikaConfigManager_getMeasurementSession();

    lv_obj_t* headContainer[STREAM_NUMBER];
    lv_obj_t* rowContainer[TABLE_LINING_TILE_ROW_CNT];
    lv_obj_t* gradLine[TABLE_LINING_TILE_ROW_CNT];
    lv_obj_t* tableElements[TABLE_LINING_TILE_ROW_CNT][TABLE_ELEMENT_NUM_OF_COLS];

    vSetupStatusTab_setup_elements(detailPageObj, (void*)&tableElements[0], &rowContainer[0], &gradLine[0], TABLE_LINING_TILE_ROW_CNT, TABLE_ELEMENT_NUM_OF_COLS);

    vSetupStatusTab_setup_header(detailPageObj, &headContainer[0], STREAM_NUMBER, lv_obj_get_width(tableElements[0][0]));

    // Update Table elements
    // Material
    lv_label_set_text(tableElements[TABLE_LINING_TILE_MATERIAL_ROW][TABLE_ELEMENT_COL_LABEL],
        (char*)u8WikaGUI_getLabel(LABELS_LINING_MATERIAL, DEFAULT_LANGUAGE));

    lv_label_set_text_fmt(tableElements[TABLE_LINING_TILE_MATERIAL_ROW][TABLE_ELEMENT_COL_STREAM1],
        "%d",
        measSession->streams[DAQ_STREAM_1].parameters.externalEnvironment.lining.Material);

    lv_label_set_text_fmt(tableElements[TABLE_LINING_TILE_MATERIAL_ROW][TABLE_ELEMENT_COL_STREAM2],
        "%d",
        measSession->streams[DAQ_STREAM_2].parameters.externalEnvironment.lining.Material);

    // Thickness
    lv_label_set_text(tableElements[TABLE_LINING_TILE_THICKNESS_ROW][TABLE_ELEMENT_COL_LABEL],
        (char*)u8WikaGUI_getLabel(LABELS_LINING_THICKNESS, DEFAULT_LANGUAGE));

    lv_label_set_text_fmt(tableElements[TABLE_LINING_TILE_THICKNESS_ROW][TABLE_ELEMENT_COL_STREAM1],
        DISTANCE_MM_PRINT_FORMAT,
        M_TO_MM(measSession->streams[DAQ_STREAM_1].parameters.externalEnvironment.lining.ThicknessInMt));

    lv_label_set_text_fmt(tableElements[TABLE_LINING_TILE_THICKNESS_ROW][TABLE_ELEMENT_COL_STREAM2],
        DISTANCE_MM_PRINT_FORMAT,
        M_TO_MM(measSession->streams[DAQ_STREAM_2].parameters.externalEnvironment.lining.ThicknessInMt));

    // Sound speed
    lv_label_set_text(tableElements[TABLE_LINING_TILE_SOUNDSPEED_ROW][TABLE_ELEMENT_COL_LABEL],
        (char*)u8WikaGUI_getLabel(LABELS_GENERIC_SOUNDSPEED, DEFAULT_LANGUAGE));

    lv_label_set_text_fmt(tableElements[TABLE_LINING_TILE_SOUNDSPEED_ROW][TABLE_ELEMENT_COL_STREAM1],
        SOUNDSPEED_MS_PRINT_FORMAT,
        measSession->streams[DAQ_STREAM_1].parameters.externalEnvironment.lining.SoundSpeed);

    lv_label_set_text_fmt(tableElements[TABLE_LINING_TILE_SOUNDSPEED_ROW][TABLE_ELEMENT_COL_STREAM2],
        SOUNDSPEED_MS_PRINT_FORMAT,
        measSession->streams[DAQ_STREAM_2].parameters.externalEnvironment.lining.SoundSpeed);

    // TODO: add temperature refresh callback
    // Add data refresh callback to update the values
    //lv_obj_add_event_cb(detailPageObj->dataTable, vSetupStatusTab_updateTimingTableValues, LV_EVENT_REFRESH, NULL);

    lv_coord_t start_point = lv_obj_get_x(tableElements[0][TABLE_ELEMENT_COL_STREAM1]);
    lv_obj_set_x(headContainer[DAQ_STREAM_1], start_point);
    lv_label_set_text(headContainer[DAQ_STREAM_1], TABLE_HEADER_STREAM1_LABEL);

    start_point = lv_obj_get_x(tableElements[0][TABLE_ELEMENT_COL_STREAM2]);
    lv_obj_set_x(headContainer[DAQ_STREAM_2], start_point);
    lv_label_set_text(headContainer[DAQ_STREAM_2], TABLE_HEADER_STREAM2_LABEL);

    // Update Header Text
    lv_label_set_text(detailPageObj->submenu_header_lbl, (char*)u8WikaGUI_getLabel(LABELS_LINING, DEFAULT_LANGUAGE));
}

static void vSetupStatusTab_drawMeasurementInfoTile(infoDetailPageTile_t* detailPageObj)
{
    // Remove the callback set by previous clicked detail tile.
    if (pTileUpdatecallbackHandler != NULL) lv_obj_remove_event_dsc(detailPageObj->data_full_row_container, pTileUpdatecallbackHandler);

    vSetupStatusTab_del_children(detailPageObj->data_full_row_container, 0);
    vSetupStatusTab_del_children(detailPageObj->header_container, 1);

    // Current Measurement Session Pointer
    MeasurementSession_t* measSession = tWikaConfigManager_getMeasurementSession();

    lv_obj_t* headContainer[STREAM_NUMBER];
    lv_obj_t* rowContainer[TABLE_MEASUREMENT_TILE_ROW_CNT];
    lv_obj_t* gradLine[TABLE_MEASUREMENT_TILE_ROW_CNT];
    lv_obj_t* tableElements[TABLE_MEASUREMENT_TILE_ROW_CNT][TABLE_ELEMENT_NUM_OF_COLS];

    vSetupStatusTab_setup_elements(detailPageObj, (void*)&tableElements[0], &rowContainer[0], &gradLine[0], TABLE_MEASUREMENT_TILE_ROW_CNT, TABLE_ELEMENT_NUM_OF_COLS);

    vSetupStatusTab_setup_header(detailPageObj, &headContainer[0], STREAM_NUMBER, lv_obj_get_width(tableElements[0][0]));

    // Update Table elements
    // Reynolds Correction
    lv_label_set_text(tableElements[TABLE_MEAS_TILE_REY_CORR_ROW][TABLE_ELEMENT_COL_LABEL],
        (char*)u8WikaGUI_getLabel(LABELS_MEASUREMENT_REYNOLDS, DEFAULT_LANGUAGE));

    lv_label_set_text(tableElements[TABLE_MEAS_TILE_REY_CORR_ROW][TABLE_ELEMENT_COL_STREAM1],
        (char*)(measSession->streams[DAQ_STREAM_1].configuration.enableReynold ? true :
            u8WikaGUI_getLabel(LABELS_GENERIC_ENABLED, DEFAULT_LANGUAGE),
            u8WikaGUI_getLabel(LABELS_GENERIC_DISABLED, DEFAULT_LANGUAGE)));

    lv_label_set_text(tableElements[TABLE_MEAS_TILE_REY_CORR_ROW][TABLE_ELEMENT_COL_STREAM2],
        (char*)(measSession->streams[DAQ_STREAM_2].configuration.enableReynold ? true :
            u8WikaGUI_getLabel(LABELS_GENERIC_ENABLED, DEFAULT_LANGUAGE),
            u8WikaGUI_getLabel(LABELS_GENERIC_DISABLED, DEFAULT_LANGUAGE)));

    // Accurate Measurement
    lv_label_set_text(tableElements[TABLE_MEAS_TILE_ACCURATE_ROW][TABLE_ELEMENT_COL_LABEL],
        (char*)u8WikaGUI_getLabel(LABELS_MEASUREMENT_ACCURATE, DEFAULT_LANGUAGE));

    lv_label_set_text(tableElements[TABLE_MEAS_TILE_ACCURATE_ROW][TABLE_ELEMENT_COL_STREAM1],
        (char*)(measSession->streams[DAQ_STREAM_1].configuration.dualChannelEnable ? true :
            u8WikaGUI_getLabel(LABELS_GENERIC_ENABLED, DEFAULT_LANGUAGE),
            u8WikaGUI_getLabel(LABELS_GENERIC_DISABLED, DEFAULT_LANGUAGE)));

    lv_label_set_text(tableElements[TABLE_MEAS_TILE_ACCURATE_ROW][TABLE_ELEMENT_COL_STREAM2],
        (char*)(measSession->streams[DAQ_STREAM_2].configuration.dualChannelEnable ? true :
            u8WikaGUI_getLabel(LABELS_GENERIC_ENABLED, DEFAULT_LANGUAGE),
            u8WikaGUI_getLabel(LABELS_GENERIC_DISABLED, DEFAULT_LANGUAGE)));

    // Meter selected
    lv_label_set_text(tableElements[TABLE_MEAS_TILE_METER_ROW][TABLE_ELEMENT_COL_LABEL],
        (char*)u8WikaGUI_getLabel(LABELS_MEASUREMENT_METER, DEFAULT_LANGUAGE));

    uint8_t selmeter[50];
    memset(&selmeter, 0, sizeof(selmeter));

    vWikaGeneric_get_active_meters(measSession->streams[DAQ_STREAM_1].configuration.userMeasure, &selmeter[0], sizeof(selmeter), DEFAULT_LANGUAGE);

    lv_label_set_text(tableElements[TABLE_MEAS_TILE_METER_ROW][TABLE_ELEMENT_COL_STREAM1],
        (char*)&selmeter[0]);

    vWikaGeneric_get_active_meters(measSession->streams[DAQ_STREAM_2].configuration.userMeasure, &selmeter[0], sizeof(selmeter), DEFAULT_LANGUAGE);

    lv_label_set_text(tableElements[TABLE_MEAS_TILE_METER_ROW][TABLE_ELEMENT_COL_STREAM2],
        (char*)&selmeter[0]);

    lv_coord_t start_point = lv_obj_get_x(tableElements[0][TABLE_ELEMENT_COL_STREAM1]);
    lv_obj_set_x(headContainer[DAQ_STREAM_1], start_point);
    lv_label_set_text(headContainer[DAQ_STREAM_1], TABLE_HEADER_STREAM1_LABEL);

    start_point = lv_obj_get_x(tableElements[0][TABLE_ELEMENT_COL_STREAM2]);
    lv_obj_set_x(headContainer[DAQ_STREAM_2], start_point);
    lv_label_set_text(headContainer[DAQ_STREAM_2], TABLE_HEADER_STREAM2_LABEL);

    // Update Header Text
    lv_label_set_text(detailPageObj->submenu_header_lbl, (char*)u8WikaGUI_getLabel(LABELS_MEASUREMENT, DEFAULT_LANGUAGE));
}

static void vSetupStatusTab_drawEnergyInfoTile(infoDetailPageTile_t* detailPageObj)
{
    // Remove the callback set by previous clicked detail tile.
    if (pTileUpdatecallbackHandler != NULL) lv_obj_remove_event_dsc(detailPageObj->data_full_row_container, pTileUpdatecallbackHandler);

    vSetupStatusTab_del_children(detailPageObj->data_full_row_container, 0);
    vSetupStatusTab_del_children(detailPageObj->header_container, 1);

    // Current Measurement Session Pointer
    MeasurementSession_t* measSession = tWikaConfigManager_getMeasurementSession();

    lv_obj_t* headContainer[STREAM_NUMBER];
    lv_obj_t* rowContainer[TABLE_ENERGY_TILE_ROW_CNT];
    lv_obj_t* gradLine[TABLE_ENERGY_TILE_ROW_CNT];
    lv_obj_t* tableElements[TABLE_ENERGY_TILE_ROW_CNT][TABLE_ELEMENT_NUM_OF_COLS];

    vSetupStatusTab_setup_elements(detailPageObj, (void*)&tableElements[0], &rowContainer[0], &gradLine[0], TABLE_ENERGY_TILE_ROW_CNT, TABLE_ELEMENT_NUM_OF_COLS);

    vSetupStatusTab_setup_header(detailPageObj, &headContainer[0], STREAM_NUMBER, lv_obj_get_width(tableElements[0][0]));

    // Update Table elements
    // Type
    lv_label_set_text(tableElements[TABLE_ENERGY_TILE_TYPE_ROW][TABLE_ELEMENT_COL_LABEL],
        (char*)u8WikaGUI_getLabel(LABELS_GENERIC_TYPE, DEFAULT_LANGUAGE));

    lv_label_set_text(tableElements[TABLE_ENERGY_TILE_TYPE_ROW][TABLE_ELEMENT_COL_STREAM1],
        (char*)u8WikaGeneric_get_energy_type_string(measSession->streams[DAQ_STREAM_1].configuration.energyMeasType, DEFAULT_LANGUAGE));

    lv_label_set_text(tableElements[TABLE_ENERGY_TILE_TYPE_ROW][TABLE_ELEMENT_COL_STREAM2],
        (char*)u8WikaGeneric_get_energy_type_string(measSession->streams[DAQ_STREAM_2].configuration.energyMeasType, DEFAULT_LANGUAGE));

    // First, check if the Energy Measurement is active or not
    if ((measSession->streams[DAQ_STREAM_1].configuration.userMeasure[USER_MEASURE_ENERGY] == true) &&
        (measSession->streams[DAQ_STREAM_2].configuration.userMeasure[USER_MEASURE_ENERGY] == true)) {

    }

    // Check whether the loss estimation is active or not.
    // If active, the send/return channel are stream 1 and stream 2
    // if not, the send/return are the Main/Aux channel of each stream

    // Send/Return
    lv_label_set_text(tableElements[TABLE_ENERGY_TILE_SEND_ROW][TABLE_ELEMENT_COL_LABEL],
        (char*)u8WikaGUI_getLabel(LABELS_ENERGY_DIRECTION, DEFAULT_LANGUAGE));

    if (measSession->streams[DAQ_STREAM_2].configuration.mainChannel == DAQ_STREAM_1) {
        lv_label_set_text(tableElements[TABLE_ENERGY_TILE_SEND_ROW][TABLE_ELEMENT_COL_STREAM1],
            (char*)u8WikaGUI_getLabel(LABELS_ENERGY_SEND, DEFAULT_LANGUAGE));
        lv_label_set_text(tableElements[ TABLE_ENERGY_TILE_SEND_ROW][TABLE_ELEMENT_COL_STREAM2],
            (char*)u8WikaGUI_getLabel(LABELS_ENERGY_RETURN, DEFAULT_LANGUAGE));
    }
    else {
        lv_label_set_text(tableElements[TABLE_ENERGY_TILE_SEND_ROW][TABLE_ELEMENT_COL_STREAM1],
            (char*)u8WikaGUI_getLabel(LABELS_ENERGY_RETURN, DEFAULT_LANGUAGE));
        lv_label_set_text(tableElements[TABLE_ENERGY_TILE_SEND_ROW][TABLE_ELEMENT_COL_STREAM2],
            (char*)u8WikaGUI_getLabel(LABELS_ENERGY_SEND, DEFAULT_LANGUAGE));
    }

    // TODO: add temperature refresh callback
    // Add data refresh callback to update the values
    //lv_obj_add_event_cb(detailPageObj->dataTable, vSetupStatusTab_updateTimingTableValues, LV_EVENT_REFRESH, NULL);

    lv_coord_t start_point = lv_obj_get_x(tableElements[0][TABLE_ELEMENT_COL_STREAM1]);
    lv_obj_set_x(headContainer[DAQ_STREAM_1], start_point);
    lv_label_set_text(headContainer[DAQ_STREAM_1], TABLE_HEADER_STREAM1_LABEL);

    start_point = lv_obj_get_x(tableElements[0][TABLE_ELEMENT_COL_STREAM2]);
    lv_obj_set_x(headContainer[DAQ_STREAM_2], start_point);
    lv_label_set_text(headContainer[DAQ_STREAM_2], TABLE_HEADER_STREAM2_LABEL);

    // Update Header Text
    lv_label_set_text(detailPageObj->submenu_header_lbl, (char*)u8WikaGUI_getLabel(LABELS_ENERGY, DEFAULT_LANGUAGE));
}

static void vSetupStatusTab_draw_AnIn_InfoTile(infoDetailPageTile_t* detailPageObj)
{
    // Remove the callback set by previous clicked detail tile.
    if (pTileUpdatecallbackHandler != NULL) lv_obj_remove_event_dsc(detailPageObj->data_full_row_container, pTileUpdatecallbackHandler);

    vSetupStatusTab_del_children(detailPageObj->data_full_row_container, 0);
    vSetupStatusTab_del_children(detailPageObj->header_container, 1);

    // Current Measurement Session Pointer
    MeasurementSession_t* measSession = tWikaConfigManager_getMeasurementSession();

    lv_obj_t* rowContainer[TABLE_ANIN_TILE_ROW_CNT];
    lv_obj_t* gradLine[TABLE_ANIN_TILE_ROW_CNT];
    lv_obj_t* tableElements[TABLE_ANIN_TILE_ROW_CNT][TABLE_ANIN_TILE_COL_CNT];

    vSetupStatusTab_setup_elements(detailPageObj, (void*)&tableElements[0], &rowContainer[0], &gradLine[0], TABLE_ANIN_TILE_ROW_CNT, TABLE_ANIN_TILE_COL_CNT);

    // Update Table elements
    // Analog In ID
    lv_label_set_text(tableElements[TABLE_ANIN_TILE_ID_ROW][TABLE_ELEMENT_LABEL_COL], TABLE_HEADER_NULL_LABEL);

    lv_label_set_text(tableElements[TABLE_ANIN_TILE_ID_ROW][TABLE_ELEMENT_ANIN1_COL], TABLE_HEADER_AN1_LABEL);

    lv_label_set_text(tableElements[TABLE_ANIN_TILE_ID_ROW][TABLE_ELEMENT_ANIN2_COL], TABLE_HEADER_AN2_LABEL);

    lv_label_set_text(tableElements[TABLE_ANIN_TILE_ID_ROW][TABLE_ELEMENT_ANIN3_COL], TABLE_HEADER_AN3_LABEL);

    lv_label_set_text(tableElements[TABLE_ANIN_TILE_ID_ROW][TABLE_ELEMENT_ANIN4_COL], TABLE_HEADER_AN4_LABEL);

    // Setup Labels First
    // Type
    lv_label_set_text(tableElements[TABLE_ANIN_TILE_TYPE_ROW][TABLE_ELEMENT_LABEL_COL],
        (char*)u8WikaGUI_getLabel(LABELS_GENERIC_TYPE, DEFAULT_LANGUAGE));
    // Unit
    lv_label_set_text(tableElements[TABLE_ANIN_TILE_UNIT_ROW][TABLE_ELEMENT_LABEL_COL],
        (char*)u8WikaGUI_getLabel(LABELS_GENERIC_UNIT, DEFAULT_LANGUAGE));
    // Endpoints
    lv_label_set_text(tableElements[TABLE_ANIN_TILE_ENDPT_ROW][TABLE_ELEMENT_LABEL_COL],
        (char*)u8WikaGUI_getLabel(LABELS_GENERIC_ENDPOINTS, DEFAULT_LANGUAGE));
    // Current Value
    lv_label_set_text(tableElements[TABLE_ANIN_TILE_CURRVAL_ROW][TABLE_ELEMENT_LABEL_COL],
        (char*)u8WikaGUI_getLabel(LABELS_GENERIC_CURR_READ, DEFAULT_LANGUAGE));

    // Value for Analog IN 1
    if (measSession->streams[DAQ_STREAM_1].parameters.analogInput[0].Enable == true) {
        lv_label_set_text(tableElements[TABLE_ANIN_TILE_TYPE_ROW][TABLE_ELEMENT_ANIN1_COL],
            vWikaGeneric_get_analog_in_type_str(measSession->streams[DAQ_STREAM_1].parameters.analogInput[0].Type, DEFAULT_LANGUAGE));

        lv_label_set_text(tableElements[TABLE_ANIN_TILE_UNIT_ROW][TABLE_ELEMENT_ANIN1_COL],
            (char*)measSession->streams[DAQ_STREAM_1].parameters.analogInput[0].Unit);

        lv_label_set_text_fmt(tableElements[TABLE_ANIN_TILE_ENDPT_ROW][TABLE_ELEMENT_ANIN1_COL],
            "%5.2f/%5.2f",
            measSession->streams[DAQ_STREAM_1].parameters.analogInput[0].EndPoint4,
            measSession->streams[DAQ_STREAM_1].parameters.analogInput[0].EndPoint20);

        lv_label_set_text_fmt(tableElements[TABLE_ANIN_TILE_CURRVAL_ROW][TABLE_ELEMENT_ANIN1_COL],
            "%5.2f",
            measSession->streams[DAQ_STREAM_1].parameters.analogInput[0].Value);
    }
    else {
        lv_label_set_text(tableElements[TABLE_ANIN_TILE_CURRVAL_ROW][TABLE_ELEMENT_ANIN1_COL],"---");
        lv_label_set_text(tableElements[TABLE_ANIN_TILE_UNIT_ROW][TABLE_ELEMENT_ANIN1_COL],"---");
        lv_label_set_text(tableElements[TABLE_ANIN_TILE_ENDPT_ROW][TABLE_ELEMENT_ANIN1_COL], "---");
        lv_label_set_text(tableElements[TABLE_ANIN_TILE_TYPE_ROW][TABLE_ELEMENT_ANIN1_COL],
                            (char*)u8WikaGUI_getLabel(LABELS_GENERIC_DISABLED, DEFAULT_LANGUAGE));
    }

    // Value for Analog IN 2
    if (measSession->streams[DAQ_STREAM_1].parameters.analogInput[1].Enable == true) {
        lv_label_set_text(tableElements[TABLE_ANIN_TILE_TYPE_ROW][TABLE_ELEMENT_ANIN2_COL],
            vWikaGeneric_get_analog_in_type_str(measSession->streams[DAQ_STREAM_1].parameters.analogInput[1].Type, DEFAULT_LANGUAGE));

        lv_label_set_text(tableElements[TABLE_ANIN_TILE_UNIT_ROW][TABLE_ELEMENT_ANIN2_COL],
            (char*)measSession->streams[DAQ_STREAM_1].parameters.analogInput[1].Unit);

        lv_label_set_text_fmt(tableElements[TABLE_ANIN_TILE_ENDPT_ROW][TABLE_ELEMENT_ANIN2_COL],
            "%5.2f/%5.2f",
            measSession->streams[DAQ_STREAM_1].parameters.analogInput[1].EndPoint4,
            measSession->streams[DAQ_STREAM_1].parameters.analogInput[1].EndPoint20);

        lv_label_set_text_fmt(tableElements[TABLE_ANIN_TILE_CURRVAL_ROW][TABLE_ELEMENT_ANIN2_COL],
            "%5.2f",
            measSession->streams[DAQ_STREAM_1].parameters.analogInput[1].Value);

    }
    else {
        lv_label_set_text(tableElements[TABLE_ANIN_TILE_CURRVAL_ROW][TABLE_ELEMENT_ANIN2_COL], "---");
        lv_label_set_text(tableElements[TABLE_ANIN_TILE_UNIT_ROW][TABLE_ELEMENT_ANIN2_COL], "---");
        lv_label_set_text(tableElements[TABLE_ANIN_TILE_ENDPT_ROW][TABLE_ELEMENT_ANIN2_COL], "---");
        lv_label_set_text(tableElements[TABLE_ANIN_TILE_TYPE_ROW][TABLE_ELEMENT_ANIN2_COL],
            (char*)u8WikaGUI_getLabel(LABELS_GENERIC_DISABLED, DEFAULT_LANGUAGE));
    }

    // Value for Analog IN 3
    if (measSession->streams[DAQ_STREAM_2].parameters.analogInput[0].Enable == true) {
        lv_label_set_text(tableElements[TABLE_ANIN_TILE_TYPE_ROW][TABLE_ELEMENT_ANIN3_COL],
            vWikaGeneric_get_analog_in_type_str(measSession->streams[DAQ_STREAM_2].parameters.analogInput[0].Type, DEFAULT_LANGUAGE));

        lv_label_set_text(tableElements[TABLE_ANIN_TILE_UNIT_ROW][TABLE_ELEMENT_ANIN3_COL],
            (char*)measSession->streams[DAQ_STREAM_2].parameters.analogInput[0].Unit);

        lv_label_set_text_fmt(tableElements[TABLE_ANIN_TILE_ENDPT_ROW][TABLE_ELEMENT_ANIN3_COL],
            "%5.2f/%5.2f",
            measSession->streams[DAQ_STREAM_2].parameters.analogInput[0].EndPoint4,
            measSession->streams[DAQ_STREAM_2].parameters.analogInput[0].EndPoint20);

        lv_label_set_text_fmt(tableElements[TABLE_ANIN_TILE_CURRVAL_ROW][TABLE_ELEMENT_ANIN3_COL],
            "%5.2f",
            measSession->streams[DAQ_STREAM_2].parameters.analogInput[0].Value);

    }
    else {
        lv_label_set_text(tableElements[TABLE_ANIN_TILE_CURRVAL_ROW][TABLE_ELEMENT_ANIN3_COL], "---");
        lv_label_set_text(tableElements[TABLE_ANIN_TILE_UNIT_ROW][TABLE_ELEMENT_ANIN3_COL], "---");
        lv_label_set_text(tableElements[TABLE_ANIN_TILE_ENDPT_ROW][TABLE_ELEMENT_ANIN3_COL], "---");
        lv_label_set_text(tableElements[TABLE_ANIN_TILE_TYPE_ROW][TABLE_ELEMENT_ANIN3_COL],
            (char*)u8WikaGUI_getLabel(LABELS_GENERIC_DISABLED, DEFAULT_LANGUAGE));
    }

    // Value for Analog IN 4
    if (measSession->streams[DAQ_STREAM_2].parameters.analogInput[1].Enable == true) {
        lv_label_set_text(tableElements[TABLE_ANIN_TILE_TYPE_ROW][TABLE_ELEMENT_ANIN4_COL],
            vWikaGeneric_get_analog_in_type_str(measSession->streams[DAQ_STREAM_2].parameters.analogInput[1].Type, DEFAULT_LANGUAGE));

        lv_label_set_text(tableElements[TABLE_ANIN_TILE_UNIT_ROW][TABLE_ELEMENT_ANIN4_COL],
            (char*)measSession->streams[DAQ_STREAM_2].parameters.analogInput[1].Unit);

        lv_label_set_text_fmt(tableElements[TABLE_ANIN_TILE_ENDPT_ROW][TABLE_ELEMENT_ANIN4_COL],
            "%5.2f/%5.2f",
            measSession->streams[DAQ_STREAM_2].parameters.analogInput[1].EndPoint4,
            measSession->streams[DAQ_STREAM_2].parameters.analogInput[1].EndPoint20);

        lv_label_set_text_fmt(tableElements[TABLE_ANIN_TILE_CURRVAL_ROW][TABLE_ELEMENT_ANIN4_COL],
            "%5.2f",
            measSession->streams[DAQ_STREAM_2].parameters.analogInput[1].Value);

    }
    else {
        lv_label_set_text(tableElements[TABLE_ANIN_TILE_CURRVAL_ROW][TABLE_ELEMENT_ANIN4_COL], "---");
        lv_label_set_text(tableElements[TABLE_ANIN_TILE_UNIT_ROW][TABLE_ELEMENT_ANIN4_COL], "---");
        lv_label_set_text(tableElements[TABLE_ANIN_TILE_ENDPT_ROW][TABLE_ELEMENT_ANIN4_COL], "---");
        lv_label_set_text(tableElements[TABLE_ANIN_TILE_TYPE_ROW][TABLE_ELEMENT_ANIN4_COL],
            (char*)u8WikaGUI_getLabel(LABELS_GENERIC_DISABLED, DEFAULT_LANGUAGE));
    }

    // Update Header Text
    lv_label_set_text(detailPageObj->submenu_header_lbl, (char*)u8WikaGUI_getLabel(LABELS_ANALOGIN, DEFAULT_LANGUAGE));
}

static inline void vSetupStatusTab_del_children(lv_obj_t* parent, uint8_t start_del_cnt)
{
    uint8_t counter = 0;
    uint8_t child_cnt = lv_obj_get_child_cnt(parent);
    if (child_cnt != 0) {
        for (counter = start_del_cnt; counter < child_cnt; counter++) {
            lv_obj_del_async(lv_obj_get_child(parent, counter));
        }
    }
}

static void vSetupStatusTab_setup_header(infoDetailPageTile_t* detailPageObj, lv_obj_t *lbl_header[], uint8_t col_cnt, lv_coord_t col_width)
{
    uint8_t counter = 0;
    for (counter = 0; counter < col_cnt; counter++) {
        lbl_header[counter] = lv_label_create(detailPageObj->header_container);
        lv_obj_add_style(lbl_header[counter], &detailPageObj->style_stream_titles, LV_PART_MAIN);
        lv_obj_set_width(lbl_header[counter], col_width);
    }
}

static void vSetupStatusTab_setup_elements( infoDetailPageTile_t* detailPageObj,
                                            void* elements[], lv_obj_t* row_container[], lv_obj_t* grad_sep_line[],
                                            const uint8_t row_count, const uint8_t col_count)
{
    uint8_t row_counter = 0;
    uint8_t col_counter = 0;

    lv_obj_t* (*tableElements)[3] = elements;

    lv_obj_align(detailPageObj->data_full_row_container, LV_ALIGN_BOTTOM_MID, 0, 0);

    lv_coord_t row_height = lv_obj_get_height(detailPageObj->data_full_row_container) / row_count;

    for (row_counter = 0; row_counter < row_count; row_counter++) {
        row_container[row_counter] = lv_obj_create(detailPageObj->data_full_row_container);
        lv_obj_set_height(row_container[row_counter], row_height);
        lv_obj_add_style(row_container[row_counter], &detailPageObj->style_data_container, LV_PART_MAIN);
        lv_obj_set_scrollbar_mode(row_container[row_counter], LV_SCROLLBAR_MODE_OFF);
        lv_obj_clear_flag(row_container[row_counter], LV_OBJ_FLAG_SCROLLABLE);
        if (row_counter > 0) {
            lv_obj_align_to(row_container[row_counter], row_container[row_counter - 1], LV_ALIGN_OUT_BOTTOM_MID, 0, 0);
        }
        lv_obj_update_layout(row_container[row_counter]);

        grad_sep_line[row_counter] = lv_obj_create(row_container[row_counter]);
        lv_obj_add_style(grad_sep_line[row_counter], &detailPageObj->style_data_separator_line, LV_PART_MAIN);
    }

    lv_coord_t col_width = ((lv_obj_get_width(row_container[0]) - 2 * lv_obj_get_style_border_width(row_container[0], LV_PART_MAIN)) / col_count);

    for (row_counter = 0; row_counter < row_count; row_counter++) {
        for (col_counter = 0; col_counter < col_count; col_counter++) {
            tableElements[row_counter][col_counter] = lv_label_create(row_container[row_counter]);
            lv_obj_set_style_bg_color(tableElements[row_counter][col_counter], tWikaGuiDesign_getColor(WIKA_RGB_COLOR_GREEN), LV_PART_MAIN);
            lv_obj_set_style_bg_opa(tableElements[row_counter][col_counter], LV_OPA_20 * (1 + col_counter), LV_PART_MAIN);
            lv_obj_set_width(tableElements[row_counter][col_counter], col_width - TILE_VIEW_VER_PAD);
            lv_obj_align(tableElements[row_counter][col_counter], LV_ALIGN_LEFT_MID, col_width * col_counter, 0);
            if (col_counter > 0) {
                lv_obj_add_style(tableElements[row_counter][col_counter], &detailPageObj->style_data_data_text, LV_PART_MAIN);
            }
            else {
                lv_obj_add_style(tableElements[row_counter][col_counter], &detailPageObj->style_data_label_text, LV_PART_MAIN);
                //lv_obj_set_style_max_width(tableElements[row_counter][col_counter], col_width - 12 * TILE_VIEW_VER_PAD, LV_PART_MAIN);
            }
            lv_label_set_long_mode(tableElements[row_counter][col_counter], LV_LABEL_LONG_WRAP);
            lv_obj_update_layout(tableElements[row_counter][col_counter]);
        }
    }
}

/////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////// EVENTS CALLBACKS /////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////
static void vSetupStatusTab_updateTimingTableValues(lv_event_t* event)
{
    lv_obj_t* table = lv_event_get_target(event);
    MeasurementData_t* guiData = lv_event_get_param(event);

    MeasurementSession_t* curr_meas_session = tWikaConfigManager_getMeasurementSession();

    // tUPS
    lv_table_set_cell_value_fmt(table,
        TABLE_TIMING_TILE_TUPS_ROW,
        (guiData->streamNumber + 1),
        TUS_PRINT_FORMAT,
        SEC_TO_US(guiData->filteredTUpInS));

    // tDNS
    lv_table_set_cell_value_fmt(table,
        TABLE_TIMING_TILE_TDNS_ROW,
        (guiData->streamNumber + 1),
        TUS_PRINT_FORMAT,
        SEC_TO_US(guiData->filteredTDownInS));

    // DTOF
    lv_table_set_cell_value_fmt(table,
        TABLE_TIMING_TILE_DTOF_ROW,
        (guiData->streamNumber + 1),
        TNS_PRINT_FORMAT,
        SEC_TO_NS(guiData->filteredDeltaTInS));

    // ZERO-FLOW OFFSET
    lv_table_set_cell_value_fmt(table,
        TABLE_TIMING_TILE_ZOFF_ROW,
        (guiData->streamNumber + 1),
        TNS_PRINT_FORMAT,
        curr_meas_session->streams[guiData->streamNumber].configuration.dTOFzeroOffsetInNs);
}



static void vSetupStatusTab_updateBtnStyle(lv_event_t* event)
{
    lv_event_code_t code = lv_event_get_code(event);

    lv_obj_t* button = lv_event_get_target(event);

    lv_obj_t* container = lv_obj_get_parent(button);

    lv_obj_t* label = lv_obj_get_child(button, 0);

    uint32_t nOfChild = lv_obj_get_child_cnt(container);

    lv_obj_t* centerImage = lv_obj_get_child(container, (nOfChild - 1));

    uint32_t currChildNum = lv_obj_get_index(button);

    if (code == LV_EVENT_FOCUSED) {
        lv_imgbtn_set_state(button, LV_IMGBTN_STATE_CHECKED_RELEASED);
        lv_obj_add_state(label, LV_STATE_CHECKED);
        lv_event_send(centerImage, LV_EVENT_VALUE_CHANGED, &currChildNum);
    }
    else if (code == LV_EVENT_CLICKED) {
        lv_imgbtn_set_state(button, LV_IMGBTN_STATE_CHECKED_RELEASED);
        lv_obj_add_state(label, LV_STATE_CHECKED);
        lv_obj_set_tile_id(pStatTabObj_obj->info_tileView_container, 0, 1, LV_ANIM_OFF);
        lv_event_send(pStatTabObj_obj->info_tileView_container, LV_EVENT_REFRESH, &currChildNum);
    }
    else if (code == LV_EVENT_RELEASED) {
        lv_imgbtn_set_state(button, LV_IMGBTN_STATE_RELEASED);
        lv_obj_clear_state(label, LV_STATE_PRESSED | LV_STATE_CHECKED);
    }
    else {
        printf("Evt: %d\n", code);
        lv_obj_clear_state(label, LV_STATE_PRESSED | LV_STATE_CHECKED);
    }

    lv_obj_update_layout(button);
}

static void vSetupStatusTab_updateCentralImage(lv_event_t* event)
{
    lv_obj_t* image = lv_event_get_target(event);

    uint32_t childNumber = *((uint32_t*)lv_event_get_param(event));

    switch (childNumber) {
    case BTN11_TILE_ANALOG_INPUTS:
        //lv_img_set_src(image, &analog_inputs_blue);
        break;
    case BTN12_TILE_TIMING:
        lv_img_set_src(image, &time_blue);
        break;
    case BTN21_TILE_ENERGY:
        break;
    case BTN22_TILE_SIGNAL:
        lv_img_set_src(image, &signalstat_blue);
        break;
    case BTN31_TILE_LINING:
        break;
    case BTN32_TILE_PIPE:
        break;
    case BTN41_TILE_FLUID:
        break;
    case BTN42_TILE_SETPOINT:
        lv_img_set_src(image, &setpoints_blue);
        break;
    case BTN51_TILE_SENSOR:
        //lv_img_set_src(image, &sensor_blue);
        break;
    case BTN52_TILE_MEASUREMENT:
        break;
    default:
        break;
    }
}

static void vSetupStatusTab_landingPageNavigation(lv_event_t* event)
{
    lv_obj_t* container = lv_event_get_target(event);

    uint32_t key_pressed = *((uint32_t*)lv_event_get_param(event));

    uint32_t counter = 0;

    // Check if the tile is the Landing page or the detail.
    // If detail tile, when Enter is pressed, return to landing page.
    lv_obj_t* tileViewContainer = lv_obj_get_parent(container);
    lv_obj_t* active_tile = lv_tileview_get_tile_act(tileViewContainer);

    if (active_tile != container) {
        if (key_pressed == LV_KEY_ENTER) {
            lv_obj_set_tile(tileViewContainer, container, LV_ANIM_OFF);
        }
        return;
    }

    // Obtain the number of children
    uint32_t childrenNumber = lv_obj_get_child_cnt(container);

    lv_obj_t* checkedBtn = NULL;

    for (counter = 0; counter < childrenNumber; counter++) {
        // Check which one is Checked
        checkedBtn = lv_obj_get_child(container, counter);

        lv_obj_t* btnLabel = lv_obj_get_child(checkedBtn, 0);

        if (lv_obj_get_state(btnLabel) == LV_STATE_CHECKED) {
            break;
        }
    }
    // Counter contains the checked element and checkedBtn the object itself.

    switch (key_pressed) {
    case LV_KEY_ENTER:
        lv_event_send(checkedBtn, LV_EVENT_CLICKED, NULL);
        break;

    case LV_KEY_STOP_MEAS:
    case LV_KEY_START_MEAS:
        // if Even, add 1
        if (counter % 2 == 0) {
            counter++;
        }
        // If Odd, subtract 1
        else {
            counter--;
        }

        // Update the checked object
        lv_event_send(lv_obj_get_child(container, counter), LV_EVENT_FOCUSED, NULL);

        // release the previously checked button
        lv_event_send(checkedBtn, LV_EVENT_RELEASED, NULL);
        break;

    case LV_KEY_UP:
        if (counter == 0) {
            counter = 8;
        }
        else if (counter == 1) {
            counter = 9;
        }
        else {
            counter -= 2;
        }

        // Update the checked object
        lv_event_send(lv_obj_get_child(container, counter), LV_EVENT_FOCUSED, NULL);

        // release the previously checked button
        lv_event_send(checkedBtn, LV_EVENT_RELEASED, NULL);
        break;

    case LV_KEY_DOWN:
        if (counter == 8) {
            counter = 0;
        }
        else if (counter == 9) {
            counter = 1;
        }
        else {
            counter += 2;
        }

        // Update the checked object
        lv_event_send(lv_obj_get_child(container, counter), LV_EVENT_FOCUSED, NULL);

        // release the previously checked button
        lv_event_send(checkedBtn, LV_EVENT_RELEASED, NULL);
        break;

    default:
        printf("WTF %d\n", key_pressed);
        break;
    }

}

/********************************* EOF ************************************************/
