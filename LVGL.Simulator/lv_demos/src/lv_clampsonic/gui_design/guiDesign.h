﻿/**************************************************************************************
 *  \file       guiDesign.h
 *
 *  \brief      Header file for design gui
 *
 *  \details    This file, groups the GUI design data structure and
 *  			declare the help functions that create the various GUI tabs and screens.
 *
 *  \author     Luca Agrippino
 *
 *  \version    0.0
 *
 *  \date		2 ago 2021
 *
 *  \copyright  (C) Copyright 2021, Euromisure Sas di WIKA Italia Srl
***************************************************************************************/

#ifndef COMPONENTS_ULTRAFLOW_APPLICATION_GUI_GUI_DESIGN_GUIDESIGN_H_
#define COMPONENTS_ULTRAFLOW_APPLICATION_GUI_GUI_DESIGN_GUIDESIGN_H_

#include <stdio.h>

#ifndef LVGL_SIMULATOR_ACTIVE
#include "lvgl.h"
#include "wika_fonts/symbol_fonts.h"
#include "images/buttons/info_buttons.h"
#include "images/info_imgs/info_images.h"
#include "images/main_scr_imgs/main_scr_images.h"
#include "dictionary/wikaGui_labels.h"
#include "measure_conversion.h"
#else
#include "lvgl/lvgl.h"
#include "images/buttons/info_buttons.h"
#include "images/info_imgs/info_images.h"
#include "images/main_scr_imgs/main_scr_images.h"
#include "dictionary/wikaGui_labels.h"
#include "../measure_lib/measure_conversion.h"
#include "fonts/symbol_fonts.h"
#endif // !LVGL_SIMULATOR_ACTIVE


/***************************************************************************
 ************* GENERIC DEFINITIONS *****************************************
 ***************************************************************************/
#define NUMBER_OF_SHOWN_MEASURE 		6

#define PCT_FULL_SIZE   100
#define PCT_HALF_SIZE   50
#define PCT_QRTR_SIZE   25
#define PCT_THRD_SIZE   33

 /*******************************************************************************
  * \enum	guiAppScreensLabels_t
  * 			Labels to describe the different screens.
  * 			When adding new labels or screens the GUI_SCREEN_NUMBER must be
  * 			kept updated.
  *******************************************************************************/
typedef enum _GUI_SCREEN_LABEL_ {
    GUI_SCR_LBL_WELCOME,
    GUI_SCR_LBL_MAIN,
    GUI_SCR_LBL_TEST,
    GUI_SCR_LBL_RESTART,
    GUI_SCR_LBL_FWUPDATE,
    GUI_SCR_LBL_POPUP_ENVIRONMENT,
    GUI_SCR_LBL_POPUP_NETWORK,
    GUI_SCR_LBL_POPUP_START_MEASUREMENT,
    GUI_SCR_LBL_POPUP_STOP_MEASUREMENT,
} guiAppScreensLabels_t;


/***************************************************************************
 ************* OBJECTS STRUCTURES ******************************************
 ***************************************************************************/
 /***************************************************************************************
  *	\struct	settingTabActive_objs
  ***************************************************************************************/
typedef struct _SETTINGS_TAB_ACT_OBJS_ {
    lv_obj_t* tabScreen_sysSetting_btn;
    lv_obj_t* tabScreen_logSetting_btn;
    lv_obj_t* tabScreen_measSetting_btn;
    lv_obj_t* tabScreen_sensorSetting_btn;
    lv_obj_t* tabScreen_wirelessSetting_btn;
    lv_obj_t* tabScreen_userSetting_btn;
    lv_obj_t* tabScreen_pipeSetting_btn;
    lv_obj_t* tabScreen_interfSetting_btn;
} settingTabActive_objs;

/***************************************************************************************
 *	\struct	settingTabStatic_objs
 ***************************************************************************************/
typedef struct _SETTINGS_TAB_STATIC_OBJS_ {
    lv_obj_t* tabScreen_systemSetting_lbl;
    lv_obj_t* tabScreen_logSetting_lbl;
    lv_obj_t* tabScreen_measureSetting_lbl;
    lv_obj_t* tabScreen_sensorSetting_lbl;
    lv_obj_t* tabScreen_wirelessSetting_lbl;
    lv_obj_t* tabScreen_userSetting_lbl;
    lv_obj_t* tabScreen_pipeSetting_lbl;
    lv_obj_t* tabScreen_interfaceSetting_lbl;
} settingTabStatic_objs;

//-----------------------------------------------------------------------------
typedef struct _SIDE_BUTTONS_ELEMENTS_ {
    lv_obj_t* img_bg;
    lv_obj_t* lbl_title;
    lv_obj_t* lbl_value;
} side_info_obj_t;

typedef struct _MEASURE_TILE_OBJ_ {
    // Tile - the parent of all the objects
    lv_obj_t* obj_main_tileView_tile;

    // Title label
    lv_obj_t* lbl_title;

    // Measure indicator
    lv_obj_t* lbl_main_indicator_value;
    lv_obj_t* lbl_main_indicator_unit;

    // Bar
    lv_obj_t* bar_fluid_bar;
    lv_obj_t* line_zero_point;

    // Line
    lv_obj_t* line_bar_detail_sep;

    // Details
    lv_obj_t* ico_fluid;
    lv_obj_t* lbl_fluid;
    lv_obj_t* lbl_channel;
    lv_obj_t* led_status;
    lv_obj_t* lbl_status;

    // side elements
    side_info_obj_t obj_12;
    side_info_obj_t obj_22;
    side_info_obj_t obj_32;
    side_info_obj_t obj_42;
    side_info_obj_t obj_52;

    // Object styles
    lv_style_t style_lbl_header;
    lv_style_t style_lbl_main_value;
    lv_style_t style_lbl_main_unit;
    lv_style_t style_bar_item;
    lv_style_t style_bar_bar_ok;
    lv_style_t style_bar_bar_warn;
    lv_style_t style_bar_bar_err;
    lv_style_t style_bar_cursor;
    lv_style_t style_bar_lines;
    lv_style_t style_lines;
    lv_style_t style_lbl_details;
    lv_style_t style_led_stat_ok;
    lv_style_t style_led_stat_nok;
    lv_style_t style_led_stat_off;

    lv_style_t style_img_bg;
    lv_style_t style_lbl_title;
    lv_style_t style_lbl_value;
    lv_style_t style_img_bg_disabled;
} measureTile_obj;

typedef struct _GRAPH_TILE_OBJS_ {
    // Container Tile - the main parent
    lv_obj_t* obj_main_tileView_tile;

    // Chart
    lv_obj_t* chart_UpsDnsWaveform;
    // series
    lv_chart_series_t* series_upsWaveform;
    lv_chart_series_t* series_dnsWaveform;
    // Chart Legend
    lv_obj_t* lbl_UpsLegend;
    lv_obj_t* lbl_DnsLegend;

    // Stat container
    lv_obj_t* obj_statContainer;
    lv_obj_t* lbl_Temp;
    lv_obj_t* lbl_Gain;
    lv_obj_t* lbl_SigLvl;
    lv_obj_t* lbl_Temp_val;
    lv_obj_t* lbl_Gain_val;
    lv_obj_t* lbl_SigLvl_val;

    // Styles
    lv_style_t style_UpsDnsChart;
    lv_style_t style_legend;
    lv_style_t style_StatText;

} graphTile_obj;

typedef struct _MEASURE_TAB_OBJS_ {
    lv_obj_t* tabScreen_tileView_container;
    measureTile_obj tabScreen_tileView_tile1;
    graphTile_obj 	tabScreen_tileView_tile2;

    // Objects styles
    lv_style_t style_tiles;
    lv_style_t style_scrollbar;
} measureTab_objs;

//-----------------------------------------------------------------------------
typedef enum _LABEL_VALUE_UNIT_ELEMENTS_ {
    LVU_LABEL = 1,
    LVU_LABEL_VALUE = 2,
    LVU_LABEL_VALUE_UNIT = 3,
} labelValueUnitElements_t;

typedef struct _LABEL_VALUE_UNIT_STRUCT_ {
    lv_obj_t* obj_container;
    lv_obj_t* lbl_label;
    lv_obj_t* lbl_value_str1;
    lv_obj_t* lbl_unit_str1;
    lv_obj_t* lbl_value_str2;
    lv_obj_t* lbl_unit_str2;
    labelValueUnitElements_t      n_of_elements;
} labelValueUnitStr_t;
//-----------------------------------------------------------------------------
typedef struct _SETTINGS_TAB_OBJECTS_ {
    // Buttons
    

    // Elements
    lv_obj_t* obj_container;
    lv_obj_t* obj_wifi_info_container;
    lv_obj_t* obj_web_info_container;
    lv_obj_t* img_bg_right;

    // Styles
    lv_style_t style_obj_info_container;
    lv_style_t style_img_bg;
    lv_style_t style_container_info_label;

} settingsTabObjs_t;

//-----------------------------------------------------------------------------
typedef struct _INFO_DETAIL_PAGE_ {
    // Container Tile - the main parent
    lv_obj_t* obj_main_tileView_tile;

    // Elements
    lv_obj_t* header_container;
    lv_obj_t* data_full_row_container;
    lv_obj_t* img_head_container_bg;

    // Common Elements
    lv_obj_t* submenu_header_lbl;
    lv_obj_t* submenu_header_bg_img;

    //////////////////////////////////////    

    // Submenu Styles
    lv_style_t style_header_text;
    lv_style_t style_header_bg_img;
    lv_style_t style_header_img;
    lv_style_t style_stream_titles;
    lv_style_t style_data_container;
    lv_style_t style_data_label_text;
    lv_style_t style_data_data_text;
    lv_style_t style_data_separator_line;
}infoDetailPageTile_t;

typedef struct _INFO_LANDING_PAGE_ {
    // Container Tile - the main parent
    lv_obj_t* obj_main_tileView_tile;

    // Objects
    lv_obj_t* btn_info[STAT_BUTTON_NUMBER];
    lv_obj_t* btn_info_label[STAT_BUTTON_NUMBER];
    lv_obj_t* centralImage;

    // Styles
    lv_style_t style_buttons_released;
    lv_style_t style_buttons_pressed;
    lv_style_t style_center_image;
    lv_style_t style_button_labels_left_side;
    lv_style_t style_button_labels_right_side;
    lv_style_t style_text_released;
    lv_style_t style_text_checked;

} infoLandingPageTile_t;


typedef struct _STATUS_TAB_OBJS_ {
    // TileView Containter.
    lv_obj_t* info_tileView_container;

    // First Tile of the TileView
    infoLandingPageTile_t   tile_info;
    infoDetailPageTile_t    tile_detail;

} statusTab_objs;

//-----------------------------------------------------------------------------

typedef struct _STATUS_BAR_OBJS_ {
    //////////////////////////////////////
    // The Status Bar as a MAIN container
    lv_obj_t* statusBar;
    //////////////////////////////////////
    // The Date/Time labels container
    lv_obj_t* statusBar_dateTime_cont;
    // The Date Label field
    lv_obj_t* statusBar_date_lbl;
    // The Time Label field
    lv_obj_t* statusBar_time_lbl;
    //////////////////////////////////////
    // Hostname Label
    lv_obj_t* statusBar_hostname_lbl;
    //////////////////////////////////////
    // Notify Icons container
    lv_obj_t* statusBar_notifyIcons_cont;
    // Wifi status icon
    lv_obj_t* statusBar_wifiStat;
    // Bluetooth status icon
    lv_obj_t* statusBar_bluetoothStat;
    // Serial Connection status icon
    lv_obj_t* statusBar_serialStat;
    // System Status icon
    lv_obj_t* statusBar_errNfy;
    //////////////////////////////////////
    // Line separators
    lv_obj_t* lineSeparator_L;
    lv_obj_t* lineSeparator_R;
    //////////////////////////////////////

    // Styles
    lv_style_t style_status_bar;
    lv_style_t style_containers;
    lv_style_t style_stat_icons;
    lv_style_t style_line;
    lv_style_t style_status_lbls;
} statusBar_objs;

/***************************************************************************************
 ******************************* TIMERS ************************************************
 ***************************************************************************************/
typedef struct _TIMERS_OBJS_ {
    /////////////////////////////////////////////
    // Timer to Update the Measurement Values
    /////////////////////////////////////////////
    lv_timer_t* updateMeasureTab_tmr;
    lv_timer_t* updateGraphTab_tmr;
    lv_timer_t* updateStatusTab_tmr;
    lv_timer_t* updateWaveFormGraph_tmr;
    lv_timer_t* updateMeasStat_tmr;
    /////////////////////////////////////////////
    // Timer to Update the Status Bar
    /////////////////////////////////////////////
    lv_timer_t* updateDateTime_tmr;
    lv_timer_t* updateNotifyIco_tmr;
    lv_timer_t* updateCenterField_tmr;
    /////////////////////////////////////////////
    // Timer to Update the Firmware Update Prog
    /////////////////////////////////////////////
    lv_timer_t* updateFwStats_tmr;
} timerStruct_objs;

/***************************************************************************************
 ******************************* SCREENS ***********************************************
 ***************************************************************************************/

 /***************************************************************************************
  *	\struct	testingScreen_objs
  *	TESTING SCREEN
  ***************************************************************************************/
typedef struct _TESTING_SCREEN_OBJS_ {
    lv_obj_t* headContainer;
    lv_obj_t* headLabel;
    lv_obj_t* bodyContainer;
    lv_obj_t* deltaT_lbl;
    lv_obj_t* tUp_lbl;
    lv_obj_t* tDown_lbl;
    lv_obj_t* error_lbl;
    lv_obj_t* flow_lbl;
    lv_obj_t* speed_lbl;
    lv_obj_t* deltaT_value_lbl;
    lv_obj_t* tUp_value_lbl;
    lv_obj_t* tDown_value_lbl;
    lv_obj_t* error_code_lbl;
    lv_obj_t* flow_value_lbl;
    lv_obj_t* speed_value_lbl;
    lv_obj_t* deltaT_unit_lbl;
    lv_obj_t* tUp_unit_lbl;
    lv_obj_t* tDown_unit_lbl;
    lv_obj_t* flow_unit_lbl;
    lv_obj_t* speed_unit_lbl;
    lv_obj_t* volume_lbl;
    lv_obj_t* volume_value_lbl;
    lv_obj_t* volume_unit_lbl;
    lv_obj_t* temperature_lbl;
    lv_obj_t* temperature_value_lbl;
    lv_obj_t* temperature_unit_lbl;
    lv_obj_t* massFlow_lbl;
    lv_obj_t* massFlow_value_lbl;
    lv_obj_t* massFlow_unit_lbl;
    lv_timer_t* updateValues_tmr;
} testingScreen_objs;

/***************************************************************************************
 *	\struct	restartScreen_objs
 *	RESTART SCREEN
 ***************************************************************************************/
typedef struct _RESTART_SCREEN_OBJS_ {
    lv_obj_t* wika_logo_img;
    lv_obj_t* progressBar_bar;
    lv_obj_t* restartPhase_lbl;
    lv_anim_t barAnimation_anim;
} restartScreen_objs;

/***************************************************************************************
 *	\struct	restartScreen_objs
 *	FIRMWARE UPDATE SCREEN
 ***************************************************************************************/
typedef struct _FW_UPDATE_SCREEN_OBJS_ {
    lv_obj_t* wika_logo_img;
    lv_obj_t* progressBar_bar;
    lv_obj_t* fwUpdatePhase_lbl;
    lv_obj_t* fwUpdateDataStats_lbl;
    lv_obj_t* customMessage_lbl;
} fwUpdateScreen_objs;

/***************************************************************************************
 *	\struct	welcomeScreen_objs
 *	WELCOME SCREEN
 ***************************************************************************************/
typedef struct _WELCOME_SCREEN_OBJS_ {
    lv_obj_t* wika_logo_img;
    lv_obj_t* progressBar_bar;
    lv_obj_t* loadPhase_lbl;
    lv_obj_t* hmiFWv_lbl;
    lv_obj_t* daqFWv_lbl;
    lv_obj_t* fwRel_lbl;
    lv_anim_t barAnimation_anim;
} welcomeScreen_objs;

/***************************************************************************************
 *	\struct	mainScreen_objs
 *	MAIN SCREEN
 ***************************************************************************************/
typedef struct _MAIN_SCREEN_OBJS_ {
    ////////////////////////
    // Tabview
    ////////////////////////
    lv_obj_t* tabView;
    lv_obj_t* tabButtons;
    lv_obj_t* tab_stream1;
    lv_obj_t* tab_stream2;
    lv_obj_t* tab_info;
    lv_obj_t* tab_settings;

    ////////////////////////
    // Tab Content
    ////////////////////////
    measureTab_objs 		tab_stream1_objs;
    measureTab_objs 		tab_stream2_objs;
    statusTab_objs			tab_status_objs;
    settingsTabObjs_t       tab_settings_objs;

    ////////////////////////
    // Bottom Status Bar
    ////////////////////////
    statusBar_objs statusBar_objs;

    ////////////////////////
    // Styles
    ////////////////////////
    lv_style_t style_tabs;
    lv_style_t style_tabview_default;
    lv_style_t style_tabview_checked;
} mainScreen_objs;

/***************************************************************************************
 ******************************* POPUPS ************************************************
 ***************************************************************************************/

typedef struct _ENV_RECAP_POPUP_OBJS_ {
    // TEXT STYLE
    lv_style_t style_text_head;
    lv_style_t style_text_value;
    lv_style_t style_label_continers;

    lv_obj_t* lbl_containers[15];

    // PIPE PARAMETERS
    lv_obj_t* lbl_pipeMaterial_head;
    lv_obj_t* lbl_pipeMaterial_value;
    lv_obj_t* lbl_pipeScheduleLbl_head;
    lv_obj_t* lbl_pipeScheduleLbl_value;
    lv_obj_t* lbl_pipeScheduleNum_head;
    lv_obj_t* lbl_pipeScheduleNum_value;
    lv_obj_t* lbl_pipeDiameter_head;
    lv_obj_t* lbl_pipeDiameter_value;
    lv_obj_t* lbl_pipeThickness_head;
    lv_obj_t* lbl_pipeThickness_value;
    lv_obj_t* lbl_pipeSoundSpeed_head;
    lv_obj_t* lbl_pipeSoundSpeed_value;
    // LINING PARAMETERS
    lv_obj_t* lbl_liningMaterial_head;
    lv_obj_t* lbl_liningMaterial_value;
    lv_obj_t* lbl_liningThickness_head;
    lv_obj_t* lbl_liningThickness_value;
    lv_obj_t* lbl_liningSoundSpeed_head;
    lv_obj_t* lbl_liningSoundSpeed_value;
    // FLUID PARAMETERS
    lv_obj_t* lbl_fluidSoundSpeed_head;
    lv_obj_t* lbl_fluidSoundSpeed_value;
    lv_obj_t* lbl_fluidSoundSpeedCoeff_head;
    lv_obj_t* lbl_fluidSoundSpeedCoeff_value;
    lv_obj_t* lbl_fluidAbsDensity_head;
    lv_obj_t* lbl_fluidAbsDensity_value;
    lv_obj_t* lbl_fluidKinViscosity_head;
    lv_obj_t* lbl_fluidKinViscosity_value;
    lv_obj_t* lbl_fluidEnthpy_head;
    lv_obj_t* lbl_fluidEnthpy_value;
    lv_obj_t* lbl_fluidSpecGrvty_head;
    lv_obj_t* lbl_fluidSpecGrvty_value;
} envRecapPopup_objs;


typedef struct {
    lv_obj_t* messageLabel;
    lv_obj_t* commonMessage;

} startStopMeasurePopup_objs;

typedef struct {
    lv_obj_t* wifiPopUp_screen_wifi_sw;
    lv_obj_t* wifiPopUp_screen_wifiMode_sw;
    lv_obj_t* wifiPopUp_screen_bluetooth_sw;
    lv_obj_t* wifiPopUp_screen_apply_btn;
} wifiPopupActive_objs;

typedef struct {
    lv_obj_t* wifiPopUp_screen_cont_1;
    lv_obj_t* wifiPopUp_screen_popupTitle;
    lv_obj_t* wifiPopUp_screen_line_1;
    lv_obj_t* wifiPopUp_screen_wifi_lbl;
    lv_obj_t* wifiPopUp_screen_wifiMode_lbl;
    lv_obj_t* wifiPopUp_screen_bluetooth_lbl;
    lv_obj_t* wifiPopUp_screen_line_4;
    lv_obj_t* wifiPopUp_screen_ssid_lbl;
    lv_obj_t* wifiPopUp_screen_ssid_value_lbl;
    lv_obj_t* wifiPopUp_screen_ip_lbl;
    lv_obj_t* wifiPopUp_screen_ip_value_lbl;
    lv_obj_t* wifiPopUp_screen_hostname_lbl;
    lv_obj_t* wifiPopUp_screen_hostname_value_lbl;
    lv_obj_t* wifiPopUp_screen_mask_lbl;
    lv_obj_t* wifiPopUp_screen_gateway_lbl;
    lv_obj_t* wifiPopUp_screen_mask_value_lbl;
    lv_obj_t* wifiPopUp_screen_gateway_value_lbl;
    lv_obj_t* wifiPopUp_screen_psw_lbl;
    lv_obj_t* wifiPopUp_screen_psw_value_lbl;
    lv_obj_t* wifiPopUp_screen_wifi_on_lbl;
    lv_obj_t* wifiPopUp_screen_wifi_off_lbl;
    lv_obj_t* wifiPopUp_screen_wifi_sta_lbl;
    lv_obj_t* wifiPopUp_screen_wifi_ap_lbl;
    lv_obj_t* wifiPopUp_screen_bluetooth_on_lbl;
    lv_obj_t* wifiPopUp_screen_bluetooth_off_lbl;
    lv_obj_t* wifiPopUp_screen_apply_btn_label;
    lv_obj_t* wifiPopUp_screen_status_lbl;
} wifiPopupStatic_objs;

/***************************************************************************************
 ******************************* GESTURE DRIVERS ***************************************
 ***************************************************************************************/
typedef struct _GUI_HELPER_OBJS_ {
    lv_group_t* guiGroup;
} guiHelper_objs;

/***************************************************************************************
 ******************************* GUI CONTAINER *****************************************
 ***************************************************************************************/
 /***************************************************************************************
  *	\struct	gui_objs
  *	\brief	GUI CONTAINER
  *			This structure contains all the GUI elements.
  ***************************************************************************************/
typedef struct _GUI_OBJS_ {
    /////////////////////////////////////////////
    // SCREENS
    /////////////////////////////////////////////
    // Standard Operation Screens
    lv_obj_t* screen_Welcome;
    lv_obj_t* screen_Main;
    lv_obj_t* screen_Restart;
    lv_obj_t* screen_Testing;
    lv_obj_t* screen_FwUpdate;

    // Popups
    lv_obj_t* popup_genericPopup;

    // Common elements
    lv_obj_t* lbl_popUpTitle;
    lv_obj_t* obj_dataContainer;
    lv_obj_t* btn_closePopUp;

    // Shared style of the PopUps windows
    lv_style_t style_genPopUp;
    lv_style_t style_popupTitle;
    lv_style_t style_dataContainer;
    lv_style_t style_closeBtn;
    lv_style_t style_closeBtn_pr;

    // All Screen Objects
    welcomeScreen_objs 	welcomeScreenObjs;
    mainScreen_objs 	mainScreenObjs;
    restartScreen_objs 	restartScreenObjs;
    testingScreen_objs	testingScreenObjs;
    fwUpdateScreen_objs fwUpdateScreenObjs;
    envRecapPopup_objs  envRecapPopUpObjs;
    startStopMeasurePopup_objs	ssDaqPopUpObjs;

    ////////////////////////
    // Timers
    ////////////////////////
    timerStruct_objs timers_objs;

    /////////////////////////////////////////////
    // INPUT DEVICE DRIVERS
    /////////////////////////////////////////////
    guiHelper_objs		guiHelpersObjs;
} gui_objs;


//extern wifiPopupActive_objs      wifiPopupActiveObjs;
//extern wifiPopupStatic_objs      wifiPopupStaticObjs;

/***************************************************************************
 ****************** IMAGE DECLARATIONS *************************************
 ***************************************************************************/
LV_IMG_DECLARE(wika_logo);
LV_IMG_DECLARE(statusError);
LV_IMG_DECLARE(statusOk);
LV_IMG_DECLARE(wifiErr);
LV_IMG_DECLARE(wifiFull);
LV_IMG_DECLARE(wifiMid);
LV_IMG_DECLARE(wifiPoor);
LV_IMG_DECLARE(img_skew_strip);
LV_IMG_DECLARE(warning_img_75x75);
LV_IMG_DECLARE(img_energy_direction_arrow);
LV_IMG_DECLARE(img_energy_meas_bg);
LV_IMG_DECLARE(wifiAP_nc);
LV_IMG_DECLARE(wifiAP_conn);
LV_IMG_DECLARE(BT_conn);
LV_IMG_DECLARE(BT_nc);
LV_IMG_DECLARE(BT_off);

/***************************************************************************************
 ******************************* OTHERS ************************************************
 ***************************************************************************************/
 /***************************************************************************
  * \struct  rgbColor_t
  * Structure containing the RGB colors
  ***************************************************************************/
typedef struct _RGB_COLOR_ {
    uint8_t Red;
    uint8_t Green;
    uint8_t Blue;
} rgbColor_t;

/***************************************************************************************
 ******************************* FUNCTIONS *********************************************
 ***************************************************************************************/

 // Screen Init Functions
void vSetupMainScreen_init(gui_objs* screenObjs, lv_obj_t* parent);
void vSetupWelcomeScreen_init(welcomeScreen_objs* screenObjs, lv_obj_t* parent);
void vSetupRestartScreen_init(restartScreen_objs* screenObjs, lv_obj_t* parent);
void vSetupTestingScreen_init(testingScreen_objs* screenObjs, lv_obj_t* parent);
void vFwUpdateScreen_init(fwUpdateScreen_objs* screenObjs, lv_obj_t* parent);

// Popup Elements 
void setupPopUpStructures(gui_objs* guiObjects);
void createGenericPopUp(gui_objs* guiStruct, guiAppScreensLabels_t screenLbl, uint8_t streamNumber);

// Various Objects init
void vSetupGraphTile_init(graphTile_obj* tile_objs, uint8_t streamNumber);
void vSetupMeasureTab_init(measureTab_objs* measTab, lv_obj_t* parent, streamNumber_t stream);
void vSetupStatusTab_init(statusTab_objs* statTabObj, lv_obj_t* parent);
void setupStatusBar(lv_obj_t* parent);
void vSetupSettingsTab_init(settingsTabObjs_t* objects, lv_obj_t* parent);

#endif /* COMPONENTS_ULTRAFLOW_APPLICATION_GUI_GUI_DESIGN_GUIDESIGN_H_ */
