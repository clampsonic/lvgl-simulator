﻿/**************************************************************************************
 *  \file       setupTestingScreen.c
 *
 *  \brief      File Brief Description
 *
 *  \details    File Detailed Description
 *
 *  \author     Bodini Andrea
 *
 *  \version    0.0
 *
 *  \date		24 mag 2022
 *
 *  \copyright  (C) Copyright 2021, Euromisure Sas di WIKA Italia Srl
***************************************************************************************/
#ifndef LVGL_SIMULATOR_ACTIVE
#include "wikaGraphic.h"
#else
#include "..\wika_graphic\wikaGraphic.h"
#endif // !LVGL_SIMULATOR_ACTIVE


#define HEADER_LABEL_Y_SIZE 30
#define HEADER_LABEL_X_SIZE WIKA_SCREEN_X_SIZE

#define HEADER_LABEL_WIDTH  320

#define BODY_CONTAINER_Y_SIZE   (WIKA_SCREEN_Y_SIZE - HEADER_LABEL_Y_SIZE)

#define BODY_STRING_NUMBER 10

testingScreen_objs testingScreenObjs;

const char* screenLabels[BODY_STRING_NUMBER] = {
    "Testing View\0",
    "Delta T :\0",
    "Tups :\0",
    "Tdns :\0",
    "Error Number :\0",
    "Flowrate :\0",
    "Fluid Speed :\0",
    "Volume :\0",
    "MassFlow :\0",
    "Temperature :\0",
};

const char* measUnits[] = {
    "ns\0",
    "us\0",
    "m^3\0",
    "m/s\0",
    "°C\0",
	"l/h\0",
};

void vSetupTestingScreen_init(testingScreen_objs * screenObjs, lv_obj_t* parent)
{

    // Create the header of the screen
    testingScreenObjs.headContainer = lv_obj_create(parent);

    // Create the style
    static lv_style_t style_container;
    lv_style_init(&style_container);

    // Create the upper blue header
    lv_style_set_radius(&style_container, 0);
    lv_style_set_bg_color(&style_container, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_BLUE));
    lv_style_set_bg_grad_color(&style_container, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_BLUE));
    lv_style_set_bg_grad_dir(&style_container, LV_GRAD_DIR_VER);
    lv_style_set_bg_opa(&style_container, LV_OPA_COVER);
    lv_style_set_border_color(&style_container, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_LIGHT_GREY));
    lv_style_set_border_width(&style_container, 0);
    lv_style_set_border_opa(&style_container, LV_OPA_COVER);
    lv_obj_add_style(testingScreenObjs.headContainer, &style_container, LV_PART_MAIN);
    lv_obj_set_align(testingScreenObjs.headContainer, LV_ALIGN_TOP_MID);
    lv_obj_set_size(testingScreenObjs.headContainer, HEADER_LABEL_X_SIZE, HEADER_LABEL_Y_SIZE);

    lv_obj_set_scrollbar_mode(testingScreenObjs.headContainer, LV_SCROLLBAR_MODE_OFF);
    lv_obj_clear_flag(testingScreenObjs.headContainer, LV_OBJ_FLAG_SCROLLABLE);


    // Create the header Label
    testingScreenObjs.headLabel = lv_label_create(testingScreenObjs.headContainer);
    lv_obj_set_width(testingScreenObjs.headLabel, HEADER_LABEL_WIDTH);
    lv_obj_set_style_text_align(testingScreenObjs.headLabel, LV_TEXT_ALIGN_CENTER, 0);
    lv_obj_set_align(testingScreenObjs.headLabel, LV_ALIGN_CENTER);
    lv_label_set_long_mode(testingScreenObjs.headLabel, LV_LABEL_LONG_WRAP);
    lv_label_set_text(testingScreenObjs.headLabel, screenLabels[0]);

    // Create the Header label style
    static lv_style_t style_testScreen_headLabel;
    lv_style_init(&style_testScreen_headLabel);

    //Write style state: LV_STATE_DEFAULT for style_testScreen_label_2_main
    lv_style_set_radius(&style_testScreen_headLabel, 0);
    lv_style_set_bg_opa(&style_testScreen_headLabel, LV_OPA_0);
    lv_style_set_text_color(&style_testScreen_headLabel, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_LIGHT_GREY));
    lv_style_set_text_font(&style_testScreen_headLabel, &lv_font_montserrat_16);
    lv_style_set_text_letter_space(&style_testScreen_headLabel, 0);
    lv_style_set_pad_all(&style_testScreen_headLabel, 0);
    lv_obj_add_style(testingScreenObjs.headLabel, &style_testScreen_headLabel, LV_PART_MAIN);

    lv_obj_center(testingScreenObjs.headLabel);

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Create the Container of the measurements
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    testingScreenObjs.bodyContainer = lv_obj_create(parent);

    lv_obj_set_pos(testingScreenObjs.bodyContainer, 0, HEADER_LABEL_Y_SIZE);
    lv_obj_set_size(testingScreenObjs.bodyContainer, WIKA_SCREEN_X_SIZE, BODY_CONTAINER_Y_SIZE);

    lv_obj_set_flex_flow(testingScreenObjs.bodyContainer, LV_FLEX_FLOW_ROW_WRAP);

    // Create the style of the different labels
    static lv_style_t style_bodyContainer;
    lv_style_init(&style_bodyContainer);

    lv_style_set_radius(&style_bodyContainer, 0);
    lv_style_set_bg_color(&style_bodyContainer, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_LIGHT_GREY));
    lv_style_set_bg_grad_color(&style_bodyContainer, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_LIGHT_GREY));
    lv_style_set_bg_grad_dir(&style_bodyContainer, LV_GRAD_DIR_VER);
    lv_style_set_bg_opa(&style_bodyContainer, LV_OPA_COVER);
    lv_style_set_border_color(&style_bodyContainer, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_DARK_GREY));
    lv_style_set_border_width(&style_bodyContainer, 0);
    lv_style_set_border_opa(&style_bodyContainer, LV_OPA_COVER);
    lv_style_set_pad_all(&style_bodyContainer, 0);
    lv_obj_add_style(testingScreenObjs.bodyContainer, &style_bodyContainer, LV_PART_MAIN);

    // Create the Style for the Labels
    static lv_style_t style_bodyLabels;
    lv_style_init(&style_bodyLabels);

    lv_style_set_bg_opa(&style_bodyLabels, LV_OPA_0);
    lv_style_set_text_color(&style_bodyLabels, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_BLACK));
    lv_style_set_text_font(&style_bodyLabels, &lv_font_montserrat_18);
    lv_style_set_text_letter_space(&style_bodyLabels, 0);
    lv_style_set_pad_left(&style_bodyLabels, 15);
    lv_style_set_pad_top(&style_bodyLabels, 2);
    lv_style_set_align(&style_bodyLabels, LV_ALIGN_TOP_LEFT);
    lv_style_set_width(&style_bodyLabels, lv_pct(35));
    lv_style_set_height(&style_bodyLabels, LV_SIZE_CONTENT);
    lv_style_set_text_align(&style_bodyLabels, LV_TEXT_ALIGN_LEFT);

    static lv_style_t style_bodyValues;
    lv_style_init(&style_bodyValues);

    lv_style_set_bg_opa(&style_bodyValues, LV_OPA_0);
    lv_style_set_text_color(&style_bodyValues, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_BLACK));
    lv_style_set_text_font(&style_bodyValues, &lv_font_montserrat_18);
    lv_style_set_text_letter_space(&style_bodyValues, 0);
    lv_style_set_pad_all(&style_bodyValues, 0);
    lv_style_set_pad_top(&style_bodyValues, 2);
    lv_style_set_align(&style_bodyValues, LV_ALIGN_TOP_MID);
    lv_style_set_width(&style_bodyValues, lv_pct(45));
    lv_style_set_height(&style_bodyValues, LV_SIZE_CONTENT);
    lv_style_set_text_align(&style_bodyValues, LV_TEXT_ALIGN_RIGHT);

    static lv_style_t style_bodyUnits;
    lv_style_init(&style_bodyUnits);

    lv_style_set_bg_opa(&style_bodyUnits, LV_OPA_0);
    lv_style_set_text_color(&style_bodyUnits, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_BLACK));
    lv_style_set_text_font(&style_bodyUnits, &lv_font_montserrat_16);
    lv_style_set_text_letter_space(&style_bodyUnits, 0);
    lv_style_set_pad_right(&style_bodyUnits, 15);
    lv_style_set_pad_top(&style_bodyUnits, 2);
    lv_style_set_align(&style_bodyUnits, LV_ALIGN_TOP_RIGHT);
    lv_style_set_width(&style_bodyUnits, lv_pct(15));
    lv_style_set_height(&style_bodyUnits, LV_SIZE_CONTENT);
    lv_style_set_text_align(&style_bodyUnits, LV_TEXT_ALIGN_RIGHT);

    // Now create the different Labels

    // DELTA T
    testingScreenObjs.deltaT_lbl = lv_label_create(testingScreenObjs.bodyContainer);
    testingScreenObjs.deltaT_value_lbl = lv_label_create(testingScreenObjs.bodyContainer);
    testingScreenObjs.deltaT_unit_lbl = lv_label_create(testingScreenObjs.bodyContainer);

    // Tups
    testingScreenObjs.tUp_lbl= lv_label_create(testingScreenObjs.bodyContainer);
    testingScreenObjs.tUp_value_lbl = lv_label_create(testingScreenObjs.bodyContainer);
    testingScreenObjs.tUp_unit_lbl = lv_label_create(testingScreenObjs.bodyContainer);

    // Tdns
    testingScreenObjs.tDown_lbl = lv_label_create(testingScreenObjs.bodyContainer);
    testingScreenObjs.tDown_value_lbl = lv_label_create(testingScreenObjs.bodyContainer);
    testingScreenObjs.tDown_unit_lbl = lv_label_create(testingScreenObjs.bodyContainer);

    // Error
    testingScreenObjs.error_lbl = lv_label_create(testingScreenObjs.bodyContainer);
    testingScreenObjs.error_code_lbl = lv_label_create(testingScreenObjs.bodyContainer);

    // Flowrate
    testingScreenObjs.flow_lbl = lv_label_create(testingScreenObjs.bodyContainer);
    testingScreenObjs.flow_value_lbl = lv_label_create(testingScreenObjs.bodyContainer);
    testingScreenObjs.flow_unit_lbl = lv_label_create(testingScreenObjs.bodyContainer);

    // Fluid Speed
    testingScreenObjs.speed_lbl = lv_label_create(testingScreenObjs.bodyContainer);
    testingScreenObjs.speed_value_lbl = lv_label_create(testingScreenObjs.bodyContainer);
    testingScreenObjs.speed_unit_lbl = lv_label_create(testingScreenObjs.bodyContainer);

    // Volume
    testingScreenObjs.volume_lbl = lv_label_create(testingScreenObjs.bodyContainer);
    testingScreenObjs.volume_value_lbl = lv_label_create(testingScreenObjs.bodyContainer);
    testingScreenObjs.volume_unit_lbl = lv_label_create(testingScreenObjs.bodyContainer);

    // MassFlow
    testingScreenObjs.massFlow_lbl = lv_label_create(testingScreenObjs.bodyContainer);
    testingScreenObjs.massFlow_value_lbl = lv_label_create(testingScreenObjs.bodyContainer);
    testingScreenObjs.massFlow_unit_lbl = lv_label_create(testingScreenObjs.bodyContainer);

    // Temperature
    testingScreenObjs.temperature_lbl = lv_label_create(testingScreenObjs.bodyContainer);
    testingScreenObjs.temperature_value_lbl = lv_label_create(testingScreenObjs.bodyContainer);
    testingScreenObjs.temperature_unit_lbl = lv_label_create(testingScreenObjs.bodyContainer);


    lv_label_set_text(testingScreenObjs.deltaT_lbl, screenLabels[1]);
    lv_label_set_text(testingScreenObjs.deltaT_value_lbl, "0.0");
    lv_label_set_text(testingScreenObjs.deltaT_unit_lbl, measUnits[0]);

    lv_label_set_text(testingScreenObjs.tUp_lbl, screenLabels[2]);
    lv_label_set_text(testingScreenObjs.tUp_value_lbl, "0.0");
    lv_label_set_text(testingScreenObjs.tUp_unit_lbl, measUnits[1]);

    lv_label_set_text(testingScreenObjs.tDown_lbl, screenLabels[3]);
    lv_label_set_text(testingScreenObjs.tDown_value_lbl, "0.0");
    lv_label_set_text(testingScreenObjs.tDown_unit_lbl, measUnits[1]);

    lv_label_set_text(testingScreenObjs.error_lbl, screenLabels[4]);
    lv_label_set_text(testingScreenObjs.error_code_lbl, "code");

    lv_label_set_text(testingScreenObjs.flow_lbl, screenLabels[5]);
    lv_label_set_text(testingScreenObjs.flow_value_lbl, "0.0");
    lv_label_set_text(testingScreenObjs.flow_unit_lbl, measUnits[5]);

    lv_label_set_text(testingScreenObjs.speed_lbl, screenLabels[6]);
    lv_label_set_text(testingScreenObjs.speed_value_lbl, "0.0");
    lv_label_set_text(testingScreenObjs.speed_unit_lbl, measUnits[3]);

    lv_label_set_text(testingScreenObjs.volume_lbl, screenLabels[7]);
    lv_label_set_text(testingScreenObjs.volume_value_lbl, "0.0");
    lv_label_set_text(testingScreenObjs.volume_unit_lbl, measUnits[2]);

    lv_label_set_text(testingScreenObjs.massFlow_lbl, screenLabels[8]);
    lv_label_set_text(testingScreenObjs.massFlow_value_lbl, "0.0");
    lv_label_set_text(testingScreenObjs.massFlow_unit_lbl, measUnits[2]);

    lv_label_set_text(testingScreenObjs.temperature_lbl, screenLabels[9]);
    lv_label_set_text(testingScreenObjs.temperature_value_lbl, "0.0");
    lv_label_set_text(testingScreenObjs.temperature_unit_lbl, measUnits[4]);

    lv_obj_add_style(testingScreenObjs.deltaT_lbl, &style_bodyLabels, LV_PART_MAIN);
    lv_obj_add_style(testingScreenObjs.deltaT_value_lbl, &style_bodyValues, LV_PART_MAIN);
    lv_obj_add_style(testingScreenObjs.deltaT_unit_lbl, &style_bodyUnits, LV_PART_MAIN);

    lv_obj_add_style(testingScreenObjs.tUp_lbl, &style_bodyLabels, LV_PART_MAIN);
    lv_obj_add_style(testingScreenObjs.tUp_value_lbl, &style_bodyValues, LV_PART_MAIN);
    lv_obj_add_style(testingScreenObjs.tUp_unit_lbl, &style_bodyUnits, LV_PART_MAIN);

    lv_obj_add_style(testingScreenObjs.tDown_lbl, &style_bodyLabels, LV_PART_MAIN);
    lv_obj_add_style(testingScreenObjs.tDown_value_lbl, &style_bodyValues, LV_PART_MAIN);
    lv_obj_add_style(testingScreenObjs.tDown_unit_lbl, &style_bodyUnits, LV_PART_MAIN);

    lv_obj_add_style(testingScreenObjs.error_lbl, &style_bodyLabels, LV_PART_MAIN);
    lv_obj_add_style(testingScreenObjs.error_code_lbl, &style_bodyValues, LV_PART_MAIN);

    lv_obj_add_style(testingScreenObjs.flow_lbl, &style_bodyLabels, LV_PART_MAIN);
    lv_obj_add_style(testingScreenObjs.flow_value_lbl, &style_bodyValues, LV_PART_MAIN);
    lv_obj_add_style(testingScreenObjs.flow_unit_lbl, &style_bodyUnits, LV_PART_MAIN);

    lv_obj_add_style(testingScreenObjs.speed_lbl, &style_bodyLabels, LV_PART_MAIN);
    lv_obj_add_style(testingScreenObjs.speed_value_lbl, &style_bodyValues, LV_PART_MAIN);
    lv_obj_add_style(testingScreenObjs.speed_unit_lbl, &style_bodyUnits, LV_PART_MAIN);

    lv_obj_add_style(testingScreenObjs.volume_lbl, &style_bodyLabels, LV_PART_MAIN);
    lv_obj_add_style(testingScreenObjs.volume_value_lbl, &style_bodyValues, LV_PART_MAIN);
    lv_obj_add_style(testingScreenObjs.volume_unit_lbl, &style_bodyUnits, LV_PART_MAIN);

    lv_obj_add_style(testingScreenObjs.massFlow_lbl, &style_bodyLabels, LV_PART_MAIN);
    lv_obj_add_style(testingScreenObjs.massFlow_value_lbl, &style_bodyValues, LV_PART_MAIN);
    lv_obj_add_style(testingScreenObjs.massFlow_unit_lbl, &style_bodyUnits, LV_PART_MAIN);

    lv_obj_add_style(testingScreenObjs.temperature_lbl, &style_bodyLabels, LV_PART_MAIN);
    lv_obj_add_style(testingScreenObjs.temperature_value_lbl, &style_bodyValues, LV_PART_MAIN);
    lv_obj_add_style(testingScreenObjs.temperature_unit_lbl, &style_bodyUnits, LV_PART_MAIN);
}


