﻿/**************************************************************************************
 *  \file       setupMainScreen.c
 *
 *  \brief      Setup the main screen
 *
 *  \details    Setup the main screen
 *
 *  \author     Luca Agrippino
 *
 *  \version    0.0
 *
 *  \date		2 ago 2021
 *
 *  \copyright  (C) Copyright 2021, Euromisure Sas di WIKA Italia Srl
***************************************************************************************/

/********************************** INCLUDE *******************************************/
#include "guiDesign.h"
#include "math.h"
#ifndef LVGL_SIMULATOR_ACTIVE
#include "wikaGraphic.h"
#include "esp_log.h"
#else
#include "..\wika_graphic\wikaGraphic.h"
#endif // !LVGL_SIMULATOR_ACTIVE

/********************************** LOCAL VARIABLES ***********************************/
#define NUMBER_OF_TABS              4
#define TABVIEW_TAB_SIZE_PCT        10

#define TABVIEW_TABS_HOR_PAD   	    3
#define TABVIEW_TABS_VER_PAD   	    4

#define DATETIME_CONT_SIZE_X 		(WIKA_SCREEN_X_SIZE/3)
#define NOTIFY_ICONS_CONT_SIZE_X 	((WIKA_SCREEN_X_SIZE/3)-10)
#define NOTIFY_ICONS_CONT_SIZE_Y 	(STATUS_BAR_HEIGHT - 2)

#define NOTIFY_ICON_SIZE_X	        19
#define NOTIFY_ICON_SIZE_Y	        16

#define TABVIEW_LABEL_STREAM1	    "Stream 1"
#define TABVIEW_LABEL_STREAM2	    "Stream 2"
#define TABVIEW_LABEL_INFO		    "Info"
#define TABVIEW_LABEL_SETTINGS      "Settings"


// Gui Structure pointer
static gui_objs* guiObjs;

// Static pointer to the MainScreen structure
static mainScreen_objs* mainScreenObjs;

// Static pointer to the Input Device driver
static guiHelper_objs* guiHelpersObjs;

// Static pointer to measurement session structure
static MeasurementSession_t* measSession;

// Stati pointer to device configuration structure
static DeviceConfig_t* deviceParams;

/********************************** FUNCTION PROTOTYPES *******************************/
// CALLBACKS
static void tabview_key_event_cb(lv_event_t* event);
static void tabview_refresh_cb(lv_event_t* event);
static void tabview_content_update_cb(lv_event_t* event);
static void dateTimeRefresh_cb(lv_event_t* event);
static void notifySectionUpdate_cb(lv_event_t* event);
static void hostnameSectionUpdate_cb(lv_event_t* event);

static void setupTabView(lv_obj_t* parent);


/********************************** FUNCTIONS *****************************************/

void vSetupMainScreen_init(gui_objs* screenObjs, lv_obj_t* parent)
{
    // Associate the local pointer to the one passed by the function
    guiObjs = screenObjs;
    mainScreenObjs = &screenObjs->mainScreenObjs;
    guiHelpersObjs = &screenObjs->guiHelpersObjs;

    measSession = tWikaConfigManager_getMeasurementSession();
    deviceParams = tWikaConfigManager_getDeviceConfig();

    // Create the Tabview element
    mainScreenObjs->tabView = lv_tabview_create(parent, LV_DIR_TOP, lv_pct(TABVIEW_TAB_SIZE_PCT));

    setupTabView(mainScreenObjs->tabView);

    mainScreenObjs->tab_stream1  = lv_tabview_add_tab(mainScreenObjs->tabView, TAB_STREAM1_ICON  " " TABVIEW_LABEL_STREAM1	"\t");
    mainScreenObjs->tab_stream2  = lv_tabview_add_tab(mainScreenObjs->tabView, TAB_STREAM2_ICON	 " " TABVIEW_LABEL_STREAM2	"\t");
    mainScreenObjs->tab_info     = lv_tabview_add_tab(mainScreenObjs->tabView, TAB_INFO_ICON  	 " " TABVIEW_LABEL_INFO     "\t\t\t\t");
    mainScreenObjs->tab_settings = lv_tabview_add_tab(mainScreenObjs->tabView, TAB_SETTINGS_ICON " " TABVIEW_LABEL_SETTINGS	"\t");

    lv_group_add_obj(guiHelpersObjs->guiGroup, mainScreenObjs->tabView);

    // The style of the Tabview Tabs
    lv_style_init(&mainScreenObjs->style_tabs);

    lv_style_set_pad_ver(&mainScreenObjs->style_tabs, TABVIEW_TABS_VER_PAD);
    lv_style_set_pad_hor(&mainScreenObjs->style_tabs, TABVIEW_TABS_HOR_PAD);
    lv_style_set_bg_color(&mainScreenObjs->style_tabs, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_LIGHT_GREY));

    // Apply the style to the different tabs
    lv_obj_add_style(mainScreenObjs->tab_stream1,   &mainScreenObjs->style_tabs, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_add_style(mainScreenObjs->tab_stream2,   &mainScreenObjs->style_tabs, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_add_style(mainScreenObjs->tab_info,      &mainScreenObjs->style_tabs, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_add_style(mainScreenObjs->tab_settings,  &mainScreenObjs->style_tabs, LV_PART_MAIN | LV_STATE_DEFAULT);

    // Setup TAB0 - Stream 1
    vSetupMeasureTab_init(&mainScreenObjs->tab_stream1_objs,
        mainScreenObjs->tab_stream1,
        DAQ_STREAM_1);

    // Setup TAB1 - Stream 2
    vSetupMeasureTab_init(&mainScreenObjs->tab_stream2_objs,
        mainScreenObjs->tab_stream2,
        DAQ_STREAM_2);

    // Setup TAB2 - Status
    vSetupStatusTab_init(&mainScreenObjs->tab_status_objs, mainScreenObjs->tab_info);

    // Setup TAB3 - Settings
    vSetupSettingsTab_init(&mainScreenObjs->tab_settings_objs, mainScreenObjs->tab_settings);    

    // Setup the Status BAR
    setupStatusBar(parent);

    // Send the refresh events to update the fields
    lv_event_send(mainScreenObjs->statusBar_objs.statusBar_dateTime_cont, LV_EVENT_REFRESH, NULL);
    lv_event_send(mainScreenObjs->statusBar_objs.statusBar_notifyIcons_cont, LV_EVENT_REFRESH, NULL);

    lv_obj_update_layout(mainScreenObjs->tabView);
}

void setupTabView(lv_obj_t* parent)
{
    // Change the size of the container
    lv_obj_set_size(parent, WIKA_SCREEN_X_SIZE, (WIKA_SCREEN_Y_SIZE - STATUS_BAR_HEIGHT));

    // Set position
    lv_obj_set_pos(parent, 0, 0);

    // Add the callback to catch events
    lv_obj_add_event_cb(mainScreenObjs->tabView, tabview_refresh_cb, LV_EVENT_REFRESH, NULL);
    lv_obj_add_event_cb(mainScreenObjs->tabView, tabview_key_event_cb, LV_EVENT_KEY, NULL);
    lv_obj_add_event_cb(mainScreenObjs->tabView, tabview_content_update_cb, LV_EVENT_VALUE_CHANGED, NULL);

    // Setup the style of the tabview
    lv_obj_set_style_bg_color(parent, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_LIGHT_GREY), 0);

    // Get the buttons of the tabview
    mainScreenObjs->tabButtons = lv_tabview_get_tab_btns(parent);

    // The style of the default items
    lv_style_init(&mainScreenObjs->style_tabview_default);

    lv_style_set_bg_opa(&mainScreenObjs->style_tabview_default, LV_OPA_40);
    lv_style_set_bg_color(&mainScreenObjs->style_tabview_default, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_DARK_GREY));
    lv_style_set_text_color(&mainScreenObjs->style_tabview_default, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_DARK_GREY));
    lv_style_set_text_font(&mainScreenObjs->style_tabview_default, &wika_montserrat_20_semibold_symbols);
    lv_style_set_text_opa(&mainScreenObjs->style_tabview_default, LV_OPA_COVER);
    lv_style_set_text_letter_space(&mainScreenObjs->style_tabview_default, 0);
    lv_style_set_pad_all(&mainScreenObjs->style_tabview_default, 0);
    lv_style_set_align(&mainScreenObjs->style_tabview_default, LV_ALIGN_CENTER);
    lv_style_set_height(&mainScreenObjs->style_tabview_default, 19);
    lv_style_set_text_align(&mainScreenObjs->style_tabview_default, LV_TEXT_ALIGN_LEFT);
    lv_style_set_border_color(&mainScreenObjs->style_tabview_default, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_DARK_GREY));
    lv_style_set_border_side(&mainScreenObjs->style_tabview_default, LV_BORDER_SIDE_FULL);
    lv_style_set_border_opa(&mainScreenObjs->style_tabview_default, LV_OPA_COVER);
    lv_style_set_border_width(&mainScreenObjs->style_tabview_default, 1);

    // The style of the checked items
    lv_style_init(&mainScreenObjs->style_tabview_checked);

    lv_style_set_bg_opa(&mainScreenObjs->style_tabview_checked, LV_OPA_COVER);
    lv_style_set_bg_color(&mainScreenObjs->style_tabview_checked, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_LIGHT_GREY));
    lv_style_set_text_color(&mainScreenObjs->style_tabview_checked, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_BLUE));
    lv_style_set_text_font(&mainScreenObjs->style_tabview_checked, &wika_montserrat_20_semibold_symbols);
    lv_style_set_text_opa(&mainScreenObjs->style_tabview_checked, LV_OPA_COVER);
    lv_style_set_text_letter_space(&mainScreenObjs->style_tabview_checked, 0);
    lv_style_set_pad_all(&mainScreenObjs->style_tabview_checked, 0);
    lv_style_set_align(&mainScreenObjs->style_tabview_checked, LV_ALIGN_CENTER);
    lv_style_set_height(&mainScreenObjs->style_tabview_checked, 22);
    lv_style_set_text_align(&mainScreenObjs->style_tabview_checked, LV_TEXT_ALIGN_LEFT);
    lv_style_set_border_color(&mainScreenObjs->style_tabview_checked, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_DARK_GREY));
    lv_style_set_border_side(&mainScreenObjs->style_tabview_checked, LV_BORDER_SIDE_TOP | LV_BORDER_SIDE_LEFT | LV_BORDER_SIDE_RIGHT);
    lv_style_set_border_opa(&mainScreenObjs->style_tabview_checked, LV_OPA_COVER);
    lv_style_set_border_width(&mainScreenObjs->style_tabview_checked, 1);

    // Apply the styles
    lv_obj_add_style(mainScreenObjs->tabButtons, &mainScreenObjs->style_tabview_checked, LV_PART_ITEMS | LV_STATE_CHECKED);
    lv_obj_add_style(mainScreenObjs->tabButtons, &mainScreenObjs->style_tabview_default, LV_PART_ITEMS | LV_STATE_DEFAULT);

    lv_obj_update_layout(mainScreenObjs->tabButtons);
}

void setupStatusBar(lv_obj_t* parent)
{
    // Create a shortcut to the structure
    statusBar_objs* statusBar_p = &mainScreenObjs->statusBar_objs;

    // Create the status bar at the bottom of the screen
    statusBar_p->statusBar = lv_obj_create(parent);

    // Create the Style of the status bar
    lv_style_init(&statusBar_p->style_status_bar);

    lv_style_set_radius(&statusBar_p->style_status_bar, 0);
    lv_style_set_bg_color(&statusBar_p->style_status_bar, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_DARK_GREY));
    lv_style_set_bg_opa(&statusBar_p->style_status_bar, LV_OPA_COVER);
    lv_style_set_border_color(&statusBar_p->style_status_bar, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_LIGHT_GREY));
    lv_style_set_border_width(&statusBar_p->style_status_bar, 0);
    lv_style_set_border_opa(&statusBar_p->style_status_bar, LV_OPA_COVER);
    lv_style_set_pad_left(&statusBar_p->style_status_bar, 4);
    lv_style_set_pad_right(&statusBar_p->style_status_bar, 4);
    lv_style_set_pad_top(&statusBar_p->style_status_bar, 1);
    lv_style_set_pad_bottom(&statusBar_p->style_status_bar, 1);

    lv_obj_add_style(statusBar_p->statusBar, &statusBar_p->style_status_bar, LV_PART_MAIN);
    lv_obj_set_align(statusBar_p->statusBar, LV_ALIGN_BOTTOM_MID);
    lv_obj_set_size(statusBar_p->statusBar, WIKA_SCREEN_X_SIZE, STATUS_BAR_HEIGHT);

    lv_obj_set_scrollbar_mode(statusBar_p->statusBar, LV_SCROLLBAR_MODE_OFF);
    lv_obj_clear_flag(statusBar_p->statusBar, LV_OBJ_FLAG_SCROLLABLE);

    // Create the objects of the status bar
    statusBar_p->statusBar_dateTime_cont = lv_obj_create(statusBar_p->statusBar);
    statusBar_p->statusBar_date_lbl = lv_label_create(statusBar_p->statusBar_dateTime_cont);
    statusBar_p->statusBar_time_lbl = lv_label_create(statusBar_p->statusBar_dateTime_cont);
    statusBar_p->statusBar_hostname_lbl = lv_label_create(statusBar_p->statusBar);

    statusBar_p->statusBar_notifyIcons_cont = lv_obj_create(statusBar_p->statusBar);
    statusBar_p->statusBar_bluetoothStat = lv_img_create(statusBar_p->statusBar_notifyIcons_cont);
    statusBar_p->statusBar_wifiStat = lv_img_create(statusBar_p->statusBar_notifyIcons_cont);
    statusBar_p->statusBar_serialStat = lv_img_create(statusBar_p->statusBar_notifyIcons_cont);
    statusBar_p->statusBar_errNfy = lv_img_create(statusBar_p->statusBar_notifyIcons_cont);

    // BOOKMARK: CONTAINERS
    // init style
    lv_style_init(&statusBar_p->style_containers);

    // Layout
    lv_style_set_layout(&statusBar_p->style_containers, LV_LAYOUT_FLEX);
    lv_style_set_flex_flow(&statusBar_p->style_containers, LV_FLEX_FLOW_ROW);
    lv_style_set_flex_main_place(&statusBar_p->style_containers, LV_FLEX_ALIGN_END);
    lv_style_set_flex_cross_place(&statusBar_p->style_containers, LV_FLEX_ALIGN_CENTER);
    lv_style_set_flex_track_place(&statusBar_p->style_containers, LV_FLEX_ALIGN_CENTER);
    // Background
    lv_style_set_bg_color(&statusBar_p->style_containers, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_DARK_GREY));
    lv_style_set_bg_opa(&statusBar_p->style_containers, LV_OPA_COVER);
    // Container Roundness
    lv_style_set_radius(&statusBar_p->style_containers, 0);
    // Borders
    lv_style_set_border_opa(&statusBar_p->style_containers, LV_OPA_0);
    lv_style_set_border_width(&statusBar_p->style_containers, 0);
    // Padding
    lv_style_set_pad_all(&statusBar_p->style_containers, 0);

    ///////////////////////////////////////////////////////////////////////////
    ////////////////////// NOTIFY ICONS SECTION ///////////////////////////////
    ///////////////////////////////////////////////////////////////////////////

    // BOOKMARK: NOTIFY ICON SPECIFIC PROPERTIES
    // add style to container
    lv_obj_add_style(statusBar_p->statusBar_notifyIcons_cont, &statusBar_p->style_containers, LV_PART_MAIN);
    // scrolling
    lv_obj_set_scrollbar_mode(statusBar_p->statusBar_notifyIcons_cont, LV_SCROLLBAR_MODE_OFF);
    lv_obj_clear_flag(statusBar_p->statusBar_notifyIcons_cont, LV_OBJ_FLAG_SCROLLABLE);
    // alignment
    lv_obj_align(statusBar_p->statusBar_notifyIcons_cont, LV_ALIGN_RIGHT_MID, 0, 0);


    // BOOKMARK: NOTIFY ICONS
    // init the style
    lv_style_init(&statusBar_p->style_stat_icons);

    // Set opacity to max
    lv_style_set_img_opa(&statusBar_p->style_stat_icons, LV_OPA_COVER);

    lv_style_set_pad_all(&statusBar_p->style_stat_icons, 0);

    lv_style_set_height(&statusBar_p->style_stat_icons, NOTIFY_ICON_SIZE_Y);
    lv_style_set_width(&statusBar_p->style_stat_icons, NOTIFY_ICON_SIZE_X);

    // Apply the style to the image
    lv_obj_add_style(statusBar_p->statusBar_bluetoothStat, &statusBar_p->style_stat_icons, LV_STATE_DEFAULT);
    lv_obj_add_style(statusBar_p->statusBar_wifiStat, &statusBar_p->style_stat_icons, LV_STATE_DEFAULT);
    lv_obj_add_style(statusBar_p->statusBar_errNfy, &statusBar_p->style_stat_icons, LV_STATE_DEFAULT);
    lv_obj_add_style(statusBar_p->statusBar_serialStat, &statusBar_p->style_stat_icons, LV_STATE_DEFAULT);

    // Set origin position of the images
    lv_img_set_pivot(statusBar_p->statusBar_bluetoothStat, 0, 0);
    lv_img_set_angle(statusBar_p->statusBar_bluetoothStat, 0);

    lv_img_set_pivot(statusBar_p->statusBar_wifiStat, 0, 0);
    lv_img_set_angle(statusBar_p->statusBar_wifiStat, 0);

    lv_img_set_pivot(statusBar_p->statusBar_errNfy, 0, 0);
    lv_img_set_angle(statusBar_p->statusBar_errNfy, 0);

    lv_img_set_pivot(statusBar_p->statusBar_serialStat, 0, 0);
    lv_img_set_angle(statusBar_p->statusBar_serialStat, 0);

    ///////////////////////////////////////////////////////////////////////////
    ///////////////////////// SEPARATOR LINES /////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////

    // Create the line separators between the labels.
    statusBar_p->lineSeparator_L = lv_line_create(statusBar_p->statusBar);
    statusBar_p->lineSeparator_R = lv_line_create(statusBar_p->statusBar);

    lv_style_init(&statusBar_p->style_line);

    lv_style_set_line_color(&statusBar_p->style_line, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_WHITE));
    lv_style_set_line_width(&statusBar_p->style_line, 2);
    lv_style_set_line_rounded(&statusBar_p->style_line, true);

    lv_obj_add_style(statusBar_p->lineSeparator_R, &statusBar_p->style_line, LV_PART_MAIN);
    lv_obj_add_style(statusBar_p->lineSeparator_L, &statusBar_p->style_line, LV_PART_MAIN);

    static lv_point_t lineL[] = { {SEP_LINE_L_X,SEP_LINE_Y1},{SEP_LINE_L_X,SEP_LINE_Y2} };
    static lv_point_t lineR[] = { {SEP_LINE_R_X,SEP_LINE_Y1},{SEP_LINE_R_X,SEP_LINE_Y2} };

    lv_line_set_points(statusBar_p->lineSeparator_L, lineL, 2);
    lv_line_set_points(statusBar_p->lineSeparator_R, lineR, 2);

    ///////////////////////////////////////////////////////////////////////////
    ///////////////////////////////// LABELS //////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////

    // BOOKMARK: Date Time Container definition
    lv_obj_add_style(statusBar_p->statusBar_dateTime_cont, &statusBar_p->style_containers, LV_PART_MAIN);

    lv_obj_set_size(statusBar_p->statusBar_dateTime_cont, DATETIME_CONT_SIZE_X, NOTIFY_ICONS_CONT_SIZE_Y);
    lv_obj_align(statusBar_p->statusBar_dateTime_cont, LV_ALIGN_LEFT_MID, 0, 0);

    // BOOKMARK: Date/Time/Hostname labels style definition
    lv_style_init(&statusBar_p->style_status_lbls);

    lv_style_set_bg_opa(&statusBar_p->style_status_lbls, LV_OPA_0);
    lv_style_set_bg_color(&statusBar_p->style_status_lbls, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_LIGHT_GREY));
    lv_style_set_text_color(&statusBar_p->style_status_lbls, tWikaGuiDesign_getColor(WIKA_RGB_COLOR_WHITE));
    lv_style_set_text_opa(&statusBar_p->style_status_lbls, LV_OPA_COVER);
    lv_style_set_text_font(&statusBar_p->style_status_lbls, &lv_font_montserrat_14);
    lv_style_set_text_letter_space(&statusBar_p->style_status_lbls, 0);
    lv_style_set_pad_all(&statusBar_p->style_status_lbls, 0);
    lv_style_set_height(&statusBar_p->style_status_lbls, 18);

    lv_obj_align(statusBar_p->statusBar_date_lbl, LV_ALIGN_LEFT_MID, 0, 0);
    lv_obj_align(statusBar_p->statusBar_time_lbl, LV_ALIGN_LEFT_MID, 0, 0);

    lv_obj_set_align(statusBar_p->statusBar_hostname_lbl, LV_ALIGN_CENTER);

    // Assign label widths
    lv_obj_set_width(statusBar_p->statusBar_date_lbl, (lv_coord_t)(DATETIME_CONT_SIZE_X / 2 + 5));
    lv_obj_set_width(statusBar_p->statusBar_time_lbl, (lv_coord_t)(DATETIME_CONT_SIZE_X / 2 - 15));

    // Assign Hostname Label
    lv_label_set_text(statusBar_p->statusBar_hostname_lbl, (char*)deviceParams->name);

    ///////////////////////////////////////////////////////////////////////////
    /////////////////////////// APPLY STYLES //////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////

    lv_obj_add_style(statusBar_p->statusBar_date_lbl, &statusBar_p->style_status_lbls, LV_PART_MAIN);
    lv_obj_add_style(statusBar_p->statusBar_time_lbl, &statusBar_p->style_status_lbls, LV_PART_MAIN);
    lv_obj_add_style(statusBar_p->statusBar_hostname_lbl, &statusBar_p->style_status_lbls, LV_PART_MAIN);

    ///////////////////////////////////////////////////////////////////////////
    /////////////////////////// CALLBACKS /////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////
    lv_obj_add_event_cb(statusBar_p->statusBar_dateTime_cont, dateTimeRefresh_cb, LV_EVENT_REFRESH, NULL);
    lv_obj_add_event_cb(statusBar_p->statusBar_notifyIcons_cont, notifySectionUpdate_cb, LV_EVENT_REFRESH, NULL);
    lv_obj_add_event_cb(statusBar_p->statusBar_hostname_lbl, hostnameSectionUpdate_cb, LV_EVENT_REFRESH, NULL);
}

static void dateTimeRefresh_cb(lv_event_t* event)
{
#ifndef LVGL_SIMULATOR_ACTIVE
    uint8_t dateTime[15];

    vWikaGeneric_formatDateTime(deviceParams->system.DateTime, &dateTime[0], DATE_TIME_DATE_ONLY);

    lv_label_set_text(mainScreenObjs->statusBar_objs.statusBar_date_lbl, (char*)&dateTime[0]);

    vWikaGeneric_formatDateTime(deviceParams->system.DateTime, &dateTime[0], DATE_TIME_TIME_ONLY);

    lv_label_set_text(mainScreenObjs->statusBar_objs.statusBar_time_lbl, (char*)&dateTime[0]);
#endif
}

static void hostnameSectionUpdate_cb(lv_event_t* event)
{
    lv_obj_t* obj = lv_event_get_target(event);
    // We have the label.

    if (strcmp(lv_label_get_text(obj), (char*)deviceParams->name) == 0)
    {
        lv_label_set_text(obj, (char*)deviceParams->wifi.IP);
    }
    else
    {
        lv_label_set_text(obj, (char*)deviceParams->name);
    }
}

static void notifySectionUpdate_cb(lv_event_t* event)
{
    // Associate img object to the image structure
    // BOOKMARK: associate image to the notify icon
    if (deviceParams->wifi.WiFiMode == WifiMode_SoftAP)
    {
        if (deviceParams->wifi.WiFiConnected)
        {
            lv_img_set_src(mainScreenObjs->statusBar_objs.statusBar_wifiStat, &wifiAP_conn);
        }
        else
        {
            lv_img_set_src(mainScreenObjs->statusBar_objs.statusBar_wifiStat, &wifiAP_nc);
        }
    }
    else
    {
        lv_img_set_src(mainScreenObjs->statusBar_objs.statusBar_wifiStat, &wifiFull);
    }

    if (deviceParams->bluetooth.enable)
    {
        lv_img_set_src(mainScreenObjs->statusBar_objs.statusBar_bluetoothStat, &BT_nc);
    }
    else
    {
        lv_img_set_src(mainScreenObjs->statusBar_objs.statusBar_bluetoothStat, &BT_off);
    }

    lv_img_set_src(mainScreenObjs->statusBar_objs.statusBar_serialStat, &wifiErr);
    lv_img_set_src(mainScreenObjs->statusBar_objs.statusBar_errNfy, &statusOk);
}

static void tabview_refresh_cb(lv_event_t* event)
{
    int16_t tab = lv_tabview_get_tab_act(mainScreenObjs->tabView);
    restorePageGroup(tab);
}

static void tabview_content_update_cb(lv_event_t* event)
{
    // The measurement session reference is already created and init'ed as a static function,
    // called measSession

    int32_t lowFlowSpeedCutoff;
    int32_t maxFlowSpeedWarnThresh;
    int32_t maxFlowSpeed;
    int32_t flowRateCutoff;
    int32_t maxFlowRate;
    int32_t maxFlowRateWarnThresh;

}

/***************************************************************************************
 * 	\author		BodiniA
 *
 *	\brief		Table view event callback.
 *				This function is called when a gesture command is triggered when
 *				the tabview is shown on screen.
 *
 *	\param[in]	event	-	\see lv_event_t
 *
 *	\return		none
 ***************************************************************************************/
static void tabview_key_event_cb(lv_event_t* event)
{
    int16_t tab = lv_tabview_get_tab_act(mainScreenObjs->tabView);

    uint32_t key_event = *((uint32_t*)lv_event_get_param(event));

    switch (key_event) {
    case LV_KEY_RIGHT:
        tab++;
        if (tab > MAIN_TAB_LBL_CONFIG) tab = MAIN_TAB_LBL_STREAM1;

        lv_tabview_set_act(mainScreenObjs->tabView, tab, LV_ANIM_ON);

        lv_event_send(guiObjs->mainScreenObjs.tabView, LV_EVENT_REFRESH, NULL);

        // send led event
        //tWikaOs_sendLedEvent(EVENT_EAST_2_WEST);
        break;

    case LV_KEY_LEFT:
        tab--;
        if (tab < MAIN_TAB_LBL_STREAM1) tab = MAIN_TAB_LBL_CONFIG;

        lv_tabview_set_act(mainScreenObjs->tabView, tab, LV_ANIM_ON);

        lv_event_send(guiObjs->mainScreenObjs.tabView, LV_EVENT_REFRESH, NULL);

        // send led event
        //tWikaOs_sendLedEvent(EVENT_WEST_2_EAST);
        break;

    case LV_KEY_ENTER:
        //			if((tab == MAIN_TAB_LBL_STREAM1) || (tab == MAIN_TAB_LBL_STREAM2)) {
        //				createGenericPopUp(guiObjs, GUI_SCR_LBL_POPUP_ENVIRONMENT, tab);
        //				// send led event
        //				tWikaOs_sendLedEvent(EVENT_DOUBLE_TAP_CENTER);
        //			}
        //			else
        if (tab == MAIN_TAB_LBL_INFO) {
            // enter in the info submenu.
            // the tile change is associated to the LV_EVENT_REFRESH
            lv_event_send(guiObjs->mainScreenObjs.tab_status_objs.tile_info.obj_main_tileView_tile, LV_EVENT_KEY, &key_event);
            // send led event
            //tWikaOs_sendLedEvent(EVENT_DOUBLE_TAP_CENTER);
        }
        lv_event_send(guiObjs->mainScreenObjs.tabView, LV_EVENT_REFRESH, NULL);
        break;

    case LV_KEY_END:
        lv_event_send(guiObjs->mainScreenObjs.tabView, LV_EVENT_REFRESH, NULL);
        break;

    case LV_KEY_HOME:
        break;

    case LV_KEY_DOWN:
        if (tab == MAIN_TAB_LBL_STREAM1) {
            lv_obj_set_tile_id(mainScreenObjs->tab_stream1_objs.tabScreen_tileView_container, 0, 1, LV_ANIM_ON);
            lv_timer_resume(guiObjs->timers_objs.updateWaveFormGraph_tmr);
            lv_timer_resume(guiObjs->timers_objs.updateMeasStat_tmr);
        }
        else if (tab == MAIN_TAB_LBL_STREAM2) {
            lv_obj_set_tile_id(mainScreenObjs->tab_stream2_objs.tabScreen_tileView_container, 0, 1, LV_ANIM_ON);
            lv_timer_resume(guiObjs->timers_objs.updateWaveFormGraph_tmr);
            lv_timer_resume(guiObjs->timers_objs.updateMeasStat_tmr);
        }
        else if (tab == MAIN_TAB_LBL_INFO) {
            lv_event_send(guiObjs->mainScreenObjs.tab_status_objs.tile_info.obj_main_tileView_tile, LV_EVENT_KEY, &key_event);
        }

        if ((tab == MAIN_TAB_LBL_STREAM1) || (tab == MAIN_TAB_LBL_STREAM2)) {
            // refresh tabview
            lv_event_send(guiObjs->mainScreenObjs.tabView, LV_EVENT_REFRESH, NULL);

            // send led event
            //tWikaOs_sendLedEvent(EVENT_SOUTH_2_NORTH);
        }
        else {
            //tWikaOs_sendLedEvent(EVENT_TAP_SOUTH);
        }
        break;

    case LV_KEY_UP:
        if (tab == MAIN_TAB_LBL_STREAM1) {
            lv_obj_set_tile_id(mainScreenObjs->tab_stream1_objs.tabScreen_tileView_container, 0, 0, LV_ANIM_ON);
            // Delete the Waveform Update Timer
            lv_timer_pause(guiObjs->timers_objs.updateWaveFormGraph_tmr);
            lv_timer_pause(guiObjs->timers_objs.updateMeasStat_tmr);
        }
        else if (tab == MAIN_TAB_LBL_STREAM2) {
            lv_obj_set_tile_id(mainScreenObjs->tab_stream2_objs.tabScreen_tileView_container, 0, 0, LV_ANIM_ON);
            // Delete the Waveform Update Timer
            lv_timer_pause(guiObjs->timers_objs.updateWaveFormGraph_tmr);
            lv_timer_pause(guiObjs->timers_objs.updateMeasStat_tmr);
        }
        else if (tab == MAIN_TAB_LBL_INFO) {
            lv_event_send(guiObjs->mainScreenObjs.tab_status_objs.tile_info.obj_main_tileView_tile, LV_EVENT_KEY, &key_event);
        }

        if ((tab == MAIN_TAB_LBL_STREAM1) || (tab == MAIN_TAB_LBL_STREAM2)) {
            // refresh tabview
            lv_event_send(guiObjs->mainScreenObjs.tabView, LV_EVENT_REFRESH, NULL);

            // send led event
            //tWikaOs_sendLedEvent(EVENT_NORTH_2_SOUTH);
        }
        else {
            //tWikaOs_sendLedEvent(EVENT_TAP_NORTH);
        }
        break;

    case LV_KEY_START_MEAS:
        if ((tab == MAIN_TAB_LBL_STREAM1) || (tab == MAIN_TAB_LBL_STREAM2)) {
            if ((measSession->streams[tab].measureInProgress == 0) &&
                (measSession->streams[tab].parameters.daq.channels[0].enable == 1))
            {
                createGenericPopUp(guiObjs, GUI_SCR_LBL_POPUP_START_MEASUREMENT, tab);
                // send led event
                //tWikaOs_sendLedEvent(EVENT_CW);
            }
        }
        else if (tab == MAIN_TAB_LBL_INFO) {
            lv_event_send(guiObjs->mainScreenObjs.tab_status_objs.tile_info.obj_main_tileView_tile, LV_EVENT_KEY, &key_event);
            // send led event
            //tWikaOs_sendLedEvent(EVENT_TAP_EAST);
        }
        break;

    case LV_KEY_STOP_MEAS:
        if ((tab == MAIN_TAB_LBL_STREAM1) || (tab == MAIN_TAB_LBL_STREAM2)) {
            if (measSession->streams[tab].measureInProgress == 1)
            {
                createGenericPopUp(guiObjs, GUI_SCR_LBL_POPUP_STOP_MEASUREMENT, tab);
                // send led event
                //tWikaOs_sendLedEvent(EVENT_CCW);
            }
        }
        else if (tab == MAIN_TAB_LBL_INFO) {
            lv_event_send(guiObjs->mainScreenObjs.tab_status_objs.tile_info.obj_main_tileView_tile, LV_EVENT_KEY, &key_event);
            // send led event
            //tWikaOs_sendLedEvent(EVENT_TAP_WEST);
        }
        break;

    default:
        printf("Unknown Code:%d\n", key_event);
        break;
    }
}

/********************************* EOF ************************************************/
