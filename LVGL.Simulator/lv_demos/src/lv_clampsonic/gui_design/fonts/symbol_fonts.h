﻿/**************************************************************************************
 *  \file       symbol_fonts.h
 *  
 *  \brief      Specific Fonts with Symbols
 *  
 *  \details    This file contains the definitions of generated FONTS, including
 *              specific symbols not included in original fonts. Furthermore, we
 *              have included semibold fonts.
 *  
 *  \author     BodiniA
 *  
 *  \version    0.1
 *  
 *  \date		13 giu 2022
 *  
 *  \copyright  (C) Copyright 2021, Euromisure Sas di WIKA Italia Srl
***************************************************************************************/

#ifndef SYMBOL_FONTS_H_
#define SYMBOL_FONTS_H_

#include "lvgl/lvgl.h"

/***************************************************************************
 ****************** SYMBOL DECLARATIONS *************************************
 ***************************************************************************/
LV_FONT_DECLARE(wika_montserrat_18_semibold_symbols);
LV_FONT_DECLARE(wika_montserrat_20_semibold_symbols);
LV_FONT_DECLARE(wika_dseg_64_regular);
LV_FONT_DECLARE(wika_montserrat_32_semibold);
LV_FONT_DECLARE(wika_montserrat_24_semibold);

#define TAB_INFO_ICON       "\xEE\xA2\x8E"
#define TAB_STREAM1_ICON    "\xEE\xAE\xA4"
#define TAB_STREAM2_ICON    "\xEE\xAE\x9A"
#define TAB_SETTINGS_ICON   "\xEE\xA2\xB8"

#define WIKA_SYMBOLS_MODBUS "\xEE\xA7\xB4"

#endif /* SYMBOL_FONTS_H_ */
